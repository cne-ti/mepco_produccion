## Mecanismo Estabilización Precios de Combustibles - MEPCO
### Requerimientos Técnicos
* Apache >=2.4.6
* Mysql >=5.6.26
* Php >=5.5.9

### Como Instalar
#### Apache
Antes de instalar la aplicación *MEPCO*, se debe configurar el tamaño máximo de archivo y máximo tiempo de ejecución.

1. Se debe editar el fichero php.ini (por ejemplo, /etc/php.ini) para modificar el tiempo máximo de ejecución de un script (ya que la carga masiva puede llegar a tardar hasta 5 minutos aproximadamente). Le daremos un tiempo  de 10 minutos máximos (600 segundos) en la siguiente sintaxis:
o	max_execution_time = 600;
2. También se debe editar este fichero para modificar el tamaño máximo para la subida de un archivo. Le daremos un tamaño de 8MB en la siguiente sintaxis:
o	upload_max_filesize: 8M

> Al momento de configurar el php.ini se debe reiniciar el Apache (por ejemplo, con el comando “service httpd restart “)

#### Base de Datos
Dentro de la carpeta */base de datos* se encuentran los siguientes archivos, los cuales crean la estructura y cargan la información inicial para el sistema.

* ***1-MEPCO_DB.sql***: Crea las tablas de la base de datos.
* ***2-MEPCO_INICIAL.sql***: Registra la carga inicial de datos.
* ***Ejecuta.sql***: sql que puede ser ejecutado por una shell de instalación y ejecuta los scripts 1 y 2.

#### Aplicación Web
Para la correcta configuración de la aplicación web se deben considerar los siguientes pasos:

1. Se debe copiar la carpeta */webapp* en la ruta especificada para la aplicación dentro de la configuración de Apache.
2. Configurar el archivo *.env* ubicado en la raiz del proyecto con los parametros requeridos
~~~
APP_ENV=local
APP_DEBUG=true
APP_KEY=
DB_HOST=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
MAIL_DRIVER=
MAIL_HOST=
MAIL_PORT=
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_ENCRYPTION=
~~~
3. Ya configurada la aplicación para que acceda a la BD.
~~~
chmod 775 webapp -R
chmod 777 webapp/storage -R
~~~

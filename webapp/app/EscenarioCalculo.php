<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EscenarioCalculo extends Model 
{
    protected $table='MEPCO_ESCENARIO_CALCULO';

    protected $primaryKey='ESCA_ID';

    public $timestamps=false;

	protected $fillable=[
        'ESCE_ID',
        'HIDR_ID', 
        'ESCA_N',
        'ESCA_M',
        'ESCA_S',
        'ESCA_T',
        'ESCA_F',
        'ESCA_PARIDAD',
        'ESCA_CRUDO',
        'ESCA_MARGEN',
        'ESCA_PREFINF',
        'ESCA_PRESUP',
        'ESCA_SUGERIR',
        'ESCA_PUBLICADO'
    ];

    /**
    * Relación 1:N con hidrocarburos
    */
    public function hidrocarburo() {
        return $this->belongsTo('App\Hidrocarburo');
    }

    /**
    * Relación 1:1 con Escenario
    */
    public function escenario() {
        return $this->belongsTo('App\Escenario', 'ESCE_ID');
    }
}

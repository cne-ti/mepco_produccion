<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolverReferencia extends Model 
{
    protected $table='MEPCO_SOLVER_REFERENCIA';

    protected $primaryKey='SOLR_ID';

    public $timestamps=false;

	protected $fillable=[
        'SOLV_ID',
        'HIDR_ID',
        'SOLR_N',
        'SOLR_M',
        'SOLR_S',
        'SOLR_F',
        'SOLR_CRUDO',
        'SOLR_MARGEN',
        'SOLR_PREFINF',
        'SOLR_PREFINT',
        'SOLR_PREFSUP',
    ];

    /**
    * Relación 1:N con Solver
    */
    public function solver() {
        return $this->hasOne('App\Solver', 'SOLV_ID');
    }
}

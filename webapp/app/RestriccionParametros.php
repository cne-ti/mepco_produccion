<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestriccionParametros extends Model 
{
    protected $table='MEPCO_RESTRICCION_PARAMETROS';

    protected $primaryKey='REPA_ID';

    public $timestamps=false;

	protected $fillable=[
        'REPA_VALOR_MINIMO',
        'REPA_VALOR_MAXIMO', 
        'REPA_FECHA_VIGENCIA',
        'REPA_DESCRIPCION', 
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContenidoPortal extends Model
{
    protected $table = 'MEPCO_CONTENIDO_PORTAL';

    protected $primaryKey = 'COPO_ID';

    public $timestamps = false;

    protected $fillable = [
        'COPO_NOMBRE', 
        'COPO_NOMENCLATURA'
    ];

    /**
    * Relación N:M con Perfiles
    */
    public function perfiles() 
    {
        return $this->belongsToMany('App\Perfil', 'MEPCO_PERF_COPO', 'COPO_ID', 'PERF_ID')->withPivot('PECO_ID');
    }

    /**
    * Función para construir el menú dinámico
    */
    public static function menu($contenidoPortal) {

        $reporte        = '<li class="dropdown" id="reportes">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reportes <span class="caret"></span></a>
                            <ul class="dropdown-menu">';

        $administracion = '<li class="dropdown" id="administracion">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administración <span class="caret"></span></a>
                            <ul class="dropdown-menu">';

        $configuracion  = '<li class="dropdown" id="configuracion">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Configuración <span class="caret"></span></a>
                            <ul class="dropdown-menu">';

        $itemsReporte        = '';
        $itemsConfiguracion  = '';
        $itemsAdministracion = '';
        $menu                = '';

        foreach($contenidoPortal as $item) {
            switch($item->COPO_ID) {
                case Util::CONTENIDO_PORTAL_DESCARGA_DOCUMENTOS:
                        $itemsReporte = $itemsReporte.
                            '<li id="descargaDocumentos"><a href='. route('descargaDocumentos').' class="loading-message">Descarga de documentos</a></li>';
                    break;
                case Util::CONTENIDO_PORTAL_RESULTADOS:
                        $itemsReporte = $itemsReporte.
                            '<li id="resultados"><a href='. route('resultados') .' class="loading-message">Resultados</a></li>';
                    break;
                case Util::CONTENIDO_PORTAL_DATOS_HISTORICOS:
                        $itemsReporte = $itemsReporte.
                            '<li id="datosHistoricos"><a href='. route('datosHistoricos').' chistoricolass="loading-message">Datos históricos</a></li>';
                    break;
                case Util::CONTENIDO_PORTAL_DATOS_ENTRADA:
                        $itemsReporte = $itemsReporte.
                            '<li id="datosEntrada"><a href='. route('datosEntrada').' class="loading-message">Datos de entrada</a></li>';
                    break;
                case Util::CONTENIDO_PORTAL_LOG:
                        $itemsReporte = $itemsReporte.
                            '<li id="log"><a href='. route('log').' class="loading-message">Log de acciones</a></li>';
                    break;
                case Util::CONTENIDO_PORTAL_USUARIOS:
                        $itemsConfiguracion = $itemsConfiguracion.
                            '<li id="usuario"><a href='. route('usuario').' class="loading-message">Usuarios</a></li>';
                    break;
                case Util::CONTENIDO_PORTAL_PERFILES:
                        $itemsConfiguracion = $itemsConfiguracion.
                            '<li id="perfil"><a href='. route('perfil').' class="loading-message">Perfiles</a></li>';
                    break;
                case Util::CONTENIDO_PORTAL_SESION:
                        $itemsConfiguracion = $itemsConfiguracion.
                            '<li id="sesion"><a href="#" data-toggle="modal" data-backdrop="static" data-target="#modalDeslog">Sesión generador</a></li>';
                    break;
                case Util::CONTENIDO_PORTAL_VARIABLES:
                        $itemsConfiguracion = $itemsConfiguracion.
                            '<li id="variable"><a href='. route('variable').' class="loading-message">Variables</a></li>';
                    break;
                case Util::CONTENIDO_PORTAL_FORMULAS:
                        $itemsConfiguracion = $itemsConfiguracion.
                            '<li id="formula"><a href='. route('formula').' class="loading-message">Fórmulas</a></li>';
                    break;
                case Util::CONTENIDO_PORTAL_CARGA:
                        $itemsAdministracion = $itemsAdministracion.
                            '<li id="cargaHistorica"><a href='. route('cargaHistorica').' class="loading-message">Carga histórica</a></li>'.'<li id="historico"><a href='. route('historico').' class="loading-message">Datos hístóricos</a></li>';
                    break;
                case Util::CONTENIDO_PORTAL_DOCUMENTOS:
                        $itemsAdministracion = $itemsAdministracion.
                            '<li id="administrarDocumentos"><a href='. route('plantillaDocumento').' class="loading-message">Administrar plantillas</a></li>';
                    break;
                case Util::CONTENIDO_PORTAL_CALCULO:
                    $menu = $menu.
                        '<li id="proceso"><a href='. route('proceso').' class="loading-message">Cálculo</a></li>';
                    break;
            }
        }
        if ($itemsAdministracion != '') {
            $administracion .= $itemsAdministracion . '</ul></li>';
            $menu           .= $administracion;
        }
        if ($itemsConfiguracion != '') {
            $configuracion .= $itemsConfiguracion . '</ul></li>';
            $menu          .= $configuracion;
        }
        if ($itemsReporte != '') {
            $reporte .= $itemsReporte . '</ul></li>';
            $menu    .= $reporte;
        }

        return $menu;
    }

    /**
    * Función para construir el menú dinámico para cambio de perfil 'en caliente'
    */
    public static function menuPerfiles($perfiles, $perf_id) {
        $menuPerfiles = '</i><span class="caret"></span></a><ul class="dropdown-menu"><li class="dropdown-header">CAMBIAR PERFIL</li><form id="cambioPerfil" method="post" action="'.url().'/perfil/cambioPerfil">';

        switch($perf_id) {
            case Util::PERFIL_USUARIO_ADMINISTRADOR:
                    $icono = '<i class="fa fa-user">';
                    break;
            case Util::PERFIL_USUARIO_GENERADOR:
                    $icono = '<i class="fa fa-cog">';
                    break;
            default:
                $icono = '<i class="fa fa-hand-paper-o">';
        }

        foreach($perfiles as $item) {
            $activo = '';
            if ($item->PERF_ID == $perf_id) {
                $activo = ' class="active"';
            }
            switch($item->PERF_ID) {
                case Util::PERFIL_USUARIO_ADMINISTRADOR:
                    $menuPerfiles = $menuPerfiles.
                        '<li'.$activo.'>
                            <a href="#" onclick="cambiarPerfil('.$item->PERF_ID.')" data-toggle="modal" data-keyboard="false" data-backdrop="static" class="link">
                                '.view('partials.icon.administrador').'
                                Administrador
                            </a>
                        </li>';
                    break;
                case Util::PERFIL_USUARIO_GENERADOR:
                        $menuPerfiles = $menuPerfiles.
                            '<li'.$activo.'>
                                <a href="#" onclick="cambiarPerfil('.$item->PERF_ID.')" data-toggle="modal" data-keyboard="false" data-backdrop="static" class="link">
                                    '.view('partials.icon.generador').'
                                    Generador
                                </a>
                            </li>';
                    break;
                case Util::PERFIL_USUARIO_VALIDADOR:
                        $menuPerfiles = $menuPerfiles.
                            '<li'.$activo.'>
                                <a href="#" onclick="cambiarPerfil('.$item->PERF_ID.')" data-toggle="modal" data-keyboard="false" data-backdrop="static" class="link">
                                    '.view('partials.icon.validador').'
                                    Validador
                                </a>
                            </li>';
                    break;
                default:
                    $menuPerfiles = $menuPerfiles.
                        '<li'.$activo.'>
                            <a href="#" onclick="cambiarPerfil('.$item->PERF_ID.')" data-toggle="modal" data-keyboard="false" data-backdrop="static" class="link">
                                '.view('partials.icon.otros').
                                $item->PERF_NOMBRE.'
                            </a>
                        </li>';
            }
        }
        $menuPerfiles = $icono.$menuPerfiles.
                            '<input type="hidden" id="perf_id_nuevo" name="perf_id_nuevo" /><input type="hidden" name="_token" id="_token" value="'. csrf_token() .'" /></form><li role="separator" class="divider"></li>
                                <li><a href='. route('auth/logout') .'>Salir</a></li>
                            </ul>';

        return $menuPerfiles;
    }

    /**
    * Función para construir el home correspondiente a cada perfil
    */
    public static function home($contenidoPortal) {

        $home = '';
        foreach($contenidoPortal as $item) {
            switch($item->COPO_ID) {
                case Util::CONTENIDO_PORTAL_REPORTES:
                    $home = $home.
                        '<div class="col-md-3">
                            <div class="panel panel-default">
                              <div class="panel-heading">REPORTES</div>
                              <div class="panel-body text-center">
                                <a href='. route('descargaDocumentos').' class="btn btn-default btn-lg loading-message">
                                  <i class="fa fa-file-text-o"></i> Descarga documentos
                                </a>
                              </div>
                            </div>
                        </div>';
                    break;
                case Util::CONTENIDO_PORTAL_USUARIOS:
                        $home = $home.
                            '<div class="col-md-3">
                                <div class="panel panel-default">
                                  <div class="panel-heading">USUARIOS</div>
                                  <div class="panel-body text-center">
                                    <a href='. route('usuario').' class="btn btn-default btn-lg loading-message">
                                      <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Acceso Usuarios
                                    </a>
                                  </div>
                                </div>
                            </div>';
                    break;
                case Util::CONTENIDO_PORTAL_CALCULO:
                    $home = $home.
                        '<div class="col-md-3">
                            <div class="panel panel-default">
                              <div class="panel-heading">CÁLCULO</div>
                              <div class="panel-body text-center">
                                <a href='. route('proceso').' class="btn btn-default btn-lg loading-message">
                                  <i class="fa fa-calculator"></i> Proceso de calculo
                                </a>
                              </div>
                            </div>
                        </div>';
                    break;
                case Util::CONTENIDO_PORTAL_DOCUMENTOS:
                    $home = $home.
                        '<div class="col-md-3">
                            <div class="panel panel-default">
                              <div class="panel-heading">PLANTILLAS</div>
                              <div class="panel-body text-center">
                                <a href='. route('plantillaDocumento').' class="btn btn-default btn-lg loading-message">
                                  <i class="fa fa-file"></i> Listado de Plantillas
                                </a>
                              </div>
                            </div>
                        </div>';
                    break;
                case Util::CONTENIDO_PORTAL_PERFILES:
                    $home = $home.
                        '<div class="col-md-3">
                            <div class="panel panel-default">
                              <div class="panel-heading">PERFILES</div>
                              <div class="panel-body text-center">
                                <a href='. route('perfil').' class="btn btn-default btn-lg loading-message">
                                  <i class="fa fa-check-square-o"></i> Listado de Perfiles
                                </a>
                              </div>
                            </div>
                        </div>';
                    break;
                case Util::CONTENIDO_PORTAL_FORMULAS:
                    $home = $home.
                        '<div class="col-md-3">
                            <div class="panel panel-default">
                              <div class="panel-heading">FÓRMULAS</div>
                              <div class="panel-body text-center">
                                <a href='. route('formula').' class="btn btn-default btn-lg loading-message">
                                  <i class="fa fa-cogs"></i> Fórmulas
                                </a>
                              </div>
                            </div>
                        </div>';
                    break;
                case Util::CONTENIDO_PORTAL_VARIABLES:
                    $home = $home.
                        '<div class="col-md-3">
                            <div class="panel panel-default">
                              <div class="panel-heading">VARIABLES</div>
                              <div class="panel-body text-center">
                                <a href='. route('variable').' class="btn btn-default btn-lg loading-message">
                                  <i class="fa fa-cubes"></i> Variables
                                </a>
                              </div>
                            </div>
                        </div>';
                    break;
            }
        }

        return $home;
    }
}

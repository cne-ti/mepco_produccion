<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    protected $table = 'MEPCO_PERFIL';

    protected $primaryKey = 'PERF_ID';

    public $timestamps = false;

    protected $fillable = [
        'PERF_NOMBRE', 
        'PERF_DESCRIPCION'
    ];

    /**
    * Atributos por defecto
    */
    protected $attributes = ['PERF_ELIMINADO' => 0];

    /**
    * Relación 1:N con Usuarios
    */
    public function usuarios() {
        return $this->hasMany('App\Usuario', 'PERF_ID');
    }

    /**
    * Relación N:M con Contenido Portal
    */
    public function contenidoPortal() 
    {
        return $this->belongsToMany('App\ContenidoPortal', 'MEPCO_PERF_COPO', 'PERF_ID', 'COPO_ID')->withPivot('PECO_ID');
    }

    /**
    * Comprueba si el perfil está habilitado para ver el contenido
    */
    public static function hasContent($copo_id, $contenidoPortal) {
        
        foreach($contenidoPortal as $item) {
            if ($item->COPO_ID == $copo_id) {
                return true;
            }
        }
        return false;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VameUspe extends Model 
{
    protected $table='MEPCO_VAME_USPE';

    protected $primaryKey='VAUS_ID';

    public $timestamps=false;

    protected $fillable=[
        'PROC_ID',
        'USPE_ID',
        'VAUS_FECHA_CREACION',
        'VAUS_FECHA_VALIDADO',
        'VAUS_FECHA_ELIMINADO',
        'VAUS_VALIDACION_CORRECTA'
    ];

    protected $attributes = [
        'VAUS_ELIMINADO' => 0
    ];

    /**
    * Relación 1:N con PROCESO
    */
    public function proceso()
    {
        return $this->belongsTo('App\Proceso', 'PROC_ID');
    }

    /**
    * Relación 1:N con Usuario
    */
    public function usuario() {
        return $this->belongsTo('App\Usuario', 'USPE_ID');
    }
}

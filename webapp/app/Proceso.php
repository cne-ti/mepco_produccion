<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proceso extends Model
{
    protected $table = 'MEPCO_PROCESO';

    protected $primaryKey = 'PROC_ID';

    public $timestamps = false;

	protected $fillable = [
        'PRES_ID', 
        'PROC_FECHA_INICIO_DATO_ORIGINAL', 
        'PROC_FECHA_FIN_DATO_ORIGINAL', 
        'PROC_FECHA_VIGENCIA',
        'PROC_ELIMINADO'
    ];

    protected $attributes = [
        'PRES_ID' => Util::PROCESO_ESTADO_NUEVO,
        'PROC_ELIMINADO' => 0
    ];

    public function estados()
    {
        return $this->hasOne('App\ProcesoEstado', 'PRES_ID');
    }

    public function usuarios() 
    {
        return $this->belongsToMany('App\Usuario', 'MEPCO_PROC_USPE', 'PROC_ID', 'USPE_ID')
                    ->withPivot('PRUS_FECHA_CREACION', 
                                'PRUS_FECHA_MODIFICADO',
                                'PRUS_FECHA_VALIDADO', 
                                'PRUS_FECHA_ELIMINADO');
    }

    /**
    * Relación 1:N con Escenario
    */
    public function escenarios() {
        return $this->hasMany('App\Escenario', 'PROC_ID');
    }

    /**
    * Relación 1:N con la tabla de log de valores de mercado
    */
    public function vameUspe() {
        return $this->hasMany('App\VameUspe', 'PROC_ID');
    }

    /**
    * Relación 1:N con la tabla SOLVER
    */
    public function solver() {
        return $this->hasMany('App\Solver', 'PROC_ID');
    }

    public function scopeExisteProcesoVigente($query)
    {
        return $query->whereIn('PRES_ID', [
            Util::PROCESO_ESTADO_NUEVO,
            Util::PROCESO_ESTADO_INICIADO,
            Util::PROCESO_ESTADO_INGRESO_VALORES_MERCADO
        ]);
    }
}



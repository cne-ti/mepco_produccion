<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variable extends Model 
{
    protected $table='MEPCO_PARAMETROS_VARIABLE';

    protected $primaryKey='PAVA_ID';

    public $timestamps=false;

	protected $fillable=[
        'PAVA_LLAVE',
        'PAVA_NOMBRE', 
        'PAVA_VALOR_CALCULO',
        'PAVA_VALOR_PLANTILLA', 
        'PAVA_DESCRIPCION',
        'PAVA_FECHA_MODIFICACION',
    ];

    protected $attributes = [
        'PAVA_ELIMINADO' => 0,
    ];

    public function scopeLlave($query, $llave)
    {
        return $query->where('PAVA_LLAVE', $llave);
    }

    /**
    * Relación 1:N con hidrocarburos
    */
    public function hidrocarburo() {
        return $this->hasOne('App\Hidrocarburo', 'HIDR_ID');
    }


}

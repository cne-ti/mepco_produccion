<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = 'MEPCO_LOG';

    protected $primaryKey = 'LOG_ID';

    public $timestamps = false;

    protected $fillable = [
        'USPE_ID', 
        'LOG_FECHA',
        'LOG_NOMENCLATURA',
        'LOG_NOMBRE_ACCION',
        'LOG_DESCRIPCION_ACCION',
        'LOG_VALOR_ACCION'
    ];


    /**
    * Relación 1:N con Usuarios
    */
    public function usuario() {
        return $this->hasOne('App\Usuario', 'USPE_ID');
    }
}

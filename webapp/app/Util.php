<?php

namespace App;

use Carbon\Carbon;
use PHPExcel_Shared_Date;
use Rezzza\Formulate\Formula as Form;
use App\DatosHistoricos;
use DB;
use App\Log as AppLog;
use Log;
use Illuminate\Http\Request;

class Util
{
	// SI ES DEBUG TRUE GENERARÁ LOGS
	const DEBUG = false;

	const MAX_FILE_SIZE = 2000000; //2MB

	// VARIABLES DE SESIÓN
	const USUARIO 	    = 'USUARIO';
	const MENU 		    = 'MENU';
	const MENU_PERFILES = 'MENU_PERFILES';
	const CAMBIO_PERFIL = 'CAMBIO_PERFIL';
	const HOME          = 'HOME';
	
	//PERFIL
	const PERFIL_USUARIO_ADMINISTRADOR = 1;
	const PERFIL_USUARIO_GENERADOR     = 2;
	const PERFIL_USUARIO_VALIDADOR     = 3;

	//PROCESO
	const PROCESO_ESTADO_NUEVO                        = 1;
	const PROCESO_ESTADO_INICIADO                     = 2;
	const PROCESO_ESTADO_INGRESO_VALORES_MERCADO      = 3;
	const PROCESO_ESTADO_VALOR_DE_MERCADO_VALIDADO    = 7;
	const PROCESO_ESTADO_VALOR_DE_MERCADO_NO_VALIDADO = 9;
	const PROCESO_ESTADO_FINALIZADO				      = 5;
	const PROCESO_ESTADO_PROCESO_SUGERIDO    	      = 10;
	const PROCESO_ESTADO_RECALCULAR                   = 6;

	// CONTENIDO DEL PORTAL
	const CONTENIDO_PORTAL_REPORTES 		= 1;
	const CONTENIDO_PORTAL_USUARIOS 		= 2;
	const CONTENIDO_PORTAL_PERFILES 		= 3;
	const CONTENIDO_PORTAL_SESION 			= 4;
	const CONTENIDO_PORTAL_VARIABLES 		= 5;
	const CONTENIDO_PORTAL_FORMULAS 		= 6;
	const CONTENIDO_PORTAL_CARGA 			= 7;
	const CONTENIDO_PORTAL_DOCUMENTOS 		= 8; 
	const CONTENIDO_PORTAL_CALCULO 			= 9;

	const CONTENIDO_PORTAL_DESCARGA_DOCUMENTOS = 10;
	const CONTENIDO_PORTAL_RESULTADOS          = 11;
	const CONTENIDO_PORTAL_DATOS_HISTORICOS    = 12;
	const CONTENIDO_PORTAL_DATOS_ENTRADA       = 13;
	const CONTENIDO_PORTAL_LOG                 = 14;

	// TIPOS DE FÓRMULAS
	const FORMULA_CALCULO					= 0;
	const FORMULA_SOLVER					= 1;
	const FORMULA_PROYECCION				= 2;

	// TIPO DE VARIABLE
	const VARIABLE_CALCULO					= 0;
	const VARIABLE_PLANTILLA				= 1;
	const VARIABLE_QUERY					= 2;
	const VARIABLE_INPUT					= 3;
	const VARIABLE_QUERY_PLANTILLA			= 4;
	const VARIABLE_PROYECCION				= 5;
	const VARIABLE_PROYECCION_QUERY			= 6;

	//PATH ARCHIVOS
	const PATH_VALOR_MERCADO      	 = "proceso/{fecha}/valores de mercado/";
	const PATH_DOCUMENTOS_RESPALDO   = "proceso/{fecha}/documentos respaldo/";
	const PATH_PLANTILLAS			 = "plantillas/";
	const PATH_INFORMES				 = "informes/";
	const PATH_CARGA_HISTORICA		 = "cargaHistorica/";

	//CONSTANTES PARA COMPROBAR LOS TIPOS DE DATOS DE ENTRADA
	const IS_FLOAT 	  = 1;
	const IS_INT      = 2;

	//CONSTANTE PARA DEFINIR SI LA CARGA ES DE VALORES DE MERCADO O HISTÓRICA
	const PROCESAR_VARIABLES_MERCADO = 1;
	const PROCESAR_CARGA_HISTORICA   = 2;

	//EXCEL CONTENIDO COLUMNAS
	const EXCEL_FECHA 			= 'A';
	const EXCEL_GAS_UNL_87 		= 'E';
	const EXCEL_GAS_UNL_93 		= 'I';
	const EXCEL_DIESEL 			= 'M';
	const EXCEL_USG_BUTANO 		= 'Q';
	const EXCEL_UKC_USG 		= 'S';
	const EXCEL_BRENT 			= 'W';	
	const EXCEL_PPT_USG 		= 'Y';
	const EXCEL_MONT_BELVIEU_1 	= 'AA';
	const EXCEL_MONT_BELVIEU_2 	= 'AB';
	const EXCEL_CIF_ARA_1 		= 'AE';
	const EXCEL_CIF_ARA_2 		= 'AF';
	const EXCEL_WTI 			= 'AK';
	const EXCEL_WTI_M1 			= 'AL';
	const EXCEL_WTI_M2 			= 'AM';
	const EXCEL_WTI_M3 			= 'AN';
	const EXCEL_WTI_M4 			= 'AO';
	const EXCEL_WTI_M5 			= 'AP';
	const EXCEL_WTI_M6 			= 'AQ';
	const EXCEL_TC_PUBLICADO  	= 'AR';
	const EXCEL_UTM 			= 'AS';
	const EXCEL_LIBOR 			= 'AT';
	const EXCEL_TARIFA_DIARIA_82000 = 'AU';
	const EXCEL_TARIFA_DIARIA_59000 = 'AV';
	const EXCEL_IFO_380_HOUSTON 	= 'AW';
	const EXCEL_MDO_HOUSTON 		= 'AX';
	const EXCEL_IFO_380_CRISTOBAL 	= 'AY';
	const EXCEL_MDO_CRISTOBAL 		= 'AZ';
	const EXCEL_BRENT_M1 		= 'BA';
	const EXCEL_BRENT_M2 		= 'BB';
	const EXCEL_BRENT_M3 		= 'BC';
	const EXCEL_BRENT_M4 		= 'BD';
	const EXCEL_BRENT_M5 		= 'BE';
	const EXCEL_BRENT_M6 		= 'BF';
	const EXCEL_N_GAS_87 			= 'BG';
	const EXCEL_M_GAS_87 			= 'BH';
	const EXCEL_S_GAS_87			= 'BI';
	const EXCEL_T_GAS_87 			= 'BJ';
	const EXCEL_F_GAS_87 			= 'BK';
	const EXCEL_N_GAS_93			= 'BL';
	const EXCEL_M_GAS_93 			= 'BM';
	const EXCEL_S_GAS_93			= 'BN';
	const EXCEL_T_GAS_93			= 'BO';
	const EXCEL_F_GAS_93			= 'BP';
	const EXCEL_N_DIESEL			= 'BQ';
	const EXCEL_M_DIESEL			= 'BR';
	const EXCEL_S_DIESEL			= 'BS';
	const EXCEL_T_DIESEL			= 'BT';
	const EXCEL_F_DIESEL			= 'BU';
	const EXCEL_N_GLP				= 'BV';
	const EXCEL_M_GLP				= 'BW';
	const EXCEL_S_GLP				= 'BX';
	const EXCEL_T_GLP				= 'BY';
	const EXCEL_F_GLP				= 'BZ';
	const EXCEL_CORRECCION_RVP		= 'CA';
	const EXCEL_PROM_PARIDAD_SEMANAL_GAS_87	= 'CB';
	const EXCEL_PROM_PARIDAD_SEMANAL_GAS_93 = 'CC';
	const EXCEL_PROM_PARIDAD_SEMANAL_DIESEL = 'CD';
	const EXCEL_PROM_PARIDAD_SEMANAL_GLP	= 'CE';
	const EXCEL_PARIDAD_GAS_87				= 'CF';
	const EXCEL_PARIDAD_GAS_93				= 'CG';
	const EXCEL_PARIDAD_DIESEL				= 'CH';
	const EXCEL_PARIDAD_GLP					= 'CI';
	const EXCEL_PROM_BRENT_SEMANAL 			= 'CJ';
	const EXCEL_BRENT_PROM_HISTORICO_GAS_87 = 'CK';
	const EXCEL_BRENT_PROM_HISTORICO_GAS_93 = 'CL';
	const EXCEL_BRENT_PROM_HISTORICO_DIESEL = 'CM';
	const EXCEL_BRENT_PROM_HISTORICO_GLP 	= 'CN';
	const EXCEL_BRENT_PROM_FUTUROS_GAS_87 	= 'CO';
	const EXCEL_BRENT_PROM_FUTUROS_GAS_93 	= 'CP';
	const EXCEL_BRENT_PROM_FUTUROS_DIESEL 	= 'CQ';
	const EXCEL_BRENT_PROM_FUTUROS_GLP 		= 'CR';
	const EXCEL_BRENT_PROM_PONDERADO_GAS_87 = 'CS';
	const EXCEL_BRENT_PROM_PONDERADO_GAS_93 = 'CT';
	const EXCEL_BRENT_PROM_PONDERADO_DIESEL = 'CU';
	const EXCEL_BRENT_PROM_PONDERADO_GLP 	= 'CV';
	const EXCEL_PROM_WTI 					= 'CW';
	const EXCEL_WTI_PROM_HISTORICO_GAS_87 	= 'CX';
	const EXCEL_WTI_PROM_HISTORICO_GAS_93 	= 'CY';
	const EXCEL_WTI_PROM_HISTORICO_DIESEL 	= 'CZ';
	const EXCEL_WTI_PROM_HISTORICO_GLP 		= 'DA';
	const EXCEL_WTI_PROM_FUTUROS_GAS_87 	= 'DB';
	const EXCEL_WTI_PROM_FUTUROS_GAS_93 	= 'DC';
	const EXCEL_WTI_PROM_FUTUROS_DIESEL 	= 'DD';
	const EXCEL_WTI_PROM_FUTUROS_GLP 		= 'DE';
	const EXCEL_WTI_PROM_PONDERADO_GAS_87 	= 'DF';
	const EXCEL_WTI_PROM_PONDERADO_GAS_93 	= 'DG';
	const EXCEL_WTI_PROM_PONDERADO_DIESEL 	= 'DH';
	const EXCEL_WTI_PROM_PONDERADO_GLP 		= 'DI';
	const EXCEL_PROM_MARGEN_SEMANAL_BRENT_GAS_87 	= 'DJ';
	const EXCEL_PROM_MARGEN_SEMANAL_BRENT_GAS_93 	= 'DK';
	const EXCEL_PROM_MARGEN_SEMANAL_BRENT_DIESEL 	= 'DL';
	const EXCEL_PROM_MARGEN_SEMANAL_BRENT_GLP 		= 'DM';
	const EXCEL_PROM_MARGEN_SEMANAL_WTI_GAS_87		= 'DN';
	const EXCEL_PROM_MARGEN_SEMANAL_WTI_GAS_93		= 'DO';
	const EXCEL_PROM_MARGEN_SEMANAL_WTI_DIESEL		= 'DP';
	const EXCEL_PROM_MARGEN_SEMANAL_WTI_GLP		= 'DQ';
	const EXCEL_TIPO_CRUDO					= 'DR';
	const EXCEL_MARGEN_PROMEDIO_GAS_87 		= 'DS';
	const EXCEL_MARGEN_PROMEDIO_GAS_93 		= 'DT';
	const EXCEL_MARGEN_PROMEDIO_DIESEL 		= 'DU';
	const EXCEL_MARGEN_PROMEDIO_GLP 		= 'DV';
	const EXCEL_FOB_TEORICO_GAS_87 			= 'DW';
	const EXCEL_FOB_TEORICO_GAS_93 			= 'DX';
	const EXCEL_FOB_TEORICO_DIESEL 			= 'DY';
	const EXCEL_FOB_TEORICO_GLP 			= 'DZ';
	const EXCEL_PROM_REFERENCIA_SEMANAL_GAS_87 	= 'EA';
	const EXCEL_PROM_REFERENCIA_SEMANAL_GAS_93 	= 'EB';
	const EXCEL_PROM_REFERENCIA_SEMANAL_DIESEL 	= 'EC';
	const EXCEL_PROM_REFERENCIA_SEMANAL_GLP 	= 'ED';

	const EXCEL_MONT_BELVIEU_CALCULADO			= 'AC';
	const EXCEL_CIF_ARA_CALCULADO				= 'AG';
	
	const CANTIDAD_FILAS_VALOR_MERCADO = 5;
	const PPT_USG_UMBRAL               = 161;
	
	const SIN_HIDROCARBURO_ASOCIADO = 0;
	const COMBUSTIBLE_93     		= 1;
	const COMBUSTIBLE_97     		= 2;
	const COMBUSTIBLE_DIESEL 		= 3;
	const COMBUSTIBLE_GLP    		= 4;
	
	const P_REF_INF = 'PREFINF';
	const P_REF_INT = 'PREFINT';
	const P_REF_SUP = 'PREFSUP';
	const P_PARIDAD = 'PPARIDAD';
	const MARGEN    = 'MARGEN';
	const CRUDO     = 'CRUDO';

	const BRENT 	= 1;
	const WTI 		= 2;

	// Llaves
	const N 		= '{{N}}';
	const M 		= '{{M}}';
	const S 		= '{{S}}';
	const F 		= '{{F}}';
	const T 		= '{{T}}';

	/**
	* Almacena valores de formula segun fecha
	*/
	public static $arrFormulaValores = array();

	/**
	* Almcena valor en caso de recibir condicion en 0
	*/
	public static $arrResultadoEjecutarFormula = array();

	/**
	* Flag para determinar si debe almacenar resultado de formulas sin fecha
	*/
	public static $almacenaFormulaValoresSinFecha = false;

	public static $arrAuxVariablesCache  = array('{{BR_BRENT_PROMEDIO_SEM_HISTORICO_$/m3}}' => null);

	/**
	 * Obtiene path para almacentar documentos asociados al 
	 * proceso de cálculo
	 * @return string
	 */
	public static function getPath($llave, $fechaVigencia)
	{
		$txt = null;
		$fechaVigencia = self::dateFormat($fechaVigencia, 'Ymd');
		switch ($llave) {
			case 'VALOR_MERCADO':
				$txt = str_replace('{fecha}', $fechaVigencia, self::PATH_VALOR_MERCADO);
				break;
			case 'DOCUMENTOS_RESPALDO':
				$txt = str_replace('{fecha}', $fechaVigencia, self::PATH_DOCUMENTOS_RESPALDO);
				break;
			
			default:
				break;
		}

		return $txt;
	}

	/**
	 * Obtiene fecha y hora actual de Chile
	 * @return datetime
	 */
	public static function dateTimeNow()
	{
		return Carbon::now('America/Santiago');
	}

	/**
	 * Trasnforma string fecha d/m/Y a Y-m-d
	 * @return datetime
	 */
	public static function strToDate($strDate)
	{
		$result = null;
		$arrDate = explode("/", $strDate);
		if(count($arrDate) == 3)
		{
			$result = Carbon::create($arrDate[2], $arrDate[1], $arrDate[0], 0,0,0);
		}
		return $result;
	}	

	/**
	 * Obtiene un string y formatea las variables encontradas y las palabras reservadas de MySQL
	 * @return datetime
	 */
	public static function formatText($str)
	{
		// Formatea variables
		preg_match_all('/\{\{(.*?)\}\}/s', $str, $coincidences);
		foreach($coincidences[0] as $coincidence) {
			$str = str_replace($coincidence, '<span style="color:blue;">'.$coincidence.'</span>', $str);
		}

		// Formatea condiciones si en fórmulas
		preg_match_all('/SI\((.*?)\)/', $str, $coincidences);
		foreach($coincidences[0] as $coincidence) {
			$str = str_replace($coincidence, '<span style="color:red;">'.$coincidence.'</span>', $str);
		}
		
		// Formatea signo ?
		preg_match_all('/\?/', $str, $coincidences);
		foreach($coincidences[0] as $coincidence) {
			$str = str_replace($coincidence, '<span style="color:orange;"><strong>'.$coincidence.'</strong></span>', $str);
		}

		// Formatea palabras reservadas de sql
        $reservedWords = ['SELECT', 'FROM', 'WHERE', 'AFTER', 'AND', 'ANY', 'AS', 'AVG', 'BEFORE', 'BETWEEN', 'BIGINT', 'BIT', 'BOOLEAN', 'BOTH', 'CASE', 'CHAR', 'COLUMN', 'COLUMNS', 'CONTAINS', 'DESC', 'ASC', 'EACH', 'FIRST', 'ON', 'OR', 'FLOAT', 'HAVING', 'INNER', 'INT', 'JOIN', 'KEY', 'LIKE', 'LIMIT', 'LONG', 'NO', 'NOT', 'TABLE', 'THEN', 'TEXT', 'TINYINT', 'TO', 'VALUE', 'VARCHAR', 'UNION', 'UNIQUE', 'USING'];

       	foreach($reservedWords as $key=>$word) {
       		preg_match_all('/\b'.$word.'\b/i', $str, $coincidences);
			foreach($coincidences[0] as $coincidence) {
				$str = str_replace($coincidence, '<strong><span style="color:blue;">'.$coincidence.'</span></strong>', $str);
			}
       	}
       	return $str;
	}

	/**
	 * Obtiene fecha y formatea segun input
	 * @return datetime
	 */
	public static function dateFormat($date, $format)
	{
		$result = null;
		if($date != "")
		{
			$dt = Carbon::parse($date);
			$result = $dt->format($format);
		}
		return $result;
	}

	/**
	 * Obtiene nombre de archivo y retorna en formato
	 * YmdHis_nombre_extension, si el nombre supera los 25
	 * caracteres trunca el nombre
	 * @return string
	 */
	public static function formatFileName($name)
	{
		$strAux = null;
		$strExt = null;

		$listDots = explode(".", $name);
		$countDots = count($listDots);
		if($countDots > 0)
		{
			$strExt = $listDots[--$countDots];
		}

		if(strlen($name) > 25)
		{
			$strAux = substr($name, 0, 25);
		}else
		{
			$strAux = str_replace("." . $strExt, "", $name);
		}

		$strNow = self::dateTimeNow()->format('YmdHis');
		return $strNow . '_' . $strAux . '.' . $strExt;
	}

	/**
	 * Obtiene valor almacenado en tabla variable y ejecuta query 
	 * según llave y tipo
	 * @return string
	 */
	public static function obtenerVariablePorLlave($llave, $valores)
	{
		$result = null;
		try{
			if(!self::contains($llave, '{{') && !self::contains($llave, '}}'))
			{
				$llave = '{{' . $llave . '}}';
			}

			$variable = \App\Variable::Llave($llave)->first();

			self::log('OVPL.VARIABLE : ' . $llave);
			
			if($variable != null && isset($variable))
			{
				switch ($variable->PAVA_TIPO_VARIABLE) {
					case self::VARIABLE_CALCULO:
						$result = $variable->PAVA_VALOR_CALCULO;
						break;
					case self::VARIABLE_PLANTILLA:
						$result = $variable->PAVA_VALOR_PLANTILLA;
						break;
					case self::VARIABLE_QUERY_PLANTILLA:
						$query = $variable->PAVA_VALOR_PLANTILLA;

						$variables = null;
						if(self::contains($query, '{{'))
						{
							$variables = self::obtenerLlaves($query);
						}

						if ($variables!=null)
						{
							foreach ($variables as $item) {
								$query = str_replace('{{' . $item . '}}', $valores[$item], $query);
							}
						}
						$result = DB::select(DB::raw($query));
						break;
					case self::VARIABLE_QUERY:
						self::log('OVPL.TIPO     : QUERY');
						$query = $variable->PAVA_VALOR_PLANTILLA;
						$variables = null;
						
						if(self::contains($query, '{{'))
						{
							$variables = self::obtenerLlaves($query);
						}

						self::log('OVPL.QUERY    : ' . $query);
						if ($variables != null)
						{
							foreach ($variables as $item) {
								self::log('OVPL.VAR      : ' . $item);
								$query = str_replace('{{' . $item . '}}', $valores[$item], $query);
							}
						}
						self::log('OVPL.QUERY    : ' . $query);
						$list = DB::select(DB::raw($query));
						if($list != null && count($list) > 0)
						{
							$result = (float)reset($list[0]);
						}
						break;
					case self::VARIABLE_PROYECCION:
						$result = $variable->PAVA_VALOR_CALCULO;
						break;
					case self::VARIABLE_PROYECCION_QUERY:
						self::log('OVPL.TIPO     : QUERY');
						$query = $variable->PAVA_VALOR_PLANTILLA;
						$variables = null;
						
						if(self::contains($query, '{{'))
						{
							$variables = self::obtenerLlaves($query);
						}

						self::log('OVPL.QUERY    : ' . $query);
						if ($variables != null)
						{
							foreach ($variables as $item) {
								self::log('OVPL.VAR      : ' . $item);
								$query = str_replace('{{' . $item . '}}', $valores[$item], $query);
							}
						}
						self::log('OVPL.QUERY    : ' . $query);
						$list = DB::select(DB::raw($query));
						if($list != null && count($list) > 0)
						{
							$result = (float)reset($list[0]);
						}
						break;
					case self::VARIABLE_INPUT:
						$auxVariable = $variable->PAVA_LLAVE;
						$auxVariable = str_replace('{{', '', $auxVariable);
						$auxVariable = str_replace('}}', '', $auxVariable);
						$result = $valores[$auxVariable];
						break;
					default:
						$result = null;
						break;
				}
			}else
			{
				$result = self::ejecutarFormulaInception($llave, $valores);
				self::log('LLAVE FORM    : ' . $llave);
				self::log('RESULTAD FORM : ' . $result);
			}
			return $result;
		}catch(Exception $e)
		{
			Log::error("Util.obtenerVariablePorLlave EX: " . $e->message);
		}
	}

	/**
	 * Obtiene valor entre tags
	 * @return string
	 */
	public static function obtenerValorPorTag($string, $start = "{{", $end = "}}"){
	    $string = " " . $string;
	    $ini = strpos($string, $start);
	    if ($ini == 0) return "";
	    $ini += strlen($start);
	    $len = strpos($string, $end, $ini) - $ini;
	    return substr($string, $ini, $len);
	}

	/**
	 * Obtiene lista de tags dentro de formula
	 * @return string[]
	 */
	public static function obtenerLlaves($strForm){
		$listAux = null;
        do
        {
            if((strpos($strForm, '{{') !== false) && (strpos($strForm, '}}') !== false))
            {
                $strAux = self::obtenerValorPorTag($strForm);
                $listAux[] = $strAux;
                $strAux = '{{' . $strAux . '}}';
                $strForm = str_replace($strAux, '', $strForm);
                $continuar = true;
            }
            else
            {
                $continuar = false;
            }
        } while ($continuar);

        return $listAux;
	}

	/**
	* volca formVal
	*/
	public static function obtenerFormulaValores()
	{
		self::log('*********************** INICIO **************************');
		$key1len = 0;
		$key2len = 0;

		if(self::$almacenaFormulaValoresSinFecha){
			foreach (self::$arrFormulaValores as $key1 => $value) {
				if(strlen($key1) > $key1len)
				{
					$key1len = strlen($key1);
				}
			}

			foreach (self::$arrFormulaValores as $key1 => $value) {
				
				$aux1     = strlen($key1);
				$auxDiff1 = $key1len - $aux1;
				$auxStr1  = $key1;
				for($i=0; $i<$auxDiff1; $i++)
					$auxStr1 .= ' ';

				self::log('FormulaValores |' . $auxStr1 . '|VALUE: ' . $value);
			}

		}else{
			foreach (self::$arrFormulaValores as $key1 => $value) {
				if(is_array($value)){
					foreach ($value as $key2 => $value2) {
						if(strlen($key1) > $key1len)
						{
							$key1len = strlen($key1);
						}
						if(strlen($key2) > $key2len)
						{
							$key2len = strlen($key2);
						}
					}
				}
			}

			foreach (self::$arrFormulaValores as $key1 => $value) {
				if(is_array($value)){
					foreach ($value as $key2 => $value2) {
						$aux1     = strlen($key1);
						$auxDiff1 = $key1len - $aux1;
						$auxStr1  = $key1;
						for($i=0; $i<$auxDiff1; $i++)
							$auxStr1 .= ' ';

						$aux2     = strlen($key2);
						$auxDiff2 = $key2len - $aux2;
						$auxStr2  = $key2;
						for($i=0; $i<$auxDiff2; $i++)
							$auxStr2 .= ' ';

						self::log('FormulaValores |' . $auxStr1 . '|' . $auxStr2 . '|VALUE: ' . $value2);
					}
				}
			}
		}
		self::log('***********************  FIN  **************************');
	}

	public static function ejecutarFormula($llave, $arrValores = null)
	{
		self::$arrResultadoEjecutarFormula = array();
		self::$arrFormulaValores = array();
		$resultado = self::ejecutarFormulaInception($llave, $arrValores);
		self::obtenerFormulaValores();
		return $resultado;
	}

	/**
	* Recibe llave de formula y ejecuta su contenido
	* Valores: array con input
	* @return float
	*/
	public static function ejecutarFormulaInception($llave, $arrValores = null)	
	{
		try{
			//Todo a mayusculas (llave, array[key => value])
			$llave = strtoupper($llave);
			if($arrValores != null && count($arrValores) > 0)
			{
				$arrValores = array_change_key_case($arrValores, CASE_UPPER);
				$arrValores = array_map('strtoupper', $arrValores);
			}

			$formula = Formula::Llave($llave)->first();
			self::log('E: ' . $llave);
			$strForm = strtoupper($formula->PAFO_FORMULA);
			self::log("EJEF.FORMULA  : " . $strForm);

			// Primero buscar las condicionales
			preg_match_all('/SI\((.*?)\)/i', $strForm, $condicionales);
			$matches = $condicionales[1];
			foreach($matches as $match) {

				try {
					
					$instruccCondicional = explode('|', $match, 3);
					$condicion 	         = $instruccCondicional[0];
					$condTrue 	         = $instruccCondicional[1];
					$condFalse 	         = $instruccCondicional[2];

					// Buscar si hay variable en $condicion
					$pava_llave          = self::obtenerLlaves($condicion)[0];

					if ($pava_llave) {
						// Hay variable, obtener el valor
						$pava_valor = self::obtenerVariablePorLlave($pava_llave, $arrValores);
						$condicion  = str_replace('{{'.$pava_llave.'}}', $pava_valor, $condicion);

					}
					// Comprobar que $condicion es seguro para ejecutar eval
					$operators = array('==', '!=', '<', '>', '<=', '>=');
					foreach ($operators as $operator) {
						if (self::contains($condicion, $operator)) {
							// Ejecutar condicional
							self::log('$condicion: ' . $condicion);
							$condicion   = eval('return '.$condicion.';');
							if ($condicion) {
								if($condTrue == '0')
								{
									self::$arrResultadoEjecutarFormula[$llave] = 0;
									$strForm = preg_replace('/SI\((.*?)\)/', $condFalse, $strForm);
								}else{
									$strForm = preg_replace('/SI\((.*?)\)/', $condTrue, $strForm);
								}
							}
							else {
								$strForm = preg_replace('/SI\((.*?)\)/', $condFalse, $strForm);
							}
							self::log('$strForm: ' . $strForm);
							break;
						}
					}
				}
				catch(Exception $e) {
					Log::error("Exception: " . $e->getMessage());
					return null;
				}
			}
				
			$variables = self::obtenerLlaves($strForm);

			$debugVariables = "";
			foreach ($variables as $item) {
				$debugVariables .= $item . ", ";
			}
			$debugVariables = substr($debugVariables, '0', strlen($debugVariables) - 2);

			$form = new Form($strForm);

			foreach ($variables as $item) {
				if($arrValores != null)
				{
					if(array_key_exists('FECHA', $arrValores))
					{
						if(array_key_exists($item, self::$arrFormulaValores))
						{
							//YA EXISTE LLAVE FECHA
							if(self::multiKeyExists($arrValores['FECHA'], self::$arrFormulaValores[$item]))
							{
								$auxResult = (float)self::$arrFormulaValores[$item][$arrValores['FECHA']];
							}else
							{
								//AGREGA
								$auxResult = (float)self::obtenerVariablePorLlave($item, $arrValores);
								self::$arrFormulaValores[$item][$arrValores['FECHA']] = $auxResult;	
							}
						}else
						{   //AGREGA
							$auxResult = (float)self::obtenerVariablePorLlave($item, $arrValores);
							self::$arrFormulaValores[$item][$arrValores['FECHA']] = $auxResult;
						}
					}else
					{
						//NO VIENE FECHA, SOLO SE EJECUTA
						$auxResult = (float)self::obtenerVariablePorLlave($item, $arrValores);
						if(self::$almacenaFormulaValoresSinFecha)
						{
							self::$arrFormulaValores[$item] = $auxResult;
						}
					}
				}else
				{
					//NO VIENE FECHA, SOLO SE EJECUTA
					$auxResult = (float)self::obtenerVariablePorLlave($item, $arrValores);
					if(self::$almacenaFormulaValoresSinFecha)
					{
						self::$arrFormulaValores[$item] = $auxResult;
					}
				}
				$form->setParameter($item, $auxResult);
			}

			// $result = $form->render();
			// self::log('--- FORM: ' . $result);
			$form->setIsCalculable(true);
			$result = $form->render();

			if(array_key_exists($llave, self::$arrResultadoEjecutarFormula))
            {
                $result = self::$arrResultadoEjecutarFormula[$llave];
            }

            if($formula->PAFO_CANTIDAD_DECIMALES != null)
            {
            	$result = round($result, $formula->PAFO_CANTIDAD_DECIMALES);
            }

			return $result;
		}
		catch(Exception $e)
		{
			Log::error("Util.ejecutarFormula EX: " . $e->getMessage());
			return null;
		}
	}

	public static function multiKeyExists($key, array $arr) {
	    if (array_key_exists($key, $arr)) {
	        return true;
	    }

	    foreach ($arr as $element) {
	        if (is_array($element)) {
	            if (self::multiKeyExists($key, $element)) {
	                return true;
	            }
	        }
	    }
	    return false;
	}

	/**
	* Retorna formateado valor numerico ####.#
	* @return string #,###.#
	*/
	public static function formatNumberPlantilla($strInput)
	{
		$strOutput = "";
		$negative = false;
		if(self::contains($strInput, '-'))
		{
			$negative = true;
			$strInput = str_replace('-', '', $strInput);
		}

		$arrNum = explode('.', $strInput);
		$len = strlen($arrNum[0]) - 1;
		$x = 1;
		for($i = $len; $i >= 0; $i--)
		{
			$strOutput .= $arrNum[0][(strlen($arrNum[0]) - 1) -$i];
			if($i != 0 && $i % 3 === 0)
			{
				$strOutput .= ".";
			}
		}

		if((count($arrNum) == 2) && ($arrNum[1] !== null))
		{
			$strOutput = $strOutput . ',' .$arrNum[1];
		}

		if($negative)
		{
			$strOutput = '-' . $strOutput;	
		}
		return $strOutput;
	}

	/**
	* Retorna formateado valor numerico ####.#
	* @return string #.###,#
	*/
	public static function formatNumber($strInput)
	{
		$strOutput = "";
		$negative = false;
		if(self::contains($strInput, '-'))
		{
			$negative = true;
			$strInput = str_replace('-', '', $strInput);
		}
		$strInput = (float)$strInput;
		$strAux = str_replace('.', ',', $strInput);
		$arrNum = explode(',', $strAux);
		$len = strlen($arrNum[0]) - 1;
		$x = 1;
		for($i = $len; $i >= 0; $i--)
		{
			$strOutput .= $arrNum[0][(strlen($arrNum[0]) - 1) -$i];
			if($i != 0 && $i % 3 === 0)
			{
				$strOutput .= ".";
			}
		}

		if((count($arrNum) == 2) && ($arrNum[1] !== null))
		{
			$strOutput = $strOutput . ',' .$arrNum[1];
		}

		if($negative)
		{
			$strOutput = '-' . $strOutput;	
		}
		return $strOutput;
	}

	/**
	* Retorna valor quitando formato #.###,#
	* @return string ####.#
	*/
	public static function unFormatNumber($strInput)
	{
		$strOutput = "";
		$strOutput = str_replace('.', '', $strInput);
		$strOutput = str_replace(',', '.', $strOutput);
		return $strOutput;
	}

	/**
	* Retorna fecha 
	* @return string
	*/
	public static function excelToDate($date, $format = 'd/m/Y')
	{
		$datePhp = PHPExcel_Shared_Date::ExcelToPhp($date);
		$datePhp = date($format, $datePhp);
		return $datePhp;
	}

	/**
	* Retorna true si el dato coincide con el tipo indicado
	* @return boolean
	*/
	public static function compruebaTipo($value, $type, $row, $column, $errors)
	{
		// Si $value es distinto a nulo, se comprueba el tipo
		if ($value != NULL) {
			switch($type) {
				case self::IS_FLOAT:
					if (!is_float($value)) {
						$errors['fila'.$row] = $errors['fila'.$row].'Columna '.$column.', tipo de dato no válido. '; 
						$errors['fila'] = 1; 
					}
					break;
				case self::IS_INT:
					if (is_float($value)) {
						$value = (int)$value;
					}
					else {
						if (!is_int($value)) {
							$errors['fila'.$row] = $errors['fila'.$row].'Columna '.$column.', tipo de dato no válido. '; 
							$errors['fila'] = 1; 
						}
					}
					break;
			}
		}
		return $errors;
	}

	/**
	* Retorna true si el parámetro cumple con las restricciones
	* @return boolean 
	*/
	public static function restriccionParametro($parametro_nombre, $parametro_valor = null)
	{
		// Obtener el id del parámetro
		$param_id = DB::table('MEPCO_PARAMETROS_VARIABLE')->select('PAVA_ID')->where('PAVA_NOMBRE', $parametro_nombre)->first();
		// Obtener las restricciones
		$restricciones_param = RestriccionParametros::where('PAVA_ID', $param_id)->first();

		// Comprobar si los parámetros cumplen con las restricciones de rango
		if ($parametro_valor == NULL) {
			$parametro_valor = DB::table('MEPCO_PARAMETROS_VARIABLE')->select('PAVA_VALOR_CALCULO')->where('PAVA_NOMBRE', $parametro_nombre)->first();
		}
			
		if (($parametro_valor<$restricciones_param->REPA_VALOR_MINIMO) || ($parametro_valor>$restricciones_param->REPA_VALOR_MAXIMO)) {
			return false;
		}
		else {
			return true;
		}
	}

	/*
	* Comprueba si el dato es nulo y si lo es introduce el error en el array de errores
	* @return array 
	*/
	public static function compruebaNulos($param, $row, $columna, $errors) {
	    if (is_null($param)) {
	        $errors['fila'.$row] =  $errors['fila'.$row].'Columna '.$columna.', valor nulo. ';
	       	$errors['fila'] = 1; 
	    }
	    return $errors; 
	}

	/*
	* Comprueba si el dato es nulo y si lo es introduce un 0
	* @return array 
	*/
	public static function nulosACero($param) {
	    if (is_null($param)) {
	        $param = 0;
	    }
	    return $param; 
	}

	/*
	* Aplica la fórmula de BRENT corregido a una fila determinada
	* @return array 
	*/
	public static function brentCorregido($brent, $ukc_usg) {
		$brentCorregido = null;
		$formBrentCorregido = \App\Formula::llave('{{BR_BRENT_CORREGIDO}}')->first();
		if(($brent != null)&&($ukc_usg != null))
		{
			try {
				$formBrentCorregidoAux = $formBrentCorregido;
				$formBrentCorregidoAux = str_replace('{{BRENT}}'  , $brent  , $formBrentCorregidoAux);
				$formBrentCorregidoAux = str_replace('{{UKC_USG}}', $ukc_usg, $formBrentCorregidoAux);
				$form = new Form($formBrentCorregidoAux);
				$variables = self::obtenerLlaves($formBrentCorregidoAux);
				foreach ($variables as $item) {
				    $form->setParameter($item, (float)self::obtenerVariablePorLlave($item, null));
				}
				$form->setIsCalculable(true);
				$brentCorregido = $form->render();
			}
			catch(\Exception $e) {
				$brentCorregido = null;
			}
		}else
		{
			$brentCorregido = $brent;
		}
		return $brentCorregido;
	}

	/**
	* Procesa un fichero de variables de mercado o de carga histórica y devuelve un array con los errores encontrados
	* Busca los errores en los tipos de las variables
	* ESta función devuelve un array, donde el primer parámetro es el listado de valores leídos del excel
	* y el segundo es el listado de errores encontrados
	* @return array (array[0] = valores, array[1] = errores, array[2] = warnings)
	*/
	public static function procesarDatos($reader, $tipo_carga, $proc_id = null, $vaus_id = null) {
           	// Inicializar la tabla de errores 
            $errors = array();
            // Inicializar la tabla de valores
            $valores = array();
            // Inicializa la tabla de warnings
            $warnings = array();

            $sheet = $reader->getActiveSheet();
            $highestRow = $sheet->getHighestDataRow();

            // Evitar problemas al cargar variables de mercado con el uso de plantillas 'sucias'
            if ($tipo_carga == self::PROCESAR_VARIABLES_MERCADO) {
            	$highestRow =  8;
            }

            //Valida SQL Rollback
            $errorSQLVM = false;
            $errorFechas = 0;

            //Obtiene formula BRENT CORREGIDO
            $formBrentCorregido = \App\Formula::llave('{{BR_BRENT_CORREGIDO}}')->first();
            if($formBrentCorregido == null)
            {
	            $errors[] = 'Favor revisar formula Brent Corregido llave: {{BR_BRENT_CORREGIDO}}';
            	return array($valores, $errors, $warnings);
            }
            $formBrentCorregido = $formBrentCorregido->PAFO_FORMULA;

            for ($row=4; $row<$highestRow+1; $row++) {
            	// Variable que indica si existió un error que impida insertar la fila.
	    		$errors['fila'] = 0; 
	    		$errors['fila'.$row] = '';

            	// Obtener los valores correspondientes
            	$fecha = $sheet->getCell(self::EXCEL_FECHA.$row)->getCalculatedValue();
            	// La fecha no puede ser nula, ya que se utiliza para indexar los valores
            	if ($fecha == NULL) 
					$errors[$row] = $errors[$row].'Columna '.self::EXCEL_FECHA.'. Fecha nula. ';
                $fecha          = str_replace('/', '-', self::excelToDate($fecha));
                $gas_unl_87     = $sheet->getCell(self::EXCEL_GAS_UNL_87.$row)->getCalculatedValue();
                $gas_unl_93     = $sheet->getCell(self::EXCEL_GAS_UNL_93.$row)->getCalculatedValue();
                $diesel_ls      = $sheet->getCell(self::EXCEL_DIESEL.$row)->getCalculatedValue();
                $usg_butano     = $sheet->getCell(self::EXCEL_USG_BUTANO.$row)->getCalculatedValue();
                $ukc_usg        = $sheet->getCell(self::EXCEL_UKC_USG.$row)->getCalculatedValue();
                $brent          = $sheet->getCell(self::EXCEL_BRENT.$row)->getCalculatedValue();
                $ppt_usg        = $sheet->getCell(self::EXCEL_PPT_USG.$row)->getCalculatedValue();
                $mont_belvieu_1 = $sheet->getCell(self::EXCEL_MONT_BELVIEU_1.$row)->getCalculatedValue();
                $mont_belvieu_2 = $sheet->getCell(self::EXCEL_MONT_BELVIEU_2.$row)->getCalculatedValue();
                $cif_ara_1      = $sheet->getCell(self::EXCEL_CIF_ARA_1.$row)->getCalculatedValue();
                $cif_ara_2      = $sheet->getCell(self::EXCEL_CIF_ARA_2.$row)->getCalculatedValue();
                $wti            = $sheet->getCell(self::EXCEL_WTI.$row)->getCalculatedValue();
                $wti_m1         = $sheet->getCell(self::EXCEL_WTI_M1.$row)->getCalculatedValue();
                $wti_m2         = $sheet->getCell(self::EXCEL_WTI_M2.$row)->getCalculatedValue();
                $wti_m3         = $sheet->getCell(self::EXCEL_WTI_M3.$row)->getCalculatedValue();
                $wti_m4         = $sheet->getCell(self::EXCEL_WTI_M4.$row)->getCalculatedValue();
                $wti_m5         = $sheet->getCell(self::EXCEL_WTI_M5.$row)->getCalculatedValue();
                $wti_m6         = $sheet->getCell(self::EXCEL_WTI_M6.$row)->getCalculatedValue();
                $dolar 		    = $sheet->getCell(self::EXCEL_TC_PUBLICADO.$row)->getCalculatedValue();
                $libor          = $sheet->getCell(self::EXCEL_LIBOR.$row)->getCalculatedValue();
                $tarifa_diaria_82000    = $sheet->getCell(self::EXCEL_TARIFA_DIARIA_82000.$row)->getCalculatedValue();  
                $tarifa_diaria_59000    = $sheet->getCell(self::EXCEL_TARIFA_DIARIA_59000.$row)->getCalculatedValue();
                $ifo_380_houston        = $sheet->getCell(self::EXCEL_IFO_380_HOUSTON.$row)->getCalculatedValue();
                $mdo_houston            = $sheet->getCell(self::EXCEL_MDO_HOUSTON.$row)->getCalculatedValue();
                $ifo_380_cristobal      = $sheet->getCell(self::EXCEL_IFO_380_CRISTOBAL.$row)->getCalculatedValue();
                $mdo_cristobal          = $sheet->getCell(self::EXCEL_MDO_CRISTOBAL.$row)->getCalculatedValue();
                $brent_m1               = $sheet->getCell(self::EXCEL_BRENT_M1.$row)->getCalculatedValue();
                $brent_m2               = $sheet->getCell(self::EXCEL_BRENT_M2.$row)->getCalculatedValue();
                $brent_m3               = $sheet->getCell(self::EXCEL_BRENT_M3.$row)->getCalculatedValue();
                $brent_m4               = $sheet->getCell(self::EXCEL_BRENT_M4.$row)->getCalculatedValue();
                $brent_m5               = $sheet->getCell(self::EXCEL_BRENT_M5.$row)->getCalculatedValue();
                $brent_m6               = $sheet->getCell(self::EXCEL_BRENT_M6.$row)->getCalculatedValue();
                $utm                    = $sheet->getCell(self::EXCEL_UTM.$row)->getCalculatedValue();

                // Comprobaciones básicas del tipo de dato
                $is_date = date_parse($fecha);
                if ($is_date['error_count']!=0)
                	$errors['fila'.$row] = $errors['fila'.$row].'Columna '.self::EXCEL_FECHA.'. Fecha no válida. ';
                $errors = self::compruebaTipo($gas_unl_87, self::IS_FLOAT, $row, self::EXCEL_GAS_UNL_87, $errors); 
                $errors = self::compruebaTipo($gas_unl_93, self::IS_FLOAT, $row, self::EXCEL_GAS_UNL_93, $errors); 
                $errors = self::compruebaTipo($diesel_ls, self::IS_FLOAT, $row, self::EXCEL_DIESEL, $errors); 
                $errors = self::compruebaTipo($usg_butano, self::IS_FLOAT, $row, self::EXCEL_USG_BUTANO, $errors); 
                $errors = self::compruebaTipo($ukc_usg, self::IS_FLOAT, $row, self::EXCEL_UKC_USG, $errors); 
                $errors = self::compruebaTipo($brent, self::IS_FLOAT, $row, self::EXCEL_BRENT, $errors);
                $errors = self::compruebaTipo($ppt_usg, self::IS_FLOAT, $row, self::EXCEL_PPT_USG, $errors);
                $errors = self::compruebaTipo($mont_belvieu_1, self::IS_FLOAT, $row, self::EXCEL_MONT_BELVIEU_1, $errors);
                $errors = self::compruebaTipo($mont_belvieu_2, self::IS_FLOAT, $row, self::EXCEL_MONT_BELVIEU_2, $errors);
                $errors = self::compruebaTipo($cif_ara_1, self::IS_FLOAT, $row, self::EXCEL_CIF_ARA_1, $errors);
                $errors = self::compruebaTipo($cif_ara_2, self::IS_FLOAT, $row, self::EXCEL_CIF_ARA_2, $errors);
                $errors = self::compruebaTipo($wti, self::IS_FLOAT, $row, self::EXCEL_WTI, $errors);
                $errors = self::compruebaTipo($wti_m1, self::IS_FLOAT, $row, self::EXCEL_WTI_M1, $errors);
                $errors = self::compruebaTipo($wti_m2, self::IS_FLOAT, $row, self::EXCEL_WTI_M2, $errors);
                $errors = self::compruebaTipo($wti_m3, self::IS_FLOAT, $row, self::EXCEL_WTI_M3, $errors);
                $errors = self::compruebaTipo($wti_m4, self::IS_FLOAT, $row, self::EXCEL_WTI_M4, $errors);
                $errors = self::compruebaTipo($wti_m5, self::IS_FLOAT, $row, self::EXCEL_WTI_M5, $errors);
                $errors = self::compruebaTipo($wti_m6, self::IS_FLOAT, $row, self::EXCEL_WTI_M6, $errors);
                $errors = self::compruebaTipo($dolar, self::IS_FLOAT, $row, self::EXCEL_TC_PUBLICADO, $errors);
                $errors = self::compruebaTipo($libor, self::IS_FLOAT, $row, self::EXCEL_LIBOR, $errors);
                $errors = self::compruebaTipo($tarifa_diaria_82000, self::IS_FLOAT, $row, self::EXCEL_TARIFA_DIARIA_82000, $errors);
                $errors = self::compruebaTipo($tarifa_diaria_59000, self::IS_FLOAT, $row, self::EXCEL_TARIFA_DIARIA_59000, $errors);
                $errors = self::compruebaTipo($ifo_380_houston, self::IS_FLOAT, $row, self::EXCEL_IFO_380_HOUSTON, $errors);
                $errors = self::compruebaTipo($mdo_houston, self::IS_FLOAT, $row, self::EXCEL_MDO_HOUSTON, $errors);
                $errors = self::compruebaTipo($ifo_380_cristobal, self::IS_FLOAT, $row, self::EXCEL_IFO_380_CRISTOBAL, $errors);
                $errors = self::compruebaTipo($mdo_cristobal, self::IS_FLOAT, $row, self::EXCEL_MDO_CRISTOBAL, $errors);
                $errors = self::compruebaTipo($brent_m1, self::IS_FLOAT, $row, self::EXCEL_BRENT_M1, $errors);
                $errors = self::compruebaTipo($brent_m2, self::IS_FLOAT, $row, self::EXCEL_BRENT_M2, $errors);
                $errors = self::compruebaTipo($brent_m3, self::IS_FLOAT, $row, self::EXCEL_BRENT_M3, $errors);
                $errors = self::compruebaTipo($brent_m4, self::IS_FLOAT, $row, self::EXCEL_BRENT_M4, $errors);
                $errors = self::compruebaTipo($brent_m5, self::IS_FLOAT, $row, self::EXCEL_BRENT_M5, $errors);
                $errors = self::compruebaTipo($brent_m6, self::IS_FLOAT, $row, self::EXCEL_BRENT_M6, $errors);
                $errors = self::compruebaTipo($utm, self::IS_FLOAT, $row, self::EXCEL_UTM, $errors);

                /* Estos valores sólo se rellenan el viernes */ 
                $dia_semana = date('N', strtotime($fecha));
                // Si es el viernes, comprobar que no haya nulos
                if ($dia_semana == 5) {
                	// Comprobar que no haya nulos
                	if ($tarifa_diaria_82000 == NULL) {
                		$tarifa_diaria_82000 = 0;
                	}
                	if ($tarifa_diaria_59000 == NULL) {
                		$tarifa_diaria_59000 = 0;
                	}
                	if ($ifo_380_houston == NULL) {
                		$ifo_380_houston = 0;
                	}
                	if ($mdo_houston == NULL) {
                		$mdo_houston = 0;
                	}
                	if ($ifo_380_cristobal == NULL) {
                		$ifo_380_cristobal = 0;
                	}
                	if ($mdo_cristobal == NULL) {
                		$mdo_cristobal = 0;
                	}
                }

                // Almacenar la fila
                $fila = array();
                $fila[self::EXCEL_FECHA] 			= $fecha;
                $fila[self::EXCEL_GAS_UNL_87] 		= $gas_unl_87;
                $fila[self::EXCEL_GAS_UNL_93] 		= $gas_unl_93;
                $fila[self::EXCEL_DIESEL] 			= $diesel_ls;
                $fila[self::EXCEL_USG_BUTANO] 		= $usg_butano;
                $fila[self::EXCEL_UKC_USG] 			= $ukc_usg;
                $fila[self::EXCEL_BRENT] 			= $brent;
                $fila[self::EXCEL_PPT_USG] 			= $ppt_usg;
                $fila[self::EXCEL_MONT_BELVIEU_1] 	= $mont_belvieu_1;
                $fila[self::EXCEL_MONT_BELVIEU_2] 	= $mont_belvieu_2;
                $fila[self::EXCEL_CIF_ARA_1] 		= $cif_ara_1;
                $fila[self::EXCEL_CIF_ARA_2] 		= $cif_ara_2;
                $fila[self::EXCEL_WTI] 				= $wti;
                $fila[self::EXCEL_WTI_M1] 			= $wti_m1;
                $fila[self::EXCEL_WTI_M2] 			= $wti_m2;
                $fila[self::EXCEL_WTI_M3] 			= $wti_m3;
                $fila[self::EXCEL_WTI_M4] 			= $wti_m4;
                $fila[self::EXCEL_WTI_M5] 			= $wti_m5;
                $fila[self::EXCEL_WTI_M6] 			= $wti_m6;
                $fila[self::EXCEL_TC_PUBLICADO] 	= $dolar;
                $fila[self::EXCEL_LIBOR] 			= $libor;
                $fila[self::EXCEL_TARIFA_DIARIA_82000] 	= $tarifa_diaria_82000;
                $fila[self::EXCEL_TARIFA_DIARIA_59000] 	= $tarifa_diaria_59000;
                $fila[self::EXCEL_IFO_380_HOUSTON] 	 	= $ifo_380_houston;
                $fila[self::EXCEL_MDO_HOUSTON] 			= $mdo_houston;
                $fila[self::EXCEL_IFO_380_CRISTOBAL] 	= $ifo_380_cristobal;
                $fila[self::EXCEL_MDO_CRISTOBAL] 		= $mdo_cristobal;
                $fila[self::EXCEL_BRENT_M1] = $brent_m1;
                $fila[self::EXCEL_BRENT_M2] = $brent_m2;
                $fila[self::EXCEL_BRENT_M3] = $brent_m3;
                $fila[self::EXCEL_BRENT_M4] = $brent_m4;
                $fila[self::EXCEL_BRENT_M5] = $brent_m5;
                $fila[self::EXCEL_BRENT_M6] = $brent_m6;
                $fila[self::EXCEL_UTM] 		= $utm;

                // Si se está procesando la carga histórica se obtienen también n, m, s, t, f
                // Se obtienen también los promedios
               	if ($tipo_carga == self::PROCESAR_CARGA_HISTORICA) {
               		$n_gas_87      = $sheet->getCell(self::EXCEL_N_GAS_87.$row)->getCalculatedValue();
	                $m_gas_87      = $sheet->getCell(self::EXCEL_M_GAS_87.$row)->getCalculatedValue();
	                $s_gas_87      = $sheet->getCell(self::EXCEL_S_GAS_87.$row)->getCalculatedValue();
	                $t_gas_87      = $sheet->getCell(self::EXCEL_T_GAS_87.$row)->getCalculatedValue();
	                $f_gas_87      = $sheet->getCell(self::EXCEL_F_GAS_87.$row)->getCalculatedValue();

	                $n_gas_93 = $sheet->getCell(self::EXCEL_N_GAS_93.$row)->getCalculatedValue();
	                $m_gas_93 = $sheet->getCell(self::EXCEL_M_GAS_93.$row)->getCalculatedValue();
	                $s_gas_93 = $sheet->getCell(self::EXCEL_S_GAS_93.$row)->getCalculatedValue();
	                $t_gas_93 = $sheet->getCell(self::EXCEL_T_GAS_93.$row)->getCalculatedValue();
	                $f_gas_93 = $sheet->getCell(self::EXCEL_F_GAS_93.$row)->getCalculatedValue();

	                $n_diesel = $sheet->getCell(self::EXCEL_N_DIESEL.$row)->getCalculatedValue();
	                $m_diesel = $sheet->getCell(self::EXCEL_M_DIESEL.$row)->getCalculatedValue();
	                $s_diesel = $sheet->getCell(self::EXCEL_S_DIESEL.$row)->getCalculatedValue();
	                $t_diesel = $sheet->getCell(self::EXCEL_T_DIESEL.$row)->getCalculatedValue();
	                $f_diesel = $sheet->getCell(self::EXCEL_F_DIESEL.$row)->getCalculatedValue();

	                $n_glp = $sheet->getCell(self::EXCEL_N_GLP.$row)->getCalculatedValue();
	                $m_glp = $sheet->getCell(self::EXCEL_M_GLP.$row)->getCalculatedValue();
	                $s_glp = $sheet->getCell(self::EXCEL_S_GLP.$row)->getCalculatedValue();
	                $t_glp = $sheet->getCell(self::EXCEL_T_GLP.$row)->getCalculatedValue();
	                $f_glp = $sheet->getCell(self::EXCEL_F_GLP.$row)->getCalculatedValue(); 


	                $fila[self::EXCEL_CORRECCION_RVP] 	= $sheet->getCell(self::EXCEL_CORRECCION_RVP.$row)->getCalculatedValue();
					$fila[self::EXCEL_PROM_PARIDAD_SEMANAL_GAS_87] 	= $sheet->getCell(self::EXCEL_PROM_PARIDAD_SEMANAL_GAS_87.$row)->getCalculatedValue();
					$fila[self::EXCEL_PROM_PARIDAD_SEMANAL_GAS_93] 	= $sheet->getCell(self::EXCEL_PROM_PARIDAD_SEMANAL_GAS_93.$row)->getCalculatedValue();
					$fila[self::EXCEL_PROM_PARIDAD_SEMANAL_DIESEL] 	= $sheet->getCell(self::EXCEL_PROM_PARIDAD_SEMANAL_DIESEL.$row)->getCalculatedValue();
					$fila[self::EXCEL_PROM_PARIDAD_SEMANAL_GLP]		= $sheet->getCell(self::EXCEL_PROM_PARIDAD_SEMANAL_GLP.$row)->getCalculatedValue();
					$fila[self::EXCEL_PROM_BRENT_SEMANAL] 			= $sheet->getCell(self::EXCEL_PROM_BRENT_SEMANAL.$row)->getCalculatedValue();
					$fila[self::EXCEL_PARIDAD_GAS_87] 			= $sheet->getCell(self::EXCEL_PARIDAD_GAS_87.$row)->getCalculatedValue();
					$fila[self::EXCEL_PARIDAD_GAS_93] 			= $sheet->getCell(self::EXCEL_PARIDAD_GAS_93.$row)->getCalculatedValue();
					$fila[self::EXCEL_PARIDAD_DIESEL] 			= $sheet->getCell(self::EXCEL_PARIDAD_DIESEL.$row)->getCalculatedValue();
					$fila[self::EXCEL_PARIDAD_GLP] 			= $sheet->getCell(self::EXCEL_PARIDAD_GLP.$row)->getCalculatedValue();
					$fila[self::EXCEL_BRENT_PROM_HISTORICO_GAS_87] 	= $sheet->getCell(self::EXCEL_BRENT_PROM_HISTORICO_GAS_87.$row)->getCalculatedValue();
					$fila[self::EXCEL_BRENT_PROM_HISTORICO_GAS_93] 	= $sheet->getCell(self::EXCEL_BRENT_PROM_HISTORICO_GAS_93.$row)->getCalculatedValue();
					$fila[self::EXCEL_BRENT_PROM_HISTORICO_DIESEL] 	= $sheet->getCell(self::EXCEL_BRENT_PROM_HISTORICO_DIESEL.$row)->getCalculatedValue();
					$fila[self::EXCEL_BRENT_PROM_HISTORICO_GLP] 	= $sheet->getCell(self::EXCEL_BRENT_PROM_HISTORICO_GLP.$row)->getCalculatedValue();
					$fila[self::EXCEL_BRENT_PROM_FUTUROS_GAS_87] 	= $sheet->getCell(self::EXCEL_BRENT_PROM_FUTUROS_GAS_87.$row)->getCalculatedValue();
					$fila[self::EXCEL_BRENT_PROM_FUTUROS_GAS_93] 	= $sheet->getCell(self::EXCEL_BRENT_PROM_FUTUROS_GAS_93.$row)->getCalculatedValue();
					$fila[self::EXCEL_BRENT_PROM_FUTUROS_DIESEL] 	= $sheet->getCell(self::EXCEL_BRENT_PROM_FUTUROS_DIESEL.$row)->getCalculatedValue();
					$fila[self::EXCEL_BRENT_PROM_FUTUROS_GLP] 		= $sheet->getCell(self::EXCEL_BRENT_PROM_FUTUROS_GLP.$row)->getCalculatedValue();
					$fila[self::EXCEL_BRENT_PROM_PONDERADO_GAS_87] 	= $sheet->getCell(self::EXCEL_BRENT_PROM_PONDERADO_GAS_87.$row)->getCalculatedValue();
					$fila[self::EXCEL_BRENT_PROM_PONDERADO_GAS_93] 	= $sheet->getCell(self::EXCEL_BRENT_PROM_PONDERADO_GAS_93.$row)->getCalculatedValue();
					$fila[self::EXCEL_BRENT_PROM_PONDERADO_DIESEL] 	= $sheet->getCell(self::EXCEL_BRENT_PROM_PONDERADO_DIESEL.$row)->getCalculatedValue();
					$fila[self::EXCEL_BRENT_PROM_PONDERADO_GLP] 	= $sheet->getCell(self::EXCEL_BRENT_PROM_PONDERADO_GLP.$row)->getCalculatedValue();
					$fila[self::EXCEL_PROM_WTI] 					= $sheet->getCell(self::EXCEL_PROM_WTI.$row)->getCalculatedValue();
					$fila[self::EXCEL_WTI_PROM_HISTORICO_GAS_87] 	= $sheet->getCell(self::EXCEL_WTI_PROM_HISTORICO_GAS_87.$row)->getCalculatedValue();
					$fila[self::EXCEL_WTI_PROM_HISTORICO_GAS_93] 	= $sheet->getCell(self::EXCEL_WTI_PROM_HISTORICO_GAS_93.$row)->getCalculatedValue();
					$fila[self::EXCEL_WTI_PROM_HISTORICO_DIESEL]	= $sheet->getCell(self::EXCEL_WTI_PROM_HISTORICO_DIESEL.$row)->getCalculatedValue();
					$fila[self::EXCEL_WTI_PROM_HISTORICO_GLP] 		= $sheet->getCell(self::EXCEL_WTI_PROM_HISTORICO_GLP.$row)->getCalculatedValue();
					$fila[self::EXCEL_WTI_PROM_FUTUROS_GAS_87] 		= $sheet->getCell(self::EXCEL_WTI_PROM_FUTUROS_GAS_87.$row)->getCalculatedValue();
					$fila[self::EXCEL_WTI_PROM_FUTUROS_GAS_93] 		= $sheet->getCell(self::EXCEL_WTI_PROM_FUTUROS_GAS_93.$row)->getCalculatedValue();
					$fila[self::EXCEL_WTI_PROM_FUTUROS_DIESEL] 		= $sheet->getCell(self::EXCEL_WTI_PROM_FUTUROS_DIESEL.$row)->getCalculatedValue();
					$fila[self::EXCEL_WTI_PROM_FUTUROS_GLP] 		= $sheet->getCell(self::EXCEL_WTI_PROM_FUTUROS_GLP.$row)->getCalculatedValue();
					$fila[self::EXCEL_WTI_PROM_PONDERADO_GAS_87] 	= $sheet->getCell(self::EXCEL_WTI_PROM_PONDERADO_GAS_87.$row)->getCalculatedValue();
					$fila[self::EXCEL_WTI_PROM_PONDERADO_GAS_93] 	= $sheet->getCell(self::EXCEL_WTI_PROM_PONDERADO_GAS_93.$row)->getCalculatedValue();
					$fila[self::EXCEL_WTI_PROM_PONDERADO_DIESEL] 	= $sheet->getCell(self::EXCEL_WTI_PROM_PONDERADO_DIESEL.$row)->getCalculatedValue();
					$fila[self::EXCEL_WTI_PROM_PONDERADO_GLP] 		= $sheet->getCell(self::EXCEL_WTI_PROM_PONDERADO_GLP.$row)->getCalculatedValue();
					$fila[self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_GAS_87] = $sheet->getCell(self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_GAS_87.$row)->getCalculatedValue();
					$fila[self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_GAS_93] = $sheet->getCell(self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_GAS_93.$row)->getCalculatedValue();
					$fila[self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_DIESEL] = $sheet->getCell(self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_DIESEL.$row)->getCalculatedValue();
					$fila[self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_GLP] = $sheet->getCell(self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_GLP.$row)->getCalculatedValue();
					$fila[self::EXCEL_PROM_MARGEN_SEMANAL_WTI_GAS_87] = $sheet->getCell(self::EXCEL_PROM_MARGEN_SEMANAL_WTI_GAS_87.$row)->getCalculatedValue();
					$fila[self::EXCEL_PROM_MARGEN_SEMANAL_WTI_GAS_93] = $sheet->getCell(self::EXCEL_PROM_MARGEN_SEMANAL_WTI_GAS_93.$row)->getCalculatedValue();
					$fila[self::EXCEL_PROM_MARGEN_SEMANAL_WTI_DIESEL] = $sheet->getCell(self::EXCEL_PROM_MARGEN_SEMANAL_WTI_DIESEL.$row)->getCalculatedValue();
					$fila[self::EXCEL_PROM_MARGEN_SEMANAL_WTI_GLP] = $sheet->getCell(self::EXCEL_PROM_MARGEN_SEMANAL_WTI_GLP.$row)->getCalculatedValue();
					$fila[self::EXCEL_MARGEN_PROMEDIO_GAS_87]		= $sheet->getCell(self::EXCEL_MARGEN_PROMEDIO_GAS_87.$row)->getCalculatedValue();
					$fila[self::EXCEL_MARGEN_PROMEDIO_GAS_93] 		= $sheet->getCell(self::EXCEL_MARGEN_PROMEDIO_GAS_93.$row)->getCalculatedValue();
					$fila[self::EXCEL_MARGEN_PROMEDIO_DIESEL] 		= $sheet->getCell(self::EXCEL_MARGEN_PROMEDIO_DIESEL.$row)->getCalculatedValue();
					$fila[self::EXCEL_MARGEN_PROMEDIO_GLP] 			= $sheet->getCell(self::EXCEL_MARGEN_PROMEDIO_GLP.$row)->getCalculatedValue();
					$fila[self::EXCEL_TIPO_CRUDO] 					= $sheet->getCell(self::EXCEL_TIPO_CRUDO.$row)->getCalculatedValue();
					$fila[self::EXCEL_FOB_TEORICO_GAS_87] 			= $sheet->getCell(self::EXCEL_FOB_TEORICO_GAS_87.$row)->getCalculatedValue();
					$fila[self::EXCEL_FOB_TEORICO_GAS_93] 			= $sheet->getCell(self::EXCEL_FOB_TEORICO_GAS_93.$row)->getCalculatedValue();
					$fila[self::EXCEL_FOB_TEORICO_DIESEL]			= $sheet->getCell(self::EXCEL_FOB_TEORICO_DIESEL.$row)->getCalculatedValue();
					$fila[self::EXCEL_FOB_TEORICO_GLP] 				= $sheet->getCell(self::EXCEL_FOB_TEORICO_GLP.$row)->getCalculatedValue();
					$fila[self::EXCEL_PROM_REFERENCIA_SEMANAL_GAS_87] = $sheet->getCell(self::EXCEL_PROM_REFERENCIA_SEMANAL_GAS_87.$row)->getCalculatedValue();
					$fila[self::EXCEL_PROM_REFERENCIA_SEMANAL_GAS_93] = $sheet->getCell(self::EXCEL_PROM_REFERENCIA_SEMANAL_GAS_93.$row)->getCalculatedValue();
					$fila[self::EXCEL_PROM_REFERENCIA_SEMANAL_DIESEL] = $sheet->getCell(self::EXCEL_PROM_REFERENCIA_SEMANAL_DIESEL.$row)->getCalculatedValue();
					$fila[self::EXCEL_PROM_REFERENCIA_SEMANAL_GLP] 	  = $sheet->getCell(self::EXCEL_PROM_REFERENCIA_SEMANAL_GLP.$row)->getCalculatedValue();

					// Poner nulos a cero
					$fila[self::EXCEL_CORRECCION_RVP] 				= self::nulosACero($fila[self::EXCEL_CORRECCION_RVP]);
					$fila[self::EXCEL_PROM_PARIDAD_SEMANAL_GAS_87] 	= self::nulosACero($fila[self::EXCEL_PROM_PARIDAD_SEMANAL_GAS_87]);
					$fila[self::EXCEL_PROM_PARIDAD_SEMANAL_GAS_93] 	= self::nulosACero($fila[self::EXCEL_PROM_PARIDAD_SEMANAL_GAS_93]);
					$fila[self::EXCEL_PROM_PARIDAD_SEMANAL_DIESEL] 	= self::nulosACero($fila[self::EXCEL_PROM_PARIDAD_SEMANAL_DIESEL]);
					$fila[self::EXCEL_PROM_PARIDAD_SEMANAL_GLP]		= self::nulosACero($fila[self::EXCEL_PROM_PARIDAD_SEMANAL_GLP]);
					$fila[self::EXCEL_PARIDAD_GAS_87]				= self::nulosACero($fila[self::EXCEL_PARIDAD_GAS_87]);
					$fila[self::EXCEL_PARIDAD_GAS_93]				= self::nulosACero($fila[self::EXCEL_PARIDAD_GAS_93]);
					$fila[self::EXCEL_PARIDAD_DIESEL]				= self::nulosACero($fila[self::EXCEL_PARIDAD_DIESEL]);
					$fila[self::EXCEL_PARIDAD_GLP]					= self::nulosACero($fila[self::EXCEL_PARIDAD_GLP]);
					$fila[self::EXCEL_PROM_BRENT_SEMANAL] 			= self::nulosACero($fila[self::EXCEL_PROM_BRENT_SEMANAL]);
					$fila[self::EXCEL_BRENT_PROM_HISTORICO_GAS_87] 	= self::nulosACero($fila[self::EXCEL_BRENT_PROM_HISTORICO_GAS_87]);
					$fila[self::EXCEL_BRENT_PROM_HISTORICO_GAS_93] 	= self::nulosACero($fila[self::EXCEL_BRENT_PROM_HISTORICO_GAS_93]);
					$fila[self::EXCEL_BRENT_PROM_HISTORICO_DIESEL] 	= self::nulosACero($fila[self::EXCEL_BRENT_PROM_HISTORICO_DIESEL]);
					$fila[self::EXCEL_BRENT_PROM_HISTORICO_GLP] 	= self::nulosACero($fila[self::EXCEL_BRENT_PROM_HISTORICO_GLP]);
					$fila[self::EXCEL_BRENT_PROM_FUTUROS_GAS_87] 	= self::nulosACero($fila[self::EXCEL_BRENT_PROM_FUTUROS_GAS_87]);
					$fila[self::EXCEL_BRENT_PROM_FUTUROS_GAS_93] 	= self::nulosACero($fila[self::EXCEL_BRENT_PROM_FUTUROS_GAS_93]);
					$fila[self::EXCEL_BRENT_PROM_FUTUROS_DIESEL] 	= self::nulosACero($fila[self::EXCEL_BRENT_PROM_FUTUROS_DIESEL]);
					$fila[self::EXCEL_BRENT_PROM_FUTUROS_GLP] 		= self::nulosACero($fila[self::EXCEL_BRENT_PROM_FUTUROS_GLP]);
					$fila[self::EXCEL_BRENT_PROM_PONDERADO_GAS_87] 	= self::nulosACero($fila[self::EXCEL_BRENT_PROM_PONDERADO_GAS_87]);
					$fila[self::EXCEL_BRENT_PROM_PONDERADO_GAS_93] 	= self::nulosACero($fila[self::EXCEL_BRENT_PROM_PONDERADO_GAS_93]);
					$fila[self::EXCEL_BRENT_PROM_PONDERADO_DIESEL] 	= self::nulosACero($fila[self::EXCEL_BRENT_PROM_PONDERADO_DIESEL]);
					$fila[self::EXCEL_BRENT_PROM_PONDERADO_GLP] 	= self::nulosACero($fila[self::EXCEL_BRENT_PROM_PONDERADO_GLP]);
					$fila[self::EXCEL_PROM_WTI] 					= self::nulosACero($fila[self::EXCEL_PROM_WTI]);
					$fila[self::EXCEL_WTI_PROM_HISTORICO_GAS_87] 	= self::nulosACero($fila[self::EXCEL_WTI_PROM_HISTORICO_GAS_87]);
					$fila[self::EXCEL_WTI_PROM_HISTORICO_GAS_93] 	= self::nulosACero($fila[self::EXCEL_WTI_PROM_HISTORICO_GAS_93]);
					$fila[self::EXCEL_WTI_PROM_HISTORICO_DIESEL]	= self::nulosACero($fila[self::EXCEL_WTI_PROM_HISTORICO_DIESEL]);
					$fila[self::EXCEL_WTI_PROM_HISTORICO_GLP] 		= self::nulosACero($fila[self::EXCEL_WTI_PROM_HISTORICO_GLP]);
					$fila[self::EXCEL_WTI_PROM_FUTUROS_GAS_87] 		= self::nulosACero($fila[self::EXCEL_WTI_PROM_FUTUROS_GAS_87]);
					$fila[self::EXCEL_WTI_PROM_FUTUROS_GAS_93] 		= self::nulosACero($fila[self::EXCEL_WTI_PROM_FUTUROS_GAS_93]);
					$fila[self::EXCEL_WTI_PROM_FUTUROS_DIESEL] 		= self::nulosACero($fila[self::EXCEL_WTI_PROM_FUTUROS_DIESEL]);
					$fila[self::EXCEL_WTI_PROM_FUTUROS_GLP] 		= self::nulosACero($fila[self::EXCEL_WTI_PROM_FUTUROS_GLP]);
					$fila[self::EXCEL_WTI_PROM_PONDERADO_GAS_87] 	= self::nulosACero($fila[self::EXCEL_WTI_PROM_PONDERADO_GAS_87]);
					$fila[self::EXCEL_WTI_PROM_PONDERADO_GAS_93] 	= self::nulosACero($fila[self::EXCEL_WTI_PROM_PONDERADO_GAS_93]);
					$fila[self::EXCEL_WTI_PROM_PONDERADO_DIESEL] 	= self::nulosACero($fila[self::EXCEL_WTI_PROM_PONDERADO_DIESEL]);
					$fila[self::EXCEL_WTI_PROM_PONDERADO_GLP] 		= self::nulosACero($fila[self::EXCEL_WTI_PROM_PONDERADO_GLP]);
					$fila[self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_GAS_87] 	= self::nulosACero($fila[self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_GAS_87]);
					$fila[self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_GAS_93] 	= self::nulosACero($fila[self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_GAS_93]);
					$fila[self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_DIESEL] 	= self::nulosACero($fila[self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_DIESEL]);
					$fila[self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_GLP] 		= self::nulosACero($fila[self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_GLP]);
					$fila[self::EXCEL_PROM_MARGEN_SEMANAL_WTI_GAS_87]		= self::nulosACero($fila[self::EXCEL_PROM_MARGEN_SEMANAL_WTI_GAS_87]);
					$fila[self::EXCEL_PROM_MARGEN_SEMANAL_WTI_GAS_93]		= self::nulosACero($fila[self::EXCEL_PROM_MARGEN_SEMANAL_WTI_GAS_93]);
					$fila[self::EXCEL_PROM_MARGEN_SEMANAL_WTI_DIESEL]		= self::nulosACero($fila[self::EXCEL_PROM_MARGEN_SEMANAL_WTI_DIESEL]);
					$fila[self::EXCEL_PROM_MARGEN_SEMANAL_WTI_GLP]		= self::nulosACero($fila[self::EXCEL_PROM_MARGEN_SEMANAL_WTI_GLP]);
					$fila[self::EXCEL_MARGEN_PROMEDIO_GAS_87]		= self::nulosACero($fila[self::EXCEL_MARGEN_PROMEDIO_GAS_87]);
					$fila[self::EXCEL_MARGEN_PROMEDIO_GAS_93] 		= self::nulosACero($fila[self::EXCEL_MARGEN_PROMEDIO_GAS_93]);
					$fila[self::EXCEL_MARGEN_PROMEDIO_DIESEL] 		= self::nulosACero($fila[self::EXCEL_MARGEN_PROMEDIO_DIESEL]);
					$fila[self::EXCEL_MARGEN_PROMEDIO_GLP] 			= self::nulosACero($fila[self::EXCEL_MARGEN_PROMEDIO_GLP]);
					$fila[self::EXCEL_TIPO_CRUDO] 					= self::nulosACero($fila[self::EXCEL_TIPO_CRUDO]);
					$fila[self::EXCEL_FOB_TEORICO_GAS_87] 			= self::nulosACero($fila[self::EXCEL_FOB_TEORICO_GAS_87]);
					$fila[self::EXCEL_FOB_TEORICO_GAS_93] 			= self::nulosACero($fila[self::EXCEL_FOB_TEORICO_GAS_93]);
					$fila[self::EXCEL_FOB_TEORICO_DIESEL]			= self::nulosACero($fila[self::EXCEL_FOB_TEORICO_DIESEL]);
					$fila[self::EXCEL_FOB_TEORICO_GLP] 				= self::nulosACero($fila[self::EXCEL_FOB_TEORICO_GLP]);
					$fila[self::EXCEL_PROM_REFERENCIA_SEMANAL_GAS_87] = self::nulosACero($fila[self::EXCEL_PROM_REFERENCIA_SEMANAL_GAS_87]);
					$fila[self::EXCEL_PROM_REFERENCIA_SEMANAL_GAS_93] = self::nulosACero($fila[self::EXCEL_PROM_REFERENCIA_SEMANAL_GAS_93]);
					$fila[self::EXCEL_PROM_REFERENCIA_SEMANAL_DIESEL] = self::nulosACero($fila[self::EXCEL_PROM_REFERENCIA_SEMANAL_DIESEL]);
					$fila[self::EXCEL_PROM_REFERENCIA_SEMANAL_GLP] 	  = self::nulosACero($fila[self::EXCEL_PROM_REFERENCIA_SEMANAL_GLP]);

	                $fila[self::EXCEL_N_GAS_87] = $n_gas_87;
	                $fila[self::EXCEL_M_GAS_87] = $m_gas_87;
	                $fila[self::EXCEL_S_GAS_87] = $s_gas_87;
	                $fila[self::EXCEL_F_GAS_87] = $f_gas_87;
	                $fila[self::EXCEL_T_GAS_87] = $t_gas_87;

	                $fila[self::EXCEL_N_GAS_93] = $n_gas_93;
	                $fila[self::EXCEL_M_GAS_93] = $m_gas_93;
	                $fila[self::EXCEL_S_GAS_93] = $s_gas_93;
	                $fila[self::EXCEL_F_GAS_93] = $f_gas_93;
	                $fila[self::EXCEL_T_GAS_93] = $t_gas_93;

	                $fila[self::EXCEL_N_DIESEL] = $n_diesel;
	                $fila[self::EXCEL_M_DIESEL] = $m_diesel;
	                $fila[self::EXCEL_S_DIESEL] = $s_diesel;
	                $fila[self::EXCEL_F_DIESEL] = $f_diesel;
	                $fila[self::EXCEL_T_DIESEL] = $t_diesel;

	                $fila[self::EXCEL_N_GLP] = $n_glp;
	                $fila[self::EXCEL_M_GLP] = $m_glp;
	                $fila[self::EXCEL_S_GLP] = $s_glp;
	                $fila[self::EXCEL_F_GLP] = $f_glp;
	                $fila[self::EXCEL_T_GLP] = $t_glp;

	                // Comprueba nulos
	                $errors = self::compruebaNulos($n_gas_87, 	$row, self::EXCEL_N_GAS_87, $errors);
	                $errors = self::compruebaNulos($n_gas_93, 	$row, self::EXCEL_N_GAS_93, $errors);
	                $errors = self::compruebaNulos($n_diesel, 	$row, self::EXCEL_N_DIESEL, $errors);
	                $errors = self::compruebaNulos($n_glp, 		$row, self::EXCEL_N_GLP, 	$errors);

	                $errors = self::compruebaNulos($m_gas_87, 	$row, self::EXCEL_M_GAS_87, $errors);
	                $errors = self::compruebaNulos($m_gas_93, 	$row, self::EXCEL_M_GAS_93, $errors);
	                $errors = self::compruebaNulos($m_diesel, 	$row, self::EXCEL_M_DIESEL, $errors);
	                $errors = self::compruebaNulos($m_glp, 		$row, self::EXCEL_M_GLP, 	$errors);

	                $errors = self::compruebaNulos($s_gas_87, 	$row, self::EXCEL_S_GAS_87, $errors);
	                $errors = self::compruebaNulos($s_gas_93, 	$row, self::EXCEL_S_GAS_93, $errors);
	                $errors = self::compruebaNulos($s_diesel, 	$row, self::EXCEL_S_DIESEL, $errors);
	                $errors = self::compruebaNulos($s_glp, 		$row, self::EXCEL_S_GLP, 	$errors);

	                $errors = self::compruebaNulos($t_gas_87, 	$row, self::EXCEL_T_GAS_87, $errors);
	                $errors = self::compruebaNulos($t_gas_93, 	$row, self::EXCEL_T_GAS_93, $errors);
	                $errors = self::compruebaNulos($t_diesel, 	$row, self::EXCEL_T_DIESEL, $errors);
	                $errors = self::compruebaNulos($t_glp, 		$row, self::EXCEL_T_GLP, 	$errors);

	                $errors = self::compruebaNulos($f_gas_87, 	$row, self::EXCEL_F_GAS_87, $errors);
	                $errors = self::compruebaNulos($f_gas_93, 	$row, self::EXCEL_F_GAS_93, $errors);
	                $errors = self::compruebaNulos($f_diesel, 	$row, self::EXCEL_F_DIESEL, $errors);
	                $errors = self::compruebaNulos($f_glp, 		$row, self::EXCEL_F_GLP, 	$errors);

	                // Comprobar tipos
                	$errors = self::compruebaTipo($n_gas_87, self::IS_FLOAT, $row, self::EXCEL_N_GAS_87, $errors);
                	$errors = self::compruebaTipo($m_gas_87, self::IS_FLOAT, $row, self::EXCEL_M_GAS_87, $errors);
                	$errors = self::compruebaTipo($s_gas_87, self::IS_FLOAT, $row, self::EXCEL_S_GAS_87, $errors);
                	$errors = self::compruebaTipo($t_gas_87, self::IS_FLOAT, $row, self::EXCEL_T_GAS_87, $errors);
                	$errors = self::compruebaTipo($f_gas_87, self::IS_FLOAT, $row, self::EXCEL_F_GAS_87, $errors);

                	$errors = self::compruebaTipo($n_gas_93, self::IS_FLOAT, $row, self::EXCEL_N_GAS_93, $errors);
                	$errors = self::compruebaTipo($m_gas_93, self::IS_FLOAT, $row, self::EXCEL_M_GAS_93, $errors);
                	$errors = self::compruebaTipo($s_gas_93, self::IS_FLOAT, $row, self::EXCEL_S_GAS_93, $errors);
                	$errors = self::compruebaTipo($t_gas_93, self::IS_FLOAT, $row, self::EXCEL_T_GAS_93, $errors);
                	$errors = self::compruebaTipo($f_gas_93, self::IS_FLOAT, $row, self::EXCEL_F_GAS_93, $errors);

                	$errors = self::compruebaTipo($n_diesel, self::IS_FLOAT, $row, self::EXCEL_N_DIESEL, $errors);
                	$errors = self::compruebaTipo($m_diesel, self::IS_FLOAT, $row, self::EXCEL_M_DIESEL, $errors);
                	$errors = self::compruebaTipo($s_diesel, self::IS_FLOAT, $row, self::EXCEL_S_DIESEL, $errors);
                	$errors = self::compruebaTipo($t_diesel, self::IS_FLOAT, $row, self::EXCEL_T_DIESEL, $errors);
                	$errors = self::compruebaTipo($f_diesel, self::IS_FLOAT, $row, self::EXCEL_F_DIESEL, $errors);

                	$errors = self::compruebaTipo($n_glp, self::IS_FLOAT, $row, self::EXCEL_N_GLP, $errors);
                	$errors = self::compruebaTipo($m_glp, self::IS_FLOAT, $row, self::EXCEL_M_GLP, $errors);
                	$errors = self::compruebaTipo($s_glp, self::IS_FLOAT, $row, self::EXCEL_S_GLP, $errors);
                	$errors = self::compruebaTipo($t_glp, self::IS_FLOAT, $row, self::EXCEL_T_GLP, $errors);
                	$errors = self::compruebaTipo($f_glp, self::IS_FLOAT, $row, self::EXCEL_F_GLP, $errors);
	            }

		        /*
		        * INSERTAR DATOS
		        */

				/*
				* Obtener promedios para los campos que requieren promedio
				*/ 
				$mont_belvieu_1 = $fila[self::EXCEL_MONT_BELVIEU_1];
				$mont_belvieu_2 = $fila[self::EXCEL_MONT_BELVIEU_2];
				$mont_belvieu = ($mont_belvieu_1+$mont_belvieu_2)/2;

				$cif_ara_1 = $fila[self::EXCEL_CIF_ARA_1];
				$cif_ara_2 = $fila[self::EXCEL_CIF_ARA_2];
				$cif_ara = ($cif_ara_1+$cif_ara_2)/2;

				/*
				* Calcula BRENT Corregido
				*/
				if(($fila[self::EXCEL_BRENT] != null)&&($fila[self::EXCEL_UKC_USG] != null))
				{
					try {
						$formBrentCorregidoAux = $formBrentCorregido;
						$formBrentCorregidoAux = str_replace('{{BRENT}}'  , $fila[self::EXCEL_BRENT]  , $formBrentCorregidoAux);
						$formBrentCorregidoAux = str_replace('{{UKC_USG}}', $fila[self::EXCEL_UKC_USG], $formBrentCorregidoAux);
						$form = new Form($formBrentCorregidoAux);
						$variables = self::obtenerLlaves($formBrentCorregidoAux);
						foreach ($variables as $item) {
						    $form->setParameter($item, (float)self::obtenerVariablePorLlave($item, null));
						}
						$form->setIsCalculable(true);
						$brentCorregido = $form->render();
					}
					catch(\Exception $e) {}
				}else
				{
					$brentCorregido = $fila[self::EXCEL_BRENT];
				}

				/*
				* Comprobar valores nulos en aquellos campos donde hay que tomar el inmediatamente 
				* anterior que tenga un valor
				*/ 

				// LIBOR
				$libor = $fila[self::EXCEL_LIBOR];
				if ($libor == NULL) {
					if ($tipo_carga == self::PROCESAR_VARIABLES_MERCADO) {
						// Si cargamos valores de mercado, primero miramos en la tabla 
						// de valores de mercado
						$libor = DB::table('MEPCO_VALOR_MERCADO')->select('VAME_LIBOR')->whereIn('VAME_FECHA', function($query) use (&$fila){
							$query->select(DB::raw('MAX(VAME_FECHA)'))
								  ->from('MEPCO_VALOR_MERCADO')
								  ->whereRaw("VAME_FECHA < '".date('Y-m-d', strtotime($fila[self::EXCEL_FECHA]))."'")
								  ->whereNotNull('VAME_LIBOR');
						})->first();
						if ($libor) $libor = $libor->VAME_LIBOR;
					}
					if ($libor == NULL) {
						// Si ahí no se ha encontrado nada, o es carga histórica, buscar en DATOS_HISTORICOS
						$libor = DB::table('MEPCO_DATOS_HISTORICOS')->select('DAHI_LIBOR')->whereIn('DAHI_FECHA', function($query) use (&$fila){
							$query->select(DB::raw('MAX(DAHI_FECHA)'))
								  ->from('MEPCO_DATOS_HISTORICOS')
								  ->whereRaw("DAHI_FECHA < '".date('Y-m-d', strtotime($fila[self::EXCEL_FECHA]))."'")
								  ->whereNotNull('DAHI_LIBOR');
						})->first();
						if ($libor) $libor = $libor->DAHI_LIBOR;
					}
					// Si aún así no tenemos resultado, error en la fila
					if($libor == NULL) {
						$libor = 0;
						// array_push($warnings, 'Fila '.$row.'. No existe información correspondiente al libor para la fecha '.$fila[self::EXCEL_FECHA]);
					}
				}

				// DOLAR
				$tc_publicado = $fila[self::EXCEL_TC_PUBLICADO];
				if ($tc_publicado == NULL) {
					if ($tipo_carga == self::PROCESAR_VARIABLES_MERCADO) {
						// Si cargamos valores de mercado, primero miramos en la tabla 
						// de valores de mercado
						$tc_publicado = DB::table('MEPCO_VALOR_MERCADO AS VM')
						->select('VAME_TC_DOLAR')
						->join('MEPCO_VAME_USPE AS VU', 'VU.VAUS_ID', '=', 'VM.VAUS_ID')
						->where('VU.VAUS_ELIMINADO', 0)
						->whereIn('VM.VAME_FECHA', function($query) use (&$fila){
							$query->select(DB::raw('MAX(VM.VAME_FECHA)'))
								  ->from('MEPCO_VALOR_MERCADO AS VM')
								  ->join('MEPCO_VAME_USPE AS VU', 'VU.VAUS_ID', '=', 'VM.VAUS_ID')
								  ->where('VU.VAUS_ELIMINADO', 0)
								  ->whereRaw("VM.VAME_FECHA < '".date('Y-m-d', strtotime($fila[self::EXCEL_FECHA]))."'")
								  ->whereNotNull('VM.VAME_TC_DOLAR');
						})->first();
						if ($tc_publicado) $tc_publicado = $tc_publicado->VAME_TC_DOLAR;
					}
					if ($tc_publicado == NULL) {
						// Si ahí no se ha encontrado nada, o es carga histórica, buscar en DATOS_HISTORICOS
						$tc_publicado = DB::table('MEPCO_DATOS_HISTORICOS')->select('DAHI_TC_DOLAR')->whereIn('DAHI_FECHA', function($query) use (&$fila){
							$query->select(DB::raw('MAX(DAHI_FECHA)'))
								  ->from('MEPCO_DATOS_HISTORICOS')
								  ->whereRaw("DAHI_FECHA < '".date('Y-m-d', strtotime($fila[self::EXCEL_FECHA]))."'")
								  ->whereNotNull('DAHI_TC_DOLAR');
						})->first();
						if ($tc_publicado) $tc_publicado = $tc_publicado->DAHI_TC_DOLAR;
					}
					// Si aún así no tenemos resultado, error en la fila
					if($tc_publicado == NULL) {
						$tc_publicado = 0;
						// array_push($warnings, 'Fila '.$row.'. No existe información correspondiente al tc publicado para la fecha '.$fila[self::EXCEL_FECHA]);
					}
				}

				// UTM
				$utm = $fila[self::EXCEL_UTM];
				$mes = date('n', strtotime($fila[self::EXCEL_FECHA]));
				$year = date('Y', strtotime($fila[self::EXCEL_FECHA]));

				if ($utm == NULL) {
					if ($tipo_carga == self::PROCESAR_VARIABLES_MERCADO) {
						// Si cargamos valores de mercado, primero miramos en la tabla 
						// de valores de mercado
						$utm = DB::table('MEPCO_VALOR_MERCADO')->select('VAME_UTM')->whereIn('VAME_FECHA', function($query) use (&$mes, &$year){
							$query->select(DB::raw('MAX(VAME_FECHA)'))
								  ->from('MEPCO_VALOR_MERCADO')
								  ->whereRaw("MONTH(VAME_FECHA) = ".$mes)
								  ->whereRaw("YEAR(VAME_FECHA) = ".$year)
								  ->whereNotNull('VAME_UTM');
						})->first();
						if ($utm) $utm = $utm->VAME_UTM;
					}
					if ($utm == NULL) {
						// Si ahí no se ha encontrado nada, o es carga histórica, buscar en DATOS_HISTORICOS
						$utm = DB::table('MEPCO_DATOS_HISTORICOS')->select('DAHI_UTM')->whereIn('DAHI_FECHA', function($query) use (&$mes, &$year){
							$query->select(DB::raw('MAX(DAHI_FECHA)'))
								  ->from('MEPCO_DATOS_HISTORICOS')
								  ->whereRaw("MONTH(DAHI_FECHA) = ".$mes)
								  ->whereRaw("YEAR(DAHI_FECHA) = ".$year)
								  ->whereNotNull('DAHI_UTM');
						})->first();
						if ($utm) $utm = $utm->DAHI_UTM;
					}

					// Si aún así no tenemos resultado, error en la fila
					if($utm == NULL) {
				       	if ($tipo_carga == self::PROCESAR_VARIABLES_MERCADO) {
				       		$errors['fila'.$row] =  $errors['fila'.$row].'No hay información suficiente para UTM. ';
				       		$errorSQLVM = 1;
				       	}
				       	else {
				        	$errors['fila'.$row] =  $errors['fila'.$row].'Columna '.self::EXCEL_UTM.', No hay información suficiente para UTM. ';
				       		$errors['fila'] = 1; 
				       	}
					}
				}

				/*
				* Comprobar valores para los campos que sólo se rellenan el viernes
				*/ 
	            $dia_semana = date('N', strtotime($fila[self::EXCEL_FECHA]));
	            if ($dia_semana != 5) {
	            	$result = NULL;
	         		$viernes_anterior = date('Y-m-d', strtotime('last Friday '.$fila[self::EXCEL_FECHA]));
		         	// Buscar en base de datos los valores
					// $result = DB::table('MEPCO_DATOS_HISTORICOS')->select('DAHI_TARIFA_DIARIA_82000', 'DAHI_TARIFA_DIARIA_59000', 'DAHI_IFO_380_HOUSTON', 'DAHI_MDO_HOUSTON', 'DAHI_IFO_380_CRISTOBAL', 'DAHI_MDO_CRISTOBAL')->where('DAHI_FECHA', $viernes_anterior)->first();
		         	$result = DB::table('MEPCO_DATOS_HISTORICOS')
								->select('DAHI_TARIFA_DIARIA_82000', 'DAHI_TARIFA_DIARIA_59000', 'DAHI_IFO_380_HOUSTON', 'DAHI_MDO_HOUSTON', 'DAHI_IFO_380_CRISTOBAL', 'DAHI_MDO_CRISTOBAL')
								->where('DAHI_FECHA','<=', $viernes_anterior)
								->where('DAHI_TARIFA_DIARIA_82000','<>', 0)
								->where('DAHI_TARIFA_DIARIA_59000','<>', 0)
								->where('DAHI_IFO_380_HOUSTON','<>', 0)
								->where('DAHI_MDO_HOUSTON','<>', 0)
								->where('DAHI_IFO_380_CRISTOBAL','<>', 0)
								->where('DAHI_MDO_CRISTOBAL','<>', 0)
								->orderBy('DAHI_FECHA','DESC')
								->first();
		         	if (!$result&&$tipo_carga == self::PROCESAR_CARGA_HISTORICA) {
		         		// Asignar los valores
		         		$fila[self::EXCEL_TARIFA_DIARIA_82000] 	= null;
		         		$fila[self::EXCEL_TARIFA_DIARIA_59000] 	= null;
		         		$fila[self::EXCEL_IFO_380_HOUSTON] 		= null;
		         		$fila[self::EXCEL_MDO_HOUSTON] 			= null;
		         		$fila[self::EXCEL_IFO_380_CRISTOBAL] 	= null;
		         		$fila[self::EXCEL_MDO_CRISTOBAL] 		= null;		   
		         	}
		         	else if (!$result&&$tipo_carga == self::PROCESAR_VARIABLES_MERCADO) {
		         		$fila[self::EXCEL_TARIFA_DIARIA_82000] 	= $sheet->getCell(self::EXCEL_TARIFA_DIARIA_82000.$row)->getCalculatedValue();
		         		$fila[self::EXCEL_TARIFA_DIARIA_59000] 	= $sheet->getCell(self::EXCEL_TARIFA_DIARIA_59000.$row)->getCalculatedValue();
		         		$fila[self::EXCEL_IFO_380_HOUSTON] 		= $sheet->getCell(self::EXCEL_IFO_380_HOUSTON.$row)->getCalculatedValue();
		         		$fila[self::EXCEL_MDO_HOUSTON] 			= $sheet->getCell(self::EXCEL_MDO_HOUSTON.$row)->getCalculatedValue();
		         		$fila[self::EXCEL_IFO_380_CRISTOBAL] 	= $sheet->getCell(self::EXCEL_IFO_380_CRISTOBAL.$row)->getCalculatedValue();
		         		$fila[self::EXCEL_MDO_CRISTOBAL] 		= $sheet->getCell(self::EXCEL_MDO_CRISTOBAL.$row)->getCalculatedValue();
		         		$fila[self::EXCEL_TARIFA_DIARIA_82000] 	=self::nulosACero($fila[self::EXCEL_TARIFA_DIARIA_82000]);
		         		$fila[self::EXCEL_TARIFA_DIARIA_59000] 	=self::nulosACero($fila[self::EXCEL_TARIFA_DIARIA_59000]);
		         		$fila[self::EXCEL_IFO_380_HOUSTON] 		=self::nulosACero($fila[self::EXCEL_IFO_380_HOUSTON]);
		         		$fila[self::EXCEL_MDO_HOUSTON] 			=self::nulosACero($fila[self::EXCEL_MDO_HOUSTON]);
		         		$fila[self::EXCEL_IFO_380_CRISTOBAL] 	=self::nulosACero($fila[self::EXCEL_IFO_380_CRISTOBAL]);
		         		$fila[self::EXCEL_MDO_CRISTOBAL] 		=self::nulosACero($fila[self::EXCEL_MDO_CRISTOBAL]);
		         	}
		         	else if ($result){
		         		// Asignar los valores
		         		$fila[self::EXCEL_TARIFA_DIARIA_82000] 	= $result->DAHI_TARIFA_DIARIA_82000;
		         		$fila[self::EXCEL_TARIFA_DIARIA_59000] 	= $result->DAHI_TARIFA_DIARIA_59000;
		         		$fila[self::EXCEL_IFO_380_HOUSTON] 		= $result->DAHI_IFO_380_HOUSTON;
		         		$fila[self::EXCEL_MDO_HOUSTON] 			= $result->DAHI_MDO_HOUSTON;
		         		$fila[self::EXCEL_IFO_380_CRISTOBAL] 	= $result->DAHI_IFO_380_CRISTOBAL;
		         		$fila[self::EXCEL_MDO_CRISTOBAL] 		= $result->DAHI_MDO_CRISTOBAL;
		         	}
		        }
	
				if ($tipo_carga == self::PROCESAR_CARGA_HISTORICA) {
		         	$fechaYaIntroducida = DB::table('MEPCO_DATOS_HISTORICOS')->select('DAHI_ID', 'DAHI_FECHA')->where('DAHI_FECHA', date('Y-m-d', strtotime($fila[self::EXCEL_FECHA])))->first();
	         	}
	         	else {
		         	$fechaYaIntroducida = DB::table('MEPCO_VALOR_MERCADO AS MVM')
		         							->join('MEPCO_VAME_USPE AS MVU', 'MVM.VAUS_ID', '=', 'MVU.VAUS_ID')
		         							->join('MEPCO_USUA_PERF AS MUP', 'MUP.USPE_ID', '=', 'MVU.USPE_ID')
	         								->select('MVM.VAME_ID')
	         								->where('MVM.VAME_FECHA', date('Y-m-d', strtotime($fila[self::EXCEL_FECHA])))
	         								->where('MUP.PERF_ID', '=', session(self::USUARIO)->PERF_ID)
	         								->where('MVU.VAUS_ELIMINADO', 0)
	         								->first();
	         	}

		        // NO ha habido errores, continuar la inserción
		        if(($errors['fila'] != 1)&&(!$fechaYaIntroducida)) {
					// Si se está procesando la carga histórica
					if ($tipo_carga == self::PROCESAR_CARGA_HISTORICA) {
						try {
					        $datosHistoricos = DatosHistoricos::updateOrCreate([
						        'DAHI_FECHA' 				=> date('Y-m-d', strtotime($fila[self::EXCEL_FECHA])),
						        'DAHI_GAS_UNL_87' 			=> $fila[self::EXCEL_GAS_UNL_87],
						        'DAHI_GAS_UNL_93' 			=> $fila[self::EXCEL_GAS_UNL_93],
						        'DAHI_LD_DIESEL' 			=> $fila[self::EXCEL_DIESEL],
						        'DAHI_USG_BUTANO' 			=> $fila[self::EXCEL_USG_BUTANO],
						        'DAHI_UKC_USG' 				=> $fila[self::EXCEL_UKC_USG],
						        'DAHI_BRENT' 				=> $fila[self::EXCEL_BRENT],
						        'DAHI_BRENT_CORREGIDO'      => $brentCorregido,
						        'DAHI_BRENT_M1'		 		=> $fila[self::EXCEL_BRENT_M1],
						        'DAHI_BRENT_M2' 			=> $fila[self::EXCEL_BRENT_M2],
						        'DAHI_BRENT_M3' 			=> $fila[self::EXCEL_BRENT_M3],
						        'DAHI_BRENT_M4' 			=> $fila[self::EXCEL_BRENT_M4],
						        'DAHI_BRENT_M5' 			=> $fila[self::EXCEL_BRENT_M5],
						        'DAHI_BRENT_M6' 			=> $fila[self::EXCEL_BRENT_M6],
						        'DAHI_PPT_USG' 				=> $fila[self::EXCEL_PPT_USG],
						        'DAHI_MONT_BELVIEU_1' 		=> $fila[self::EXCEL_MONT_BELVIEU_1],
						        'DAHI_MONT_BELVIEU_2' 		=> $fila[self::EXCEL_MONT_BELVIEU_2],
						        'DAHI_MONT_BELVIEU' 		=> $mont_belvieu,
						        'DAHI_CIF_ARA_1'	 		=> $fila[self::EXCEL_CIF_ARA_1],
						        'DAHI_CIF_ARA_2' 			=> $fila[self::EXCEL_CIF_ARA_2],
						        'DAHI_CIF_ARA' 				=> $cif_ara,
						        'DAHI_WTI' 					=> $fila[self::EXCEL_WTI],
						        'DAHI_WTI_M1' 				=> $fila[self::EXCEL_WTI_M1],
						        'DAHI_WTI_M2' 				=> $fila[self::EXCEL_WTI_M2],
						        'DAHI_WTI_M3' 				=> $fila[self::EXCEL_WTI_M3],
						        'DAHI_WTI_M4' 				=> $fila[self::EXCEL_WTI_M4],
						        'DAHI_WTI_M5' 				=> $fila[self::EXCEL_WTI_M5],
						        'DAHI_WTI_M6' 				=> $fila[self::EXCEL_WTI_M6],
						        'DAHI_TC_DOLAR'				=> $tc_publicado,
						        'DAHI_TC_PUBLICADO' 		=> $fila[self::EXCEL_TC_PUBLICADO],
						        'DAHI_LIBOR' 				=> $libor,
						        'DAHI_TARIFA_DIARIA_82000' 	=> $fila[self::EXCEL_TARIFA_DIARIA_82000],
						        'DAHI_TARIFA_DIARIA_59000' 	=> $fila[self::EXCEL_TARIFA_DIARIA_59000],
						        'DAHI_IFO_380_HOUSTON' 		=> $fila[self::EXCEL_IFO_380_HOUSTON],
						        'DAHI_MDO_HOUSTON' 			=> $fila[self::EXCEL_MDO_HOUSTON],
						        'DAHI_IFO_380_CRISTOBAL' 	=> $fila[self::EXCEL_IFO_380_CRISTOBAL],
						        'DAHI_MDO_CRISTOBAL' 		=> $fila[self::EXCEL_MDO_CRISTOBAL],
						        'DAHI_UTM' 					=> $utm,
						        'DAHI_N_GAS_87' 			=> $fila[self::EXCEL_N_GAS_87],
						        'DAHI_M_GAS_87' 			=> $fila[self::EXCEL_M_GAS_87],
						        'DAHI_S_GAS_87' 			=> $fila[self::EXCEL_S_GAS_87],
						        'DAHI_T_GAS_87'				=> $fila[self::EXCEL_T_GAS_87],
						        'DAHI_F_GAS_87' 			=> $fila[self::EXCEL_F_GAS_87],
						        'DAHI_N_GAS_93' 			=> $fila[self::EXCEL_N_GAS_93],
						        'DAHI_M_GAS_93' 			=> $fila[self::EXCEL_M_GAS_93],
						        'DAHI_S_GAS_93' 			=> $fila[self::EXCEL_S_GAS_93],
						        'DAHI_T_GAS_93'				=> $fila[self::EXCEL_T_GAS_93],
						        'DAHI_F_GAS_93' 			=> $fila[self::EXCEL_F_GAS_93],
						        'DAHI_N_DIESEL' 			=> $fila[self::EXCEL_N_DIESEL],
						        'DAHI_M_DIESEL' 			=> $fila[self::EXCEL_M_DIESEL],
						        'DAHI_S_DIESEL' 			=> $fila[self::EXCEL_S_DIESEL],
						        'DAHI_T_DIESEL'				=> $fila[self::EXCEL_T_DIESEL],
						        'DAHI_F_DIESEL' 			=> $fila[self::EXCEL_F_DIESEL],
						        'DAHI_N_GLP' 				=> $fila[self::EXCEL_N_GLP],
						        'DAHI_M_GLP' 				=> $fila[self::EXCEL_M_GLP],
						        'DAHI_S_GLP' 				=> $fila[self::EXCEL_S_GLP],
						        'DAHI_T_GLP'				=> $fila[self::EXCEL_T_GLP],
						        'DAHI_F_GLP'	 			=> $fila[self::EXCEL_F_GLP],
								'DAHI_PROM_PARIDAD_SEMANAL_GAS_87'	=> $fila[self::EXCEL_PROM_PARIDAD_SEMANAL_GAS_87],
								'DAHI_PROM_PARIDAD_SEMANAL_GAS_93'	=> $fila[self::EXCEL_PROM_PARIDAD_SEMANAL_GAS_93],
								'DAHI_PROM_PARIDAD_SEMANAL_DIESEL'	=> $fila[self::EXCEL_PROM_PARIDAD_SEMANAL_DIESEL],
								'DAHI_PROM_PARIDAD_SEMANAL_GLP'		=> $fila[self::EXCEL_PROM_PARIDAD_SEMANAL_GLP],
								'DAHI_PROM_BRENT_SEMANAL'			=> $fila[self::EXCEL_PROM_BRENT_SEMANAL],
								'DAHI_PARIDAD_GAS_87'				=> $fila[self::EXCEL_PARIDAD_GAS_87],
								'DAHI_PARIDAD_GAS_93'				=> $fila[self::EXCEL_PARIDAD_GAS_93],
								'DAHI_PARIDAD_DIESEL'				=> $fila[self::EXCEL_PARIDAD_DIESEL],
								'DAHI_PARIDAD_GLP'					=> $fila[self::EXCEL_PARIDAD_GLP],
								'DAHI_BRENT_PROM_HISTORICO_GAS_87'	=> $fila[self::EXCEL_BRENT_PROM_HISTORICO_GAS_87],
								'DAHI_BRENT_PROM_HISTORICO_GAS_93'	=> $fila[self::EXCEL_BRENT_PROM_HISTORICO_GAS_93],
								'DAHI_BRENT_PROM_HISTORICO_DIESEL'	=> $fila[self::EXCEL_BRENT_PROM_HISTORICO_DIESEL],
								'DAHI_BRENT_PROM_HISTORICO_GLP'		=> $fila[self::EXCEL_BRENT_PROM_HISTORICO_GLP],
								'DAHI_BRENT_PROM_FUTUROS_GAS_87'	=> $fila[self::EXCEL_BRENT_PROM_FUTUROS_GAS_87],
								'DAHI_BRENT_PROM_FUTUROS_GAS_93'	=> $fila[self::EXCEL_BRENT_PROM_FUTUROS_GAS_93],
								'DAHI_BRENT_PROM_FUTUROS_DIESEL'	=> $fila[self::EXCEL_BRENT_PROM_FUTUROS_DIESEL],
								'DAHI_BRENT_PROM_FUTUROS_GLP'		=> $fila[self::EXCEL_BRENT_PROM_FUTUROS_GLP],
								'DAHI_BRENT_PROM_PONDERADO_GAS_87'	=> $fila[self::EXCEL_BRENT_PROM_PONDERADO_GAS_87],
								'DAHI_BRENT_PROM_PONDERADO_GAS_93'	=> $fila[self::EXCEL_BRENT_PROM_PONDERADO_GAS_93],
								'DAHI_BRENT_PROM_PONDERADO_DIESEL'	=> $fila[self::EXCEL_BRENT_PROM_PONDERADO_DIESEL],
								'DAHI_BRENT_PROM_PONDERADO_GLP'		=> $fila[self::EXCEL_BRENT_PROM_PONDERADO_GLP],
								'DAHI_PROM_WTI_SEMANAL'				=> $fila[self::EXCEL_PROM_WTI],
								'DAHI_WTI_PROM_HISTORICO_GAS_87'	=> $fila[self::EXCEL_WTI_PROM_HISTORICO_GAS_87],
								'DAHI_WTI_PROM_HISTORICO_GAS_93'	=> $fila[self::EXCEL_WTI_PROM_HISTORICO_GAS_93],
								'DAHI_WTI_PROM_HISTORICO_DIESEL'	=> $fila[self::EXCEL_WTI_PROM_HISTORICO_DIESEL],
								'DAHI_WTI_PROM_HISTORICO_GLP'		=> $fila[self::EXCEL_WTI_PROM_HISTORICO_GLP],
								'DAHI_WTI_PROM_FUTUROS_GAS_87'		=> $fila[self::EXCEL_WTI_PROM_FUTUROS_GAS_87],
								'DAHI_WTI_PROM_FUTUROS_GAS_93'		=> $fila[self::EXCEL_WTI_PROM_FUTUROS_GAS_93],
								'DAHI_WTI_PROM_FUTUROS_DIESEL'		=> $fila[self::EXCEL_WTI_PROM_FUTUROS_DIESEL],
								'DAHI_WTI_PROM_FUTUROS_GLP'			=> $fila[self::EXCEL_WTI_PROM_FUTUROS_GLP],
								'DAHI_WTI_PROM_PONDERADO_GAS_87'	=> $fila[self::EXCEL_WTI_PROM_PONDERADO_GAS_87],
								'DAHI_WTI_PROM_PONDERADO_GAS_93'	=> $fila[self::EXCEL_WTI_PROM_PONDERADO_GAS_93],
								'DAHI_WTI_PROM_PONDERADO_DIESEL'	=> $fila[self::EXCEL_WTI_PROM_PONDERADO_DIESEL],
								'DAHI_WTI_PROM_PONDERADO_GLP'		=> $fila[self::EXCEL_WTI_PROM_PONDERADO_GLP],
								'DAHI_PROM_MARGEN_SEMANAL_BRENT_GAS_87' => $fila[self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_GAS_87],
								'DAHI_PROM_MARGEN_SEMANAL_BRENT_GAS_93' => $fila[self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_GAS_93],
								'DAHI_PROM_MARGEN_SEMANAL_BRENT_DIESEL' => $fila[self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_DIESEL],
								'DAHI_PROM_MARGEN_SEMANAL_BRENT_GLP' 	=> $fila[self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_GLP],
								'DAHI_PROM_MARGEN_SEMANAL_WTI_GAS_87' 	=> $fila[self::EXCEL_PROM_MARGEN_SEMANAL_WTI_GAS_87],
								'DAHI_PROM_MARGEN_SEMANAL_WTI_GAS_93' 	=> $fila[self::EXCEL_PROM_MARGEN_SEMANAL_WTI_GAS_93],
								'DAHI_PROM_MARGEN_SEMANAL_WTI_DIESEL' 	=> $fila[self::EXCEL_PROM_MARGEN_SEMANAL_WTI_DIESEL],
								'DAHI_PROM_MARGEN_SEMANAL_WTI_GLP' 		=> $fila[self::EXCEL_PROM_MARGEN_SEMANAL_WTI_GLP],
								'DAHI_MARGEN_PROMEDIO_GAS_87'		=> $fila[self::EXCEL_MARGEN_PROMEDIO_GAS_87],
								'DAHI_MARGEN_PROMEDIO_GAS_93'		=> $fila[self::EXCEL_MARGEN_PROMEDIO_GAS_93],
								'DAHI_MARGEN_PROMEDIO_DIESEL'		=> $fila[self::EXCEL_MARGEN_PROMEDIO_DIESEL],
								'DAHI_MARGEN_PROMEDIO_GLP'			=> $fila[self::EXCEL_MARGEN_PROMEDIO_GLP],
								'DAHI_TIPO_CRUDO'					=> $fila[self::EXCEL_TIPO_CRUDO],
								'DAHI_FOB_TEORICO_GAS_87'			=> $fila[self::EXCEL_FOB_TEORICO_GAS_87],
								'DAHI_FOB_TEORICO_GAS_93'			=> $fila[self::EXCEL_FOB_TEORICO_GAS_93],
								'DAHI_FOB_TEORICO_DIESEL'			=> $fila[self::EXCEL_FOB_TEORICO_DIESEL],
								'DAHI_FOB_TEORICO_GLP'				=> $fila[self::EXCEL_FOB_TEORICO_GLP],
								'DAHI_PROM_REFERENCIA_SEMANAL_GAS_87'	=> $fila[self::EXCEL_PROM_REFERENCIA_SEMANAL_GAS_87],
								'DAHI_PROM_REFERENCIA_SEMANAL_GAS_93'	=> $fila[self::EXCEL_PROM_REFERENCIA_SEMANAL_GAS_93],
								'DAHI_PROM_REFERENCIA_SEMANAL_DIESEL'	=> $fila[self::EXCEL_PROM_REFERENCIA_SEMANAL_DIESEL],
								'DAHI_PROM_REFERENCIA_SEMANAL_GLP'		=> $fila[self::EXCEL_PROM_REFERENCIA_SEMANAL_GLP],
					        	'DAHI_CORRECCION_RVP'					=> $fila[self::EXCEL_CORRECCION_RVP],
					        ]);
							
							$valores[] = $datosHistoricos;
						} 
						catch(\PDOException $e)
						{
							array_push($errors, 'Error en Base de datos al insertar la fila '.$row.'. Detalle del error \''.$e->getMessage().'\''); 
						}
						catch(Exception $e) {
							array_push($errors, 'Error general al insertar la fila '.$row.'. Detalle del error\''.$e->getMessage().'\'');
						}
					}
					else if ($tipo_carga == self::PROCESAR_VARIABLES_MERCADO) {
						/*
						* Calcula BRENT Corregido m1, m2, m3, m4, m5, m6
						*/
						if(($fila[self::EXCEL_BRENT_M1] != null)&&($fila[self::EXCEL_UKC_USG] != null))
						{
							try {
								$formBrentCorregidoAux = $formBrentCorregido;
								$formBrentCorregidoAux = str_replace('{{BRENT}}'  , $fila[self::EXCEL_BRENT_M1]  , $formBrentCorregidoAux);
								$formBrentCorregidoAux = str_replace('{{UKC_USG}}', $fila[self::EXCEL_UKC_USG], $formBrentCorregidoAux);
								$form = new Form($formBrentCorregidoAux);
								$variables = self::obtenerLlaves($formBrentCorregidoAux);
								foreach ($variables as $item) {
								    $form->setParameter($item, (float)self::obtenerVariablePorLlave($item, null));
								}
								$form->setIsCalculable(true);
								$brent_corregido_m1 = $form->render();
							}
							catch(\Exception $e) {}
						}else
						{
							$brent_corregido_m1 = $fila[self::EXCEL_BRENT_M1];
						}
						if(($fila[self::EXCEL_BRENT_M2] != null)&&($fila[self::EXCEL_UKC_USG] != null))
						{
							try {
								$formBrentCorregidoAux = $formBrentCorregido;
								$formBrentCorregidoAux = str_replace('{{BRENT}}'  , $fila[self::EXCEL_BRENT_M2]  , $formBrentCorregidoAux);
								$formBrentCorregidoAux = str_replace('{{UKC_USG}}', $fila[self::EXCEL_UKC_USG], $formBrentCorregidoAux);
								$form = new Form($formBrentCorregidoAux);
								$variables = self::obtenerLlaves($formBrentCorregidoAux);
								foreach ($variables as $item) {
								    $form->setParameter($item, (float)self::obtenerVariablePorLlave($item, null));
								}
								$form->setIsCalculable(true);
								$brent_corregido_m2 = $form->render();
							}
							catch(\Exception $e) {}
						}else
						{
							$brent_corregido_m2 = $fila[self::EXCEL_BRENT_M2];
						}
						if(($fila[self::EXCEL_BRENT_M3] != null)&&($fila[self::EXCEL_UKC_USG] != null))
						{
							try {
								$formBrentCorregidoAux = $formBrentCorregido;
								$formBrentCorregidoAux = str_replace('{{BRENT}}'  , $fila[self::EXCEL_BRENT_M3]  , $formBrentCorregidoAux);
								$formBrentCorregidoAux = str_replace('{{UKC_USG}}', $fila[self::EXCEL_UKC_USG], $formBrentCorregidoAux);
								$form = new Form($formBrentCorregidoAux);
								$variables = self::obtenerLlaves($formBrentCorregidoAux);
								foreach ($variables as $item) {
								    $form->setParameter($item, (float)self::obtenerVariablePorLlave($item, null));
								}
								$form->setIsCalculable(true);
								$brent_corregido_m3 = $form->render();
							}
							catch(\Exception $e) {}
						}else
						{
							$brent_corregido_m3 = $fila[self::EXCEL_BRENT_M3];
						}
						if(($fila[self::EXCEL_BRENT_M4] != null)&&($fila[self::EXCEL_UKC_USG] != null))
						{
							try {
								$formBrentCorregidoAux = $formBrentCorregido;
								$formBrentCorregidoAux = str_replace('{{BRENT}}'  , $fila[self::EXCEL_BRENT_M4]  , $formBrentCorregidoAux);
								$formBrentCorregidoAux = str_replace('{{UKC_USG}}', $fila[self::EXCEL_UKC_USG], $formBrentCorregidoAux);
								$form = new Form($formBrentCorregidoAux);
								$variables = self::obtenerLlaves($formBrentCorregidoAux);
								foreach ($variables as $item) {
								    $form->setParameter($item, (float)self::obtenerVariablePorLlave($item, null));
								}
								$form->setIsCalculable(true);
								$brent_corregido_m4 = $form->render();
							}
							catch(\Exception $e) {}
						}else
						{
							$brent_corregido_m4 = $fila[self::EXCEL_BRENT_M4];
						}
						if(($fila[self::EXCEL_BRENT_M5] != null)&&($fila[self::EXCEL_UKC_USG] != null))
						{
							try {
								$formBrentCorregidoAux = $formBrentCorregido;
								$formBrentCorregidoAux = str_replace('{{BRENT}}'  , $fila[self::EXCEL_BRENT_M5]  , $formBrentCorregidoAux);
								$formBrentCorregidoAux = str_replace('{{UKC_USG}}', $fila[self::EXCEL_UKC_USG], $formBrentCorregidoAux);
								$form = new Form($formBrentCorregidoAux);
								$variables = self::obtenerLlaves($formBrentCorregidoAux);
								foreach ($variables as $item) {
								    $form->setParameter($item, (float)self::obtenerVariablePorLlave($item, null));
								}
								$form->setIsCalculable(true);
								$brent_corregido_m5 = $form->render();
							}
							catch(\Exception $e) {}
						}else
						{
							$brent_corregido_m5 = $fila[self::EXCEL_BRENT_M5];
						}
						if(($fila[self::EXCEL_BRENT_M6] != null)&&($fila[self::EXCEL_UKC_USG] != null))
						{
							try {
								$formBrentCorregidoAux = $formBrentCorregido;
								$formBrentCorregidoAux = str_replace('{{BRENT}}'  , $fila[self::EXCEL_BRENT_M6]  , $formBrentCorregidoAux);
								$formBrentCorregidoAux = str_replace('{{UKC_USG}}', $fila[self::EXCEL_UKC_USG], $formBrentCorregidoAux);
								$form = new Form($formBrentCorregidoAux);
								$variables = self::obtenerLlaves($formBrentCorregidoAux);
								foreach ($variables as $item) {
								    $form->setParameter($item, (float)self::obtenerVariablePorLlave($item, null));
								}
								$form->setIsCalculable(true);
								$brent_corregido_m6 = $form->render();
							}
							catch(\Exception $e) {}
						}else
						{
							$brent_corregido_m6 = $fila[self::EXCEL_BRENT_M6];
						}

						/*
						* Comprobar la fecha de los valores de mercado
						*/

						// Obtener fecha de vigencia
						$fecha_vigencia = DB::table('MEPCO_PROCESO')
										->select('PROC_FECHA_VIGENCIA')
										->where('PROC_ID', $proc_id)
										->pluck('PROC_FECHA_VIGENCIA');

						$fecha_vigencia = date('Y-m-d', strtotime($fecha_vigencia));
						$fecha_minima = date('Y-m-d', strtotime($fecha_vigencia. '-10 days'));
						$fecha_maxima = date('Y-m-d', strtotime($fecha_vigencia. '-6 days'));
						
						$fecha_vm = date('Y-m-d', strtotime($fila[self::EXCEL_FECHA]));

						if (($fecha_vm < $fecha_minima) || ($fecha_vm>$fecha_maxima)) {
							$errors['fila'.$row] =  $errors['fila'.$row].' La fecha especificada no corresponde con la fecha de vigencia del proceso. '; 
							$errorFechas = 1;
						}

						// Comprobar las fechas según el número de fila
						switch ($row) {
							case 4:
								// Lunes
								if ((date('w', strtotime($fila[self::EXCEL_FECHA])) != 1)&&($errorFechas==0)) {
						       		$errors['fila'.$row] =  $errors['fila'.$row].'Introduzca las fechas de valores de mercado en orden ascendente. ';
						       		$errorSQLVM = 1;
								}
								break;
							case 5:
								// Martes
								if ((date('w', strtotime($fila[self::EXCEL_FECHA])) != 2)&&($errorFechas==0)) {
						       		$errors['fila'.$row] =  $errors['fila'.$row].'Introduzca las fechas de valores de mercado en orden ascendente. ';
						       		$errorSQLVM = 1;
								}
								break;
							case 6:
								// Miércoles
								if ((date('w', strtotime($fila[self::EXCEL_FECHA])) != 3)&&($errorFechas==0)) {
						       		$errors['fila'.$row] =  $errors['fila'.$row].'Introduzca las fechas de valores de mercado en orden ascendente. ';
						       		$errorSQLVM = 1;
								}
								break;
							case 7:
								// Jueves
								if ((date('w', strtotime($fila[self::EXCEL_FECHA])) != 4)&&($errorFechas==0)) {
						       		$errors['fila'.$row] =  $errors['fila'.$row].'Introduzca las fechas de valores de mercado en orden ascendente. ';
						       		$errorSQLVM = 1;
								}
								break;
							case 8:
								// Viernes
								if ((date('w', strtotime($fila[self::EXCEL_FECHA])) != 5)&&($errorFechas==0)) {
						       		$errors['fila'.$row] =  $errors['fila'.$row].'Introduzca las fechas de valores de mercado en orden ascendente. ';
						       		$errorSQLVM = 1;
								}
								break;
							default:
								break;
						}

						// No se carga más allá de la fila 5
						if ($row<9) {
							try{
								//Si es valor de mercado crea registro en VAME_USPE
								if($vaus_id == null)
								{
									$vaus_id = DB::table('MEPCO_VAME_USPE')->insertGetId([
										'USPE_ID' => session(self::USUARIO)->USPE_ID, 
							        	'PROC_ID'                  => $proc_id,
										'VAUS_FECHA_CREACION'      => self::dateTimeNow(),
										'VAUS_FECHA_VALIDADO'      => null,
										'VAUS_FECHA_ELIMINADO'     => null,
										'VAUS_VALIDACION_CORRECTA' => 0,
										'VAUS_ELIMINADO'           => 0]);
								}

						        $valorMercado = ValorMercado::create([
						        	'VAUS_ID'                   => self::quitarEspacios($vaus_id),
							        'VAME_FECHA' 				=> self::quitarEspacios(date('Y-m-d', strtotime($fila[self::EXCEL_FECHA]))),
							        'VAME_GAS_UNL_87' 			=> self::quitarEspacios($fila[self::EXCEL_GAS_UNL_87]),
							        'VAME_GAS_UNL_93' 			=> self::quitarEspacios($fila[self::EXCEL_GAS_UNL_93]),
							        'VAME_LD_DIESEL' 			=> self::quitarEspacios($fila[self::EXCEL_DIESEL]),
							        'VAME_USG_BUTANO' 			=> self::quitarEspacios($fila[self::EXCEL_USG_BUTANO]),
							        'VAME_UKC_USG' 				=> self::quitarEspacios($fila[self::EXCEL_UKC_USG]),
							        'VAME_BRENT' 				=> self::quitarEspacios($fila[self::EXCEL_BRENT]),
							        'VAME_BRENT_CORREGIDO'		=> self::quitarEspacios($brentCorregido),
							        'VAME_BRENT_M1'		 		=> self::quitarEspacios($fila[self::EXCEL_BRENT_M1]),
							        'VAME_BRENT_M2' 			=> self::quitarEspacios($fila[self::EXCEL_BRENT_M2]),
							        'VAME_BRENT_M3' 			=> self::quitarEspacios($fila[self::EXCEL_BRENT_M3]),
							        'VAME_BRENT_M4' 			=> self::quitarEspacios($fila[self::EXCEL_BRENT_M4]),
							        'VAME_BRENT_M5' 			=> self::quitarEspacios($fila[self::EXCEL_BRENT_M5]),
							        'VAME_BRENT_M6' 			=> self::quitarEspacios($fila[self::EXCEL_BRENT_M6]),
							        'VAME_BRENT_CORREGIDO_M1'	=> self::quitarEspacios($brent_corregido_m1),
							        'VAME_BRENT_CORREGIDO_M2'	=> self::quitarEspacios($brent_corregido_m2),
							        'VAME_BRENT_CORREGIDO_M3'	=> self::quitarEspacios($brent_corregido_m3),
							        'VAME_BRENT_CORREGIDO_M4'	=> self::quitarEspacios($brent_corregido_m4),
							        'VAME_BRENT_CORREGIDO_M5'	=> self::quitarEspacios($brent_corregido_m5),
							        'VAME_BRENT_CORREGIDO_M6'	=> self::quitarEspacios($brent_corregido_m6),
							        'VAME_PPT_USG' 				=> self::quitarEspacios($fila[self::EXCEL_PPT_USG]),
							        'VAME_MONT_BELVIEU_1'		=> self::quitarEspacios($fila[self::EXCEL_MONT_BELVIEU_1]),
							        'VAME_MONT_BELVIEU_2'		=> self::quitarEspacios($fila[self::EXCEL_MONT_BELVIEU_2]),
							        'VAME_MONT_BELVIEU' 		=> self::quitarEspacios($mont_belvieu),
							        'VAME_CIF_ARA_1'			=> self::quitarEspacios($fila[self::EXCEL_CIF_ARA_1]),
							        'VAME_CIF_ARA_2'			=> self::quitarEspacios($fila[self::EXCEL_CIF_ARA_2]),
							        'VAME_CIF_ARA' 				=> self::quitarEspacios($cif_ara),
							        'VAME_WTI' 					=> self::quitarEspacios($fila[self::EXCEL_WTI]),
							        'VAME_WTI_M1' 				=> self::quitarEspacios($fila[self::EXCEL_WTI_M1]),
							        'VAME_WTI_M2' 				=> self::quitarEspacios($fila[self::EXCEL_WTI_M2]),
							        'VAME_WTI_M3' 				=> self::quitarEspacios($fila[self::EXCEL_WTI_M3]),
							        'VAME_WTI_M4' 				=> self::quitarEspacios($fila[self::EXCEL_WTI_M4]),
							        'VAME_WTI_M5' 				=> self::quitarEspacios($fila[self::EXCEL_WTI_M5]),
							        'VAME_WTI_M6' 				=> self::quitarEspacios($fila[self::EXCEL_WTI_M6]),
							        'VAME_TC_DOLAR'				=> self::quitarEspacios($tc_publicado),
							        'VAME_TC_PUBLICADO' 		=> self::quitarEspacios($fila[self::EXCEL_TC_PUBLICADO]),
							        'VAME_LIBOR' 				=> self::quitarEspacios($libor),
							        'VAME_TARIFA_DIARIA_82000' 	=> self::quitarEspacios($fila[self::EXCEL_TARIFA_DIARIA_82000]),
							        'VAME_TARIFA_DIARIA_59000' 	=> self::quitarEspacios($fila[self::EXCEL_TARIFA_DIARIA_59000]),
							        'VAME_IFO_380_HOUSTON' 		=> self::quitarEspacios($fila[self::EXCEL_IFO_380_HOUSTON]),
							        'VAME_MDO_HOUSTON' 			=> self::quitarEspacios($fila[self::EXCEL_MDO_HOUSTON]),
							        'VAME_IFO_380_CRISTOBAL' 	=> self::quitarEspacios($fila[self::EXCEL_IFO_380_CRISTOBAL]),
							        'VAME_MDO_CRISTOBAL' 		=> self::quitarEspacios($fila[self::EXCEL_MDO_CRISTOBAL]),
							        'VAME_UTM' 					=> self::quitarEspacios($utm),
						        ]);

								$valores[] = $valorMercado;
	
						    } 
							catch(\PDOException $e)
							{
								$errorSQLVM = true;
								array_push($errors, 'Error en Base de datos al insertar la fila '.$row.'. Detalle del error \''.$e->getMessage().'\''); 
							}
							catch(Exception $e) {
								$errorSQLVM = true;
								array_push($errors, 'Error general al insertar la fila '.$row.'. Detalle del error\''.$e->getMessage().'\'');
							}
						}
						else {
							array_push($warnings, 'No se insertó la fila '.$row.'. Fichero de Valores de Mercado con filas extra.');
						}
					}
				}
				else if (($errors['fila'] != 1) && ($fechaYaIntroducida)) {
					if ($tipo_carga == self::PROCESAR_CARGA_HISTORICA) {
						// Obtener la fila

						$filaActualizar = DatosHistoricos::where('DAHI_FECHA', $fechaYaIntroducida->DAHI_FECHA)->first();

				        $filaActualizar->DAHI_FECHA 				= date('Y-m-d', strtotime($fila[self::EXCEL_FECHA]));
				        $filaActualizar->DAHI_GAS_UNL_87 			= $fila[self::EXCEL_GAS_UNL_87];
				        $filaActualizar->DAHI_GAS_UNL_93 			= $fila[self::EXCEL_GAS_UNL_93];
				        $filaActualizar->DAHI_LD_DIESEL 			= $fila[self::EXCEL_DIESEL];
				        $filaActualizar->DAHI_USG_BUTANO 			= $fila[self::EXCEL_USG_BUTANO];
				        $filaActualizar->DAHI_UKC_USG 				= $fila[self::EXCEL_UKC_USG];
				        $filaActualizar->DAHI_BRENT 				= $fila[self::EXCEL_BRENT];
				        $filaActualizar->DAHI_BRENT_CORREGIDO      	= $brentCorregido;
				        $filaActualizar->DAHI_BRENT_M1		 		= $fila[self::EXCEL_BRENT_M1];
				        $filaActualizar->DAHI_BRENT_M2 				= $fila[self::EXCEL_BRENT_M2];
				        $filaActualizar->DAHI_BRENT_M3 				= $fila[self::EXCEL_BRENT_M3];
				        $filaActualizar->DAHI_BRENT_M4 				= $fila[self::EXCEL_BRENT_M4];
				        $filaActualizar->DAHI_BRENT_M5 				= $fila[self::EXCEL_BRENT_M5];
				        $filaActualizar->DAHI_BRENT_M6 				= $fila[self::EXCEL_BRENT_M6];
				        $filaActualizar->DAHI_PPT_USG 				= $fila[self::EXCEL_PPT_USG];
				        $filaActualizar->DAHI_MONT_BELVIEU 			= $mont_belvieu;
				        $filaActualizar->DAHI_CIF_ARA 				= $cif_ara;
				        $filaActualizar->DAHI_WTI 					= $fila[self::EXCEL_WTI];
				        $filaActualizar->DAHI_WTI_M1 				= $fila[self::EXCEL_WTI_M1];
				        $filaActualizar->DAHI_WTI_M2 				= $fila[self::EXCEL_WTI_M2];
				        $filaActualizar->DAHI_WTI_M3 				= $fila[self::EXCEL_WTI_M3];
				        $filaActualizar->DAHI_WTI_M4 				= $fila[self::EXCEL_WTI_M4];
				        $filaActualizar->DAHI_WTI_M5 				= $fila[self::EXCEL_WTI_M5];
				        $filaActualizar->DAHI_WTI_M6 				= $fila[self::EXCEL_WTI_M6];
				        $filaActualizar->DAHI_TC_DOLAR				= $tc_publicado;
				        $filaActualizar->DAHI_TC_PUBLICADO 			= $fila[self::EXCEL_TC_PUBLICADO];
				        $filaActualizar->DAHI_LIBOR 				= $libor;
				        $filaActualizar->DAHI_TARIFA_DIARIA_82000 	= $fila[self::EXCEL_TARIFA_DIARIA_82000];
				        $filaActualizar->DAHI_TARIFA_DIARIA_59000 	= $fila[self::EXCEL_TARIFA_DIARIA_59000];
				        $filaActualizar->DAHI_IFO_380_HOUSTON 		= $fila[self::EXCEL_IFO_380_HOUSTON];
				        $filaActualizar->DAHI_MDO_HOUSTON 			= $fila[self::EXCEL_MDO_HOUSTON];
				        $filaActualizar->DAHI_IFO_380_CRISTOBAL 	= $fila[self::EXCEL_IFO_380_CRISTOBAL];
				        $filaActualizar->DAHI_MDO_CRISTOBAL 		= $fila[self::EXCEL_MDO_CRISTOBAL];
				        $filaActualizar->DAHI_UTM 					= $fila[self::EXCEL_UTM];
				        $filaActualizar->DAHI_N_GAS_87 			= $fila[self::EXCEL_N_GAS_87];
				        $filaActualizar->DAHI_M_GAS_87 			= $fila[self::EXCEL_M_GAS_87];
				        $filaActualizar->DAHI_S_GAS_87 			= $fila[self::EXCEL_S_GAS_87];
				        $filaActualizar->DAHI_T_GAS_87				= $fila[self::EXCEL_T_GAS_87];
				        $filaActualizar->DAHI_F_GAS_87 			= $fila[self::EXCEL_F_GAS_87];
				        $filaActualizar->DAHI_N_GAS_93 			= $fila[self::EXCEL_N_GAS_93];
				        $filaActualizar->DAHI_M_GAS_93 			= $fila[self::EXCEL_M_GAS_93];
				        $filaActualizar->DAHI_S_GAS_93 			= $fila[self::EXCEL_S_GAS_93];
				        $filaActualizar->DAHI_T_GAS_93				= $fila[self::EXCEL_T_GAS_93];
				        $filaActualizar->DAHI_F_GAS_93 			= $fila[self::EXCEL_F_GAS_93];
				        $filaActualizar->DAHI_N_DIESEL 			= $fila[self::EXCEL_N_DIESEL];
				        $filaActualizar->DAHI_M_DIESEL 			= $fila[self::EXCEL_M_DIESEL];
				        $filaActualizar->DAHI_S_DIESEL 			= $fila[self::EXCEL_S_DIESEL];
				        $filaActualizar->DAHI_T_DIESEL				= $fila[self::EXCEL_T_DIESEL];
				        $filaActualizar->DAHI_F_DIESEL 			= $fila[self::EXCEL_F_DIESEL];
				        $filaActualizar->DAHI_N_GLP 				= $fila[self::EXCEL_N_GLP];
				        $filaActualizar->DAHI_M_GLP 				= $fila[self::EXCEL_M_GLP];
				        $filaActualizar->DAHI_S_GLP 				= $fila[self::EXCEL_S_GLP];
				        $filaActualizar->DAHI_T_GLP				= $fila[self::EXCEL_T_GLP];
				        $filaActualizar->DAHI_F_GLP	 			= $fila[self::EXCEL_F_GLP];
						$filaActualizar->DAHI_PROM_PARIDAD_SEMANAL_GAS_87	= $fila[self::EXCEL_PROM_PARIDAD_SEMANAL_GAS_87];
						$filaActualizar->DAHI_PROM_PARIDAD_SEMANAL_GAS_93	= $fila[self::EXCEL_PROM_PARIDAD_SEMANAL_GAS_93];
						$filaActualizar->DAHI_PROM_PARIDAD_SEMANAL_DIESEL	= $fila[self::EXCEL_PROM_PARIDAD_SEMANAL_DIESEL];
						$filaActualizar->DAHI_PROM_PARIDAD_SEMANAL_GLP		= $fila[self::EXCEL_PROM_PARIDAD_SEMANAL_GLP];
						$filaActualizar->DAHI_PROM_BRENT_SEMANAL			= $fila[self::EXCEL_PROM_BRENT_SEMANAL];
						$filaActualizar->DAHI_PARIDAD_GAS_87				= $fila[self::EXCEL_PARIDAD_GAS_87];
						$filaActualizar->DAHI_PARIDAD_GAS_93				= $fila[self::EXCEL_PARIDAD_GAS_93];
						$filaActualizar->DAHI_PARIDAD_DIESEL				= $fila[self::EXCEL_PARIDAD_DIESEL];
						$filaActualizar->DAHI_PARIDAD_GLP					= $fila[self::EXCEL_PARIDAD_GLP];
						$filaActualizar->DAHI_BRENT_PROM_HISTORICO_GAS_87	= $fila[self::EXCEL_BRENT_PROM_HISTORICO_GAS_87];
						$filaActualizar->DAHI_BRENT_PROM_HISTORICO_GAS_93	= $fila[self::EXCEL_BRENT_PROM_HISTORICO_GAS_93];
						$filaActualizar->DAHI_BRENT_PROM_HISTORICO_DIESEL	= $fila[self::EXCEL_BRENT_PROM_HISTORICO_DIESEL];
						$filaActualizar->DAHI_BRENT_PROM_HISTORICO_GLP		= $fila[self::EXCEL_BRENT_PROM_HISTORICO_GLP];
						$filaActualizar->DAHI_BRENT_PROM_FUTUROS_GAS_87	= $fila[self::EXCEL_BRENT_PROM_FUTUROS_GAS_87];
						$filaActualizar->DAHI_BRENT_PROM_FUTUROS_GAS_93	= $fila[self::EXCEL_BRENT_PROM_FUTUROS_GAS_93];
						$filaActualizar->DAHI_BRENT_PROM_FUTUROS_DIESEL	= $fila[self::EXCEL_BRENT_PROM_FUTUROS_DIESEL];
						$filaActualizar->DAHI_BRENT_PROM_FUTUROS_GLP		= $fila[self::EXCEL_BRENT_PROM_FUTUROS_GLP];
						$filaActualizar->DAHI_BRENT_PROM_PONDERADO_GAS_87	= $fila[self::EXCEL_BRENT_PROM_PONDERADO_GAS_87];
						$filaActualizar->DAHI_BRENT_PROM_PONDERADO_GAS_93	= $fila[self::EXCEL_BRENT_PROM_PONDERADO_GAS_93];
						$filaActualizar->DAHI_BRENT_PROM_PONDERADO_DIESEL	= $fila[self::EXCEL_BRENT_PROM_PONDERADO_DIESEL];
						$filaActualizar->DAHI_BRENT_PROM_PONDERADO_GLP		= $fila[self::EXCEL_BRENT_PROM_PONDERADO_GLP];
						$filaActualizar->DAHI_PROM_WTI_SEMANAL				= $fila[self::EXCEL_PROM_WTI];
						$filaActualizar->DAHI_WTI_PROM_HISTORICO_GAS_87	= $fila[self::EXCEL_WTI_PROM_HISTORICO_GAS_87];
						$filaActualizar->DAHI_WTI_PROM_HISTORICO_GAS_93	= $fila[self::EXCEL_WTI_PROM_HISTORICO_GAS_93];
						$filaActualizar->DAHI_WTI_PROM_HISTORICO_DIESEL	= $fila[self::EXCEL_WTI_PROM_HISTORICO_DIESEL];
						$filaActualizar->DAHI_WTI_PROM_HISTORICO_GLP		= $fila[self::EXCEL_WTI_PROM_HISTORICO_GLP];
						$filaActualizar->DAHI_WTI_PROM_FUTUROS_GAS_87		= $fila[self::EXCEL_WTI_PROM_FUTUROS_GAS_87];
						$filaActualizar->DAHI_WTI_PROM_FUTUROS_GAS_93		= $fila[self::EXCEL_WTI_PROM_FUTUROS_GAS_93];
						$filaActualizar->DAHI_WTI_PROM_FUTUROS_DIESEL		= $fila[self::EXCEL_WTI_PROM_FUTUROS_DIESEL];
						$filaActualizar->DAHI_WTI_PROM_FUTUROS_GLP			= $fila[self::EXCEL_WTI_PROM_FUTUROS_GLP];
						$filaActualizar->DAHI_WTI_PROM_PONDERADO_GAS_87	= $fila[self::EXCEL_WTI_PROM_PONDERADO_GAS_87];
						$filaActualizar->DAHI_WTI_PROM_PONDERADO_GAS_93	= $fila[self::EXCEL_WTI_PROM_PONDERADO_GAS_93];
						$filaActualizar->DAHI_WTI_PROM_PONDERADO_DIESEL	= $fila[self::EXCEL_WTI_PROM_PONDERADO_DIESEL];
						$filaActualizar->DAHI_WTI_PROM_PONDERADO_GLP		= $fila[self::EXCEL_WTI_PROM_PONDERADO_GLP];
						$filaActualizar->DAHI_PROM_MARGEN_SEMANAL_BRENT_GAS_87 	= $fila[self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_GAS_87];
						$filaActualizar->DAHI_PROM_MARGEN_SEMANAL_BRENT_GAS_93 	= $fila[self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_GAS_93];
						$filaActualizar->DAHI_PROM_MARGEN_SEMANAL_BRENT_DIESEL 	= $fila[self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_DIESEL];
						$filaActualizar->DAHI_PROM_MARGEN_SEMANAL_BRENT_GLP 	= $fila[self::EXCEL_PROM_MARGEN_SEMANAL_BRENT_GLP];
						$filaActualizar->DAHI_PROM_MARGEN_SEMANAL_WTI_GAS_87 	= $fila[self::EXCEL_PROM_MARGEN_SEMANAL_WTI_GAS_87];
						$filaActualizar->DAHI_PROM_MARGEN_SEMANAL_WTI_GAS_93 	= $fila[self::EXCEL_PROM_MARGEN_SEMANAL_WTI_GAS_93];
						$filaActualizar->DAHI_PROM_MARGEN_SEMANAL_WTI_DIESEL 	= $fila[self::EXCEL_PROM_MARGEN_SEMANAL_WTI_DIESEL];
						$filaActualizar->DAHI_PROM_MARGEN_SEMANAL_WTI_GLP 		= $fila[self::EXCEL_PROM_MARGEN_SEMANAL_WTI_GLP];
						$filaActualizar->DAHI_MARGEN_PROMEDIO_GAS_87		= $fila[self::EXCEL_MARGEN_PROMEDIO_GAS_87];
						$filaActualizar->DAHI_MARGEN_PROMEDIO_GAS_93		= $fila[self::EXCEL_MARGEN_PROMEDIO_GAS_93];
						$filaActualizar->DAHI_MARGEN_PROMEDIO_DIESEL		= $fila[self::EXCEL_MARGEN_PROMEDIO_DIESEL];
						$filaActualizar->DAHI_MARGEN_PROMEDIO_GLP			= $fila[self::EXCEL_MARGEN_PROMEDIO_GLP];
						$filaActualizar->DAHI_TIPO_CRUDO					= $fila[self::EXCEL_TIPO_CRUDO];
						$filaActualizar->DAHI_FOB_TEORICO_GAS_87			= $fila[self::EXCEL_FOB_TEORICO_GAS_87];
						$filaActualizar->DAHI_FOB_TEORICO_GAS_93			= $fila[self::EXCEL_FOB_TEORICO_GAS_93];
						$filaActualizar->DAHI_FOB_TEORICO_DIESEL			= $fila[self::EXCEL_FOB_TEORICO_DIESEL];
						$filaActualizar->DAHI_FOB_TEORICO_GLP				= $fila[self::EXCEL_FOB_TEORICO_GLP];
						$filaActualizar->DAHI_PROM_REFERENCIA_SEMANAL_GAS_87	= $fila[self::EXCEL_PROM_REFERENCIA_SEMANAL_GAS_87];
						$filaActualizar->DAHI_PROM_REFERENCIA_SEMANAL_GAS_93	= $fila[self::EXCEL_PROM_REFERENCIA_SEMANAL_GAS_93];
						$filaActualizar->DAHI_PROM_REFERENCIA_SEMANAL_DIESEL	= $fila[self::EXCEL_PROM_REFERENCIA_SEMANAL_DIESEL];
						$filaActualizar->DAHI_PROM_REFERENCIA_SEMANAL_GLP		= $fila[self::EXCEL_PROM_REFERENCIA_SEMANAL_GLP];
						$filaActualizar->DAHI_CORRECCION_RVP					= $fila[self::EXCEL_CORRECCION_RVP];
						// Guardar los cambios
						$filaActualizar->save();
					}
					else {
						// SI  la fecha ya ha sido introducida en los valores de mercado, error
						$errors['fila'.$row] =  $errors['fila'.$row].'Fecha de valor de mercado ya introducida. ';
	         			$errors['fila'] = 1;
					}
				}
				// Si ha habido errores para introducir la fila
				if ($errors['fila'.$row]!= '') {
	            	array_push($errors, 'Errores en la fila '.$row.': '.$errors['fila'.$row]);
       			}
       			unset($errors['fila'.$row]);
            } // Cierre del FOR

            if(($errorSQLVM)||($errorFechas == 1))
            {
            	//SE ELIMINAN LOS REGISTROS VM ASOCIADOS
            	DB::delete('DELETE FROM MEPCO_VALOR_MERCADO WHERE VAUS_ID=?',[$vaus_id]);
            	//SE ELIMINAN LOS REGISTROS VM_US
            	DB::delete('DELETE FROM MEPCO_VAME_USPE WHERE VAUS_ID=?',[$vaus_id]);
				array_push($errors, 'Se deshabilitan los valores de mercado del Generador, por Fecha(s) Incorrecta(s)'); 
            }

        // Eliminar la marca de errores en la fila del array de errores
		unset($errors['fila']);

		return array($valores, $errors, $warnings, $vaus_id);
	} // Cierre de la función

	/**
	* Retorna true si string contiene kkey
	* @return boolean 
	*/
	public static function contains($str, $kkey){
		return (strpos($str, $kkey) !== false);
	}

	public static function quitarEspacios($input)
	{
		return trim($input);
	}

	public static function isNullReplaceZero($input)
	{
		if($input == null)
		{
			return '0.000000000000';
		}else
		{
			return $input;
		}
	}

	public static function obtenerRestriccionPorParametro($llave, $combustible, $proc_id)
	{
		$arrResult = array();
		$arrResult['MIN']      = null;
		$arrResult['MAX']      = null;
		$arrResult['INC']      = null;
		$arrResult['DISABLED'] = false;
		
		$variable = DB::table('MEPCO_RESTRICCION_PARAMETROS AS MRP')
						->join('MEPCO_PARAMETROS_VARIABLE AS MPV', 'MRP.PAVA_ID', '=', 'MPV.PAVA_ID')
						->select(
							'MRP.REPA_VALOR_MINIMO',
							'MRP.REPA_VALOR_MAXIMO',
							'MRP.REPA_INCREMENTO',
							'MRP.REPA_RESTRICCION_SEMANAS')
						->where('MPV.PAVA_LLAVE', '=', $llave)
						->first();

		if($variable)
		{
			$arrResult['MIN']      = (float)$variable->REPA_VALOR_MINIMO;
			$arrResult['MAX']      = (float)$variable->REPA_VALOR_MAXIMO;
			$arrResult['INC']      = (float)$variable->REPA_INCREMENTO;
			$arrResult['DISABLED'] = false;
		}
		return $arrResult;
	}

	public static function comprobarRestriccionSemanas()
	{
		$restriccion_semanas = DB::table('MEPCO_RESTRICCION_PARAMETROS AS MRP')
						->join('MEPCO_PARAMETROS_VARIABLE AS MPV', 'MRP.PAVA_ID', '=', 'MPV.PAVA_ID')
						->select('MPV.PAVA_LLAVE', 'MRP.REPA_RESTRICCION_SEMANAS')
						->get();

		$datos_historicos = DB::table('MEPCO_DATOS_HISTORICOS')
						   ->select('DAHI_FECHA', 'DAHI_N_GAS_87', 'DAHI_M_GAS_87', 'DAHI_S_GAS_87', 'DAHI_T_GAS_87', 'DAHI_F_GAS_87', 'DAHI_N_GAS_93', 'DAHI_M_GAS_93', 'DAHI_S_GAS_93', 'DAHI_T_GAS_93', 'DAHI_F_GAS_93', 'DAHI_N_DIESEL', 'DAHI_M_DIESEL', 'DAHI_S_DIESEL', 'DAHI_T_DIESEL', 'DAHI_F_DIESEL', 'DAHI_N_GLP', 'DAHI_M_GLP', 'DAHI_S_GLP', 'DAHI_T_GLP', 'DAHI_F_GLP')
						   ->orderBy('DAHI_FECHA', 'desc')
						   ->get();

		// Restricciones
		foreach ($restriccion_semanas as $row) {
			switch($row->PAVA_LLAVE) {
				case Util::N:
					$restriccion_n = $row->REPA_RESTRICCION_SEMANAS;
					break;
				case Util::M:
					$restriccion_m = $row->REPA_RESTRICCION_SEMANAS;
					break;
				case Util::S:
					$restriccion_s = $row->REPA_RESTRICCION_SEMANAS;
					break;
				case Util::T:
					$restriccion_t = $row->REPA_RESTRICCION_SEMANAS;
					break;
				case Util::F:
					$restriccion_f = $row->REPA_RESTRICCION_SEMANAS;
					break;
			}
		}

		if ((!$datos_historicos)||(count($datos_historicos) == 0)) {
			$resultados = 0;
		}
		else {
			/* 
			* Parámetros N
			*/
			$n_gas_87 	= self::numeroSemanas('DAHI_N_GAS_87', $restriccion_n, $datos_historicos);
			$n_gas_93 	= self::numeroSemanas('DAHI_N_GAS_93', $restriccion_n, $datos_historicos);
			$n_diesel 	= self::numeroSemanas('DAHI_N_DIESEL', $restriccion_n, $datos_historicos);
			$n_glp 		= self::numeroSemanas('DAHI_N_GLP', $restriccion_n, $datos_historicos);

			/*
			* Parámetros M
			*/
			$m_gas_87 	= self::numeroSemanas('DAHI_M_GAS_87', $restriccion_m, $datos_historicos);
			$m_gas_93 	= self::numeroSemanas('DAHI_M_GAS_93', $restriccion_m, $datos_historicos);
			$m_diesel 	= self::numeroSemanas('DAHI_M_DIESEL', $restriccion_m, $datos_historicos);
			$m_glp 		= self::numeroSemanas('DAHI_M_GLP', $restriccion_m, $datos_historicos);

			/*
			* Parámetros S
			*/
			$s_gas_87 	= self::numeroSemanas('DAHI_S_GAS_87', $restriccion_s, $datos_historicos);
			$s_gas_93 	= self::numeroSemanas('DAHI_S_GAS_93', $restriccion_s, $datos_historicos);
			$s_diesel 	= self::numeroSemanas('DAHI_S_DIESEL', $restriccion_s, $datos_historicos);
			$s_glp 		= self::numeroSemanas('DAHI_S_GLP', $restriccion_s, $datos_historicos);

			/*
			* Parámetros T
			*/
			$t_gas_87 	= self::numeroSemanas('DAHI_T_GAS_87', $restriccion_t, $datos_historicos);
			$t_gas_93 	= self::numeroSemanas('DAHI_T_GAS_93', $restriccion_t, $datos_historicos);
			$t_diesel 	= self::numeroSemanas('DAHI_T_DIESEL', $restriccion_t, $datos_historicos);
			$t_glp 		= self::numeroSemanas('DAHI_T_GLP', $restriccion_t, $datos_historicos);

			/*
			* Parámetros F
			*/
			$f_gas_87 	= self::numeroSemanas('DAHI_F_GAS_87', $restriccion_f, $datos_historicos);
			$f_gas_93 	= self::numeroSemanas('DAHI_F_GAS_93', $restriccion_f, $datos_historicos);
			$f_diesel 	= self::numeroSemanas('DAHI_F_DIESEL', $restriccion_f, $datos_historicos);
			$f_glp 		= self::numeroSemanas('DAHI_F_GLP', $restriccion_f, $datos_historicos);

			$resultados = array(
				'n_gas_87' => $n_gas_87, 
				'n_gas_93' => $n_gas_93,
				'n_diesel' => $n_diesel,
				'n_glp'    => $n_glp,
				'm_gas_87' => $m_gas_87,
				'm_gas_93' => $m_gas_93,
				'm_diesel' => $m_diesel,
				'm_glp'    => $m_glp,
				's_gas_87' => $s_gas_87,
				's_gas_93' => $s_gas_93,
				's_diesel' => $s_diesel,
				's_glp'    => $s_glp,
				't_gas_87' => $t_gas_87,
				't_gas_93' => $t_gas_93,
				't_diesel' => $t_diesel,
				't_glp'    => $t_glp,
				'f_gas_87' => $f_gas_87,
				'f_gas_93' => $f_gas_93,
				'f_diesel' => $f_diesel,
				'f_glp'    => $f_glp,
			);
		}

		return $resultados;
	}

	public static function numeroSemanas($parametro, $restriccion, $datos_historicos)
	{
		// Primera fecha
		$primer_valor = $datos_historicos[0]->$parametro;
		$fecha = $datos_historicos[0]->DAHI_FECHA;
		$num_semanas = 1;
		$siguiente_fecha = date('Y-m-d', strtotime($fecha. ' - 7 days'));

		foreach ($datos_historicos as $key=>$row) {
			if ($row->DAHI_FECHA == $siguiente_fecha) {
				$valor_parametro = $row->$parametro;
				if ($valor_parametro == $primer_valor) {
					$siguiente_fecha = date('Y-m-d', strtotime($siguiente_fecha. ' - 7 days'));
					$num_semanas++;
				}
				else{
					break;
				}
			}
		}

		if ($num_semanas >= (int)$restriccion) {
			return false;
		}
		else {
			return true;
		}
	}

	/**
	* Crea fila para log de registro
	* @return boolean 
	*/
	public static function crearFilaLog($btn, $accion, $objeto, $id, $descripcion = null){

		$log_nomenclatura 	= $btn.' '.$accion.' '.$objeto;
		$nombre_accion 		= $accion.' '.$objeto;
		
		if ($descripcion == null) {
			$descripcion_accion = $accion.' '.$objeto.' con identificador '.$id;
		}
		else {
			$descripcion_accion = $descripcion;
		}

        AppLog::create([
                'USPE_ID'                   => session(Util::USUARIO)->USPE_ID,
                'LOG_FECHA'                 => Util::dateTimeNow(),
                'LOG_NOMENCLATURA'          => $log_nomenclatura,
                'LOG_NOMBRE_ACCION'         => $nombre_accion,
                'LOG_DESCRIPCION_ACCION'    => $descripcion_accion,
                'LOG_VALOR_ACCION'          => '0',
        ]);
	}

	public static function log($mensaje)
	{
		if(self::DEBUG)
		{
			Log::debug($mensaje);
		}
	}

	// public static function x()
	// {
	// 	$arrAuxTemp = array();
	// 	if(self::$arrFormulaValores != null && count(self::$arrFormulaValores) > 0)
	// 	{
	// 	    foreach (self::$arrAuxVariablesCache as $itemCache) {
	//             if(array_key_exists($itemCache, self::$arrFormulaValores))
	//             {
	//                 $arrAuxTemp[$itemCache] = self::$arrFormulaValores[$itemCache];
	//                 break;
	// 	        }
	// 	    }    
	// 	    self::$arrFormulaValores = $arrAuxTemp;
	// 	}
	// }
}
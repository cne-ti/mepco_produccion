<?php 

namespace App\Http\Middleware;

use Closure;
use App\Util;
use App\Usuario;
use App\Perfil;

class ProtectRoutes 
{

    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Comprueba si existe la sesión del usuario
        if (!session(Util::USUARIO))
        {
            return redirect('auth/login');
        }
        else {
        // Comprueba si está activo en base de datos, ya que si
        // por ejemplo el administrador ha destruido su sesión, no debe 
        // permanecer activo
            $usua_email = session(Util::USUARIO)->USUA_EMAIL;
            $perf_id    = session(Util::USUARIO)->PERF_ID;

            $sesion = Usuario::where('USUA_EMAIL', $usua_email)
                             ->where('PERF_ID', $perf_id)
                             ->first();
                             //->where('USPE_ACTIVO', 1)
                             //->count();
            if ($sesion->USPE_ACTIVO == 0) {
                // Usuario con sesión cerrada desde bbdd
                return redirect('auth/login');
            }
            else {
                // Cambiar el valor de USPE_ULTIMA_CONEXION
                $sesion->USPE_ULTIMA_CONEXION = Util::dateTimeNow();
                $sesion->save();
                // Obtener el contenido del portal asociado al perfil
                // Comprobar los roles
                $copo_id = $this->getName($request->route());
                $perfil = Perfil::findOrFail($perf_id);
                $contenidoPortal = $perfil->contenidoPortal()->get();
                // Si el id del perfil no coincide con los obtenidos en $roles, error 
                if ((sizeof($copo_id)>0) && ($perfil->hasContent($copo_id,$contenidoPortal) == false)) {
                    return redirect('auth/contenidoNoAutorizado');
                }
            }
        }

        return $next($request);
    }

    // Obtiene el nombre del contenido del portal asociado, para comprobar si el usuario tiene permiso
    private function getName($route) {
        $actions = $route->getAction();
        return isset($actions['contenido']) ? $actions['contenido'] : null;
    }

}
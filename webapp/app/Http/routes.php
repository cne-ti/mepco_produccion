<?php
// Para traer las constantes
use App\Util;

/*oute::get('/', function () {
    return view('auth.login');
});*/

Route::get('home', ['as' => 'home','middleware' => 'authenticated', function () {
    return view('home');
}]);
Route::get('construccion', ['as' => 'construccion','middleware' => 'authenticated', function () {
    return view('construccion');
}]);
Route::get('construccionProceso', ['as' => 'construccionProceso','middleware' => 'authenticated', function () {
    return view('construccion-proceso');
}]);
Route::get('auth/contenidoNoAutorizado', ['as' => 'auth/contenidoNoAutorizado','middleware' => 'authenticated', function () {
    return view('auth.contenidoNoAutorizado');
}]);

// Authentication routes...
Route::get ('auth/login',   	  ['as' =>'auth/login',   		 'uses' => 'Auth\AuthController@getLogin']); 
Route::post('auth/authenticate',  ['as' =>'auth/authenticate',   'uses' => 'Auth\AuthController@authenticate']);
Route::get ('auth/logout',        ['as' => 'auth/logout',        'uses' => 'Auth\AuthController@getLogout']);
Route::post('auth/chooseProfile', ['as' => 'auth/chooseProfile', 'uses' => 'Auth\AuthController@chooseProfile']);
Route::get ('/', 	  			  ['as' => '/', 	 			 'uses' => 'Auth\AuthController@getLogin']);

// Usuarios
Route::get ('usuario/export-excel',            ['as' => 'usuario/exportarAExcel',           'uses' => 'usuarioController@exportarAExcel',		'contenido' => Util::CONTENIDO_PORTAL_USUARIOS]);
Route::get('usuario/export-pdf', 			   ['as' => 'usuario/exportarAPdf', 			'uses' => 'usuarioController@exportarAPdf', 		'contenido' => Util::CONTENIDO_PORTAL_USUARIOS]);
Route::get ('usuario/cerrarSesionGenerador',   ['as' => 'usuario/cerrarSesionGenerador',    'uses' => 'usuarioController@cerrarSesionGenerador','contenido' => Util::CONTENIDO_PORTAL_USUARIOS]);
Route::get ('usuario/comprobarSesionGenerador',['as' => 'usuario/comprobarSesionGenerador', 'uses' => 'usuarioController@comprobarSesionGenerador','contenido' => Util::CONTENIDO_PORTAL_USUARIOS]);
Route::resource('usuario', 'usuarioController');
Route::get ('usuario',                         ['as' => 'usuario',                          'uses' => 'usuarioController@index',				'contenido' => Util::CONTENIDO_PORTAL_USUARIOS]);
Route::post('usuario/editar',                  ['as' => 'usuario/editar',                   'uses' => 'usuarioController@editar']);
Route::post('usuario/actualizar',              ['as' => 'usuario/actualizar',               'uses' => 'usuarioController@actualizar']);
Route::post('usuario/eliminar',                ['as' => 'usuario/eliminar',                 'uses' => 'usuarioController@eliminar']);

// Perfiles
Route::get ('perfil/export-excel', ['as' => 'perfil/exportarAExcel', 'uses' => 'perfilController@exportarAExcel',		'contenido' => Util::CONTENIDO_PORTAL_PERFILES]);
Route::get ('perfil/export-pdf',   ['as' => 'perfil/exportarAPdf',   'uses' => 'perfilController@exportarAPdf',			'contenido' => Util::CONTENIDO_PORTAL_PERFILES]);
Route::resource('perfil', 'perfilController');
Route::get ('perfil/choose',       ['as' => 'perfil/choose',       'uses' => 'perfilController@choose',					'contenido' => Util::CONTENIDO_PORTAL_PERFILES]);
Route::get ('perfil',              ['as' => 'perfil',              'uses' => 'perfilController@index',					'contenido' => Util::CONTENIDO_PORTAL_PERFILES]);
Route::get ('perfil/list',         ['as' => 'perfil/list',         'uses' => 'perfilController@list',					'contenido' => Util::CONTENIDO_PORTAL_PERFILES]);
Route::post('perfil/info',         ['as' => 'perfil/info',         'uses' => 'perfilController@info']);
Route::post('perfil/cambioPerfil', ['as' => 'perfil/cambioPerfil', 'uses' => 'perfilController@cambioPerfil']);

// Proceso
Route::get ('proceso',                      ['as' => 'proceso',                      'uses' => 'procesoController@index',		   'contenido' => Util::CONTENIDO_PORTAL_CALCULO]);
Route::get ('proceso/editar/{id}',          ['as' => 'proceso/editar',               'uses' => 'procesoController@editar',		   'contenido' => Util::CONTENIDO_PORTAL_CALCULO]);
Route::get ('proceso/validadores/{id}',     ['as' => 'proceso/validadores',          'uses' => 'procesoController@validadores',	   'contenido' => Util::CONTENIDO_PORTAL_CALCULO]);
Route::get ('proceso/valor-mercado/{id}',   ['as' => 'proceso/valorMercado',         'uses' => 'procesoController@valorMercado',   'contenido' => Util::CONTENIDO_PORTAL_CALCULO]);
Route::get ('proceso/solver/{id}',          ['as' => 'proceso/solver',               'uses' => 'procesoController@solver',		   'contenido' => Util::CONTENIDO_PORTAL_CALCULO]);
Route::get ('proceso/escenarios/{id}',      ['as' => 'proceso/escenarios',           'uses' => 'procesoController@escenarios',	   'contenido' => Util::CONTENIDO_PORTAL_CALCULO]);
Route::get ('proceso/escenarios/expo/{id}', ['as' => 'proceso/escenarios/expo',      'uses' => 'procesoController@escenariosExpo', 'contenido' => Util::CONTENIDO_PORTAL_CALCULO]);
Route::get ('proceso/proyeccion/{id}',      ['as' => 'proceso/proyeccion',           'uses' => 'procesoController@proyeccion',	   'contenido' => Util::CONTENIDO_PORTAL_CALCULO]);
Route::get ('proceso/proyeccion/expo/{id}', ['as' => 'proceso/proyeccion/expo',      'uses' => 'procesoController@proyeccionExpo', 'contenido' => Util::CONTENIDO_PORTAL_CALCULO]);
Route::get ('proceso/documentos/{id}',      ['as' => 'proceso/documentos',           'uses' => 'procesoController@documentos',	   'contenido' => Util::CONTENIDO_PORTAL_CALCULO]);
Route::get ('proceso/documentos/get/{fecha}/{doc}', ['as' => 'proceso/documentosGet',  'uses' => 'procesoController@documentosGet',  'contenido' => Util::CONTENIDO_PORTAL_CALCULO]);
Route::get ('proceso/download/{id}/{name}', ['as' => 'proceso/download',             'uses' => 'procesoController@download', 	   'contenido' => Util::CONTENIDO_PORTAL_CALCULO]);
Route::get ('proceso/rollback/{id}',        ['as' => 'proceso/rollback',             'uses' => 'procesoController@rollback',       'contenido' => Util::CONTENIDO_PORTAL_CALCULO]);
Route::get ('prueba', 'procesoController@prueba');
Route::post('proceso/crearEscenarioRequest',['as' => 'proceso/crearEscenarioRequest','uses' => 'procesoController@crearEscenarioRequest']);
Route::post('proceso/sugerir',              ['as' => 'proceso/sugerir',              'uses' => 'procesoController@sugerir']);
Route::post('proceso/sugerirCalculo',       ['as' => 'proceso/sugerirCalculo',       'uses' => 'procesoController@sugerirCalculo']);
Route::post('proceso/publicarCalculo',      ['as' => 'proceso/publicarCalculo',      'uses' => 'procesoController@publicarCalculo']);
Route::post('proceso/nuevoRequest',         ['as' => 'proceso/nuevoRequest',         'uses' => 'procesoController@nuevoRequest']);
Route::post('proceso/validadoresRequest',   ['as' => 'proceso/validadoresRequest',   'uses' => 'procesoController@validadoresRequest']);
Route::post('proceso/valorMercadoRequest',  ['as' => 'proceso/valorMercadoRequest',  'uses' => 'procesoController@valorMercadoRequest']);
Route::post('proceso/reingresoVmRequest',   ['as' => 'proceso/reingresoVmRequest',   'uses' => 'procesoController@reingresoVmRequest']);
Route::post('proceso/rollbackRequest',      ['as' => 'proceso/rollbackRequest',      'uses' => 'procesoController@rollbackRequest']);
Route::post('proceso/rechazar',      		['as' => 'proceso/rechazar',      		 'uses' => 'procesoController@rechazar']);

// Parámetros - Variables
Route::get ('variable/export-excel', 			['as' => 'variable/exportarAExcel',  			'uses' => 'variableController@exportarAExcel', 	'contenido' => Util::CONTENIDO_PORTAL_VARIABLES]);
Route::get ('variable/export-pdf/{tipo}', 		['as' => 'variable/exportarAPdf',      			'uses' => 'variableController@exportarAPdf',	'contenido' => Util::CONTENIDO_PORTAL_VARIABLES]);
Route::resource('variable', 'variableController');
Route::get ('variable',           				['as' => 'variable',           					'uses' => 'variableController@index',		  	'contenido' => Util::CONTENIDO_PORTAL_VARIABLES]);
Route::post('variable/historico', 				['as' => 'variable/historico', 					'uses' => 'variableController@historico']);
Route::post('variable/ejecutarQuery', 			['as' => 'variable/ejecutarQuery', 				'uses' => 'variableController@ejecutarQuery']);
Route::post('variable/ejecutarQueryParametros', ['as' => 'variable/ejecutarQueryParametros', 	'uses' => 'variableController@ejecutarQueryParametros']);
Route::post('variable/eliminar',				['as' => 'variable/eliminar', 					'uses' => 'variableController@eliminar']);

// Fórmulas
Route::get ('formula/export-excel', 	['as' => 'formula/exportarAExcel',      'uses' => 'formulaController@exportarAExcel',		'contenido' => Util::CONTENIDO_PORTAL_FORMULAS]);
Route::get ('formula/export-pdf/{tipo}', 		['as' => 'formula/exportarAPdf',      	'uses' => 'formulaController@exportarAPdf',			'contenido' => Util::CONTENIDO_PORTAL_FORMULAS]);
Route::resource('formula', 'formulaController');
Route::get ('formula',           		['as' => 'formula',           			'uses' => 'formulaController@index', 				'contenido' => Util::CONTENIDO_PORTAL_FORMULAS]);
Route::post('formula/historico', 		['as' => 'formula/historico', 			'uses' => 'formulaController@historico']);
Route::post('formula/validar', 			['as' => 'formula/validar', 			'uses' => 'formulaController@validar']);
Route::post('formula/ejecutarFormula', 	['as' => 'formula/ejecutarFormula', 	'uses' => 'formulaController@ejecutarFormula']);
Route::post('formula/eliminar', 		['as' => 'formula/eliminar', 			'uses' => 'formulaController@eliminar']);

// Reportes
Route::get('reporte/descarga-documentos', 	['as' => 'descargaDocumentos', 			'uses' => 'reporteController@descargaDocumentos',	'contenido' => Util::CONTENIDO_PORTAL_DESCARGA_DOCUMENTOS]);
Route::get('reporte/resultados', 			['as' => 'resultados', 					'uses' => 'reporteController@resultados',			'contenido' => Util::CONTENIDO_PORTAL_RESULTADOS]);
Route::get('reporte/datos-historicos', 		['as' => 'datosHistoricos', 			'uses' => 'reporteController@datosHistoricos',		'contenido' => Util::CONTENIDO_PORTAL_DATOS_HISTORICOS]);
Route::get('reporte/datos-entrada', 		['as' => 'datosEntrada', 				'uses' => 'reporteController@datosEntrada',			'contenido' => Util::CONTENIDO_PORTAL_DATOS_ENTRADA]);
Route::get('reporte/log', 					['as' => 'log', 						'uses' => 'reporteController@log',					'contenido' => Util::CONTENIDO_PORTAL_LOG]);
Route::post('reporte/log', 					['as' => 'reporte/getLog', 				'uses' => 'reporteController@getLog']);
Route::post('reporte/exportarAExcel', 		['as' => 'reporte/exportarAExcel', 		'uses' => 'reporteController@exportarAExcel']);
Route::post('reporte/exportarAPdf', 		['as' => 'reporte/exportarAPdf',     	'uses' => 'reporteController@exportarAPdf']);
Route::post('reporte/exportarAExcelDatosHistoricos', 		['as' => 'reporte/exportarAExcelDatosHistoricos', 		'uses' => 'reporteController@exportarAExcelDatosHistoricos']);
Route::post('reporte/descarga-documentos',	['as' => 'reporte/getDescargaDocumentos','uses' => 'reporteController@getDescargaDocumentos']);
Route::post('reporte/resultados', 			['as' => 'reporte/getResultados', 		'uses' => 'reporteController@getResultados']);
Route::post('reporte/datos-historicos', 	['as' => 'reporte/getDatosHistoricos', 	'uses' => 'reporteController@getDatosHistoricos']);
Route::post('reporte/datos-entrada', 		['as' => 'reporte/getDatosEntrada', 	'uses' => 'reporteController@getDatosEntrada']);

// Administración de documentos

Route::get ('plantillaDocumento/export-excel', 		['as' => 'plantillaDocumento/exportarAExcel', 	'uses' => 'plantillaDocumentoController@exportarAExcel',	'contenido' => Util::CONTENIDO_PORTAL_DOCUMENTOS]);
Route::get ('plantillaDocumento/export-pdf', 		['as' => 'plantillaDocumento/exportarAPdf',     'uses' => 'plantillaDocumentoController@exportarAPdf',		'contenido' => Util::CONTENIDO_PORTAL_DOCUMENTOS]);
Route::resource('plantillaDocumento', 'plantillaDocumentoController');
Route::get('plantillaDocumento', 					['as' => 'plantillaDocumento', 					'uses' => 'plantillaDocumentoController@index', 	'contenido' => Util::CONTENIDO_PORTAL_DOCUMENTOS]);
Route::post('plantillaDocumento/update', 			['as' => 'plantillaDocumento/update', 			'uses' => 'plantillaDocumentoController@update']);
Route::post('plantillaDocumento/indexAjax', 		['as' => 'plantillaDocumento/indexAjax', 		'uses' => 'plantillaDocumentoController@indexAjax']);
Route::post('plantillaDocumento/generarDocumento', 	['as' => 'plantillaDocumento/generarDocumento', 'uses' => 'plantillaDocumentoController@generarDocumento']);

// Carga Histórica
Route::resource('cargaHistorica', 'cargaHistoricaController');
Route::get('cargaHistorica', 	['as' => 'cargaHistorica', 	'uses' => 'cargaHistoricaController@index', 	'contenido' => Util::CONTENIDO_PORTAL_CARGA]);
Route::get('historico', 		['as' => 'historico', 		'uses' => 'cargaHistoricaController@historico', 'contenido'	=> Util::CONTENIDO_PORTAL_CARGA]);
Route::post('cargaHistorica/descargarExcel', ['as' => 'cargaHistorica/descargarExcel', 		'uses' => 'cargaHistoricaController@descargarExcel', 'contenido'	=> Util::CONTENIDO_PORTAL_CARGA]);

// Util
Route::get('variable/formatText/{text}', 	['as' => 'variable/formatText', 'uses' => 'variableController@formatText']);
Route::get('formula/formatText/{text}', 	['as' => 'formula/formatText', 	'uses' => 'formulaController@formatText']);

// Para descargas de documentos (plantillas)
Route::get('download/{path?}/{filename}', function ($path=null, $filename) {
	$file = storage_path('app').'/'.$path.'/'.$filename;
	return response()->download($file);
});

// Solver
Route::post('solver/calcular', ['as' => 'solver/calcular',  'uses' => 'solverController@calcular', 'contenido'	=> Util::CONTENIDO_PORTAL_CALCULO]);
Route::post('solver/calcularTiempoEjecucion', ['as' => 'solver/calcularTiempoEjecucion',  'uses' => 'solverController@calcularTiempoEjecucion', 'contenido'	=> Util::CONTENIDO_PORTAL_CALCULO]);

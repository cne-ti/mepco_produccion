<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Proceso;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ValorMercadoAdjunto;
use App\Util;
use App\Solver;
use Maatwebsite\Excel\Facades\Excel;
use App\Formula;
use Validator;
use DB;
use Log;
use Illuminate\Support\MessageBag;

class procesoController extends Controller
{

    public function prueba()
    {
        // dd('WASTED! ;)');
        try {
            Mail::send('template.email-test', [], function ($m)         {
                $m->to('jbernal.jbc@gmail.com', '')->subject('Email test');
            });    
        } catch (\Exception $e) {
            Log::error('EMAIL TEST EX: ' . $e->getMessage());
        }
        
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('authenticated');
    }

    /**
     * GET
     * Muestra grilla
     *
     * @return Response
     */
    public function index()
    {
        $procesos = DB::table('MEPCO_PROCESO')
                ->leftJoin('MEPCO_PROCESO_ESTADO', 'MEPCO_PROCESO.PRES_ID', '=', 'MEPCO_PROCESO_ESTADO.PRES_ID')
                ->get();

        return view('proceso.list', compact('procesos'));
    }

    /**
     * POST
     * Crea nuevo proceso
     *
     * @return Redirect
     */
    public function nuevoRequest()
    {
        $procesos = Proceso::ExisteProcesoVigente()->get();

        if (count($procesos) > 0)
        {
            \Session::set('ALERT-TYPE', 'ERROR');
            \Session::set('ALERT-MSG', json_encode('YA EXISTE PROCESO VIGENTE, FAVOR REVISAR'));
            return redirect()->back();
        }
        else if (session(Util::USUARIO)->PERF_ID != Util::PERFIL_USUARIO_GENERADOR)
        {
            \Session::set('ALERT-TYPE', 'ERROR');
            \Session::set('ALERT-MSG', json_encode('PARA CREAR UN PROCESO DEBE TENER ASOCIADO PERFIL GENERADOR'));
            return redirect()->back();
        }

        $proceso = new Proceso;
        $proceso->PROC_FECHA_INICIO_DATO_ORIGINAL = null;
        $proceso->PROC_FECHA_FIN_DATO_ORIGINAL = null;
        $proceso->PROC_FECHA_VIGENCIA = null;
        $proceso->save();

        $proceso->usuarios()->attach($proceso->PROC_ID, ['USPE_ID' => session(Util::USUARIO)->USPE_ID,
            'PRUS_FECHA_CREACION' => Util::dateTimeNow(),
            'PRUS_FECHA_MODIFICADO' => null,
            'PRUS_FECHA_VALIDADO' => null,
            'PRUS_FECHA_ELIMINADO' => null]);

        Util::crearFilaLog('btn Nuevo Proceso', 'Crear', 'proceso', $proceso->PROC_ID);

        return redirect()->action('procesoController@validadores', [$proceso->PROC_ID]);
    }

    /**
     * GET
     * Carga vista para seleccionar validadores y fecha vigencia
     */
    public function validadores($proc_id)
    {
        $proceso = Proceso::findOrFail($proc_id);
        if ($this->puedeContinuar($proceso))
        {
            $usuariosValidadores = \App\Usuario::Perfil(Util::PERFIL_USUARIO_VALIDADOR)->vigente()->get();
            $valores['fecha'] = Util::dateFormat($proceso->PROC_FECHA_VIGENCIA, 'd/m/Y');
            $valores['validadoresSeleccionados'] = $proceso->usuarios()->where('PERF_ID', Util::PERFIL_USUARIO_VALIDADOR)->get();
            $valores['esValidador'] = (session(Util::USUARIO)->PERF_ID === Util::PERFIL_USUARIO_VALIDADOR) ? "true" : "false";
            $valores['ESTADO_ACTUAL'] = $proceso->PRES_ID;

            // Seleccionar la última fecha de datos históricos para bloquear el datepicker
            $fechaHistorico = DB::table('MEPCO_DATOS_HISTORICOS')
                            ->select('DAHI_FECHA')
                            ->orderBy('DAHI_FECHA', 'desc')
                            ->pluck('MAX(DAHI_FECHA)');

            $fechaHistorico = date('Y-m-d', strtotime($fechaHistorico. ' + 7 days'));
            $fechaHistorico = Util::dateFormat($fechaHistorico, 'd/m/Y');

            return view('proceso.selecciona-validadores', compact('proceso', 'usuariosValidadores', 'valores', 'fechaHistorico'));
        }
        else
        {
            return redirect()->back();
        }
    }

    /**
     * POST
     * Asocia fecha vigenia y validadores a proceso
     *
     * @return Response
     */
    public function validadoresRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
                    'proc_id' => 'required',
                    'fecha' => 'required',
                    'validadores' => 'required'
        ]);

        //VALIDA SI EXISTE ERROR Y REDIRECCIONA
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $proc_id = $request->input('proc_id');
        $fecha = Util::strToDate($request->input('fecha'));

        // Chequear si existe un proceso con misma fecha de vigencia
        $existeFecha = Proceso::where('PROC_FECHA_VIGENCIA', $fecha)
                        ->where('PROC_ID', '!=', $proc_id)
                        ->first();

        if ($existeFecha) {
            $errors = new MessageBag;
            $errors->add('0', 'Existe un proceso con la fecha de vigencia seleccionada. Por favor, seleccione otra fecha de vigencia.');
            return redirect()->back()->withErrors($errors);
        }

        //ACTUALIZA LOS DATOS DE PROCESO
        $proceso = Proceso::findOrFail($request->input('proc_id'));
        $proceso->PROC_FECHA_VIGENCIA = Util::strToDate($request->input('fecha'));
        $proceso->PRES_ID = Util::PROCESO_ESTADO_INICIADO;
        $proceso->save();

        //ELIMINA REGISTROS DE VALIDADORES ASOCIADOS EN CASO DE EXISTIR
        $validadoresExistentes = $proceso->usuarios()->where('PERF_ID', Util::PERFIL_USUARIO_VALIDADOR)->get();
        foreach ($validadoresExistentes as $item)
        {
            DB::delete("DELETE FROM MEPCO_PROC_USPE WHERE PROC_ID=" . $proceso->PROC_ID . " AND USPE_ID=" . $item->USPE_ID);
        }

        //INSERTA LOS CAMPOS DE TABLA PIVOT
        foreach ($request->input('validadores') as $item)
        {
            DB::insert('INSERT INTO MEPCO_PROC_USPE (PROC_ID, USPE_ID, PRUS_FECHA_CREACION) VALUES(?,?,?)', [
                $proceso->PROC_ID,
                $item,
                Util::dateTimeNow()
            ]);
        }

        Util::crearFilaLog('btn Siguiente', 'Asignar', 'validadores', '', 'Asignar validadores al proceso '.$proceso->PROC_ID);

        return redirect()->action('procesoController@valorMercado', [$proceso->PROC_ID]);
    }

    /**
     * GET
     * Carga vista valores de mercado
     */
    public function valorMercado($proc_id)
    {
        $proceso = Proceso::findOrFail($proc_id);
        if ($this->puedeContinuar($proceso))
        {
            //OBTIENE VAUS_ID 
            $oVameUspe = DB::table('MEPCO_VAME_USPE AS MVU')
                    ->join('MEPCO_USUA_PERF AS MUP', 'MVU.USPE_ID', '=', 'MUP.USPE_ID')
                    ->select('MVU.VAUS_ID')
                    ->where('MVU.PROC_ID', '=', $proc_id)
                    ->where('MUP.PERF_ID', '=', Util::PERFIL_USUARIO_GENERADOR)
                    ->where('MVU.VAUS_ELIMINADO', '=', '0')
                    ->first();
            $vaus_id = ($oVameUspe) ? $oVameUspe->VAUS_ID : null;

            //OBTIENE ADJUNTOS EN CASO DE EXISTIR
            $adjuntos = DB::table('MEPCO_VALOR_MERCADO_ADJUNTO AS VMA')
                    ->select('*')
                    ->join('MEPCO_VAME_USPE AS MVU', 'VMA.VAUS_ID', '=', 'MVU.VAUS_ID')
                    ->join('MEPCO_USUA_PERF AS MUP', 'MVU.USPE_ID', '=', 'MUP.USPE_ID')
                    ->where('MVU.VAUS_ID', '=', $vaus_id)
                    ->where('MVU.VAUS_ELIMINADO', '=', '0')
                    ->where('MUP.PERF_ID', '=', Util::PERFIL_USUARIO_GENERADOR)
                    ->get();

            foreach ($adjuntos as $item)
            {
                $item->ARCH_LINK = route('proceso/download', [$item->ARCH_ID, $item->ARCH_NOMBRE]);
            }

            $adjuntos = ($adjuntos) ? json_encode($adjuntos) : null;

            $procesoValidado = DB::table('MEPCO_VAME_USPE')
                    ->select('VAUS_VALIDACION_CORRECTA')
                    ->where('PROC_ID', '=', $proceso->PROC_ID)
                    ->where('VAUS_ELIMINADO', '=', '0')
                    ->orderBy('VAUS_ID', 'DESC')
                    ->first();

            if ($procesoValidado != null)
            {
                $valores['valoresMercadoValidadoOk'] = ($procesoValidado->VAUS_VALIDACION_CORRECTA == 1);
            }
            else
            {
                $valores['valoresMercadoValidadoOk'] = false;
            }
            $valores['listValorMercadoAdjunto'] = $adjuntos;
            $valores['perfilUsuario'] = session(Util::USUARIO)->PERF_ID;
            $valores['ESTADO_ACTUAL'] = $proceso->PRES_ID;
            return view('proceso.valor-mercado', compact('proceso', 'valores'));
        }
        else
        {
            return redirect()->back();
        }
    }

    /**
     * POST
     * Lee VM y almacena valores en BD
     * Almacena VM y documentos asociados
     *
     * @return Response
     */
    public function valorMercadoRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
                    'proc_id' => 'required',
                    'valorMercado' => 'required',
                    'documentos' => 'required'
        ]);

        //VALIDA SI EXISTE ERROR Y REDIRECCIONA
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $fileValida = $request->file('valorMercado');
            if($fileValida->getClientSize() > Util::MAX_FILE_SIZE)
            {
                return redirect()->back()->withErrors(['Documento "' . $fileValida->getClientOriginalName() . '" supera el tamaño maximo permitido'])->withInput();
            }else{
                $documentosValida = $request->file('documentos');
                foreach ($documentosValida as $itemValida)
                {
                    if($itemValida != null){
                        if($itemValida->getClientSize() > Util::MAX_FILE_SIZE || $itemValida->getClientSize() == 0)
                        {
                            return redirect()->back()->withErrors(['Documento "' . $itemValida->getClientOriginalName() . '" supera el tamaño maximo permitido'])->withInput();
                        }    
                    }
                }
            }
        }

        $proc_id = $request->input('proc_id');

        //OBTIENE PROCESO
        $proceso = Proceso::findOrFail($proc_id);

        //OBTIENE PRUS_ID DE USUARIO LOGUEADO
        $oProcUspe = DB::table('MEPCO_PROC_USPE')
                ->select('PRUS_ID')
                ->where('PROC_ID', '=', $request->input('proc_id'))
                ->where('USPE_ID', '=', session(Util::USUARIO)->USPE_ID)
                ->orderBy('PRUS_ID', 'DESC')
                ->first();

        $prus_id = ($oProcUspe) ? $oProcUspe->PRUS_ID : null;

        //OBTIENE VAUS_ID VIGENTE FILTRADO POR PROCESO
        $oVameUspe = DB::table('MEPCO_VAME_USPE')
                ->select('VAUS_ID')
                ->where('PROC_ID', '=', $proc_id)
                ->where('VAUS_ELIMINADO', '=', 0)
                ->orderBy('VAUS_ID', 'DESC')
                ->first();

        $vaus_id = ($oVameUspe) ? $oVameUspe->VAUS_ID : null;

        //OBTIENE VM INGRESADOS POR GENERADOR
        $valoresMercadoGenerador = DB::table('MEPCO_VALOR_MERCADO AS MVM')
                ->join('MEPCO_VAME_USPE AS MVU', 'MVM.VAUS_ID', '=', 'MVU.VAUS_ID')
                ->join('MEPCO_USUA_PERF AS MUP', 'MVU.USPE_ID', '=', 'MUP.USPE_ID')
                ->select('MVM.*')
                ->where('MVU.PROC_ID', '=', $proc_id)
                ->where('MVU.VAUS_ID', '=', $vaus_id)
                ->where('MUP.PERF_ID', '=', Util::PERFIL_USUARIO_GENERADOR)
                ->where('MVU.VAUS_ELIMINADO', '=', 0)
                ->orderBy('MVM.VAME_FECHA', 'DESC')
                ->get();

        //EXCEL VALORES DE MERCADO
        $file   = $request->file('valorMercado');
        
        $reader = Excel::load($file, function($reader){});

        $resultado = Util::procesarDatos($reader, Util::PROCESAR_VARIABLES_MERCADO, $proceso->PROC_ID, $vaus_id);
        $valoresIngresados = $resultado[0];
        $errors    = $resultado[1];
        $warnings  = $resultado[2];
        $vaus_id   = $resultado[3];

        if (count($errors) > 0)
        {
            return redirect()->back()->withErrors($errors)->withInput();
        }

        //PROCESA ARCHIVOS VM 
        $nombre       = $file->getClientOriginalName();
        $strNombreAux = Util::formatFileName($nombre);
        $path         = Util::getPath('VALOR_MERCADO', $proceso->PROC_FECHA_VIGENCIA) . $strNombreAux;
        \Storage::put(
                $path, \File::get($file)
        );

        $valorMercadoAdjunto = new ValorMercadoAdjunto();
        $valorMercadoAdjunto->VAUS_ID          = $vaus_id;
        $valorMercadoAdjunto->ARCH_NOMBRE      = $strNombreAux;
        $valorMercadoAdjunto->ARCH_DESCRIPCION = 'INGRESADO POR ' . (session(Util::USUARIO)->PERF_ID == Util::PERFIL_USUARIO_GENERADOR) ? 'GENERADOR' : 'VALIDADOR';
        $valorMercadoAdjunto->ARCH_MIMETYPE    = $file->getMimeType();
        $valorMercadoAdjunto->ARCH_PATH        = $path;
        $valorMercadoAdjunto->save();

        //DOCUMENTOS ASOCIADOS
        $documentos = $request->file('documentos');
        foreach ($documentos as $item)
        {
            if ($item != null)
            {
                $nombre = $item->getClientOriginalName();
                $strNombreAux = Util::formatFileName($nombre);
                $path = Util::getPath('DOCUMENTOS_RESPALDO', $proceso->PROC_FECHA_VIGENCIA) . $strNombreAux;
                \Storage::put(
                    $path, \File::get($item)
                );

                $valorMercadoAdjunto = new ValorMercadoAdjunto();
                $valorMercadoAdjunto->VAUS_ID = $vaus_id;
                $valorMercadoAdjunto->ARCH_NOMBRE = $strNombreAux;
                $valorMercadoAdjunto->ARCH_DESCRIPCION = 'INGRESADO POR ' . (session(Util::USUARIO)->PERF_ID == Util::PERFIL_USUARIO_GENERADOR) ? 'GENERADOR' : 'VALIDADOR';
                $valorMercadoAdjunto->ARCH_MIMETYPE = $item->getMimeType();
                $valorMercadoAdjunto->ARCH_PATH = $path;
                $valorMercadoAdjunto->save();
            }
        }

        //SI USUARIO LOGUEADO ES GENERADOR
        if (session(Util::USUARIO)->PERF_ID == Util::PERFIL_USUARIO_GENERADOR)
        {

            $PROC_FECHA_INICIO_DATO_ORIGINAL = null;
            $PROC_FECHA_FIN_DATO_ORIGINAL    = null;

            $auxLen = count($valoresIngresados);

            if($auxLen == 5){
                $PROC_FECHA_INICIO_DATO_ORIGINAL = $valoresIngresados[0]->VAME_FECHA;
                $PROC_FECHA_FIN_DATO_ORIGINAL    = $valoresIngresados[4]->VAME_FECHA;
            }

            //ACTUALIZA ESTADO 
            $proceso->PROC_FECHA_INICIO_DATO_ORIGINAL = $PROC_FECHA_INICIO_DATO_ORIGINAL;
            $proceso->PROC_FECHA_FIN_DATO_ORIGINAL    = $PROC_FECHA_FIN_DATO_ORIGINAL;
            $proceso->PRES_ID                         = Util::PROCESO_ESTADO_INGRESO_VALORES_MERCADO;
            $proceso->save();

            //ASOCIA USUARIOS A PROC
            DB::update('UPDATE MEPCO_PROC_USPE SET PRUS_FECHA_MODIFICADO=? WHERE PRUS_ID=?', [
                Util::dateTimeNow(),
                $prus_id
            ]);

            //ENVIA EMAIL A VALIDADORES ASOCIADOS AL PROCESO
            $validadores = DB::table('MEPCO_USUA_PERF AS MUP')
                    ->join('MEPCO_PROC_USPE AS MPU', 'MUP.USPE_ID', '=', 'MPU.USPE_ID')
                    ->where('MUP.PERF_ID', '=', Util::PERFIL_USUARIO_VALIDADOR)
                    ->where('MPU.PROC_ID', '=', $proc_id)
                    ->where('MUP.USPE_ELIMINADO', '=', 0)
                    ->select('MUP.USUA_EMAIL')
                    ->distinct()
                    ->get();

            $fechaVigencia = $proceso->PROC_FECHA_VIGENCIA;
            $fechaVigencia = Util::dateFormat($fechaVigencia, 'd/m/Y');
            $listEmailValidadores = null;
            foreach ($validadores as $item)
            {
                $param = [
                    'generador' => session(Util::USUARIO)->USUA_EMAIL,
                    'validador' => $item->USUA_EMAIL,
                    'fechaVigencia' => $fechaVigencia
                ];
                Mail::send('template.email-validador', $param, function ($m) use ($item)
                {
                    $m->to($item->USUA_EMAIL, '')->subject('MEPCO: Notificación validador');
                });
                $listEmailValidadores[] = $item->USUA_EMAIL;
            }

            Util::crearFilaLog('btn Ingresar valores de mercado', 'Usuario generador ingresa', 'valores de mercado a proceso', $proceso->PROC_ID);

            return view('proceso.valor-mercado-resultado-generador', compact('proceso', 'warnings', 'listEmailValidadores'));
        }
        else if (session(Util::USUARIO)->PERF_ID == Util::PERFIL_USUARIO_VALIDADOR)
        {
            
            //Almacena las listas con indice fecha
            $listaValidacion = array();

            //VALIDA VM INGRESADOS CONTRA LOS VM INGRESADOS POR GENERADOR
            foreach ($valoresMercadoGenerador as $vmGenerador)
            {
				foreach ($valoresIngresados as $vmValidador)
                {
                    if ($vmGenerador->VAME_FECHA == $vmValidador->VAME_FECHA)
                    {
                        //Almacena cada celda con error
                        $camposConError = array();
                        $auxCampoConError = null;
                        $auxTemplate = '{{FIELD}}:<br />&nbsp;&nbsp; &nbsp; &nbsp;- valor ingresado por generador: {{VAL_GENERADOR}}<br />&nbsp;&nbsp; &nbsp; &nbsp;- valor ingresado por validador&nbsp;&nbsp;:&nbsp;{{VAL_VALIDADOR}}';

                        if (round($vmGenerador->VAME_GAS_UNL_87, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_GAS_UNL_87), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_GAS_UNL_87', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_GAS_UNL_87), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_GAS_UNL_87), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_GAS_UNL_93, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_GAS_UNL_93), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_GAS_UNL_93', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_GAS_UNL_93), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_GAS_UNL_93), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_LD_DIESEL, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_LD_DIESEL), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_LD_DIESEL', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_LD_DIESEL), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_LD_DIESEL), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_USG_BUTANO, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_USG_BUTANO), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_USG_BUTANO', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_USG_BUTANO), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_USG_BUTANO), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_UKC_USG, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_UKC_USG), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_UKC_USG', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_UKC_USG), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_UKC_USG), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_BRENT, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_BRENT), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_BRENT', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_BRENT), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_BRENT), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_BRENT_M1, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_BRENT_M1), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_BRENT_M1', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_BRENT_M1), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_BRENT_M1), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_BRENT_M2, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_BRENT_M2), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_BRENT_M2', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_BRENT_M2), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_BRENT_M2), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_BRENT_M3, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_BRENT_M3), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_BRENT_M3', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_BRENT_M3), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_BRENT_M3), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_BRENT_M4, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_BRENT_M4), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_BRENT_M4', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_BRENT_M4), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_BRENT_M4), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_BRENT_M5, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_BRENT_M5), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_BRENT_M5', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_BRENT_M5), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_BRENT_M5), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_BRENT_M6, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_BRENT_M6), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_BRENT_M6', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_BRENT_M6), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_BRENT_M6), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_PPT_USG, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_PPT_USG), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_PPT_USG', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_PPT_USG), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_PPT_USG), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_MONT_BELVIEU, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_MONT_BELVIEU), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_MONT_BELVIEU', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_MONT_BELVIEU), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_MONT_BELVIEU), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_CIF_ARA, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_CIF_ARA), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_CIF_ARA', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_CIF_ARA), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_CIF_ARA), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_WTI, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_WTI), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_WTI', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_WTI), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_WTI), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_WTI_M1, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_WTI_M1), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_WTI_M1', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_WTI_M1), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_WTI_M1), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_WTI_M2, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_WTI_M2), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_WTI_M2', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_WTI_M2), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_WTI_M2), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_WTI_M3, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_WTI_M3), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_WTI_M3', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_WTI_M3), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_WTI_M3), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_WTI_M4, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_WTI_M4), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_WTI_M4', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_WTI_M4), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_WTI_M4), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_WTI_M5, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_WTI_M5), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_WTI_M5', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_WTI_M5), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_WTI_M5), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_WTI_M6, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_WTI_M6), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_WTI_M6', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_WTI_M6), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_WTI_M6), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_TC_DOLAR, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_TC_DOLAR), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_TC_DOLAR', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_TC_DOLAR), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_TC_DOLAR), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_TC_PUBLICADO, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_TC_PUBLICADO), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_TC_PUBLICADO', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_TC_PUBLICADO), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_TC_PUBLICADO), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_LIBOR, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_LIBOR), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_LIBOR', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_LIBOR), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_LIBOR), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_TARIFA_DIARIA_82000, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_TARIFA_DIARIA_82000), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_TARIFA_DIARIA_82000', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_TARIFA_DIARIA_82000), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_TARIFA_DIARIA_82000), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_TARIFA_DIARIA_59000, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_TARIFA_DIARIA_59000), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_TARIFA_DIARIA_59000', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_TARIFA_DIARIA_59000), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_TARIFA_DIARIA_59000), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_IFO_380_HOUSTON, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_IFO_380_HOUSTON), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_IFO_380_HOUSTON', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_IFO_380_HOUSTON), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_IFO_380_HOUSTON), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_MDO_HOUSTON, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_MDO_HOUSTON), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_MDO_HOUSTON', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_MDO_HOUSTON), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_MDO_HOUSTON), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_IFO_380_CRISTOBAL, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_IFO_380_CRISTOBAL), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_IFO_380_CRISTOBAL', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_IFO_380_CRISTOBAL), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_IFO_380_CRISTOBAL), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_MDO_CRISTOBAL, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_MDO_CRISTOBAL), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_MDO_CRISTOBAL', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_MDO_CRISTOBAL), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_MDO_CRISTOBAL), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        if (round($vmGenerador->VAME_UTM, 10) != round(Util::isNullReplaceZero($vmValidador->VAME_UTM), 10))
                        {
                            $auxCampoConError = $auxTemplate;
                            $auxCampoConError = str_replace('{{FIELD}}', 'VAME_UTM', $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_GENERADOR}}', Util::formatNumber($vmGenerador->VAME_UTM), $auxCampoConError);
                            $auxCampoConError = str_replace('{{VAL_VALIDADOR}}', Util::formatNumber($vmValidador->VAME_UTM), $auxCampoConError);
                            $camposConError[] = $auxCampoConError;
                        }

                        $listaValidacion[Util::dateFormat($vmValidador->VAME_FECHA, 'd/m/Y')] = $camposConError;
                    }
                }
            }

            $validadoOk = false;
            if (count($listaValidacion) == Util::CANTIDAD_FILAS_VALOR_MERCADO)
            {
                $cantOk = 0;
                foreach ($listaValidacion as $key => $value)
                {
                    if (count($value) == 0)
                    {
                        $cantOk++;
                    }
                }
                if ($cantOk == Util::CANTIDAD_FILAS_VALOR_MERCADO)
                {
                    $validadoOk = true;
                }
            }else{
                $listaValidacion = array('N/A' => array('Fechas ingresadas no corresponden al proceso actual'));
            }

            //OBTIENE EMAIL GENERADOR
            $emailGenerador = DB::table('MEPCO_USUA_PERF AS MUP')
                    ->join('MEPCO_VAME_USPE AS MVU', 'MUP.USPE_ID', '=', 'MVU.USPE_ID')
                    ->select('MUP.USUA_EMAIL')
                    ->where('MVU.PROC_ID', '=', $proceso->PROC_ID)
                    ->first();

            $template = "";

            if ($validadoOk)
            {//VALIDADO OK
                $proceso->PRES_ID = Util::PROCESO_ESTADO_VALOR_DE_MERCADO_VALIDADO;
                $proceso->save();

                //ASOCIA USUARIOS A PROC
                DB::update('UPDATE MEPCO_PROC_USPE SET PRUS_FECHA_MODIFICADO=? WHERE PRUS_ID=?', [
                    Util::dateTimeNow(),
                    $prus_id
                ]);

                //ACTUALIZA VALIDADO VALOR MERCADO
                DB::update('UPDATE MEPCO_VAME_USPE SET VAUS_FECHA_VALIDADO=?, VAUS_VALIDACION_CORRECTA=1 WHERE VAUS_ID=?', [
                    Util::dateTimeNow(),
                    $vaus_id
                ]);

                Util::crearFilaLog('Validar valores de mercado', 'Usuario validador valida con resultado OK', 'valores de mercado de proceso', $proceso->PROC_ID);

                $template = 'template.email-generador-success';
                $param = [
                    'generador' => $emailGenerador->USUA_EMAIL,
                    'validador' => session(Util::USUARIO)->USUA_EMAIL
                ];
            }
            else
            {//NO VALIDADO
                //ACTUALIZA A ELIMINADO VALOR MERCADO
                DB::update('UPDATE MEPCO_VAME_USPE SET USPE_ID=?, VAUS_FECHA_ELIMINADO=?, VAUS_ELIMINADO=1 WHERE VAUS_ID=?', [
                    session(Util::USUARIO)->USPE_ID,
                    Util::dateTimeNow(),
                    $vaus_id
                ]);

                DB::update('UPDATE MEPCO_ESCENARIO SET 
                                ESCE_FECHA_ELIMINADO=?, 
                                ESCE_ELIMINADO=1 
                            WHERE PROC_ID=?', [
                        Util::dateTimeNow(), 
                        $proc_id]);

                $proceso->PRES_ID = Util::PROCESO_ESTADO_VALOR_DE_MERCADO_NO_VALIDADO;
                $proceso->save();

                //ASOCIA USUARIOS A PROC
                DB::update('UPDATE MEPCO_PROC_USPE SET PRUS_FECHA_MODIFICADO=? WHERE PRUS_ID=?', [
                    Util::dateTimeNow(),
                    $prus_id
                ]);

                Util::crearFilaLog('Validar valores de mercado', 'Usuario validador valida con resultado NO validado', 'valores de mercado de proceso', $proceso->PROC_ID);

                $template = 'template.email-generador-error';
                $param = [
                    'generador' => $emailGenerador->USUA_EMAIL,
                    'validador' => session(Util::USUARIO)->USUA_EMAIL,
                    'listaError' => $listaValidacion
                ];
            }

            //ENVIAR EMAIL SUCCESS / ERROR SEGUN CORRESPONDA
            Mail::send($template, $param, function ($m) use ($emailGenerador)
            {
                $m->to($emailGenerador->USUA_EMAIL, '')->subject('MEPCO: Notificación generador');
            });

            $listaError = $listaValidacion;
            return view('proceso.valor-mercado-resultado-validador', compact('emailGenerador', 'validadoOk', 'listaError', 'proceso'));
        }
    }

    /**
     * POST
     * Elimina los documentos asociados y vuelve proceso 
     * a estado INICIADO
     *
     * @return Response
     */
    public function reingresoVmRequest(Request $request)
    {
        $proc_id = $request->input('proc_id');
        $proceso = Proceso::findOrFail($proc_id);

        $aux = DB::table('MEPCO_VAME_USPE')
                ->select('VAUS_ID')
                ->where('PROC_ID', '=', $proc_id)
                ->orderBy('VAUS_ID', 'DESC')
                ->first();

        DB::update('UPDATE MEPCO_VAME_USPE SET 
                    VAUS_FECHA_ELIMINADO=?, 
                    VAUS_ELIMINADO=1 
                    WHERE VAUS_ID=?', [
                Util::dateTimeNow(), 
                $aux->VAUS_ID
        ]);

        DB::update('UPDATE MEPCO_ESCENARIO SET 
                    ESCE_FECHA_ELIMINADO=?, 
                    ESCE_ELIMINADO=1 
                    WHERE PROC_ID=?', [
            Util::dateTimeNow(), 
            $proc_id
        ]);

        $proceso->PRES_ID = Util::PROCESO_ESTADO_INICIADO;
        $proceso->save();

        Util::crearFilaLog('Reingresar valores de mercado', 'Usuario selecciona reingresar', 'valores de mercado de proceso', $proceso->PROC_ID);

        return redirect()->action('procesoController@valorMercado', [$proceso->PROC_ID]);
    }

    public function solver($proc_id)
    {
        $proceso = Proceso::findOrFail($proc_id);

        $parametros_93      = null;
        $parametros_97      = null;
        $parametros_diesel  = null;
        $parametros_glp     = null;

        // Obtener los valores de los últimos n, m, s, t, f utilizados en un proceso
        $parametros_93      = Solver::obtenerParametros(Util::COMBUSTIBLE_93, $proc_id);
        $parametros_97      = Solver::obtenerParametros(Util::COMBUSTIBLE_97, $proc_id);
        $parametros_diesel  = Solver::obtenerParametros(Util::COMBUSTIBLE_DIESEL, $proc_id);
        $parametros_glp     = Solver::obtenerParametros(Util::COMBUSTIBLE_GLP, $proc_id);

        // Obtener todos los datos de solver para el proceso
        $paridad = DB::table('MEPCO_SOLVER AS S')
                    ->select('S.SOLV_PRECIO_PARIDAD_MIN', 'S.SOLV_PRECIO_PARIDAD_MAX', 'S.SOLV_PRECIO_REFERENCIA_MIN', 'S.SOLV_PRECIO_REFERENCIA_MAX', 'P.SOLP_T', 'P.HIDR_ID', 'P.SOLP_PARIDAD')
                    ->join('MEPCO_SOLVER_PARIDAD AS P', 'S.SOLV_ID', '=', 'P.SOLV_ID')
                    ->where('PROC_ID', $proc_id)
                    ->get();   

        $referencia = DB::table('MEPCO_SOLVER AS S')
                    ->select('S.SOLV_PRECIO_PARIDAD_MIN', 'S.SOLV_PRECIO_PARIDAD_MAX', 'S.SOLV_PRECIO_REFERENCIA_MIN', 'S.SOLV_PRECIO_REFERENCIA_MAX', 'R.SOLR_N', 'R.SOLR_M', 'R.SOLR_F', 'R.SOLR_S', 'R.SOLR_CRUDO', 'R.SOLR_MARGEN', 'R.SOLR_PREFINF', 'R.SOLR_PREFINT', 'R.SOLR_PREFSUP', 'R.HIDR_ID')
                    ->join('MEPCO_SOLVER_REFERENCIA AS R', 'S.SOLV_ID', '=', 'R.SOLV_ID')
                    ->where('PROC_ID', $proc_id)
                    ->get();

        $rango = DB::table('MEPCO_RESTRICCION_PARAMETROS AS R')
                    ->select('R.PAVA_ID', 'R.REPA_VALOR_MINIMO', 'R.REPA_VALOR_MAXIMO', 'R.REPA_INCREMENTO', 'V.PAVA_LLAVE')
                    ->join('MEPCO_PARAMETROS_VARIABLE AS V', 'R.PAVA_ID', '=', 'V.PAVA_ID')
                    ->get();

        foreach ($rango as $key=>$parametro) {
            $parametro->REPA_VALOR_MINIMO   = (float)$parametro->REPA_VALOR_MINIMO;
            $parametro->REPA_VALOR_MAXIMO   = (float)$parametro->REPA_VALOR_MAXIMO;
            $parametro->REPA_INCREMENTO     = (float)$parametro->REPA_INCREMENTO;

            if ($parametro->PAVA_LLAVE == '{{N}}') {
                $rango_n = $parametro;
            }
            else if ($parametro->PAVA_LLAVE == '{{M}}') {
                $rango_m= $parametro;
            }
            else if ($parametro->PAVA_LLAVE == '{{S}}') {
                $rango_s = $parametro;
            }
            else if ($parametro->PAVA_LLAVE == '{{T}}') {
                $rango_t = $parametro;
            }
            else if ($parametro->PAVA_LLAVE == '{{F}}') {
                $rango_f = $parametro;
            }
        }

        $restriccion_semanas = json_encode(Util::comprobarRestriccionSemanas());

        $fecha_vigencia      = Util::dateFormat($proceso->PROC_FECHA_VIGENCIA, 'd/m/Y');

        $procesoValidado = DB::table('MEPCO_VAME_USPE')
                    ->select('VAUS_VALIDACION_CORRECTA')
                    ->where('PROC_ID', '=', $proc_id)
                    ->where('VAUS_ELIMINADO', '=', '0')
                    ->orderBy('VAUS_ID', 'DESC')
                    ->first();
        
        if(session(Util::USUARIO)->PERF_ID == Util::PERFIL_USUARIO_GENERADOR)
        {
            $puedeCalcularPublicar = ($procesoValidado != null);
        }else
        {
            $puedeCalcularPublicar = ($procesoValidado != null && $procesoValidado->VAUS_VALIDACION_CORRECTA == '1');
        }

        return view('proceso.solver', compact('proceso', 'parametros_93', 'parametros_97', 'parametros_diesel', 'parametros_glp', 'paridad', 'referencia', 'rango_n', 'rango_m', 'rango_s', 'rango_t', 'rango_f', 'restriccion_semanas', 'fecha_vigencia', 'puedeCalcularPublicar'));
    }

    private function obtenerParametros($combustible, $proc_id)
    {
        $parametros = DB::table('MEPCO_ESCENARIO_CALCULO AS MEC')
            ->select('MEC.ESCA_N', 'MEC.ESCA_M', 'MEC.ESCA_S', 'MEC.ESCA_T', 'MEC.ESCA_F', 'ESCA_CORRECCION_RVP')
            ->join('MEPCO_ESCENARIO AS ME', 'MEC.ESCE_ID', '=', 'ME.ESCE_ID')
            ->join('MEPCO_PROCESO AS P', 'P.PROC_ID', '=', 'ME.PROC_ID')
            ->where('MEC.HIDR_ID', '=', $combustible)
            ->where('P.PROC_ID', $proc_id)
            ->orderBy('MEC.ESCA_ID', 'DESC')
            ->first();

        if($parametros == null)
        {
            switch ($combustible) {
                case Util::COMBUSTIBLE_93:
                    $parametros = DB::table('MEPCO_DATOS_HISTORICOS')
                        ->select(
                            'DAHI_N_GAS_87 AS ESCA_N', 
                            'DAHI_M_GAS_87 AS ESCA_M', 
                            'DAHI_S_GAS_87 AS ESCA_S',
                            'DAHI_T_GAS_87 AS ESCA_T', 
                            'DAHI_F_GAS_87 AS ESCA_F',
                            'DAHI_CORRECCION_RVP AS ESCA_CORRECCION_RVP')
                        ->take(1)
                        ->orderBy('DAHI_ID', 'DESC')
                        ->first();
                    break;
                case Util::COMBUSTIBLE_97:
                    $parametros = DB::table('MEPCO_DATOS_HISTORICOS')
                        ->select(
                            'DAHI_N_GAS_93 AS ESCA_N', 
                            'DAHI_M_GAS_93 AS ESCA_M', 
                            'DAHI_S_GAS_93 AS ESCA_S',
                            'DAHI_T_GAS_93 AS ESCA_T', 
                            'DAHI_F_GAS_93 AS ESCA_F',
                            'DAHI_CORRECCION_RVP AS ESCA_CORRECCION_RVP')
                        ->take(1)
                        ->orderBy('DAHI_ID', 'DESC')
                        ->first();
                    break;
                case Util::COMBUSTIBLE_DIESEL:
                    $parametros = DB::table('MEPCO_DATOS_HISTORICOS')
                        ->select(
                            'DAHI_N_DIESEL AS ESCA_N', 
                            'DAHI_M_DIESEL AS ESCA_M', 
                            'DAHI_S_DIESEL AS ESCA_S',
                            'DAHI_T_DIESEL AS ESCA_T', 
                            'DAHI_F_DIESEL AS ESCA_F')
                        ->take(1)
                        ->orderBy('DAHI_ID', 'DESC')
                        ->first();
                    break;
                case Util::COMBUSTIBLE_GLP;
                    $parametros = DB::table('MEPCO_DATOS_HISTORICOS')
                        ->select(
                            'DAHI_N_GLP AS ESCA_N', 
                            'DAHI_M_GLP AS ESCA_M', 
                            'DAHI_S_GLP AS ESCA_S',
                            'DAHI_T_GLP AS ESCA_T', 
                            'DAHI_F_GLP AS ESCA_F')
                        ->take(1)
                        ->orderBy('DAHI_ID', 'DESC')
                        ->first();
                    break;
            }
            
        }

        return $parametros;
    }

    public function escenariosExpo($proc_id){
        $listEscenarios = DB::table('MEPCO_ESCENARIO_CALCULO AS MEC')
            ->join('MEPCO_ESCENARIO AS ME', 'MEC.ESCE_ID', '=', 'ME.ESCE_ID')
            ->select(
                'MEC.ESCA_ID', 
                'MEC.ESCE_ID', 
                'MEC.HIDR_ID', 
                'MEC.ESCA_N', 
                'MEC.ESCA_M', 
                'MEC.ESCA_S', 
                'MEC.ESCA_T', 
                'MEC.ESCA_F', 
                'MEC.ESCA_PARIDAD', 
                'MEC.ESCA_CRUDO_BRENT',
                'MEC.ESCA_CRUDO_WTI', 
                'MEC.ESCA_TIPO_CRUDO', 
                'MEC.ESCA_MARGEN', 
                'MEC.ESCA_PREFINF', 
                'MEC.ESCA_PREFINT', 
                'MEC.ESCA_PREFSUP', 
                'MEC.ESCA_SUGERIR', 
                'MEC.ESCA_PUBLICADO')
            ->where('ME.PROC_ID', '=', $proc_id)
            ->where('ME.ESCE_ELIMINADO', '=', 0)
            ->orderBy('MEC.ESCA_ID', 'ASC')
            ->get();

        //OBTIENE LISTA ESCENARIOS
        $listEscenarios93     = array();
        $listEscenarios97     = array();
        $listEscenariosDIESEL = array();
        $listEscenariosGLP    = array();

        foreach ($listEscenarios as $item)
        {
            if ($item->HIDR_ID == Util::COMBUSTIBLE_93)
            {
                $listEscenarios93[] = $item;
            }
            else if ($item->HIDR_ID == Util::COMBUSTIBLE_97)
            {
                $listEscenarios97[] = $item;
            }
            else if ($item->HIDR_ID == Util::COMBUSTIBLE_DIESEL)
            {
                $listEscenariosDIESEL[] = $item;
            }
            else
            {
                $listEscenariosGLP[] = $item;
            }
        }

        $listEscenarios['93']     = $listEscenarios93;
        $listEscenarios['97']     = $listEscenarios97;
        $listEscenarios['DIESEL'] = $listEscenariosDIESEL;
        $listEscenarios['GLP']    = $listEscenariosGLP;

        Excel::create('ESCENARIOS', function($excel) use($listEscenarios) {
            $excel->sheet('93', function($sheet) use($listEscenarios) {
                $sheet->loadView('proceso.exportar.escenarios', array('listEscenarios' => $listEscenarios['93']));
            });
            $excel->sheet('97', function($sheet) use($listEscenarios) {
                $sheet->loadView('proceso.exportar.escenarios', array('listEscenarios' => $listEscenarios['97']));
            });
            $excel->sheet('DIESEL', function($sheet) use($listEscenarios) {
                $sheet->loadView('proceso.exportar.escenarios', array('listEscenarios' => $listEscenarios['DIESEL']));
            });
            $excel->sheet('GLP', function($sheet) use($listEscenarios) {
                $sheet->loadView('proceso.exportar.escenarios', array('listEscenarios' => $listEscenarios['GLP']));
            });
        })->export('xls');
    }

    public function escenarios($proc_id)
    {
        $proceso = Proceso::findOrFail($proc_id);
        if ($this->puedeContinuar($proceso))
        {
            $valores['FECHA']         = Util::dateFormat($proceso->PROC_FECHA_VIGENCIA, 'd/m/Y');

            //MIN-MAX PARAMETROS 93
            $oAux                     = Util::obtenerRestriccionPorParametro('{{N}}', Util::COMBUSTIBLE_93, $proc_id);
            $valores['N_93_MIN']      = $oAux['MIN'];
            $valores['N_93_MAX']      = $oAux['MAX'];
            $valores['N_93_INC']      = $oAux['INC'];
            $valores['N_93_DISABLED'] = $oAux['DISABLED'];
            $oAux                     = Util::obtenerRestriccionPorParametro('{{M}}', Util::COMBUSTIBLE_93, $proc_id);
            $valores['M_93_MIN']      = $oAux['MIN'];
            $valores['M_93_MAX']      = $oAux['MAX'];
            $valores['M_93_INC']      = $oAux['INC'];
            $valores['M_93_DISABLED'] = $oAux['DISABLED'];
            $oAux                     = Util::obtenerRestriccionPorParametro('{{S}}', Util::COMBUSTIBLE_93, $proc_id);
            $valores['S_93_MIN']      = $oAux['MIN'];
            $valores['S_93_MAX']      = $oAux['MAX'];
            $valores['S_93_INC']      = $oAux['INC'];
            $valores['S_93_DISABLED'] = $oAux['DISABLED'];
            $oAux                     = Util::obtenerRestriccionPorParametro('{{T}}', Util::COMBUSTIBLE_93, $proc_id);
            $valores['T_93_MIN']      = $oAux['MIN'];
            $valores['T_93_MAX']      = $oAux['MAX'];
            $valores['T_93_INC']      = $oAux['INC'];
            $valores['T_93_DISABLED'] = $oAux['DISABLED'];
            $oAux                     = Util::obtenerRestriccionPorParametro('{{F}}', Util::COMBUSTIBLE_93, $proc_id);
            $valores['F_93_MIN']      = $oAux['MIN'];
            $valores['F_93_MAX']      = $oAux['MAX'];
            $valores['F_93_INC']      = $oAux['INC'];
            $valores['F_93_DISABLED'] = $oAux['DISABLED'];

            //MIN-MAX PARAMETROS 97
            $oAux                     = Util::obtenerRestriccionPorParametro('{{N}}', Util::COMBUSTIBLE_97, $proc_id);
            $valores['N_97_MIN']      = $oAux['MIN'];
            $valores['N_97_MAX']      = $oAux['MAX'];
            $valores['N_97_INC']      = $oAux['INC'];
            $valores['N_97_DISABLED'] = $oAux['DISABLED'];
            $oAux                     = Util::obtenerRestriccionPorParametro('{{M}}', Util::COMBUSTIBLE_97, $proc_id);
            $valores['M_97_MIN']      = $oAux['MIN'];
            $valores['M_97_MAX']      = $oAux['MAX'];
            $valores['M_97_INC']      = $oAux['INC'];
            $valores['M_97_DISABLED'] = $oAux['DISABLED'];
            $oAux                     = Util::obtenerRestriccionPorParametro('{{S}}', Util::COMBUSTIBLE_97, $proc_id);
            $valores['S_97_MIN']      = $oAux['MIN'];
            $valores['S_97_MAX']      = $oAux['MAX'];
            $valores['S_97_INC']      = $oAux['INC'];
            $valores['S_97_DISABLED'] = $oAux['DISABLED'];
            $oAux                     = Util::obtenerRestriccionPorParametro('{{T}}', Util::COMBUSTIBLE_97, $proc_id);
            $valores['T_97_MIN']      = $oAux['MIN'];
            $valores['T_97_MAX']      = $oAux['MAX'];
            $valores['T_97_INC']      = $oAux['INC'];
            $valores['T_97_DISABLED'] = $oAux['DISABLED'];
            $oAux                     = Util::obtenerRestriccionPorParametro('{{F}}', Util::COMBUSTIBLE_97, $proc_id);
            $valores['F_97_MIN']      = $oAux['MIN'];
            $valores['F_97_MAX']      = $oAux['MAX'];
            $valores['F_97_INC']      = $oAux['INC'];
            $valores['F_97_DISABLED'] = $oAux['DISABLED'];

            //MIN-MAX PARAMETROS DIESEL
            $oAux                         = Util::obtenerRestriccionPorParametro('{{N}}', Util::COMBUSTIBLE_DIESEL, $proc_id);
            $valores['N_DIESEL_MIN']      = $oAux['MIN'];
            $valores['N_DIESEL_MAX']      = $oAux['MAX'];
            $valores['N_DIESEL_INC']      = $oAux['INC'];
            $valores['N_DIESEL_DISABLED'] = $oAux['DISABLED'];
            $oAux                         = Util::obtenerRestriccionPorParametro('{{M}}', Util::COMBUSTIBLE_DIESEL, $proc_id);
            $valores['M_DIESEL_MIN']      = $oAux['MIN'];
            $valores['M_DIESEL_MAX']      = $oAux['MAX'];
            $valores['M_DIESEL_INC']      = $oAux['INC'];
            $valores['M_DIESEL_DISABLED'] = $oAux['DISABLED'];
            $oAux                         = Util::obtenerRestriccionPorParametro('{{S}}', Util::COMBUSTIBLE_DIESEL, $proc_id);
            $valores['S_DIESEL_MIN']      = $oAux['MIN'];
            $valores['S_DIESEL_MAX']      = $oAux['MAX'];
            $valores['S_DIESEL_INC']      = $oAux['INC'];
            $valores['S_DIESEL_DISABLED'] = $oAux['DISABLED'];
            $oAux                         = Util::obtenerRestriccionPorParametro('{{T}}', Util::COMBUSTIBLE_DIESEL, $proc_id);
            $valores['T_DIESEL_MIN']      = $oAux['MIN'];
            $valores['T_DIESEL_MAX']      = $oAux['MAX'];
            $valores['T_DIESEL_INC']      = $oAux['INC'];
            $valores['T_DIESEL_DISABLED'] = $oAux['DISABLED'];
            $oAux                         = Util::obtenerRestriccionPorParametro('{{F}}', Util::COMBUSTIBLE_DIESEL, $proc_id);
            $valores['F_DIESEL_MIN']      = $oAux['MIN'];
            $valores['F_DIESEL_MAX']      = $oAux['MAX'];
            $valores['F_DIESEL_INC']      = $oAux['INC'];
            $valores['F_DIESEL_DISABLED'] = $oAux['DISABLED'];

            //MIN-MAX PARAMETROS GLP
            $oAux                      = Util::obtenerRestriccionPorParametro('{{N}}', Util::COMBUSTIBLE_GLP, $proc_id);
            $valores['N_GLP_MIN']      = $oAux['MIN'];
            $valores['N_GLP_MAX']      = $oAux['MAX'];
            $valores['N_GLP_INC']      = $oAux['INC'];
            $valores['N_GLP_DISABLED'] = $oAux['DISABLED'];
            $oAux                      = Util::obtenerRestriccionPorParametro('{{M}}', Util::COMBUSTIBLE_GLP, $proc_id);
            $valores['M_GLP_MIN']      = $oAux['MIN'];
            $valores['M_GLP_MAX']      = $oAux['MAX'];
            $valores['M_GLP_INC']      = $oAux['INC'];
            $valores['M_GLP_DISABLED'] = $oAux['DISABLED'];
            $oAux                      = Util::obtenerRestriccionPorParametro('{{S}}', Util::COMBUSTIBLE_GLP, $proc_id);
            $valores['S_GLP_MIN']      = $oAux['MIN'];
            $valores['S_GLP_MAX']      = $oAux['MAX'];
            $valores['S_GLP_INC']      = $oAux['INC'];
            $valores['S_GLP_DISABLED'] = $oAux['DISABLED'];
            $oAux                      = Util::obtenerRestriccionPorParametro('{{T}}', Util::COMBUSTIBLE_GLP, $proc_id);
            $valores['T_GLP_MIN']      = $oAux['MIN'];
            $valores['T_GLP_MAX']      = $oAux['MAX'];
            $valores['T_GLP_INC']      = $oAux['INC'];
            $valores['T_GLP_DISABLED'] = $oAux['DISABLED'];
            $oAux                      = Util::obtenerRestriccionPorParametro('{{F}}', Util::COMBUSTIBLE_GLP, $proc_id);
            $valores['F_GLP_MIN']      = $oAux['MIN'];
            $valores['F_GLP_MAX']      = $oAux['MAX'];
            $valores['F_GLP_INC']      = $oAux['INC'];
            $valores['F_GLP_DISABLED'] = $oAux['DISABLED'];

            //VALORES NMSTF POR COMBUSTIBLE
            $parametros_93     = $this->obtenerParametros(Util::COMBUSTIBLE_93, $proc_id);
            $parametros_97     = $this->obtenerParametros(Util::COMBUSTIBLE_97, $proc_id);
            $parametros_Diesel = $this->obtenerParametros(Util::COMBUSTIBLE_DIESEL, $proc_id);
            $parametros_GLP    = $this->obtenerParametros(Util::COMBUSTIBLE_GLP, $proc_id);

            $valores['PARAM_93']     = $parametros_93;
            $valores['PARAM_97']     = $parametros_97;
            $valores['PARAM_DIESEL'] = $parametros_Diesel;
            $valores['PARAM_GLP']    = $parametros_GLP;

            $listEscenarios = DB::table('MEPCO_ESCENARIO_CALCULO AS MEC')
                    ->join('MEPCO_ESCENARIO AS ME', 'MEC.ESCE_ID', '=', 'ME.ESCE_ID')
                    ->select(
                        'MEC.ESCA_ID', 
                        'MEC.ESCE_ID', 
                        'MEC.HIDR_ID', 
                        'MEC.ESCA_N', 
                        'MEC.ESCA_M', 
                        'MEC.ESCA_S', 
                        'MEC.ESCA_T', 
                        'MEC.ESCA_F', 
                        'MEC.ESCA_PARIDAD', 
                        DB::raw('CASE WHEN MEC.ESCA_TIPO_CRUDO = \'BRENT\' THEN MEC.ESCA_CRUDO_BRENT ELSE MEC.ESCA_CRUDO_WTI END AS ESCA_CRUDO'),
                        'MEC.ESCA_TIPO_CRUDO', 
                        'MEC.ESCA_MARGEN', 
                        'MEC.ESCA_PREFINF', 
                        'MEC.ESCA_PREFINT', 
                        'MEC.ESCA_PREFSUP', 
                        DB::raw('CASE WHEN MEC.ESCA_CORRECCION_RVP = 1 THEN \'SI\' ELSE \'NO\' END AS ESCA_CORRECCION_RVP'),
                        'MEC.ESCA_SUGERIR', 
                        'MEC.ESCA_PUBLICADO')
                    ->where('ME.PROC_ID', '=', $proc_id)
                    ->where('ME.ESCE_ELIMINADO', '=', 0)
                    ->orderBy('MEC.ESCA_ID', 'ASC')
                    ->get();


            //OBTIENE LISTA ESCENARIOS
            $listEscenarios93     = array();
            $listEscenarios97     = array();
            $listEscenariosDIESEL = array();
            $listEscenariosGLP    = array();

            foreach ($listEscenarios as $item)
            {
                if ($item->HIDR_ID == Util::COMBUSTIBLE_93)
                {
                    $listEscenarios93[] = $item;
                }
                else if ($item->HIDR_ID == Util::COMBUSTIBLE_97)
                {
                    $listEscenarios97[] = $item;
                }
                else if ($item->HIDR_ID == Util::COMBUSTIBLE_DIESEL)
                {
                    $listEscenariosDIESEL[] = $item;
                }
                else
                {
                    $listEscenariosGLP[] = $item;
                }
            }

            $idUsuarioLogueado  = session(Util::USUARIO)->USPE_ID;
            $auxUsuarioLogueado = DB::table('MEPCO_PROC_USPE')
                ->select('PRUS_ID')
                ->where('USPE_ID', '=', $idUsuarioLogueado)
                ->first();

            $ultimoProcesoEnSistema = DB::table('MEPCO_PROCESO')
                ->select('PROC_ID')
                ->orderBy('PROC_ID', 'DESC')
                ->first();

            $procesoValidado = DB::table('MEPCO_VAME_USPE')
                    ->select('VAUS_VALIDACION_CORRECTA')
                    ->where('PROC_ID', '=', $proc_id)
                    ->where('VAUS_ELIMINADO', '=', '0')
                    ->orderBy('VAUS_ID', 'DESC')
                    ->first();

            $valores[Util::COMBUSTIBLE_93]       = $listEscenarios93;
            $valores[Util::COMBUSTIBLE_97]       = $listEscenarios97;
            $valores[Util::COMBUSTIBLE_DIESEL]   = $listEscenariosDIESEL;
            $valores[Util::COMBUSTIBLE_GLP]      = $listEscenariosGLP;
            $valores['PERFIL']                   = session(Util::USUARIO)->PERF_ID;
            $valores['RECALCULAR']               = $proceso->PRES_ID == Util::PROCESO_ESTADO_RECALCULAR;
            $valores['FINALIZADO']               = $proceso->PRES_ID == Util::PROCESO_ESTADO_FINALIZADO;
            $valores['ROLLBACK_VISIBLE']         = ($auxUsuarioLogueado != null) && ($proceso->PROC_ID == $ultimoProcesoEnSistema->PROC_ID);

            if(session(Util::USUARIO)->PERF_ID == Util::PERFIL_USUARIO_GENERADOR)
            {
                $valores['PUEDE_CALCULAR_PUBLICAR'] = ($procesoValidado != null);
            }else
            {
                $valores['PUEDE_CALCULAR_PUBLICAR'] = ($procesoValidado != null && $procesoValidado->VAUS_VALIDACION_CORRECTA == '1');
            }


            $restriccion_semanas = Util::comprobarRestriccionSemanas();

            $valores['N_93_DISABLED'] = $restriccion_semanas['n_gas_87'];
            $valores['M_93_DISABLED'] = $restriccion_semanas['m_gas_87'];
            $valores['S_93_DISABLED'] = $restriccion_semanas['s_gas_87'];
            $valores['T_93_DISABLED'] = $restriccion_semanas['t_gas_87'];
            $valores['F_93_DISABLED'] = $restriccion_semanas['f_gas_87'];
            $valores['N_97_DISABLED'] = $restriccion_semanas['n_gas_93'];
            $valores['M_97_DISABLED'] = $restriccion_semanas['m_gas_93'];
            $valores['S_97_DISABLED'] = $restriccion_semanas['s_gas_93'];
            $valores['T_97_DISABLED'] = $restriccion_semanas['t_gas_93'];
            $valores['F_97_DISABLED'] = $restriccion_semanas['f_gas_93'];
            $valores['N_DIESEL_DISABLED'] = $restriccion_semanas['n_diesel'];
            $valores['M_DIESEL_DISABLED'] = $restriccion_semanas['m_diesel'];
            $valores['S_DIESEL_DISABLED'] = $restriccion_semanas['s_diesel'];
            $valores['T_DIESEL_DISABLED'] = $restriccion_semanas['t_diesel'];
            $valores['F_DIESEL_DISABLED'] = $restriccion_semanas['f_diesel'];
            $valores['N_GLP_DISABLED'] = $restriccion_semanas['n_glp'];
            $valores['M_GLP_DISABLED'] = $restriccion_semanas['m_glp'];
            $valores['S_GLP_DISABLED'] = $restriccion_semanas['s_glp'];
            $valores['T_GLP_DISABLED'] = $restriccion_semanas['t_glp'];
            $valores['F_GLP_DISABLED'] = $restriccion_semanas['f_glp'];

            $restriccion_semanas = json_encode(Util::comprobarRestriccionSemanas());

            return view('proceso.lista-escenarios', compact('proceso', 'valores', 'restriccion_semanas'));
        }
        else
        {
            return redirect()->back();
        }
    }

    private function calcularCrudo($proc_id, $m, $n, $f, $idEscenario, $combustible)
    {
        Util::$almacenaFormulaValoresSinFecha = true;

        $result[0] = Util::ejecutarFormula('{{BR_BRENT_CALCULADO}}', [
                    'PROC_ID' => $proc_id,
                    'M' => $m,
                    'N' => $n,
                    'F' => $f,
                    'LIMIT' => (5 * ($n - 1))
        ]);

        DB::table('MEPCO_ESCENARIO_BRENT')->insert([
            'ESCE_ID' => $idEscenario,
            'HIDR_ID' => $combustible,
            'ESBR_FUTURO' => Util::$arrFormulaValores['BR_BRENT_FUTURO'],
            'ESBR_PROMEDIO_SEM_CORREGIDO' => Util::$arrFormulaValores['BR_PROMEDIO_SEM_CORREGIDO'],
            'ESBR_PROMEDIO_SEM_US_M3' => Util::$arrFormulaValores['BR_BRENT_PROMEDIO_SEM_US$/M3'],
            'ESBR_PROMEDIO_SEM_S_M3' => Util::$arrFormulaValores['BR_BRENT_PROMEDIO_SEM_$/M3'],
            'ESBR_PROMEDIO_SEM_HISTORICO_S_M3' => Util::$arrFormulaValores['BR_BRENT_PROMEDIO_SEM_HISTORICO_$/M3'],
            'ESBR_HISTORICO' => Util::$arrFormulaValores['BR_BRENT_HISTORICO']
        ]);
    
        //WTI
        $result[1] = Util::ejecutarFormula('{{WT_WTI_CALCULADO}}', [
                    'PROC_ID' => $proc_id,
                    'M' => $m,
                    'N' => $n,
                    'F' => $f,
                    'LIMIT' => (5 * ($n - 1))
        ]);

        DB::table('MEPCO_ESCENARIO_WTI')->insert([
            'ESCE_ID' => $idEscenario,
            'HIDR_ID' => $combustible,
            'ESWT_FUTURO' => Util::$arrFormulaValores['WT_WTI_FUTURO'],
            'ESWT_PROMEDIO_SEM_US_M3' => Util::$arrFormulaValores['WT_WTI_PROMEDIO_SEM_US$/M3'],
            'ESWT_PROMEDIO_SEM_S_M3' => Util::$arrFormulaValores['WT_WTI_PROMEDIO_SEM_$/M3'],
            'ESWT_PROMEDIO_SEM_HISTORICO_S_M3' => Util::$arrFormulaValores['WT_WTI_PROMEDIO_SEM_HISTORICO_$/M3'],
            'ESWT_HISTORICO' => Util::$arrFormulaValores['WT_WTI_HISTORICO']
        ]);

        return $result;
    }

    public function crearEscenarioRequest(Request $request)
    {
        $n = $request->input('n');
        $m = $request->input('m');
        $s = $request->input('s');
        $t = $request->input('t');
        $f = $request->input('f');
        $proc_id            = $request->input('proc_id');
        $variable           = $request->input('variable');
        $combustible        = $request->input('combustible');
        $correccionRvp      = $request->input('correccionRvp');
        $idEscenario        = $request->input('idEscenario');
        $idEscenarioCalculo = $request->input('idEscenarioCalculo');
        $tipoCrudo          = $request->input('tipoCrudo') == 'true' ? 'BRENT' : 'WTI';

        $correccionRvp      = ($correccionRvp == 'true') ? 1 : 0;

        if ($idEscenario == "-1")
        {
            $idEscenario = DB::table('MEPCO_ESCENARIO')->insertGetId([
                'PROC_ID'             => $proc_id,
                'ESCE_FECHA_CREACION' => Util::dateTimeNow(),
                'USPE_ID_CREACION'    => session(Util::USUARIO)->USPE_ID,
                'ESCE_ELIMINADO'      => 0
            ]);
        }

        if ($idEscenarioCalculo == "-1")
        {
            $idEscenarioCalculo = DB::table('MEPCO_ESCENARIO_CALCULO')->insertGetId([
                'ESCE_ID' => $idEscenario,
                'HIDR_ID' => $combustible,
                'ESCA_N'  => $n,
                'ESCA_M'  => $m,
                'ESCA_S'  => $s,
                'ESCA_T'  => $t,
                'ESCA_F'  => $f,
                'ESCA_CORRECCION_RVP' => $correccionRvp,
                'ESCA_SUGERIR'        => 0,
                'ESCA_PUBLICADO'      => 0
            ]);
        }

        $fechaValoresMercado = DB::table('MEPCO_VALOR_MERCADO AS MVM')
                ->join('MEPCO_VAME_USPE AS MVU', 'MVM.VAUS_ID', '=', 'MVU.VAUS_ID')
                ->join('MEPCO_USUA_PERF AS MUP', 'MVU.USPE_ID', '=', 'MUP.USPE_ID')
                ->select('MVM.VAME_FECHA')
                ->where('MVU.PROC_ID', '=', $proc_id)
                ->where('MUP.PERF_ID', '=', Util::PERFIL_USUARIO_GENERADOR)
                ->where('MVU.VAUS_ELIMINADO', '=', 0)
                ->take(5)
                ->get();

        $result = null;

        switch ($combustible)
        {
            case Util::COMBUSTIBLE_93:
                switch ($variable)
                {
                    case Util::P_PARIDAD:

                        Util::$almacenaFormulaValoresSinFecha = false;

                        $arrIdEscenarioParidad = array();
                        foreach ($fechaValoresMercado as $item)
                        {
                            $idEscenarioParidad = DB::table('MEPCO_ESCENARIO_PARIDAD')->insertGetId([
                                'ESCE_ID'    => $idEscenario,
                                'HIDR_ID'    => $combustible,
                                'ESPA_FECHA' => $item->VAME_FECHA
                            ]);
                            $arrIdEscenarioParidad[$item->VAME_FECHA] = $idEscenarioParidad;
                        }

                        $arrResultado = array();
                        foreach ($fechaValoresMercado as $item)
                        {
                            $temp = Util::ejecutarFormula('{{PA_PARIDAD_CA_G_87}}', [
                                'PROC_ID'        => $proc_id,
                                'FECHA'          => $item->VAME_FECHA,
                                'CORRECCION_RVP' => $correccionRvp
                            ]);
                            $arrResultado[$item->VAME_FECHA] = $temp;

                            DB::update('UPDATE MEPCO_ESCENARIO_PARIDAD SET 
                                ESPA_PARIDAD_CA=?,
                                ESPA_FOB=?,
                                ESPA_FM=?,
                                ESPA_SM=?,
                                ESPA_CIF=?,
                                ESPA_GCC=?,
                                ESPA_MERM=?,
                                ESPA_DA=?,
                                ESPA_PPIBL=?,
                                ESPA_IVA=?,
                                ESPA_CF=?,
                                ESPA_CD=?,
                                ESPA_CA=?,
                                ESPA_SOBRESTADIA=?,
                                ESPA_ALM_QUINTERO=?,
                                ESPA_FLETE=?,
                                ESPA_ARBITRAJE=?,
                                ESPA_NETBACK_US_TON=?,
                                ESPA_NETBACK_US_GAL=?,
                                ESPA_FOB_GLP_ARBITRADO=?
                                WHERE ESPA_ID=?', [
                                $arrResultado[$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_FOB_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_FM_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_SM_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_CIF_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_GCC_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_MERM_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_DA_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_PPIBL_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_IVA_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_CF_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_CD_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['CA_G_87'][$item->VAME_FECHA],
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                $arrIdEscenarioParidad[$item->VAME_FECHA]
                            ]);

                        }

                        Util::$arrFormulaValores = array();
                        Util::$almacenaFormulaValoresSinFecha = true;
                        $result = Util::ejecutarFormula('{{PA_PARIDAD_$/m_G_87}}', [
                            'PROC_ID' => $proc_id,
                            'ESCE_ID' => $idEscenario,
                            'T'       => $t,
                            'LIMIT'   => (5 * ($t - 1))
                        ]);
                        DB::update('UPDATE MEPCO_ESCENARIO_CALCULO SET ESCA_PARIDAD=?, ESCA_PARIDAD_SEMANAL=? WHERE ESCA_ID=?', [
                            $result, 
                            Util::$arrFormulaValores['PA_PARIDAD_PROMEDIO_SEMANAL_ACTUAL_G_87'],
                            $idEscenarioCalculo
                        ]);

                        break;
                    case Util::CRUDO:

                        $auxResult = $this->calcularCrudo($proc_id, $m, $n, $f, $idEscenario, $combustible);

                        DB::update('UPDATE MEPCO_ESCENARIO_CALCULO SET 
                                ESCA_CRUDO_BRENT=?, 
                                ESCA_CRUDO_WTI=?, 
                                ESCA_TIPO_CRUDO=? 
                            WHERE 
                                ESCA_ID=?', [
                                $auxResult[0], 
                                $auxResult[1], 
                                $tipoCrudo, 
                                $idEscenarioCalculo
                        ]);

                        if($tipoCrudo == 'BRENT') $result = $auxResult[0]; else $result = $auxResult[1];

                        break;
                    case Util::MARGEN:

                        Util::$almacenaFormulaValoresSinFecha = true;

                        $margenSemanalBrent = Util::ejecutarFormula('{{MG_MARGEN_SEMANAL_BRENT_G_87}}', ['PROC_ID' => $proc_id]);
                        $margenSemanalWti   = Util::ejecutarFormula('{{MG_MARGEN_SEMANAL_WTI_G_87}}', ['PROC_ID' => $proc_id]);

                        DB::table('MEPCO_ESCENARIO_MARGEN')->insert([
                            'ESCE_ID'             => $idEscenario,
                            'HIDR_ID'             => $combustible,
                            'ESMA_PROMEDIO_BRENT' => $margenSemanalBrent,
                            'ESMA_PROMEDIO_WTI'   => $margenSemanalWti
                        ]);

                        //CALCULA PROMEDIO MARGEN
                        if($tipoCrudo == 'BRENT'){
                            $result = Util::ejecutarFormula('{{MG_MARGEN_BRENT_G_87}}', [
                                        'PROC_ID' => $proc_id,
                                        'ESCE_ID' => $idEscenario,
                                        'S'       => $s,
                                        'LIMIT'   => (5 * ($s - 1))
                            ]);                            
                        }else{
                            $result = Util::ejecutarFormula('{{MG_MARGEN_WTI_G_87}}', [
                                        'PROC_ID' => $proc_id,
                                        'ESCE_ID' => $idEscenario,
                                        'S'       => $s,
                                        'LIMIT'   => (5 * ($s - 1))
                            ]);
                        }

                        DB::update('UPDATE MEPCO_ESCENARIO_CALCULO SET 
                            ESCA_MARGEN=? 
                            WHERE ESCA_ID=?', [
                            $result, 
                            $idEscenarioCalculo
                        ]);

                        break;
                    case Util::P_REF_INF:

                        Util::$almacenaFormulaValoresSinFecha = false;

                        $arrIdEscenarioReferencia = array();
                        foreach ($fechaValoresMercado as $item)
                        {
                            $idEscenarioReferencia = DB::table('MEPCO_ESCENARIO_REFERENCIA')->insertGetId([
                                'ESCE_ID'    => $idEscenario,
                                'HIDR_ID'    => $combustible,
                                'ESRE_FECHA' => $item->VAME_FECHA
                            ]);
                            $arrIdEscenarioReferencia[$item->VAME_FECHA] = $idEscenarioReferencia;
                        }

                        $arrResultado = array();
                        $auxResultado = 0;
                        foreach ($fechaValoresMercado as $item)
                        {
                            $temp = Util::ejecutarFormula('{{RE_PARIDAD_CA_G_87}}', [
                                'PROC_ID'        => $proc_id,
                                'ESCE_ID'        => $idEscenario,
                                'FECHA'          => $item->VAME_FECHA,
                                'CORRECCION_RVP' => $correccionRvp,
                                'TIPO_CRUDO'     => $tipoCrudo
                            ]);

                            $arrResultado[$item->VAME_FECHA] = $temp;
                            $auxResultado += $arrResultado[$item->VAME_FECHA];

                            DB::update('UPDATE MEPCO_ESCENARIO_REFERENCIA SET 
                                ESRE_PARIDAD_CA=?,
                                ESRE_FOB_TEORICO=?,
                                ESRE_FOB=?,
                                ESRE_FM_US=?,
                                ESRE_FM_S=?,
                                ESRE_SM=?,
                                ESRE_CIF=?,
                                ESRE_GCC=?,
                                ESRE_MERM=?,
                                ESRE_DA=?,
                                ESRE_PPIBL=?,
                                ESRE_IVA=?,
                                ESRE_CF=?,
                                ESRE_CD_US=?,
                                ESRE_CD_S=?,
                                ESRE_CA_US=?,
                                ESRE_CA_S=?                                
                                WHERE ESRE_ID=?', [
                                $arrResultado[$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_FOB_TEORICO_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_FOB_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_FM_US_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_FM_S_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_SM_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_CIF_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_GCC_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_MERM_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_DA_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_PPIBL_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_IVA_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_CF_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_CD_US_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_CD_S_G_87'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['CA_G_87'][$item->VAME_FECHA], //U$
                                Util::$arrFormulaValores['RE_CA_G_87'][$item->VAME_FECHA], //$
                                $arrIdEscenarioReferencia[$item->VAME_FECHA]
                            ]);
                        }

                        $referenciaInt = Util::ejecutarFormula('{{RE_PARIDAD_G_87}}', [
                            'PROC_ID' => $proc_id,
                            'ESCE_ID' => $idEscenario
                        ]);

                        $referenciaInf = $referenciaInt * 0.95;
                        $referenciaSup = $referenciaInt * 1.05;

                        DB::update('UPDATE MEPCO_ESCENARIO_CALCULO SET 
                                ESCA_PREFINF=?,
                                ESCA_PREFINT=?,
                                ESCA_PREFSUP=?
                            WHERE
                                ESCA_ID=?', [$referenciaInf, $referenciaInt, $referenciaSup, $idEscenarioCalculo]);

                        $result = $referenciaInf;
                        break;
                    case Util::P_REF_INT:
                        $temp = DB::table('MEPCO_ESCENARIO_CALCULO')
                                ->select('ESCA_PREFINT')
                                ->where('ESCA_ID', '=', $idEscenarioCalculo)
                                ->first();

                        $result = $temp->ESCA_PREFINT;
                        break;
                    case Util::P_REF_SUP:
                        $temp = DB::table('MEPCO_ESCENARIO_CALCULO')
                                ->select('ESCA_PREFSUP')
                                ->where('ESCA_ID', '=', $idEscenarioCalculo)
                                ->first();
                        $result = $temp->ESCA_PREFSUP;
                        break;
                }
                break;
            case Util::COMBUSTIBLE_97:
                switch ($variable)
                {
                    case Util::P_PARIDAD:

                        Util::$almacenaFormulaValoresSinFecha = false;

                        $arrIdEscenarioParidad = array();
                        foreach ($fechaValoresMercado as $item)
                        {
                            $idEscenarioParidad = DB::table('MEPCO_ESCENARIO_PARIDAD')->insertGetId([
                                'ESCE_ID'    => $idEscenario,
                                'HIDR_ID'    => $combustible,
                                'ESPA_FECHA' => $item->VAME_FECHA
                            ]);
                            $arrIdEscenarioParidad[$item->VAME_FECHA] = $idEscenarioParidad;
                        }

                        $arrResultado = array();
                        $auxResultado = 0;
                        foreach ($fechaValoresMercado as $item)
                        {
                            $temp = Util::ejecutarFormula('{{PA_PARIDAD_CA_G_93}}', [
                                'PROC_ID'        => $proc_id,
                                'FECHA'          => $item->VAME_FECHA,
                                'CORRECCION_RVP' => $correccionRvp
                            ]);
                            $arrResultado[$item->VAME_FECHA] = $temp;
                            $auxResultado += $arrResultado[$item->VAME_FECHA];

                            DB::update('UPDATE MEPCO_ESCENARIO_PARIDAD SET 
                                ESPA_PARIDAD_CA=?,
                                ESPA_FOB=?,
                                ESPA_FM=?,
                                ESPA_SM=?,
                                ESPA_CIF=?,
                                ESPA_GCC=?,
                                ESPA_MERM=?,
                                ESPA_DA=?,
                                ESPA_PPIBL=?,
                                ESPA_IVA=?,
                                ESPA_CF=?,
                                ESPA_CD=?,
                                ESPA_CA=?,
                                ESPA_SOBRESTADIA=?,
                                ESPA_ALM_QUINTERO=?,
                                ESPA_FLETE=?,
                                ESPA_ARBITRAJE=?,
                                ESPA_NETBACK_US_TON=?,
                                ESPA_NETBACK_US_GAL=?,
                                ESPA_FOB_GLP_ARBITRADO=?
                                WHERE ESPA_ID=?', [
                                $arrResultado[$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_FOB_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_FM_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_SM_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_CIF_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_GCC_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_MERM_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_DA_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_PPIBL_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_IVA_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_CF_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_CD_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['CA_G_93'][$item->VAME_FECHA],
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                $arrIdEscenarioParidad[$item->VAME_FECHA]
                            ]);
                        }

                        Util::$arrFormulaValores = array();
                        Util::$almacenaFormulaValoresSinFecha = true;
                        $result = Util::ejecutarFormula('{{PA_PARIDAD_$/m_G_93}}', [
                            'PROC_ID' => $proc_id,
                            'ESCE_ID' => $idEscenario,
                            'T'       => $t,
                            'LIMIT'   => (5 * ($t - 1))
                        ]);
                        
                        DB::update('UPDATE MEPCO_ESCENARIO_CALCULO SET ESCA_PARIDAD=?, ESCA_PARIDAD_SEMANAL=? WHERE ESCA_ID=?', [
                            $result, 
                            Util::$arrFormulaValores['PA_PARIDAD_PROMEDIO_SEMANAL_ACTUAL_G_93'],
                            $idEscenarioCalculo
                        ]);

                        break;
                    case Util::CRUDO:

                        $auxResult = $this->calcularCrudo($proc_id, $m, $n, $f, $idEscenario, $combustible);

                        DB::update('UPDATE MEPCO_ESCENARIO_CALCULO SET 
                                ESCA_CRUDO_BRENT=?, 
                                ESCA_CRUDO_WTI=?, 
                                ESCA_TIPO_CRUDO=? 
                            WHERE 
                                ESCA_ID=?', [
                                $auxResult[0], 
                                $auxResult[1], 
                                $tipoCrudo, 
                                $idEscenarioCalculo
                        ]);

                        if($tipoCrudo == 'BRENT') $result = $auxResult[0]; else $result = $auxResult[1];

                        break;
                    case Util::MARGEN:

                        Util::$almacenaFormulaValoresSinFecha = true;

                        $margenSemanalBrent = Util::ejecutarFormula('{{MG_MARGEN_SEMANAL_BRENT_G_93}}', ['PROC_ID' => $proc_id]);
                        $margenSemanalWti   = Util::ejecutarFormula('{{MG_MARGEN_SEMANAL_WTI_G_93}}', ['PROC_ID' => $proc_id]);

                        DB::table('MEPCO_ESCENARIO_MARGEN')->insert([
                            'ESCE_ID'             => $idEscenario,
                            'HIDR_ID'             => $combustible,
                            'ESMA_PROMEDIO_BRENT' => $margenSemanalBrent,
                            'ESMA_PROMEDIO_WTI'   => $margenSemanalWti
                        ]);

                        //CALCULA PROMEDIO MARGEN
                        if($tipoCrudo == 'BRENT'){
                            $result = Util::ejecutarFormula('{{MG_MARGEN_BRENT_G_93}}', [
                                        'PROC_ID' => $proc_id,
                                        'ESCE_ID' => $idEscenario,
                                        'S'       => $s,
                                        'LIMIT'   => (5 * ($s - 1))
                            ]);                            
                        }else{
                            $result = Util::ejecutarFormula('{{MG_MARGEN_WTI_G_93}}', [
                                        'PROC_ID' => $proc_id,
                                        'ESCE_ID' => $idEscenario,
                                        'S'       => $s,
                                        'LIMIT'   => (5 * ($s - 1))
                            ]);
                        }

                        DB::update('UPDATE MEPCO_ESCENARIO_CALCULO SET ESCA_MARGEN=? WHERE ESCA_ID=?', [$result, $idEscenarioCalculo]);

                        break;
                    case Util::P_REF_INF:

                        Util::$almacenaFormulaValoresSinFecha = false;

                        $arrIdEscenarioReferencia = array();
                        foreach ($fechaValoresMercado as $item)
                        {
                            $idEscenarioReferencia = DB::table('MEPCO_ESCENARIO_REFERENCIA')->insertGetId([
                                'ESCE_ID'    => $idEscenario,
                                'HIDR_ID'    => $combustible,
                                'ESRE_FECHA' => $item->VAME_FECHA
                            ]);
                            $arrIdEscenarioReferencia[$item->VAME_FECHA] = $idEscenarioReferencia;
                        }

                        $arrResultado = array();
                        $auxResultado = 0;
                        foreach ($fechaValoresMercado as $item)
                        {
                            $temp = Util::ejecutarFormula('{{RE_PARIDAD_CA_G_93}}', [
                                        'PROC_ID'        => $proc_id,
                                        'ESCE_ID'        => $idEscenario,
                                        'FECHA'          => $item->VAME_FECHA,
                                        'CORRECCION_RVP' => $correccionRvp,
                                        'TIPO_CRUDO'     => $tipoCrudo
                            ]);

                            $arrResultado[$item->VAME_FECHA] = $temp;
                            $auxResultado += $arrResultado[$item->VAME_FECHA];

                            DB::update('UPDATE MEPCO_ESCENARIO_REFERENCIA SET 
                                ESRE_PARIDAD_CA=?,
                                ESRE_FOB_TEORICO=?,
                                ESRE_FOB=?,
                                ESRE_FM_US=?,
                                ESRE_FM_S=?,
                                ESRE_SM=?,
                                ESRE_CIF=?,
                                ESRE_GCC=?,
                                ESRE_MERM=?,
                                ESRE_DA=?,
                                ESRE_PPIBL=?,
                                ESRE_IVA=?,
                                ESRE_CF=?,
                                ESRE_CD_US=?,
                                ESRE_CD_S=?,
                                ESRE_CA_US=?,
                                ESRE_CA_S=?                                
                                WHERE ESRE_ID=?', [
                                $arrResultado[$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_FOB_TEORICO_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_FOB_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_FM_US_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_FM_S_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_SM_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_CIF_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_GCC_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_MERM_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_DA_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_PPIBL_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_IVA_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_CF_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_CD_US_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_CD_S_G_93'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['CA_G_93'][$item->VAME_FECHA], //U$
                                Util::$arrFormulaValores['RE_CA_G_93'][$item->VAME_FECHA], //$
                                $arrIdEscenarioReferencia[$item->VAME_FECHA]
                            ]);
                        }

                        $referenciaInt = Util::ejecutarFormula('{{RE_PARIDAD_G_93}}', [
                            'PROC_ID' => $proc_id,
                            'ESCE_ID' => $idEscenario
                        ]);

                        $referenciaInf = $referenciaInt * 0.95;
                        $referenciaSup = $referenciaInt * 1.05;

                        DB::update('UPDATE MEPCO_ESCENARIO_CALCULO SET 
                                ESCA_PREFINF=?,
                                ESCA_PREFINT=?,
                                ESCA_PREFSUP=?
                            WHERE
                                ESCA_ID=?', [$referenciaInf, $referenciaInt, $referenciaSup, $idEscenarioCalculo]);

                        $result = $referenciaInf;
                        break;
                    case Util::P_REF_INT:
                        $temp = DB::table('MEPCO_ESCENARIO_CALCULO')
                                ->select('ESCA_PREFINT')
                                ->where('ESCA_ID', '=', $idEscenarioCalculo)
                                ->first();

                        $result = $temp->ESCA_PREFINT;
                        break;
                    case Util::P_REF_SUP:
                        $temp = DB::table('MEPCO_ESCENARIO_CALCULO')
                                ->select('ESCA_PREFSUP')
                                ->where('ESCA_ID', '=', $idEscenarioCalculo)
                                ->first();
                        $result = $temp->ESCA_PREFSUP;
                        break;
                }
                break;
            case Util::COMBUSTIBLE_DIESEL:
                switch ($variable)
                {
                    case Util::P_PARIDAD:

                        Util::$almacenaFormulaValoresSinFecha = false;

                        $arrIdEscenarioParidad = array();
                        foreach ($fechaValoresMercado as $item)
                        {
                            $idEscenarioParidad = DB::table('MEPCO_ESCENARIO_PARIDAD')->insertGetId([
                                'ESCE_ID'    => $idEscenario,
                                'HIDR_ID'    => $combustible,
                                'ESPA_FECHA' => $item->VAME_FECHA
                            ]);

                            $arrIdEscenarioParidad[$item->VAME_FECHA] = $idEscenarioParidad;
                        }

                        $arrResultado = array();
                        $auxResultado = 0;
                        foreach ($fechaValoresMercado as $item)
                        {
                            $temp = Util::ejecutarFormula('{{PA_PARIDAD_CA_D}}', [
                                'PROC_ID' => $proc_id,
                                'FECHA'   => $item->VAME_FECHA
                            ]);
                            
                            $arrResultado[$item->VAME_FECHA] = $temp;
                            $auxResultado += $arrResultado[$item->VAME_FECHA];

                            DB::update('UPDATE MEPCO_ESCENARIO_PARIDAD SET 
                                ESPA_PARIDAD_CA=?,
                                ESPA_FOB=?,
                                ESPA_FM=?,
                                ESPA_SM=?,
                                ESPA_CIF=?,
                                ESPA_GCC=?,
                                ESPA_MERM=?,
                                ESPA_DA=?,
                                ESPA_PPIBL=?,
                                ESPA_IVA=?,
                                ESPA_CF=?,
                                ESPA_CD=?,
                                ESPA_CA=?,
                                ESPA_SOBRESTADIA=?,
                                ESPA_ALM_QUINTERO=?,
                                ESPA_FLETE=?,
                                ESPA_ARBITRAJE=?,
                                ESPA_NETBACK_US_TON=?,
                                ESPA_NETBACK_US_GAL=?,
                                ESPA_FOB_GLP_ARBITRADO=?
                                WHERE ESPA_ID=?', [
                                $arrResultado[$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_FOB_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_FM_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_SM_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_CIF_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_GCC_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_MERM_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_DA_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_PPIBL_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_IVA_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_CF_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_CD_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['CA_D'][$item->VAME_FECHA],
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                $arrIdEscenarioParidad[$item->VAME_FECHA]
                            ]);
                        }
                        
                        Util::$arrFormulaValores = array();
                        Util::$almacenaFormulaValoresSinFecha = true;
                        $result = Util::ejecutarFormula('{{PA_PARIDAD_$/m_DIESEL}}', [
                                    'PROC_ID' => $proc_id,
                                    'ESCE_ID' => $idEscenario,
                                    'T'       => $t,
                                    'LIMIT'   => (5 * ($t - 1))
                        ]);

                        DB::update('UPDATE MEPCO_ESCENARIO_CALCULO SET ESCA_PARIDAD=?, ESCA_PARIDAD_SEMANAL=? WHERE ESCA_ID=?', [
                            $result, 
                            Util::$arrFormulaValores['PA_PARIDAD_PROMEDIO_SEMANAL_ACTUAL_DIESEL'],
                            $idEscenarioCalculo
                        ]);

                        break;
                    case Util::CRUDO:

                        $auxResult = $this->calcularCrudo($proc_id, $m, $n, $f, $idEscenario, $combustible);

                        DB::update('UPDATE MEPCO_ESCENARIO_CALCULO SET 
                                ESCA_CRUDO_BRENT=?, 
                                ESCA_CRUDO_WTI=?, 
                                ESCA_TIPO_CRUDO=? 
                            WHERE 
                                ESCA_ID=?', [
                                $auxResult[0], 
                                $auxResult[1], 
                                $tipoCrudo, 
                                $idEscenarioCalculo
                        ]);

                        if($tipoCrudo == 'BRENT') $result = $auxResult[0]; else $result = $auxResult[1];

                        break;
                    case Util::MARGEN:

                        Util::$almacenaFormulaValoresSinFecha = true;
                        
                        $margenSemanalBrent = Util::ejecutarFormula('{{MG_MARGEN_SEMANAL_BRENT_DIESEL}}', ['PROC_ID' => $proc_id]);
                        $margenSemanalWti   = Util::ejecutarFormula('{{MG_MARGEN_SEMANAL_WTI_DIESEL}}', ['PROC_ID' => $proc_id]);

                        DB::table('MEPCO_ESCENARIO_MARGEN')->insert([
                            'ESCE_ID'             => $idEscenario,
                            'HIDR_ID'             => $combustible,
                            'ESMA_PROMEDIO_BRENT' => $margenSemanalBrent,
                            'ESMA_PROMEDIO_WTI'   => $margenSemanalWti
                        ]);

                        //CALCULA PROMEDIO MARGEN
                        if($tipoCrudo == 'BRENT'){
                            $result = Util::ejecutarFormula('{{MG_MARGEN_BRENT_DIESEL}}', [
                                        'PROC_ID' => $proc_id,
                                        'ESCE_ID' => $idEscenario,
                                        'S'       => $s,
                                        'LIMIT'   => (5 * ($s - 1))
                            ]);                            
                        }else{
                            $result = Util::ejecutarFormula('{{MG_MARGEN_WTI_DIESEL}}', [
                                        'PROC_ID' => $proc_id,
                                        'ESCE_ID' => $idEscenario,
                                        'S'       => $s,
                                        'LIMIT'   => (5 * ($s - 1))
                            ]);
                        }


                        DB::update('UPDATE MEPCO_ESCENARIO_CALCULO SET ESCA_MARGEN=? WHERE ESCA_ID=?', [$result, $idEscenarioCalculo]);

                        break;
                    case Util::P_REF_INF:

                        Util::$almacenaFormulaValoresSinFecha = false;

                        $arrIdEscenarioReferencia = array();
                        foreach ($fechaValoresMercado as $item)
                        {
                            $idEscenarioReferencia = DB::table('MEPCO_ESCENARIO_REFERENCIA')->insertGetId([
                                'ESCE_ID'    => $idEscenario,
                                'HIDR_ID'    => $combustible,
                                'ESRE_FECHA' => $item->VAME_FECHA
                            ]);
                            $arrIdEscenarioReferencia[$item->VAME_FECHA] = $idEscenarioReferencia;
                        }

                        $arrResultado = array();
                        $auxResultado = 0;
                        foreach ($fechaValoresMercado as $item)
                        {
                            $temp = Util::ejecutarFormula('{{RE_PARIDAD_CA_D}}', [
                                'PROC_ID'    => $proc_id,
                                'ESCE_ID'    => $idEscenario,
                                'FECHA'      => $item->VAME_FECHA,
                                'TIPO_CRUDO' => $tipoCrudo
                            ]);

                            $arrResultado[$item->VAME_FECHA] = $temp;
                            $auxResultado += $arrResultado[$item->VAME_FECHA];

                            DB::update('UPDATE MEPCO_ESCENARIO_REFERENCIA SET 
                                ESRE_PARIDAD_CA=?,
                                ESRE_FOB_TEORICO=?,
                                ESRE_FOB=?,
                                ESRE_FM_US=?,
                                ESRE_FM_S=?,
                                ESRE_SM=?,
                                ESRE_CIF=?,
                                ESRE_GCC=?,
                                ESRE_MERM=?,
                                ESRE_DA=?,
                                ESRE_PPIBL=?,
                                ESRE_IVA=?,
                                ESRE_CF=?,
                                ESRE_CD_US=?,
                                ESRE_CD_S=?,
                                ESRE_CA_US=?,
                                ESRE_CA_S=?                                
                                WHERE ESRE_ID=?', [
                                $arrResultado[$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_FOB_TEORICO_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_FOB_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_FM_US_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_FM_S_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_SM_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_CIF_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_GCC_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_MERM_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_DA_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_PPIBL_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_IVA_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_CF_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_CD_US_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_CD_S_D'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['CA_D'][$item->VAME_FECHA], //U$
                                Util::$arrFormulaValores['RE_CA_D'][$item->VAME_FECHA], //$
                                $arrIdEscenarioReferencia[$item->VAME_FECHA]
                            ]);
                        }

                        $referenciaInt = Util::ejecutarFormula('{{RE_PARIDAD_DIESEL}}', [
                                    'PROC_ID' => $proc_id,
                                    'ESCE_ID' => $idEscenario
                        ]);

                        $referenciaInf = $referenciaInt * 0.95;
                        $referenciaSup = $referenciaInt * 1.05;

                        DB::update('UPDATE MEPCO_ESCENARIO_CALCULO SET 
                                ESCA_PREFINF=?,
                                ESCA_PREFINT=?,
                                ESCA_PREFSUP=?
                            WHERE
                                ESCA_ID=?', [$referenciaInf, $referenciaInt, $referenciaSup, $idEscenarioCalculo]);

                        $result = $referenciaInf;
                        break;
                    case Util::P_REF_INT:
                        $temp = DB::table('MEPCO_ESCENARIO_CALCULO')
                                ->select('ESCA_PREFINT')
                                ->where('ESCA_ID', '=', $idEscenarioCalculo)
                                ->first();

                        $result = $temp->ESCA_PREFINT;
                        break;
                    case Util::P_REF_SUP:
                        $temp = DB::table('MEPCO_ESCENARIO_CALCULO')
                                ->select('ESCA_PREFSUP')
                                ->where('ESCA_ID', '=', $idEscenarioCalculo)
                                ->first();
                        $result = $temp->ESCA_PREFSUP;
                        break;
                }
                break;
            case Util::COMBUSTIBLE_GLP:
                switch ($variable)
                {
                    case Util::P_PARIDAD:

                        Util::$almacenaFormulaValoresSinFecha = false;

                        $arrIdEscenarioParidad = array();
                        foreach ($fechaValoresMercado as $item)
                        {
                            $idEscenarioParidad = DB::table('MEPCO_ESCENARIO_PARIDAD')->insertGetId([
                                'ESCE_ID' => $idEscenario,
                                'HIDR_ID' => $combustible,
                                'ESPA_FECHA' => $item->VAME_FECHA
                            ]);
                            $arrIdEscenarioParidad[$item->VAME_FECHA] = $idEscenarioParidad;
                        }

                        $arrResultado = array();
                        $auxResultado = 0;
                        foreach ($fechaValoresMercado as $item)
                        {
                            $temp = Util::ejecutarFormula('{{PA_PARIDAD_CA_GLP}}', [
                                        'PROC_ID' => $proc_id,
                                        'FECHA' => $item->VAME_FECHA
                            ]);
                            $arrResultado[$item->VAME_FECHA] = $temp;
                            $auxResultado += $arrResultado[$item->VAME_FECHA];                           

                            DB::update('UPDATE MEPCO_ESCENARIO_PARIDAD SET 
                                ESPA_PARIDAD_CA=?,
                                ESPA_FOB=?,
                                ESPA_FM=?,
                                ESPA_SM=?,
                                ESPA_CIF=?,
                                ESPA_GCC=?,
                                ESPA_MERM=?,
                                ESPA_DA=?,
                                ESPA_PPIBL=?,
                                ESPA_IVA=?,
                                ESPA_CF=?,
                                ESPA_CD=?,
                                ESPA_CA=?,
                                ESPA_SOBRESTADIA=?,
                                ESPA_ALM_QUINTERO=?,
                                ESPA_FLETE=?,
                                ESPA_ARBITRAJE=?,
                                ESPA_NETBACK_US_TON=?,
                                ESPA_NETBACK_US_GAL=?,
                                ESPA_FOB_GLP_ARBITRADO=?
                                WHERE ESPA_ID=?', [
                                $arrResultado[$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_FOB_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_FM_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_SM_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_CIF_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_GCC_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_MERM_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_DA_GLP'][$item->VAME_FECHA],
                                0, //NO APLICA PARA GLP Util::$arrFormulaValores['PA_PPIBL_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_IVA_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_CF_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_CD_GLP'][$item->VAME_FECHA],
                                0, //NO APLIA PARA GLP Util::$arrFormulaValores['CA_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_SOBRESTADIA'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['PA_ALM_QUINTERO'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['FLETE_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['ARBITRAJE_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['NETBACK_TON'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['NETBACK_GAL'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['GLP_ARBITRADO'][$item->VAME_FECHA],
                                $arrIdEscenarioParidad[$item->VAME_FECHA]
                            ]);
                        }

                        Util::$arrFormulaValores = array();
                        Util::$almacenaFormulaValoresSinFecha = true;
                        $result = Util::ejecutarFormula('{{PA_PARIDAD_$/m_GLP}}', [
                                    'PROC_ID' => $proc_id,
                                    'ESCE_ID' => $idEscenario,
                                    'T' => $t,
                                    'LIMIT' => (5 * ($t - 1))
                        ]);

                        DB::update('UPDATE MEPCO_ESCENARIO_CALCULO SET ESCA_PARIDAD=?, ESCA_PARIDAD_SEMANAL=? WHERE ESCA_ID=?', [
                            $result, 
                            Util::$arrFormulaValores['PA_PARIDAD_PROMEDIO_SEMANAL_ACTUAL_GLP'],
                            $idEscenarioCalculo
                        ]);

                        break;
                    case Util::CRUDO:

                        $auxResult = $this->calcularCrudo($proc_id, $m, $n, $f, $idEscenario, $combustible);

                        DB::update('UPDATE MEPCO_ESCENARIO_CALCULO SET 
                                ESCA_CRUDO_BRENT=?, 
                                ESCA_CRUDO_WTI=?, 
                                ESCA_TIPO_CRUDO=? 
                            WHERE 
                                ESCA_ID=?', [
                                $auxResult[0], 
                                $auxResult[1], 
                                $tipoCrudo, 
                                $idEscenarioCalculo
                        ]);

                        if($tipoCrudo == 'BRENT') $result = $auxResult[0]; else $result = $auxResult[1];

                        break;
                    case Util::MARGEN:

                        Util::$almacenaFormulaValoresSinFecha = true;

                        $margenSemanalBrent = Util::ejecutarFormula('{{MG_MARGEN_SEMANAL_BRENT_GLP}}', [
                            'PROC_ID' => $proc_id,
                            'ESCE_ID' => $idEscenarioCalculo
                        ]);

                        $margenSemanalWti   = Util::ejecutarFormula('{{MG_MARGEN_SEMANAL_WTI_GLP}}', [
                            'PROC_ID' => $proc_id,
                            'ESCE_ID' => $idEscenarioCalculo
                        ]);

                        DB::table('MEPCO_ESCENARIO_MARGEN')->insert([
                            'ESCE_ID' => $idEscenario,
                            'HIDR_ID' => $combustible,
                            'ESMA_PROMEDIO_BRENT' => $margenSemanalBrent,
                            'ESMA_PROMEDIO_WTI'   => $margenSemanalWti
                        ]);

                        //CALCULA PROMEDIO MARGEN
                        if($tipoCrudo == 'BRENT'){
                            $result = Util::ejecutarFormula('{{MG_MARGEN_BRENT_GLP}}', [
                                        'PROC_ID' => $proc_id,
                                        'ESCE_ID' => $idEscenario,
                                        'S'       => $s,
                                        'LIMIT'   => (5 * ($s - 1))
                            ]);                            
                        }else{
                            $result = Util::ejecutarFormula('{{MG_MARGEN_WTI_GLP}}', [
                                        'PROC_ID' => $proc_id,
                                        'ESCE_ID' => $idEscenario,
                                        'S'       => $s,
                                        'LIMIT'   => (5 * ($s - 1))
                            ]);
                        }

                        DB::update('UPDATE MEPCO_ESCENARIO_CALCULO SET ESCA_MARGEN=? WHERE ESCA_ID=?', [$result, $idEscenarioCalculo]);

                        break;
                    case Util::P_REF_INF:

                        Util::$almacenaFormulaValoresSinFecha = false;

                        $arrIdEscenarioReferencia = array();
                        foreach ($fechaValoresMercado as $item)
                        {
                            $idEscenarioReferencia = DB::table('MEPCO_ESCENARIO_REFERENCIA')->insertGetId([
                                'ESCE_ID' => $idEscenario,
                                'HIDR_ID' => $combustible,
                                'ESRE_FECHA' => $item->VAME_FECHA
                            ]);
                            $arrIdEscenarioReferencia[$item->VAME_FECHA] = $idEscenarioReferencia;
                        }

                        $arrResultado = array();
                        $auxResultado = 0;
                        foreach ($fechaValoresMercado as $item)
                        {
                            $temp = Util::ejecutarFormula('{{RE_PARIDAD_CA_GLP}}', [
                                        'PROC_ID'    => $proc_id,
                                        'ESCE_ID'    => $idEscenario,
                                        'FECHA'      => $item->VAME_FECHA,
                                        'TIPO_CRUDO' => $tipoCrudo
                            ]);

                            $arrResultado[$item->VAME_FECHA] = $temp;
                            $auxResultado += $arrResultado[$item->VAME_FECHA];

                            

                            DB::update('UPDATE MEPCO_ESCENARIO_REFERENCIA SET 
                                ESRE_PARIDAD_CA=?,
                                ESRE_FOB_TEORICO=?,
                                ESRE_FOB=?,
                                ESRE_FM_US=?,
                                ESRE_FM_S=?,
                                ESRE_SM=?,
                                ESRE_CIF=?,
                                ESRE_GCC=?,
                                ESRE_MERM=?,
                                ESRE_DA=?,
                                ESRE_PPIBL=?,
                                ESRE_IVA=?,
                                ESRE_CF=?,
                                ESRE_CD_US=?,
                                ESRE_CD_S=?,
                                ESRE_CA_US=?,
                                ESRE_CA_S=?,
                                ESRE_ALM_QUINTERO_US=?,
                                ESRE_ALM_QUINTERO_S=?,
                                ESRE_SOBRESTADIA_US=?,
                                ESRE_SOBRESTADIA_S=?
                                WHERE ESRE_ID=?', [
                                $arrResultado[$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_FOB_TEORICO_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_FOB_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_FM_US_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_FM_S_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_SM_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_CIF_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_GCC_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_MERM_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_DA_GLP'][$item->VAME_FECHA],
                                0,
                                Util::$arrFormulaValores['RE_IVA_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_CF_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_CD_US_GLP'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_CD_S_GLP'][$item->VAME_FECHA],
                                0,
                                0,
                                Util::$arrFormulaValores['RE_ALM_QUINTERO_US'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_ALM_QUINTERO_S'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_SOBRESTADIA_US'][$item->VAME_FECHA],
                                Util::$arrFormulaValores['RE_SOBRESTADIA_S'][$item->VAME_FECHA],
                                $arrIdEscenarioReferencia[$item->VAME_FECHA]
                            ]);
                        }

                        $referenciaInt = Util::ejecutarFormula('{{RE_PARIDAD_GLP}}', [
                                    'PROC_ID' => $proc_id,
                                    'ESCE_ID' => $idEscenario
                        ]);

                        $referenciaInf = $referenciaInt * 0.95;
                        $referenciaSup = $referenciaInt * 1.05;

                        DB::update('UPDATE MEPCO_ESCENARIO_CALCULO SET 
                                ESCA_PREFINF=?,
                                ESCA_PREFINT=?,
                                ESCA_PREFSUP=?
                            WHERE
                                ESCA_ID=?', [$referenciaInf, $referenciaInt, $referenciaSup, $idEscenarioCalculo]);

                        $result = $referenciaInf;
                        break;
                    case Util::P_REF_INT:
                        $temp = DB::table('MEPCO_ESCENARIO_CALCULO')
                                ->select('ESCA_PREFINT')
                                ->where('ESCA_ID', '=', $idEscenarioCalculo)
                                ->first();

                        $result = $temp->ESCA_PREFINT;
                        break;
                    case Util::P_REF_SUP:
                        $temp = DB::table('MEPCO_ESCENARIO_CALCULO')
                                ->select('ESCA_PREFSUP')
                                ->where('ESCA_ID', '=', $idEscenarioCalculo)
                                ->first();
                        $result = $temp->ESCA_PREFSUP;
                        break;
                }
                break;
        }

        Util::crearFilaLog('Crear escenario', 'Crear', 'escenario', $idEscenario);

        return response()->json([
                    'result' => Util::formatNumber($result),
                    'idEscenario' => $idEscenario,
                    'idEscenarioCalculo' => $idEscenarioCalculo
        ]);
    }

    public function sugerir(Request $request)
    {
        $idEscenarioCalculo = $request->input('idEscenarioCalculo');
        $state = $request->input('state');
        $state = $state == 'true' ? 1 : 0;
        $tipo  = $request->input('tipo');

        if($tipo == "SUGERIR")
        {
            DB::table('MEPCO_ESCENARIO_CALCULO')
                ->where('ESCA_ID', '=', $idEscenarioCalculo)
                ->update(['ESCA_SUGERIR' => $state]);
        }else 
        {
            //PUBLICADO
            DB::table('MEPCO_ESCENARIO_CALCULO')
                ->where('ESCA_ID', '=', $idEscenarioCalculo)
                ->update(['ESCA_PUBLICADO' => $state]);
        }

        Util::crearFilaLog('Sugerir escenario', 'Sugerir', 'escenario', $idEscenarioCalculo);

        return response()->json([
            'result' => 'SUCCESS'
        ]);
    }

    public function sugerirCalculo(Request $request){

        try {
            if (!session(Util::USUARIO))
            {
                Util::log('SESION EXPIRADA');
                return response()->json([
                    'result' => 'TOUT'
                ]);
            }

            DB::beginTransaction();

            $proc_id = $request->input('proc_id');

            DB::table('MEPCO_ESCENARIO_CALCULO AS MEC')
                ->join('MEPCO_ESCENARIO AS ME', 'MEC.ESCE_ID', '=', 'ME.ESCE_ID')
                ->where('ME.PROC_ID', '=', $proc_id)
                ->update(['MEC.ESCA_PUBLICADO' => '0']);

            DB::table('MEPCO_PROCESO')
                ->where('PROC_ID', '=', $proc_id)
                ->update(['PRES_ID' => Util::PROCESO_ESTADO_PROCESO_SUGERIDO]);

            $validadores = DB::table('MEPCO_USUA_PERF AS MUP')
                ->join('MEPCO_PROC_USPE AS MPU', 'MUP.USPE_ID', '=', 'MPU.USPE_ID')
                ->where('MUP.PERF_ID', '=', Util::PERFIL_USUARIO_VALIDADOR)
                ->where('MPU.PROC_ID', '=', $proc_id)
                ->where('MUP.USPE_ELIMINADO', '=', 0)
                ->select('MUP.USUA_EMAIL')
                ->distinct()
                ->get();


            foreach ($validadores as $item)
            {
                Mail::send('template.email-sugerir', [], function ($m) use ($item)
                {
                    $m->to($item->USUA_EMAIL, '')->subject('MEPCO: Notificación validador');
                });
            }

            Util::crearFilaLog('Sugerir cálculo', 'Sugerir', 'ćálculo para proceso', $proc_id);

            DB::commit();

            return response()->json([
                'result' => 'SUCCESS'
            ]);   
        } catch (\Exception $e) {

            DB::rollBack();
            
            Log::error('EX sugerirCalculo: ' . $e->getMessage());
            return response()->json([
                'result' => 'ERROR',
                'msj'    => 'Error al sugerir cálculo, favor reintentar',
                'ex'     => 'EX: ' . $e->getMessage()
            ]);   
        }
    }

    public function publicarCalculo(Request $request){
        $proc_id   = $request->input('proc_id');
        $tipoCrudo = $request->input('tipoCrudo') == 'true' ? 'BRENT' : 'WTI';

        ini_set('max_execution_time', 120);

        if (!session(Util::USUARIO))
        {
            Util::log('SESION EXPIRADA');
            return response()->json([
                'result' => 'TOUT'
            ]);
        }

        try{
            
            DB::beginTransaction();

            //OBTIENE VM
            $listVM = DB::table('MEPCO_VALOR_MERCADO AS MVM')
                ->join('MEPCO_VAME_USPE AS MVU', 'MVM.VAUS_ID','=', 'MVU.VAUS_ID')
                ->join('MEPCO_USUA_PERF AS MUP', 'MVU.USPE_ID','=', 'MUP.USPE_ID')
                ->select('MVM.*')
                ->where('MVU.VAUS_ELIMINADO', '=','0')
                ->where('MVU.PROC_ID', '=', $proc_id)
                ->where('MUP.PERF_ID', '=', Util::PERFIL_USUARIO_GENERADOR)
                ->orderBy('MVM.VAME_ID', 'ASC')
                ->take(5)
                ->get();

            $arrRegistros = array();

            foreach ($listVM as $item)
            {
                $dahi_id = DB::table('MEPCO_DATOS_HISTORICOS')->insertGetId([
                    'DAHI_FECHA' => $item->VAME_FECHA,
                    'DAHI_GAS_UNL_87' => $item->VAME_GAS_UNL_87,
                    'DAHI_GAS_UNL_93' => $item->VAME_GAS_UNL_93,
                    'DAHI_LD_DIESEL' => $item->VAME_LD_DIESEL,
                    'DAHI_USG_BUTANO' => $item->VAME_USG_BUTANO,
                    'DAHI_UKC_USG' => $item->VAME_UKC_USG,
                    'DAHI_BRENT_CORREGIDO' => $item->VAME_BRENT_CORREGIDO,
                    'DAHI_BRENT' => $item->VAME_BRENT,
                    'DAHI_BRENT_M1' => $item->VAME_BRENT_M1,
                    'DAHI_BRENT_M2' => $item->VAME_BRENT_M2,
                    'DAHI_BRENT_M3' => $item->VAME_BRENT_M3,
                    'DAHI_BRENT_M4' => $item->VAME_BRENT_M4,
                    'DAHI_BRENT_M5' => $item->VAME_BRENT_M5,
                    'DAHI_BRENT_M6' => $item->VAME_BRENT_M6,
                    'DAHI_PPT_USG' => $item->VAME_PPT_USG,
                    'DAHI_MONT_BELVIEU_1' => $item->VAME_MONT_BELVIEU_1,
                    'DAHI_MONT_BELVIEU_2' => $item->VAME_MONT_BELVIEU_2,
                    'DAHI_MONT_BELVIEU' => $item->VAME_MONT_BELVIEU,
                    'DAHI_CIF_ARA_1' => $item->VAME_CIF_ARA_1,
                    'DAHI_CIF_ARA_2' => $item->VAME_CIF_ARA_2,
                    'DAHI_CIF_ARA' => $item->VAME_CIF_ARA,
                    'DAHI_WTI' => $item->VAME_WTI,
                    'DAHI_WTI_M1' => $item->VAME_WTI_M1,
                    'DAHI_WTI_M2' => $item->VAME_WTI_M2,
                    'DAHI_WTI_M3' => $item->VAME_WTI_M3,
                    'DAHI_WTI_M4' => $item->VAME_WTI_M4,
                    'DAHI_WTI_M5' => $item->VAME_WTI_M5,
                    'DAHI_WTI_M6' => $item->VAME_WTI_M6,
                    'DAHI_TC_DOLAR' => $item->VAME_TC_DOLAR,
                    'DAHI_TC_PUBLICADO' => $item->VAME_TC_PUBLICADO,
                    'DAHI_LIBOR' => $item->VAME_LIBOR,
                    'DAHI_TARIFA_DIARIA_82000' => $item->VAME_TARIFA_DIARIA_82000,
                    'DAHI_TARIFA_DIARIA_59000' => $item->VAME_TARIFA_DIARIA_59000,
                    'DAHI_IFO_380_HOUSTON' => $item->VAME_IFO_380_HOUSTON,
                    'DAHI_MDO_HOUSTON' => $item->VAME_MDO_HOUSTON,
                    'DAHI_IFO_380_CRISTOBAL' => $item->VAME_IFO_380_CRISTOBAL,
                    'DAHI_MDO_CRISTOBAL' => $item->VAME_MDO_CRISTOBAL,
                    'DAHI_UTM' => $item->VAME_UTM
                ]);

                $arrRegistros[$item->VAME_FECHA] = $dahi_id;
            }

            $listAux = DB::table('MEPCO_ESCENARIO_CALCULO AS MEC')
                ->join('MEPCO_ESCENARIO_BRENT AS MEB', 'MEC.ESCE_ID', '=', 'MEB.ESCE_ID')
                ->join('MEPCO_ESCENARIO_WTI AS MEW', 'MEC.ESCE_ID', '=', 'MEW.ESCE_ID')
                ->join('MEPCO_ESCENARIO_MARGEN AS MEM', 'MEC.ESCE_ID', '=', 'MEM.ESCE_ID')
                ->join('MEPCO_ESCENARIO AS ME', 'MEB.ESCE_ID', '=', 'ME.ESCE_ID')
                ->select('MEB.*', 'MEC.*', 'MEM.*', 'MEW.*')
                ->where('MEC.ESCA_PUBLICADO', '=', 1)
                ->where('ME.ESCE_ELIMINADO', '=', 0)
                ->where('ME.PROC_ID', '=', $proc_id)
                ->take(4)
                ->get();   

            $arrVariables93     = array();
            $arrVariables97     = array();
            $arrVariablesDIESEL = array();
            $arrVariablesGLP    = array();
            foreach ($listAux as $item) {
                switch ($item->HIDR_ID) {
                    case Util::COMBUSTIBLE_93:
                        $arrVariables93['ESCA_N'] = $item->ESCA_N;
                        $arrVariables93['ESCA_M'] = $item->ESCA_M;
                        $arrVariables93['ESCA_S'] = $item->ESCA_S;
                        $arrVariables93['ESCA_T'] = $item->ESCA_T;
                        $arrVariables93['ESCA_F'] = $item->ESCA_F;
                        $arrVariables93['ESCA_PARIDAD'] = $item->ESCA_PARIDAD;

                        $arrVariables93['ESCA_CORRECCION_RVP'] = $item->ESCA_CORRECCION_RVP;
                        $arrVariables93['ESBR_PROMEDIO_SEM_S_M3'] = $item->ESBR_PROMEDIO_SEM_S_M3;
                        $arrVariables93['ESWT_PROMEDIO_SEM_S_M3'] = $item->ESWT_PROMEDIO_SEM_S_M3;

                        $arrVariables93['ESCA_TIPO_CRUDO'] = $item->ESCA_TIPO_CRUDO;
                        $arrVariables93['BR_ESCA_CRUDO'] = $item->ESCA_CRUDO_BRENT;    
                        $arrVariables93['BR_ESBR_PROMEDIO_SEM_S_M3'] = $item->ESBR_PROMEDIO_SEM_S_M3;
                        $arrVariables93['BR_ESBR_FUTURO'] = $item->ESBR_FUTURO;
                        $arrVariables93['BR_ESBR_HISTORICO'] = $item->ESBR_HISTORICO;
                        $arrVariables93['WT_ESCA_CRUDO'] = $item->ESCA_CRUDO_WTI;    
                        $arrVariables93['WT_ESBR_PROMEDIO_SEM_S_M3'] = $item->ESBR_PROMEDIO_SEM_S_M3;
                        $arrVariables93['WT_ESBR_FUTURO'] = $item->ESBR_FUTURO;
                        $arrVariables93['WT_ESBR_HISTORICO'] = $item->ESBR_HISTORICO;
                        $arrVariables93['ESCA_MARGEN'] = $item->ESCA_MARGEN;
                        $arrVariables93['ESCA_PREFINF'] = $item->ESCA_PREFINF;
                        $arrVariables93['ESCA_PREFINT'] = $item->ESCA_PREFINT;
                        $arrVariables93['ESCA_PREFSUP'] = $item->ESCA_PREFSUP;
                        $arrVariables93['ESCA_SUGERIR'] = $item->ESCA_SUGERIR;
                        $arrVariables93['ESCA_PUBLICADO'] = $item->ESCA_PUBLICADO;
                        $arrVariables93['ESCA_PARIDAD_SEMANAL'] = $item->ESCA_PARIDAD_SEMANAL;
                        $arrVariables93['ESMA_PROMEDIO_BRENT'] = $item->ESMA_PROMEDIO_BRENT;
                        $arrVariables93['ESMA_PROMEDIO_WTI'] = $item->ESMA_PROMEDIO_WTI;
                        $arrVariables93['ESCA_PREFINT'] = $item->ESCA_PREFINT;
                        
                        break;
                    case Util::COMBUSTIBLE_97:
                        $arrVariables97['ESCA_N'] = $item->ESCA_N;
                        $arrVariables97['ESCA_M'] = $item->ESCA_M;
                        $arrVariables97['ESCA_S'] = $item->ESCA_S;
                        $arrVariables97['ESCA_T'] = $item->ESCA_T;
                        $arrVariables97['ESCA_F'] = $item->ESCA_F;
                        $arrVariables97['ESCA_PARIDAD'] = $item->ESCA_PARIDAD;
                        $arrVariables97['ESBR_PROMEDIO_SEM_S_M3'] = $item->ESBR_PROMEDIO_SEM_S_M3;
                        $arrVariables97['ESCA_TIPO_CRUDO'] = $item->ESCA_TIPO_CRUDO;
                        $arrVariables97['BR_ESCA_CRUDO'] = $item->ESCA_CRUDO_BRENT;    
                        $arrVariables97['BR_ESBR_PROMEDIO_SEM_S_M3'] = $item->ESBR_PROMEDIO_SEM_S_M3;
                        $arrVariables97['BR_ESBR_FUTURO'] = $item->ESBR_FUTURO;
                        $arrVariables97['BR_ESBR_HISTORICO'] = $item->ESBR_HISTORICO;
                        $arrVariables97['WT_ESCA_CRUDO'] = $item->ESCA_CRUDO_WTI;    
                        $arrVariables97['WT_ESBR_PROMEDIO_SEM_S_M3'] = $item->ESBR_PROMEDIO_SEM_S_M3;
                        $arrVariables97['WT_ESBR_FUTURO'] = $item->ESBR_FUTURO;
                        $arrVariables97['WT_ESBR_HISTORICO'] = $item->ESBR_HISTORICO;
                        $arrVariables97['ESCA_MARGEN'] = $item->ESCA_MARGEN;
                        $arrVariables97['ESCA_PREFINF'] = $item->ESCA_PREFINF;
                        $arrVariables97['ESCA_PREFINT'] = $item->ESCA_PREFINT;
                        $arrVariables97['ESCA_PREFSUP'] = $item->ESCA_PREFSUP;
                        $arrVariables97['ESCA_SUGERIR'] = $item->ESCA_SUGERIR;
                        $arrVariables97['ESCA_PUBLICADO'] = $item->ESCA_PUBLICADO;
                        $arrVariables97['ESCA_PARIDAD_SEMANAL'] = $item->ESCA_PARIDAD_SEMANAL;
                        $arrVariables97['ESMA_PROMEDIO_BRENT'] = $item->ESMA_PROMEDIO_BRENT;
                        $arrVariables97['ESMA_PROMEDIO_WTI'] = $item->ESMA_PROMEDIO_WTI;
                        $arrVariables97['ESCA_PREFINT'] = $item->ESCA_PREFINT;

                        break;
                    case Util::COMBUSTIBLE_DIESEL:
                        $arrVariablesDIESEL['ESCA_N'] = $item->ESCA_N;
                        $arrVariablesDIESEL['ESCA_M'] = $item->ESCA_M;
                        $arrVariablesDIESEL['ESCA_S'] = $item->ESCA_S;
                        $arrVariablesDIESEL['ESCA_T'] = $item->ESCA_T;
                        $arrVariablesDIESEL['ESCA_F'] = $item->ESCA_F;
                        $arrVariablesDIESEL['ESCA_PARIDAD'] = $item->ESCA_PARIDAD;
                        $arrVariablesDIESEL['ESBR_PROMEDIO_SEM_S_M3'] = $item->ESBR_PROMEDIO_SEM_S_M3;
                        $arrVariablesDIESEL['ESCA_TIPO_CRUDO'] = $item->ESCA_TIPO_CRUDO;
                        $arrVariablesDIESEL['BR_ESCA_CRUDO'] = $item->ESCA_CRUDO_BRENT;    
                        $arrVariablesDIESEL['BR_ESBR_PROMEDIO_SEM_S_M3'] = $item->ESBR_PROMEDIO_SEM_S_M3;
                        $arrVariablesDIESEL['BR_ESBR_FUTURO'] = $item->ESBR_FUTURO;
                        $arrVariablesDIESEL['BR_ESBR_HISTORICO'] = $item->ESBR_HISTORICO;
                        $arrVariablesDIESEL['WT_ESCA_CRUDO'] = $item->ESCA_CRUDO_WTI;    
                        $arrVariablesDIESEL['WT_ESBR_PROMEDIO_SEM_S_M3'] = $item->ESBR_PROMEDIO_SEM_S_M3;
                        $arrVariablesDIESEL['WT_ESBR_FUTURO'] = $item->ESBR_FUTURO;
                        $arrVariablesDIESEL['WT_ESBR_HISTORICO'] = $item->ESBR_HISTORICO;
                        $arrVariablesDIESEL['ESCA_MARGEN'] = $item->ESCA_MARGEN;
                        $arrVariablesDIESEL['ESCA_PREFINF'] = $item->ESCA_PREFINF;
                        $arrVariablesDIESEL['ESCA_PREFINT'] = $item->ESCA_PREFINT;
                        $arrVariablesDIESEL['ESCA_PREFSUP'] = $item->ESCA_PREFSUP;
                        $arrVariablesDIESEL['ESCA_SUGERIR'] = $item->ESCA_SUGERIR;
                        $arrVariablesDIESEL['ESCA_PUBLICADO'] = $item->ESCA_PUBLICADO;
                        $arrVariablesDIESEL['ESCA_PARIDAD_SEMANAL'] = $item->ESCA_PARIDAD_SEMANAL;
                        $arrVariablesDIESEL['ESMA_PROMEDIO_BRENT'] = $item->ESMA_PROMEDIO_BRENT;
                        $arrVariablesDIESEL['ESMA_PROMEDIO_WTI'] = $item->ESMA_PROMEDIO_WTI;
                        $arrVariablesDIESEL['ESCA_PREFINT'] = $item->ESCA_PREFINT;

                        break;
                    case Util::COMBUSTIBLE_GLP:
                        $arrVariablesGLP['ESCA_N'] = $item->ESCA_N;
                        $arrVariablesGLP['ESCA_M'] = $item->ESCA_M;
                        $arrVariablesGLP['ESCA_S'] = $item->ESCA_S;
                        $arrVariablesGLP['ESCA_T'] = $item->ESCA_T;
                        $arrVariablesGLP['ESCA_F'] = $item->ESCA_F;
                        $arrVariablesGLP['ESCA_PARIDAD'] = $item->ESCA_PARIDAD;
                        $arrVariablesGLP['ESBR_PROMEDIO_SEM_S_M3'] = $item->ESBR_PROMEDIO_SEM_S_M3;
                        $arrVariablesGLP['ESCA_TIPO_CRUDO'] = $item->ESCA_TIPO_CRUDO;
                        $arrVariablesGLP['BR_ESCA_CRUDO'] = $item->ESCA_CRUDO_BRENT;    
                        $arrVariablesGLP['BR_ESBR_PROMEDIO_SEM_S_M3'] = $item->ESBR_PROMEDIO_SEM_S_M3;
                        $arrVariablesGLP['BR_ESBR_FUTURO'] = $item->ESBR_FUTURO;
                        $arrVariablesGLP['BR_ESBR_HISTORICO'] = $item->ESBR_HISTORICO;
                        $arrVariablesGLP['WT_ESCA_CRUDO'] = $item->ESCA_CRUDO_WTI;    
                        $arrVariablesGLP['WT_ESBR_PROMEDIO_SEM_S_M3'] = $item->ESBR_PROMEDIO_SEM_S_M3;
                        $arrVariablesGLP['WT_ESBR_FUTURO'] = $item->ESBR_FUTURO;
                        $arrVariablesGLP['WT_ESBR_HISTORICO'] = $item->ESBR_HISTORICO;
                        $arrVariablesGLP['ESCA_MARGEN'] = $item->ESCA_MARGEN;
                        $arrVariablesGLP['ESCA_PREFINF'] = $item->ESCA_PREFINF;
                        $arrVariablesGLP['ESCA_PREFINT'] = $item->ESCA_PREFINT;
                        $arrVariablesGLP['ESCA_PREFSUP'] = $item->ESCA_PREFSUP;
                        $arrVariablesGLP['ESCA_SUGERIR'] = $item->ESCA_SUGERIR;
                        $arrVariablesGLP['ESCA_PUBLICADO'] = $item->ESCA_PUBLICADO;
                        $arrVariablesGLP['ESCA_PARIDAD_SEMANAL'] = $item->ESCA_PARIDAD_SEMANAL;
                        $arrVariablesGLP['ESMA_PROMEDIO_BRENT'] = $item->ESMA_PROMEDIO_BRENT;
                        $arrVariablesGLP['ESMA_PROMEDIO_WTI'] = $item->ESMA_PROMEDIO_WTI;
                        $arrVariablesGLP['ESCA_PREFINT'] = $item->ESCA_PREFINT;

                        break;
                    
                    default:

                        break;
                }
            }

            foreach ($arrRegistros as $item) {
                DB::update('UPDATE MEPCO_DATOS_HISTORICOS SET
                        DAHI_N_GAS_87=?,
                        DAHI_M_GAS_87=?,
                        DAHI_S_GAS_87=?,
                        DAHI_T_GAS_87=?,
                        DAHI_F_GAS_87=?,

                        DAHI_N_GAS_93=?,
                        DAHI_M_GAS_93=?,
                        DAHI_S_GAS_93=?,
                        DAHI_T_GAS_93=?,
                        DAHI_F_GAS_93=?,

                        DAHI_N_DIESEL=?,
                        DAHI_M_DIESEL=?,
                        DAHI_S_DIESEL=?,
                        DAHI_T_DIESEL=?,
                        DAHI_F_DIESEL=?,

                        DAHI_N_GLP=?,
                        DAHI_M_GLP=?,
                        DAHI_S_GLP=?,
                        DAHI_T_GLP=?,
                        DAHI_F_GLP=?,

                        DAHI_PROM_PARIDAD_SEMANAL_GAS_87=?,
                        DAHI_PROM_PARIDAD_SEMANAL_GAS_93=?,
                        DAHI_PROM_PARIDAD_SEMANAL_DIESEL=?,
                        DAHI_PROM_PARIDAD_SEMANAL_GLP=?,

                        DAHI_PROM_BRENT_SEMANAL=?,
                        DAHI_PROM_WTI_SEMANAL=?,

                        DAHI_PARIDAD_GAS_87=?,
                        DAHI_PARIDAD_GAS_93=?,
                        DAHI_PARIDAD_DIESEL=?,
                        DAHI_PARIDAD_GLP=?,

                        DAHI_BRENT_PROM_HISTORICO_GAS_87=?,
                        DAHI_BRENT_PROM_HISTORICO_GAS_93=?,
                        DAHI_BRENT_PROM_HISTORICO_DIESEL=?,
                        DAHI_BRENT_PROM_HISTORICO_GLP=?,

                        DAHI_BRENT_PROM_FUTUROS_GAS_87=?,
                        DAHI_BRENT_PROM_FUTUROS_GAS_93=?,
                        DAHI_BRENT_PROM_FUTUROS_DIESEL=?,
                        DAHI_BRENT_PROM_FUTUROS_GLP=?,

                        DAHI_BRENT_PROM_PONDERADO_GAS_87=?,
                        DAHI_BRENT_PROM_PONDERADO_GAS_93=?,
                        DAHI_BRENT_PROM_PONDERADO_DIESEL=?,
                        DAHI_BRENT_PROM_PONDERADO_GLP=?,

                        DAHI_WTI_PROM_HISTORICO_GAS_87=?,
                        DAHI_WTI_PROM_HISTORICO_GAS_93=?,
                        DAHI_WTI_PROM_HISTORICO_DIESEL=?,
                        DAHI_WTI_PROM_HISTORICO_GLP=?,

                        DAHI_WTI_PROM_FUTUROS_GAS_87=?,
                        DAHI_WTI_PROM_FUTUROS_GAS_93=?,
                        DAHI_WTI_PROM_FUTUROS_DIESEL=?,
                        DAHI_WTI_PROM_FUTUROS_GLP=?,

                        DAHI_WTI_PROM_PONDERADO_GAS_87=?,
                        DAHI_WTI_PROM_PONDERADO_GAS_93=?,
                        DAHI_WTI_PROM_PONDERADO_DIESEL=?,
                        DAHI_WTI_PROM_PONDERADO_GLP=?,

                        DAHI_PROM_MARGEN_SEMANAL_BRENT_GAS_87=?,
                        DAHI_PROM_MARGEN_SEMANAL_BRENT_GAS_93=?,
                        DAHI_PROM_MARGEN_SEMANAL_BRENT_DIESEL=?,
                        DAHI_PROM_MARGEN_SEMANAL_BRENT_GLP=?,

                        DAHI_PROM_MARGEN_SEMANAL_WTI_GAS_87=?,
                        DAHI_PROM_MARGEN_SEMANAL_WTI_GAS_93=?,
                        DAHI_PROM_MARGEN_SEMANAL_WTI_DIESEL=?,
                        DAHI_PROM_MARGEN_SEMANAL_WTI_GLP=?,

                        DAHI_MARGEN_PROMEDIO_GAS_87=?,
                        DAHI_MARGEN_PROMEDIO_GAS_93=?,
                        DAHI_MARGEN_PROMEDIO_DIESEL=?,
                        DAHI_MARGEN_PROMEDIO_GLP=?,

                        DAHI_PROM_REFERENCIA_SEMANAL_GAS_87=?,
                        DAHI_PROM_REFERENCIA_SEMANAL_GAS_93=?,
                        DAHI_PROM_REFERENCIA_SEMANAL_DIESEL=?,
                        DAHI_PROM_REFERENCIA_SEMANAL_GLP=?,

                        DAHI_CORRECCION_RVP=?,

                        DAHI_TIPO_CRUDO=?

                        WHERE DAHI_ID=?',[
                            $arrVariables93['ESCA_N'],
                            $arrVariables93['ESCA_M'],
                            $arrVariables93['ESCA_S'],
                            $arrVariables93['ESCA_T'],
                            $arrVariables93['ESCA_F'],

                            $arrVariables97['ESCA_N'],
                            $arrVariables97['ESCA_M'],
                            $arrVariables97['ESCA_S'],
                            $arrVariables97['ESCA_T'],
                            $arrVariables97['ESCA_F'],

                            $arrVariablesDIESEL['ESCA_N'],
                            $arrVariablesDIESEL['ESCA_M'],
                            $arrVariablesDIESEL['ESCA_S'],
                            $arrVariablesDIESEL['ESCA_T'],
                            $arrVariablesDIESEL['ESCA_F'],

                            $arrVariablesGLP['ESCA_N'],
                            $arrVariablesGLP['ESCA_M'],
                            $arrVariablesGLP['ESCA_S'],
                            $arrVariablesGLP['ESCA_T'],
                            $arrVariablesGLP['ESCA_F'],

                            $arrVariables93['ESCA_PARIDAD_SEMANAL'],
                            $arrVariables97['ESCA_PARIDAD_SEMANAL'],
                            $arrVariablesDIESEL['ESCA_PARIDAD_SEMANAL'],
                            $arrVariablesGLP['ESCA_PARIDAD_SEMANAL'],

                            $arrVariables93['ESBR_PROMEDIO_SEM_S_M3'],
                            $arrVariables93['ESWT_PROMEDIO_SEM_S_M3'],

                            $arrVariables93['ESCA_PARIDAD'],
                            $arrVariables97['ESCA_PARIDAD'],
                            $arrVariablesDIESEL['ESCA_PARIDAD'],
                            $arrVariablesGLP['ESCA_PARIDAD'],

                            $arrVariables93['BR_ESBR_HISTORICO'],
                            $arrVariables97['BR_ESBR_HISTORICO'],
                            $arrVariablesDIESEL['BR_ESBR_HISTORICO'],
                            $arrVariablesGLP['BR_ESBR_HISTORICO'],

                            $arrVariables93['BR_ESBR_FUTURO'],
                            $arrVariables97['BR_ESBR_FUTURO'],
                            $arrVariablesDIESEL['BR_ESBR_FUTURO'],
                            $arrVariablesGLP['BR_ESBR_FUTURO'],

                            $arrVariables93['BR_ESCA_CRUDO'],
                            $arrVariables97['BR_ESCA_CRUDO'],
                            $arrVariablesDIESEL['BR_ESCA_CRUDO'],
                            $arrVariablesGLP['BR_ESCA_CRUDO'],

                            $arrVariables93['WT_ESBR_HISTORICO'],
                            $arrVariables97['WT_ESBR_HISTORICO'],
                            $arrVariablesDIESEL['WT_ESBR_HISTORICO'],
                            $arrVariablesGLP['WT_ESBR_HISTORICO'],

                            $arrVariables93['WT_ESBR_FUTURO'],
                            $arrVariables97['WT_ESBR_FUTURO'],
                            $arrVariablesDIESEL['WT_ESBR_FUTURO'],
                            $arrVariablesGLP['WT_ESBR_FUTURO'],

                            $arrVariables93['WT_ESCA_CRUDO'],
                            $arrVariables97['WT_ESCA_CRUDO'],
                            $arrVariablesDIESEL['WT_ESCA_CRUDO'],
                            $arrVariablesGLP['WT_ESCA_CRUDO'],

                            $arrVariables93['ESMA_PROMEDIO_BRENT'],
                            $arrVariables97['ESMA_PROMEDIO_BRENT'],
                            $arrVariablesDIESEL['ESMA_PROMEDIO_BRENT'],
                            $arrVariablesGLP['ESMA_PROMEDIO_BRENT'],

                            $arrVariables93['ESMA_PROMEDIO_WTI'],
                            $arrVariables97['ESMA_PROMEDIO_WTI'],
                            $arrVariablesDIESEL['ESMA_PROMEDIO_WTI'],
                            $arrVariablesGLP['ESMA_PROMEDIO_WTI'],

                            $arrVariables93['ESCA_MARGEN'],
                            $arrVariables97['ESCA_MARGEN'],
                            $arrVariablesDIESEL['ESCA_MARGEN'],
                            $arrVariablesGLP['ESCA_MARGEN'],

                            $arrVariables93['ESCA_PREFINT'],
                            $arrVariables97['ESCA_PREFINT'],
                            $arrVariablesDIESEL['ESCA_PREFINT'],
                            $arrVariablesGLP['ESCA_PREFINT'],

                            $arrVariables93['ESCA_CORRECCION_RVP'],

                            $tipoCrudo,

                            $item
                        ]);
            }

            //OBTIENE FOB TEORICO DE REF
            $listAux = DB::table('MEPCO_ESCENARIO_REFERENCIA AS MER')
                ->join('MEPCO_ESCENARIO AS ME', 'MER.ESCE_ID', '=', 'ME.ESCE_ID')
                ->join('MEPCO_ESCENARIO_CALCULO AS MEC', 'MER.ESCE_ID', '=', 'MEC.ESCE_ID')
                ->select('MER.HIDR_ID','MER.ESRE_FOB_TEORICO')
                ->where('MEC.ESCA_PUBLICADO', '=', 1)
                ->where('ME.ESCE_ELIMINADO', '=', 0)
                ->where('ME.PROC_ID', '=', $proc_id)
                ->distinct()
                ->take(4)
                ->get();  

            $arrVariables93     = array();
            $arrVariables97     = array();
            $arrVariablesDIESEL = array();
            $arrVariablesGLP    = array();
            foreach ($listAux as $item) {
                switch ($item->HIDR_ID) {
                    case Util::COMBUSTIBLE_93:
                        $arrVariables93['ESRE_FOB_TEORICO'] = $item->ESRE_FOB_TEORICO;
                        break;
                    case Util::COMBUSTIBLE_97:
                        $arrVariables97['ESRE_FOB_TEORICO'] = $item->ESRE_FOB_TEORICO;
                        break;
                    case Util::COMBUSTIBLE_DIESEL:
                        $arrVariablesDIESEL['ESRE_FOB_TEORICO'] = $item->ESRE_FOB_TEORICO;
                        break;
                    case Util::COMBUSTIBLE_GLP:
                        $arrVariablesGLP['ESRE_FOB_TEORICO'] = $item->ESRE_FOB_TEORICO;
                        break;
                }
            }

            foreach ($arrRegistros as $item) {
                DB::update('UPDATE MEPCO_DATOS_HISTORICOS SET
                    DAHI_FOB_TEORICO_GAS_87=?,
                    DAHI_FOB_TEORICO_GAS_93=?,
                    DAHI_FOB_TEORICO_DIESEL=?,
                    DAHI_FOB_TEORICO_GLP=?
                WHERE DAHI_ID=?',[
                    $arrVariables93['ESRE_FOB_TEORICO'],
                    $arrVariables97['ESRE_FOB_TEORICO'],
                    $arrVariablesDIESEL['ESRE_FOB_TEORICO'],
                    $arrVariablesGLP['ESRE_FOB_TEORICO'],
                    $item
                ]);
            }

            //ACTUALIZA PROCESO
            DB::update('UPDATE MEPCO_PROCESO SET PRES_ID=? WHERE PROC_ID=?',[
                Util::PROCESO_ESTADO_FINALIZADO,
                $proc_id
            ]);

            $arrAuxIdEscenario = DB::table('MEPCO_ESCENARIO AS ME')
                ->join('MEPCO_ESCENARIO_CALCULO AS MEC', 'ME.ESCE_ID', '=', 'MEC.ESCE_ID')
                ->select('ME.ESCE_ID')
                ->where('ME.PROC_ID', '=', $proc_id)
                ->where('MEC.ESCA_PUBLICADO', '=', 1)
                ->get();

            foreach ($arrAuxIdEscenario as $item) {
                DB::update('UPDATE MEPCO_ESCENARIO SET ESCE_FECHA_VALIDADO=?, USPE_ID_VALIDADO=? WHERE ESCE_ID=?',[
                    Util::dateTimeNow(),
                    session(Util::USUARIO)->USPE_ID,
                    $item->ESCE_ID
                ]);
            }

            //OBTIENE USUARIO ASOCOADOS AL PROCESO
            $usuarios = DB::table('MEPCO_USUA_PERF AS MUP')
                    ->join('MEPCO_PROC_USPE AS MPU', 'MUP.USPE_ID', '=', 'MPU.USPE_ID')
                    ->where('MPU.PROC_ID', '=', $proc_id)
                    ->where('MUP.USPE_ELIMINADO', '=', 0)
                    ->select('MUP.USUA_EMAIL')
                    ->distinct()
                    ->get();

            $param = [
                'fecha_vigencia'  => DB::table('MEPCO_PROCESO')
                    ->select('PROC_FECHA_VIGENCIA')
                    ->where('PROC_ID', $proc_id)
                    ->pluck('PROC_FECHA_VIGENCIA')
            ];

            //ENVIA EMAIL NOTIFICACION DE PROCESO PUBLICADO
            foreach ($usuarios as $item)
            {
                Mail::send('template.email-publicar', $param, function ($m) use ($item)
                {
                    $m->to($item->USUA_EMAIL, '')->subject('MEPCO: Notificación');
                });
            }

            //PROYECCION
            Util::$almacenaFormulaValoresSinFecha = true;

            $proceso = DB::table('MEPCO_PROCESO')
                ->select('PROC_FECHA_VIGENCIA')
                ->where('PROC_ID', '=', $proc_id)
                ->first();

            $parametros = DB::table('MEPCO_DATOS_HISTORICOS')
                ->select('DAHI_N_GAS_87',
                    'DAHI_M_GAS_87',
                    'DAHI_S_GAS_87',
                    'DAHI_T_GAS_87',
                    'DAHI_F_GAS_87',

                    'DAHI_N_GAS_93',
                    'DAHI_M_GAS_93',
                    'DAHI_S_GAS_93',
                    'DAHI_T_GAS_93',
                    'DAHI_F_GAS_93',

                    'DAHI_N_DIESEL',
                    'DAHI_M_DIESEL',
                    'DAHI_S_DIESEL',
                    'DAHI_T_DIESEL',
                    'DAHI_F_DIESEL',

                    'DAHI_N_GLP',
                    'DAHI_M_GLP',
                    'DAHI_S_GLP',
                    'DAHI_T_GLP',
                    'DAHI_F_GLP',

                    'DAHI_CORRECCION_RVP')
                ->where('DAHI_ID', '=', array_values($arrRegistros)[0])
                ->first();

            $cantidadLimiteProyeccion = 11;

            /**********************************************************************/
            /*                                 G87                                 /*
            /**********************************************************************/
            $n             = $parametros->DAHI_N_GAS_87;
            $m             = $parametros->DAHI_M_GAS_87;
            $s             = $parametros->DAHI_S_GAS_87;
            $t             = $parametros->DAHI_T_GAS_87;
            $f             = $parametros->DAHI_F_GAS_87;
            $correccionRvp = $parametros->DAHI_CORRECCION_RVP;

            $paridad_ca = Util::ejecutarFormula('{{PY_PA_PARIDAD_CA_G_87}}', [
                'PROC_ID'        => $proc_id,
                'CORRECCION_RVP' => $correccionRvp
            ]);        

            $auxFechaVigencia93 = null;
            $auxFechaVigencia93 = Carbon::parse($proceso->PROC_FECHA_VIGENCIA);


            for ($i = 1; $i <= $cantidadLimiteProyeccion; $i++){

                $auxFechaVigencia93 = $auxFechaVigencia93->addDays(7);

                //INICIO PARIDAD
                $idProyeccion = DB::table('MEPCO_PROYECCION')->insertGetId([
                    'PROC_ID'            => $proc_id,
                    'HIDR_ID'            => Util::COMBUSTIBLE_93,
                    'PROY_PA_PARIDAD_CA' => $paridad_ca,
                    'PROY_FECHA'         => $auxFechaVigencia93
                ]);

                $proyeccion = ($i > $t) ? $t : $i;

                $paridadSemanal = Util::ejecutarFormula('{{PY_PA_PARIDAD_PROMEDIO_SEMANAL_ACTUAL_G_87}}', [
                    'PROC_ID' => $proc_id
                ]);

                DB::update('UPDATE MEPCO_PROYECCION SET PROY_PA_PARIDAD_SEMANAL=? WHERE PROY_ID=?',[
                    $paridadSemanal,
                    $idProyeccion
                ]);

                $paridad = Util::ejecutarFormula('{{PY_PA_PARIDAD_$/m_G_87}}', [
                    'PROC_ID'    => $proc_id,
                    'T'          => $t,
                    'LIMIT'      => (5 * ($t - $proyeccion)),
                    'PROYECCION' => $proyeccion
                ]);
                
                DB::update('UPDATE MEPCO_PROYECCION SET PROY_PA_PARIDAD=? WHERE PROY_ID=?',[
                    $paridad,
                    $idProyeccion
                ]);
                //FIN PARIDAD

                //INICIO CRUDO
                $proyeccion = ($i > $n) ? $n : $i;

                if($tipoCrudo == 'BRENT')
                {
                    $crudo = Util::ejecutarFormula('{{PY_BR_BRENT_CALCULADO}}', [
                        'PROC_ID'        => $proc_id,
                        'M'              => $m,
                        'N'              => $n,
                        'F'              => $f,
                        'LIMIT'          => (5 * ($n - $proyeccion)),
                        'CORRECCION_RVP' => $correccionRvp,
                        'PROYECCION'     => $proyeccion
                    ]);

                    DB::update('UPDATE MEPCO_PROYECCION SET 
                                PROY_TIPO_CRUDO=?, 
                                PROY_CRUDO=?,
                                PROY_CRUDO_PROM_SEM_S=?,
                                PROY_CRUDO_HISTORICO=?,
                                PROY_CRUDO_FUTURO=? 
                            WHERE PROY_ID=?',[
                        'BRENT',
                        $crudo,
                        Util::$arrFormulaValores['PY_BR_BRENT_PROMEDIO_SEM_$/M3'],
                        Util::$arrFormulaValores['PY_BR_BRENT_HISTORICO'],
                        Util::$arrFormulaValores['PY_BR_BRENT_FUTURO'],
                        $idProyeccion
                    ]);
                }else
                {
                    $crudo = Util::ejecutarFormula('{{PY_WT_WTI_CALCULADO}}', [
                        'PROC_ID'        => $proc_id,
                        'M'              => $m,
                        'N'              => $n,
                        'F'              => $f,
                        'LIMIT'          => (5 * ($n - $proyeccion)),
                        'CORRECCION_RVP' => $correccionRvp,
                        'PROYECCION'     => $proyeccion
                    ]);

                    DB::update('UPDATE MEPCO_PROYECCION SET 
                                PROY_TIPO_CRUDO=?, 
                                PROY_CRUDO=?,
                                PROY_CRUDO_PROM_SEM_S=?,
                                PROY_CRUDO_HISTORICO=?,
                                PROY_CRUDO_FUTURO=? 
                            WHERE PROY_ID=?',[
                        'WTI',
                        $crudo,
                        Util::$arrFormulaValores['PY_WT_WTI_PROMEDIO_SEM_$/M3'],
                        Util::$arrFormulaValores['PY_WT_WTI_HISTORICO'],
                        Util::$arrFormulaValores['PY_WT_WTI_FUTURO'],
                        $idProyeccion
                    ]);
                }
                //FIN CRUDO

                //INICIO MARGEN
                $proyeccion = ($i > $s) ? $s : $i;

                if($tipoCrudo == 'BRENT')
                {
                    $margenSemanal = Util::ejecutarFormula('{{PY_MG_PROMEDIO_SEMANAL_BRENT_G_87}}', [
                        'PROC_ID'    => $proc_id
                    ]);

                    DB::update('UPDATE MEPCO_PROYECCION SET PROY_MARGEN_PROM_SEM=? WHERE PROY_ID=?',[
                        $margenSemanal,
                        $idProyeccion
                    ]);

                    $margen = Util::ejecutarFormula('{{PY_MG_MARGEN_BRENT_G_87}}', [
                        'PROC_ID'    => $proc_id,
                        'S'          => $s,
                        'LIMIT'      => (5 * ($s - $proyeccion)),
                        'PROYECCION' => $proyeccion
                    ]);
                }else
                {
                    $margenSemanal = Util::ejecutarFormula('{{PY_MG_PROMEDIO_SEMANAL_WTI_G_87}}', [
                        'PROC_ID'    => $proc_id
                    ]);

                    DB::update('UPDATE MEPCO_PROYECCION SET PROY_MARGEN_PROM_SEM=? WHERE PROY_ID=?',[
                        $margenSemanal,
                        $idProyeccion
                    ]);

                    $margen = Util::ejecutarFormula('{{PY_MG_MARGEN_WTI_G_87}}', [
                        'PROC_ID'    => $proc_id,
                        'S'          => $s,
                        'LIMIT'      => (5 * ($s - $proyeccion)),
                        'PROYECCION' => $proyeccion
                    ]);
                }

                DB::update('UPDATE MEPCO_PROYECCION SET PROY_MARGEN=? WHERE PROY_ID=?',[
                    $margen,
                    $idProyeccion
                ]);
                //FIN MARGEN

                //INICIO REFERENCIA
                $referenciaCa = Util::ejecutarFormula('{{PY_RE_PARIDAD_CA_G_87}}', [
                    'PROC_ID'        => $proc_id,
                    'CORRECCION_RVP' => $correccionRvp
                ]);

                DB::update('UPDATE MEPCO_PROYECCION SET PROY_RE_PARIDAD_CA=?, PROY_RE_PARIDAD=? WHERE PROY_ID=?',[
                    $referenciaCa,
                    $referenciaCa,
                    $idProyeccion
                ]);
                //FIN REFERENCIA
            }

            /**********************************************************************/
            /*                                 G93                                */
            /**********************************************************************/
            $n             = $parametros->DAHI_N_GAS_93;
            $m             = $parametros->DAHI_M_GAS_93;
            $s             = $parametros->DAHI_S_GAS_93;
            $t             = $parametros->DAHI_T_GAS_93;
            $f             = $parametros->DAHI_F_GAS_93;
            $correccionRvp = $parametros->DAHI_CORRECCION_RVP;

            $paridad_ca = Util::ejecutarFormula('{{PY_PA_PARIDAD_CA_G_93}}', [
                'PROC_ID'        => $proc_id,
                'CORRECCION_RVP' => $correccionRvp
            ]);        

            $auxFechaVigencia97 = null;
            $auxFechaVigencia97 = Carbon::parse($proceso->PROC_FECHA_VIGENCIA);

            for ($i = 1; $i <= $cantidadLimiteProyeccion; $i++){

                $auxFechaVigencia97 = $auxFechaVigencia97->addDays(7);

                //INICIO PARIDAD
                $idProyeccion = DB::table('MEPCO_PROYECCION')->insertGetId([
                    'PROC_ID'            => $proc_id,
                    'HIDR_ID'            => Util::COMBUSTIBLE_97,
                    'PROY_PA_PARIDAD_CA' => $paridad_ca,
                    'PROY_FECHA'         => $auxFechaVigencia97
                ]);

                $proyeccion = ($i > $t) ? $t : $i;

                $paridadSemanal = Util::ejecutarFormula('{{PY_PA_PARIDAD_PROMEDIO_SEMANAL_ACTUAL_G_93}}', [
                    'PROC_ID' => $proc_id
                ]);

                DB::update('UPDATE MEPCO_PROYECCION SET PROY_PA_PARIDAD_SEMANAL=? WHERE PROY_ID=?',[
                    $paridadSemanal,
                    $idProyeccion
                ]);

                $paridad = Util::ejecutarFormula('{{PY_PA_PARIDAD_$/m_G_93}}', [
                    'PROC_ID'    => $proc_id,
                    'T'          => $t,
                    'LIMIT'      => (5 * ($t - $proyeccion)),
                    'PROYECCION' => $proyeccion
                ]);
                
                DB::update('UPDATE MEPCO_PROYECCION SET PROY_PA_PARIDAD=? WHERE PROY_ID=?',[
                    $paridad,
                    $idProyeccion
                ]);
                //FIN PARIDAD

                //INICIO CRUDO
                $proyeccion = ($i > $n) ? $n : $i;

                if($tipoCrudo == 'BRENT')
                {
                    $crudo = Util::ejecutarFormula('{{PY_BR_BRENT_CALCULADO}}', [
                        'PROC_ID'        => $proc_id,
                        'M'              => $m,
                        'N'              => $n,
                        'F'              => $f,
                        'LIMIT'          => (5 * ($n - $proyeccion)),
                        'CORRECCION_RVP' => $correccionRvp,
                        'PROYECCION'     => $proyeccion
                    ]);

                    DB::update('UPDATE MEPCO_PROYECCION SET 
                                PROY_TIPO_CRUDO=?, 
                                PROY_CRUDO=?,
                                PROY_CRUDO_PROM_SEM_S=?,
                                PROY_CRUDO_HISTORICO=?,
                                PROY_CRUDO_FUTURO=? 
                            WHERE PROY_ID=?',[
                        'BRENT',
                        $crudo,
                        Util::$arrFormulaValores['PY_BR_BRENT_PROMEDIO_SEM_$/M3'],
                        Util::$arrFormulaValores['PY_BR_BRENT_HISTORICO'],
                        Util::$arrFormulaValores['PY_BR_BRENT_FUTURO'],
                        $idProyeccion
                    ]);
                }else
                {
                    $crudo = Util::ejecutarFormula('{{PY_WT_WTI_CALCULADO}}', [
                        'PROC_ID'        => $proc_id,
                        'M'              => $m,
                        'N'              => $n,
                        'F'              => $f,
                        'LIMIT'          => (5 * ($n - $proyeccion)),
                        'CORRECCION_RVP' => $correccionRvp,
                        'PROYECCION'     => $proyeccion
                    ]);

                    DB::update('UPDATE MEPCO_PROYECCION SET 
                                PROY_TIPO_CRUDO=?, 
                                PROY_CRUDO=?,
                                PROY_CRUDO_PROM_SEM_S=?,
                                PROY_CRUDO_HISTORICO=?,
                                PROY_CRUDO_FUTURO=? 
                            WHERE PROY_ID=?',[
                        'WTI',
                        $crudo,
                        Util::$arrFormulaValores['PY_WT_WTI_PROMEDIO_SEM_$/M3'],
                        Util::$arrFormulaValores['PY_WT_WTI_HISTORICO'],
                        Util::$arrFormulaValores['PY_WT_WTI_FUTURO'],
                        $idProyeccion
                    ]);
                }
                //FIN CRUDO

                //INICIO MARGEN
                $proyeccion = ($i > $s) ? $s : $i;

                if($tipoCrudo == 'BRENT')
                {
                    $margenSemanal = Util::ejecutarFormula('{{PY_MG_PROMEDIO_SEMANAL_BRENT_G_93}}', [
                        'PROC_ID'    => $proc_id
                    ]);

                    DB::update('UPDATE MEPCO_PROYECCION SET PROY_MARGEN_PROM_SEM=? WHERE PROY_ID=?',[
                        $margenSemanal,
                        $idProyeccion
                    ]);

                    $margen = Util::ejecutarFormula('{{PY_MG_MARGEN_BRENT_G_93}}', [
                        'PROC_ID'    => $proc_id,
                        'S'          => $s,
                        'LIMIT'      => (5 * ($s - $proyeccion)),
                        'PROYECCION' => $proyeccion
                    ]);
                }else
                {
                    $margenSemanal = Util::ejecutarFormula('{{PY_MG_PROMEDIO_SEMANAL_WTI_G_93}}', [
                        'PROC_ID'    => $proc_id
                    ]);

                    DB::update('UPDATE MEPCO_PROYECCION SET PROY_MARGEN_PROM_SEM=? WHERE PROY_ID=?',[
                        $margenSemanal,
                        $idProyeccion
                    ]);

                    $margen = Util::ejecutarFormula('{{PY_MG_MARGEN_WTI_G_93}}', [
                        'PROC_ID'    => $proc_id,
                        'S'          => $s,
                        'LIMIT'      => (5 * ($s - $proyeccion)),
                        'PROYECCION' => $proyeccion
                    ]);
                }

                DB::update('UPDATE MEPCO_PROYECCION SET PROY_MARGEN=? WHERE PROY_ID=?',[
                    $margen,
                    $idProyeccion
                ]);
                //FIN MARGEN

                //INICIO REFERENCIA
                $referenciaCa = Util::ejecutarFormula('{{PY_RE_PARIDAD_CA_G_93}}', [
                    'PROC_ID'        => $proc_id,
                    'CORRECCION_RVP' => $correccionRvp
                ]);

                DB::update('UPDATE MEPCO_PROYECCION SET PROY_RE_PARIDAD_CA=?, PROY_RE_PARIDAD=? WHERE PROY_ID=?',[
                    $referenciaCa,
                    $referenciaCa,
                    $idProyeccion
                ]);
                //FIN REFERENCIA           
            }

            /**********************************************************************/
            /*                                 DIESEL                             */
            /**********************************************************************/
            $n             = $parametros->DAHI_N_DIESEL;
            $m             = $parametros->DAHI_M_DIESEL;
            $s             = $parametros->DAHI_S_DIESEL;
            $t             = $parametros->DAHI_T_DIESEL;
            $f             = $parametros->DAHI_F_DIESEL;
            $correccionRvp = $parametros->DAHI_CORRECCION_RVP;

            $paridad_ca = Util::ejecutarFormula('{{PY_PA_PARIDAD_CA_D}}', [
                'PROC_ID'        => $proc_id,
                'CORRECCION_RVP' => $correccionRvp
            ]);        

            $auxFechaVigenciaDIESEL = null;
            $auxFechaVigenciaDIESEL = Carbon::parse($proceso->PROC_FECHA_VIGENCIA);

            for ($i = 1; $i <= $cantidadLimiteProyeccion; $i++){

                $auxFechaVigenciaDIESEL = $auxFechaVigenciaDIESEL->addDays(7);

                //INICIO PARIDAD
                $idProyeccion = DB::table('MEPCO_PROYECCION')->insertGetId([
                    'PROC_ID'            => $proc_id,
                    'HIDR_ID'            => Util::COMBUSTIBLE_DIESEL,
                    'PROY_PA_PARIDAD_CA' => $paridad_ca,
                    'PROY_FECHA'         => $auxFechaVigenciaDIESEL
                ]);

                $proyeccion = ($i > $t) ? $t : $i;

                $paridadSemanal = Util::ejecutarFormula('{{PY_PA_PARIDAD_PROMEDIO_SEMANAL_ACTUAL_DIESEL}}', [
                    'PROC_ID' => $proc_id
                ]);

                DB::update('UPDATE MEPCO_PROYECCION SET PROY_PA_PARIDAD_SEMANAL=? WHERE PROY_ID=?',[
                    $paridadSemanal,
                    $idProyeccion
                ]);

                $paridad = Util::ejecutarFormula('{{PY_PA_PARIDAD_$/m_DIESEL}}', [
                    'PROC_ID'    => $proc_id,
                    'T'          => $t,
                    'LIMIT'      => (5 * ($t - $proyeccion)),
                    'PROYECCION' => $proyeccion
                ]);
                
                DB::update('UPDATE MEPCO_PROYECCION SET PROY_PA_PARIDAD=? WHERE PROY_ID=?',[
                    $paridad,
                    $idProyeccion
                ]);
                //FIN PARIDAD

                //INICIO CRUDO
                $proyeccion = ($i > $n) ? $n : $i;

                if($tipoCrudo == 'BRENT')
                {
                    $crudo = Util::ejecutarFormula('{{PY_BR_BRENT_CALCULADO}}', [
                        'PROC_ID'        => $proc_id,
                        'M'              => $m,
                        'N'              => $n,
                        'F'              => $f,
                        'LIMIT'          => (5 * ($n - $proyeccion)),
                        'CORRECCION_RVP' => $correccionRvp,
                        'PROYECCION'     => $proyeccion
                    ]);

                    DB::update('UPDATE MEPCO_PROYECCION SET 
                                PROY_TIPO_CRUDO=?, 
                                PROY_CRUDO=?,
                                PROY_CRUDO_PROM_SEM_S=?,
                                PROY_CRUDO_HISTORICO=?,
                                PROY_CRUDO_FUTURO=? 
                            WHERE PROY_ID=?',[
                        'BRENT',
                        $crudo,
                        Util::$arrFormulaValores['PY_BR_BRENT_PROMEDIO_SEM_$/M3'],
                        Util::$arrFormulaValores['PY_BR_BRENT_HISTORICO'],
                        Util::$arrFormulaValores['PY_BR_BRENT_FUTURO'],
                        $idProyeccion
                    ]);
                }else
                {
                    $crudo = Util::ejecutarFormula('{{PY_WT_WTI_CALCULADO}}', [
                        'PROC_ID'        => $proc_id,
                        'M'              => $m,
                        'N'              => $n,
                        'F'              => $f,
                        'LIMIT'          => (5 * ($n - $proyeccion)),
                        'CORRECCION_RVP' => $correccionRvp,
                        'PROYECCION'     => $proyeccion
                    ]);

                    DB::update('UPDATE MEPCO_PROYECCION SET 
                                PROY_TIPO_CRUDO=?, 
                                PROY_CRUDO=?,
                                PROY_CRUDO_PROM_SEM_S=?,
                                PROY_CRUDO_HISTORICO=?,
                                PROY_CRUDO_FUTURO=? 
                            WHERE PROY_ID=?',[
                        'WTI',
                        $crudo,
                        Util::$arrFormulaValores['PY_WT_WTI_PROMEDIO_SEM_$/M3'],
                        Util::$arrFormulaValores['PY_WT_WTI_HISTORICO'],
                        Util::$arrFormulaValores['PY_WT_WTI_FUTURO'],
                        $idProyeccion
                    ]);
                }
                //FIN CRUDO

                //INICIO MARGEN
                $proyeccion = ($i > $s) ? $s : $i;

                if($tipoCrudo == 'BRENT')
                {
                    $margenSemanal = Util::ejecutarFormula('{{PY_MG_PROMEDIO_SEMANAL_BRENT_DIESEL}}', [
                        'PROC_ID'    => $proc_id
                    ]);

                    DB::update('UPDATE MEPCO_PROYECCION SET PROY_MARGEN_PROM_SEM=? WHERE PROY_ID=?',[
                        $margenSemanal,
                        $idProyeccion
                    ]);

                    $margen = Util::ejecutarFormula('{{PY_MG_MARGEN_BRENT_DIESEL}}', [
                        'PROC_ID'    => $proc_id,
                        'S'          => $s,
                        'LIMIT'      => (5 * ($s - $proyeccion)),
                        'PROYECCION' => $proyeccion
                    ]);
                }else
                {
                    $margenSemanal = Util::ejecutarFormula('{{PY_MG_PROMEDIO_SEMANAL_WTI_DIESEL}}', [
                        'PROC_ID'    => $proc_id
                    ]);

                    DB::update('UPDATE MEPCO_PROYECCION SET PROY_MARGEN_PROM_SEM=? WHERE PROY_ID=?',[
                        $margenSemanal,
                        $idProyeccion
                    ]);

                    $margen = Util::ejecutarFormula('{{PY_MG_MARGEN_WTI_DIESEL}}', [
                        'PROC_ID'    => $proc_id,
                        'S'          => $s,
                        'LIMIT'      => (5 * ($s - $proyeccion)),
                        'PROYECCION' => $proyeccion
                    ]);
                }

                DB::update('UPDATE MEPCO_PROYECCION SET PROY_MARGEN=? WHERE PROY_ID=?',[
                    $margen,
                    $idProyeccion
                ]);
                //FIN MARGEN

                //INICIO REFERENCIA
                $referenciaCa = Util::ejecutarFormula('{{PY_RE_PARIDAD_CA_D}}', [
                    'PROC_ID'        => $proc_id,
                    'CORRECCION_RVP' => $correccionRvp, 
                    'TIPO_CRUDO'     => $tipoCrudo
                ]);

                DB::update('UPDATE MEPCO_PROYECCION SET PROY_RE_PARIDAD_CA=?, PROY_RE_PARIDAD=? WHERE PROY_ID=?',[
                    $referenciaCa,
                    $referenciaCa,
                    $idProyeccion
                ]);
                //FIN REFERENCIA
            }

            /**********************************************************************/
            /*                                 GLP                                */
            /**********************************************************************/
            $n             = $parametros->DAHI_N_GLP;
            $m             = $parametros->DAHI_M_GLP;
            $s             = $parametros->DAHI_S_GLP;
            $t             = $parametros->DAHI_T_GLP;
            $f             = $parametros->DAHI_F_GLP;
            $correccionRvp = $parametros->DAHI_CORRECCION_RVP;

            $paridad_ca = Util::ejecutarFormula('{{PY_PA_PARIDAD_CA_GLP}}', [
                'PROC_ID'        => $proc_id,
                'CORRECCION_RVP' => $correccionRvp
            ]);        

            $auxFechaVigenciaGLP = null;
            $auxFechaVigenciaGLP = Carbon::parse($proceso->PROC_FECHA_VIGENCIA);

            for ($i = 1; $i <= $cantidadLimiteProyeccion; $i++){

                $auxFechaVigenciaGLP = $auxFechaVigenciaGLP->addDays(7);

                //INICIO PARIDAD
                $idProyeccion = DB::table('MEPCO_PROYECCION')->insertGetId([
                    'PROC_ID'            => $proc_id,
                    'HIDR_ID'            => Util::COMBUSTIBLE_GLP,
                    'PROY_PA_PARIDAD_CA' => $paridad_ca,
                    'PROY_FECHA'         => $auxFechaVigenciaGLP
                ]);

                $proyeccion = ($i > $t) ? $t : $i;

                $paridadSemanal = Util::ejecutarFormula('{{PY_PA_PARIDAD_PROMEDIO_SEMANAL_ACTUAL_GLP}}', [
                    'PROC_ID' => $proc_id
                ]);

                DB::update('UPDATE MEPCO_PROYECCION SET PROY_PA_PARIDAD_SEMANAL=? WHERE PROY_ID=?',[
                    $paridadSemanal,
                    $idProyeccion
                ]);

                $paridad = Util::ejecutarFormula('{{PY_PA_PARIDAD_$/m_GLP}}', [
                    'PROC_ID'    => $proc_id,
                    'T'          => $t,
                    'LIMIT'      => (5 * ($t - $proyeccion)),
                    'PROYECCION' => $proyeccion
                ]);
                
                DB::update('UPDATE MEPCO_PROYECCION SET PROY_PA_PARIDAD=? WHERE PROY_ID=?',[
                    $paridad,
                    $idProyeccion
                ]);
                //FIN PARIDAD

                //INICIO CRUDO
                $proyeccion = ($i > $n) ? $n : $i;

                if($tipoCrudo == 'BRENT')
                {
                    $crudo = Util::ejecutarFormula('{{PY_BR_BRENT_CALCULADO}}', [
                        'PROC_ID'        => $proc_id,
                        'M'              => $m,
                        'N'              => $n,
                        'F'              => $f,
                        'LIMIT'          => (5 * ($n - $proyeccion)),
                        'CORRECCION_RVP' => $correccionRvp,
                        'PROYECCION'     => $proyeccion
                    ]);

                    DB::update('UPDATE MEPCO_PROYECCION SET 
                                PROY_TIPO_CRUDO=?, 
                                PROY_CRUDO=?,
                                PROY_CRUDO_PROM_SEM_S=?,
                                PROY_CRUDO_HISTORICO=?,
                                PROY_CRUDO_FUTURO=? 
                            WHERE PROY_ID=?',[
                        'BRENT',
                        $crudo,
                        Util::$arrFormulaValores['PY_BR_BRENT_PROMEDIO_SEM_$/M3'],
                        Util::$arrFormulaValores['PY_BR_BRENT_HISTORICO'],
                        Util::$arrFormulaValores['PY_BR_BRENT_FUTURO'],
                        $idProyeccion
                    ]);
                }else
                {
                    $crudo = Util::ejecutarFormula('{{PY_WT_WTI_CALCULADO}}', [
                        'PROC_ID'        => $proc_id,
                        'M'              => $m,
                        'N'              => $n,
                        'F'              => $f,
                        'LIMIT'          => (5 * ($n - $proyeccion)),
                        'CORRECCION_RVP' => $correccionRvp,
                        'PROYECCION'     => $proyeccion
                    ]);

                    DB::update('UPDATE MEPCO_PROYECCION SET 
                                PROY_TIPO_CRUDO=?, 
                                PROY_CRUDO=?,
                                PROY_CRUDO_PROM_SEM_S=?,
                                PROY_CRUDO_HISTORICO=?,
                                PROY_CRUDO_FUTURO=? 
                            WHERE PROY_ID=?',[
                        'WTI',
                        $crudo,
                        Util::$arrFormulaValores['PY_WT_WTI_PROMEDIO_SEM_$/M3'],
                        Util::$arrFormulaValores['PY_WT_WTI_HISTORICO'],
                        Util::$arrFormulaValores['PY_WT_WTI_FUTURO'],
                        $idProyeccion
                    ]);
                }
                //FIN CRUDO

                //INICIO MARGEN
                $proyeccion = ($i > $s) ? $s : $i;

                if($tipoCrudo == 'BRENT')
                {
                    $margenSemanal = Util::ejecutarFormula('{{PY_MG_PROMEDIO_SEMANAL_BRENT_GLP}}', [
                        'PROC_ID'    => $proc_id
                    ]);

                    DB::update('UPDATE MEPCO_PROYECCION SET PROY_MARGEN_PROM_SEM=? WHERE PROY_ID=?',[
                        $margenSemanal,
                        $idProyeccion
                    ]);

                    $margen = Util::ejecutarFormula('{{PY_MG_MARGEN_BRENT_GLP}}', [
                        'PROC_ID'    => $proc_id,
                        'S'          => $s,
                        'LIMIT'      => (5 * ($s - $proyeccion)),
                        'PROYECCION' => $proyeccion
                    ]);
                }else
                {
                    $margenSemanal = Util::ejecutarFormula('{{PY_MG_PROMEDIO_SEMANAL_WTI_GLP}}', [
                        'PROC_ID'    => $proc_id
                    ]);

                    DB::update('UPDATE MEPCO_PROYECCION SET PROY_MARGEN_PROM_SEM=? WHERE PROY_ID=?',[
                        $margenSemanal,
                        $idProyeccion
                    ]);

                    $margen = Util::ejecutarFormula('{{PY_MG_MARGEN_WTI_GLP}}', [
                        'PROC_ID'    => $proc_id,
                        'S'          => $s,
                        'LIMIT'      => (5 * ($s - $proyeccion)),
                        'PROYECCION' => $proyeccion
                    ]);
                }

                DB::update('UPDATE MEPCO_PROYECCION SET PROY_MARGEN=? WHERE PROY_ID=?',[
                    $margen,
                    $idProyeccion
                ]);
                //FIN MARGEN

                //INICIO REFERENCIA
                $referenciaCa = Util::ejecutarFormula('{{PY_RE_PARIDAD_CA_GLP}}', [
                    'PROC_ID'        => $proc_id,
                    'CORRECCION_RVP' => $correccionRvp, 
                    'TIPO_CRUDO'     => $tipoCrudo
                ]);

                DB::update('UPDATE MEPCO_PROYECCION SET PROY_RE_PARIDAD_CA=?, PROY_RE_PARIDAD=? WHERE PROY_ID=?',[
                    $referenciaCa,
                    $referenciaCa,
                    $idProyeccion
                ]);
                //FIN REFERENCIA
            }

            Util::crearFilaLog('Publicar cálculo', 'Publicar ', 'cálculo para proceso', $proc_id);

            DB::commit();

            return response()->json([
                'result' => 'SUCCESS'
            ]);
        }catch(\Exception $e)
        {

            DB::rollBack();

            Util::log('EX M: ' . $e->getMessage());
            Util::log('EX L: ' . $e->getLine());
            
            return response()->json([
                'result' => 'ERROR',
                'ex msg' => $e->getMessage(),
                'ex lin' => $e->getLine(),
                'msg'    => 'Ha ocurrido un error al tratar de publicar proceso, favor contacte a Administrador'
            ]);            
        }
    }

    public function rollback($proc_id){
        $proceso = Proceso::findOrFail($proc_id);
        return view('proceso.rollback', compact('proceso'));
    }

    public function rollbackRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'proc_id' => 'required',
            'documentos' => 'required'
        ]);

        //VALIDA SI EXISTE ERROR Y REDIRECCIONA
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $documentos = $request->file('documentos');
        if($documentos[0] == null)
        {
            return redirect()->back()->withErrors(["Debe ingresar documento(s) de respaldo"])->withInput();
        }

        $proc_id = $request->input('proc_id');
        $proceso = Proceso::findOrFail($proc_id);
        //OBTIENE VAUS_ID 
        $oVameUspe = DB::table('MEPCO_VAME_USPE AS MVU')
            ->join('MEPCO_USUA_PERF AS MUP', 'MVU.USPE_ID', '=', 'MUP.USPE_ID')
            ->select('MVU.VAUS_ID')
            ->where('MVU.PROC_ID', '=', $proc_id)
            ->where('MUP.PERF_ID', '=', Util::PERFIL_USUARIO_GENERADOR)
            ->where('MVU.VAUS_ELIMINADO', '=', '0')
            ->first();
        $vaus_id = ($oVameUspe) ? $oVameUspe->VAUS_ID : null;

        //PROCESA DOCUMENTOS
        foreach ($documentos as $item)
        {
            if ($item != null)
            {
                $nombre       = $item->getClientOriginalName();
                $strNombreAux = Util::formatFileName($nombre);
                $path         = Util::getPath('DOCUMENTOS_RESPALDO', $proceso->PROC_FECHA_VIGENCIA) . $strNombreAux;
                \Storage::put(
                    $path, \File::get($item)
                );

                DB::table('MEPCO_ESCENARIO_ROLLBACK')
                    ->insert([
                        'PROC_ID'        => $proceso->PROC_ID,
                        'USPE_ID'        => session(Util::USUARIO)->USPE_ID, 
                        'ESRO_NOMBRE'    => $item, 
                        'ESRO_FECHA'     => Util::dateTimeNow(),
                        'ESRO_MIMETYPE'  => $item->getMimeType(), 
                        'ESRO_PATH'      => $path, 
                        'ESRO_ELIMINADO' => 0
                    ]);
            }
        }

        $proceso->PRES_ID = Util::PROCESO_ESTADO_RECALCULAR;
        $proceso->save();

        //OBTIENE FECHAS DE PROCESO
        $fechaValoresMercado = DB::table('MEPCO_VALOR_MERCADO AS MVM')
                ->join('MEPCO_VAME_USPE AS MVU', 'MVM.VAUS_ID', '=', 'MVU.VAUS_ID')
                ->join('MEPCO_USUA_PERF AS MUP', 'MVU.USPE_ID', '=', 'MUP.USPE_ID')
                ->select('MVM.VAME_FECHA')
                ->where('MVU.PROC_ID', '=', $proc_id)
                ->where('MUP.PERF_ID', '=', Util::PERFIL_USUARIO_GENERADOR)
                ->where('MVU.VAUS_ELIMINADO', '=', 0)
                ->orderBy('MVM.VAME_ID', 'ASC')
                ->take(5)
                ->get();

        //ELIMINA REGISTROS HISTORICOS FILTRANDO POR FECHA
        foreach ($fechaValoresMercado as $item) {
            DB::table('MEPCO_DATOS_HISTORICOS')
                ->where('DAHI_FECHA', '=', $item->VAME_FECHA)
                ->delete();
        }

        DB::table('MEPCO_ESCENARIO_CALCULO AS MEC')
            ->join('MEPCO_ESCENARIO AS ME', 'MEC.ESCE_ID', '=', 'ME.ESCE_ID')
            ->where('ME.PROC_ID', '=', $proc_id)
            ->update([
                'MEC.ESCA_SUGERIR'   => '0',
                'MEC.ESCA_PUBLICADO' => '0'
            ]);

        //ELIMINA PROYECCION
        DB::table('MEPCO_PROYECCION')
            ->where('PROC_ID', '=', $proc_id)
            ->delete();

        try {
            $fechaVigencia = Util::dateFormat($proceso->PROC_FECHA_VIGENCIA, 'Ymd');
            $pathFolder    = 'informes/' . $fechaVigencia;
            $pathFile      = $fechaVigencia . '_MEPCO.docx';
            $file          = \Storage::disk('local')->delete($pathFolder . '/' . $pathFile);
        } catch (\Exception $e) {
            Log::debug('EX eliminar informe: ' . $e->getMessage());
        } 

        Util::crearFilaLog('Rollback', 'Rollback', 'proceso', $proc_id);

        return redirect()->action('procesoController@escenarios', [$proc_id]);
    }

    public function proyeccion($proc_id)
    {
        $proceso = Proceso::findOrFail($proc_id);

        if ($this->puedeContinuar($proceso))
        {
            $ultimoProcesoEnSistema = DB::table('MEPCO_ESCENARIO_CALCULO AS MEC')
                ->join('MEPCO_ESCENARIO AS ME', 'MEC.ESCE_ID', '=', 'ME.ESCE_ID')
                ->join('MEPCO_PROCESO AS MP', 'ME.PROC_ID', '=', 'MP.PROC_ID')
                ->select(
                    'MP.PROC_FECHA_VIGENCIA AS PROY_FECHA',
                    'MEC.ESCA_PREFINT AS PROY_RE_PARIDAD',
                    'MEC.ESCA_PARIDAD AS PROY_PA_PARIDAD',
                    'MEC.HIDR_ID')
                ->where('ME.PROC_ID', '=', $proc_id)
                ->where('MEC.ESCA_PUBLICADO', '=', 1);

            $proyeccion = DB::table('MEPCO_PROYECCION')
                ->select('PROY_FECHA', 'PROY_RE_PARIDAD', 'PROY_PA_PARIDAD', 'HIDR_ID')
                ->where('PROC_ID', '=', $proc_id)
                ->union($ultimoProcesoEnSistema)
                ->orderBy('PROY_FECHA', 'ASC')
                ->get();

            $proyeccion93     = array();
            $proyeccion97     = array();
            $proyeccionDIESEL = array();
            $proyeccionGLP    = array();

            foreach ($proyeccion as $item) {
                switch ($item->HIDR_ID) {
                    case Util::COMBUSTIBLE_93:
                        $proyeccion93[] = $item;
                        break;
                    case Util::COMBUSTIBLE_97:
                        $proyeccion97[] = $item;
                        break;
                    case Util::COMBUSTIBLE_DIESEL:
                        $proyeccionDIESEL[] = $item;
                        break;
                    case Util::COMBUSTIBLE_GLP:
                        $proyeccionGLP[] = $item;
                        break;
                    default:
                        break;
                }
            }
            $valores['PROYECCION_93']     = $proyeccion93;
            $valores['PROYECCION_97']     = $proyeccion97;
            $valores['PROYECCION_DIESEL'] = $proyeccionDIESEL;
            $valores['PROYECCION_GLP']    = $proyeccionGLP;

            $valores['PUEDE_CALCULAR_PUBLICAR'] = ($proceso->PRES_ID == Util::PROCESO_ESTADO_FINALIZADO);

            return view('proceso.proyeccion', compact('proceso', 'valores'));
        }
        else
        {
            return redirect()->back();
        }
        
    }

    public function proyeccionExpo($proc_id)
    {
        $ultimoProcesoEnSistema = DB::table('MEPCO_ESCENARIO_CALCULO AS MEC')
            ->join('MEPCO_ESCENARIO AS ME', 'MEC.ESCE_ID', '=', 'ME.ESCE_ID')
            ->join('MEPCO_PROCESO AS MP', 'ME.PROC_ID', '=', 'MP.PROC_ID')
            ->select(
                'MP.PROC_FECHA_VIGENCIA AS PROY_FECHA',
                'MEC.ESCA_PREFINT AS PROY_RE_PARIDAD',
                'MEC.ESCA_PARIDAD AS PROY_PA_PARIDAD',
                'MEC.HIDR_ID')
            ->where('ME.PROC_ID', '=', $proc_id)
            ->where('MEC.ESCA_PUBLICADO', '=', '1');

        $proyeccion = DB::table('MEPCO_PROYECCION')
            ->select('PROY_FECHA', 'PROY_RE_PARIDAD', 'PROY_PA_PARIDAD', 'HIDR_ID')
            ->where('PROC_ID', '=', $proc_id)
            ->union($ultimoProcesoEnSistema)
            ->orderBy('PROY_FECHA', 'ASC')
            ->get();

        $proyeccion93     = array();
        $proyeccion97     = array();
        $proyeccionDIESEL = array();
        $proyeccionGLP    = array();

        foreach ($proyeccion as $item) {
            switch ($item->HIDR_ID) {
                case Util::COMBUSTIBLE_93:
                    $proyeccion93[] = $item;
                    break;
                case Util::COMBUSTIBLE_97:
                    $proyeccion97[] = $item;
                    break;
                case Util::COMBUSTIBLE_DIESEL:
                    $proyeccionDIESEL[] = $item;
                    break;
                case Util::COMBUSTIBLE_GLP:
                    $proyeccionGLP[] = $item;
                    break;
                
                default:
                    break;
            }
        }

        $valores['PROYECCION_93']     = $proyeccion93;
        $valores['PROYECCION_97']     = $proyeccion97;
        $valores['PROYECCION_DIESEL'] = $proyeccionDIESEL;
        $valores['PROYECCION_GLP']    = $proyeccionGLP;

        Excel::create('PROYECCION', function($excel) use($valores) {
            $excel->sheet('93', function($sheet) use($valores) {
                $sheet->loadView('proceso.exportar.proyeccion', array('valores' => $valores['PROYECCION_93']));
            });
            $excel->sheet('97', function($sheet) use($valores) {
                $sheet->loadView('proceso.exportar.proyeccion', array('valores' => $valores['PROYECCION_97']));
            });
            $excel->sheet('DIESEL', function($sheet) use($valores) {
                $sheet->loadView('proceso.exportar.proyeccion', array('valores' => $valores['PROYECCION_DIESEL']));
            });
            $excel->sheet('GLP', function($sheet) use($valores) {
                $sheet->loadView('proceso.exportar.proyeccion', array('valores' => $valores['PROYECCION_GLP']));
            });
        })->export('xls');
    }

    public function documentos($proc_id)
    {
        $existeInforme = false;
        $fechaVigencia = null;
        $pathFolder    = null;
        $pathFile      = null;

        $proceso = Proceso::findOrFail($proc_id);

        if ($this->puedeContinuar($proceso))
        {
            if($proceso->PRES_ID == Util::PROCESO_ESTADO_FINALIZADO)
            {
                $fechaVigencia = Util::dateFormat($proceso->PROC_FECHA_VIGENCIA, 'Ymd');
                try {
                    $pathFolder    = 'informes/' . $fechaVigencia;
                    $pathFile      = $fechaVigencia . '_MEPCO.docx';
                    $file          = \Storage::disk('local')->get($pathFolder . '/' . $pathFile);
                    $existeInforme = true;
                } catch (\Exception $e) {
                    $existeInforme = false;
                }   

                if($existeInforme)
                {
                    $valores['PUEDE_CALCULAR_PUBLICAR'] = true; 
                    $valores['BTN']                     = 'DESCARGAR'; 
                    $valores['FECHA_VIGENCIA']          = $fechaVigencia; 
                    $valores['DOCUMENTO']               = $pathFile; 
                }else{
                    $valores['PUEDE_CALCULAR_PUBLICAR'] = true; 
                    $valores['BTN']                     = 'GENERAR'; 
                    $valores['FECHA_VIGENCIA']          = null; 
                    $valores['DOCUMENTO']               = null; 
                }               
            }else
            {
                $valores['PUEDE_CALCULAR_PUBLICAR'] = false; 
                $valores['BTN']                     = 'GENERAR'; 
                $valores['FECHA_VIGENCIA']          = null; 
                $valores['DOCUMENTO']               = null;
            }

            return view('proceso.documentos', compact('proceso', 'valores'));
        }
        else
        {
            return redirect()->back();
        }
    }

    public function documentosGet($fechaVigencia)
    {
        $pathFolder    = 'informes/' . $fechaVigencia;
        $pathFile      = $fechaVigencia . '_MEPCO.docx';
        $mimeTypeDocX  = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
        $file          = \Storage::disk('local')->get($pathFolder . '/' . $pathFile);
        return response($file)->header('Content-Type', $mimeTypeDocX);
    }

    /**
     * Redirije vista según el estado de proceso
     *
     * @return Response
     */
    public function editar($id)
    {
        $proceso = Proceso::findOrFail($id);
        $procesoValidado = DB::table('MEPCO_VAME_USPE')
                ->select('VAUS_VALIDACION_CORRECTA', 'USPE_ID')
                ->where('PROC_ID', '=', $proceso->PROC_ID)
                ->where('VAUS_ELIMINADO', '=', 0)
                ->orderBy('VAUS_ID', 'DESC')
                ->first();

        if (session(Util::USUARIO)->PERF_ID == Util::PERFIL_USUARIO_VALIDADOR)
        {
            if (($procesoValidado != null) && ($procesoValidado->VAUS_VALIDACION_CORRECTA == 0))
            {
                return redirect()->action('procesoController@valorMercado', [$proceso->PROC_ID]);
            }
            else
            {
                switch ($proceso->PRES_ID)
                {
                    case Util::PROCESO_ESTADO_NUEVO:
                        return redirect()->action('procesoController@validadores', [$proceso->PROC_ID]);
                        break;
                    case Util::PROCESO_ESTADO_INICIADO:
                        return redirect()->action('procesoController@valorMercado', [$proceso->PROC_ID]);
                        break;
                    case Util::PROCESO_ESTADO_INGRESO_VALORES_MERCADO:
                        return redirect()->action('procesoController@valorMercado', [$proceso->PROC_ID]);
                        break;
                    case Util::PROCESO_ESTADO_VALOR_DE_MERCADO_NO_VALIDADO:
                        return redirect()->action('procesoController@valorMercado', [$proceso->PROC_ID]);
                        break;
                    case Util::PROCESO_ESTADO_VALOR_DE_MERCADO_VALIDADO:
                        return redirect()->action('procesoController@escenarios', [$proceso->PROC_ID]);
                        break;
                    case Util::PROCESO_ESTADO_PROCESO_SUGERIDO:
                        return redirect()->action('procesoController@escenarios', [$proceso->PROC_ID]);
                        break;
                    case Util::PROCESO_ESTADO_FINALIZADO:
                        return redirect()->action('procesoController@documentos', [$proceso->PROC_ID]);
                        break;
                    case Util::PROCESO_ESTADO_RECALCULAR:
                        return redirect()->action('procesoController@escenarios', [$proceso->PROC_ID]);
                        break;
                    default:
                        return redirect()->action('procesoController@index');
                        break;
                }
            }
        }
        else
        {
            //SI EL ULTIMO REGISTRO ES DE OTRO USUARIO Y NO ESTA VALIDADO DEBE IR A VM
            if (($procesoValidado != null) && (session(Util::USUARIO)->USPE_ID != $procesoValidado->USPE_ID) && ($procesoValidado->VAUS_VALIDACION_CORRECTA == 0))
            {
                return redirect()->action('procesoController@valorMercado', [$proceso->PROC_ID]);
            }
            else
            {
                switch ($proceso->PRES_ID)
                {
                    case Util::PROCESO_ESTADO_NUEVO:
                        return redirect()->action('procesoController@validadores', [$proceso->PROC_ID]);
                        break;
                    case Util::PROCESO_ESTADO_INICIADO:
                        return redirect()->action('procesoController@valorMercado', [$proceso->PROC_ID]);
                        break;
                    case Util::PROCESO_ESTADO_INGRESO_VALORES_MERCADO:
                        return redirect()->action('procesoController@valorMercado', [$proceso->PROC_ID]);
                        break;
                    case Util::PROCESO_ESTADO_VALOR_DE_MERCADO_NO_VALIDADO:
                        return redirect()->action('procesoController@valorMercado', [$proceso->PROC_ID]);
                        break;
                    case Util::PROCESO_ESTADO_VALOR_DE_MERCADO_VALIDADO:
                        return redirect()->action('procesoController@escenarios', [$proceso->PROC_ID]);
                        break;
                    case Util::PROCESO_ESTADO_PROCESO_SUGERIDO:
                        return redirect()->action('procesoController@escenarios', [$proceso->PROC_ID]);
                        break;
                    case Util::PROCESO_ESTADO_FINALIZADO:
                        return redirect()->action('procesoController@documentos', [$proceso->PROC_ID]);
                        break;
                    case Util::PROCESO_ESTADO_RECALCULAR:
                        return redirect()->action('procesoController@escenarios', [$proceso->PROC_ID]);
                        break;
                    default:
                        return redirect()->action('procesoController@index');
                        break;
                }
            }
        }
    }

    /**
     * Obtiene archivo asociado a valor de mercado y 
     * se descarga
     *
     * @return Response
     */
    public function download($id)
    {
        $valorMercadoAdjunto = ValorMercadoAdjunto::findOrFail($id);
        $file = \Storage::disk('local')->get($valorMercadoAdjunto->ARCH_PATH);
        return response($file)->header('Content-Type', $valorMercadoAdjunto->ARCH_MIMETYPE);
    }

    /**
     * Valida si usuario en session puede cargar proceso
     *
     * @return Response
     */
    private function puedeContinuar($proceso)
    {
        $result = false;
        if($proceso->PRES_ID == Util::PROCESO_ESTADO_FINALIZADO)
        {
            $result = true;
        }else{
            foreach ($proceso->usuarios()->get() as $item)
            {
                if (session(Util::USUARIO)->USPE_ID == $item->USPE_ID)
                {
                    $result = true;
                }
            }
            if (!$result)
            {
				if (session(Util::USUARIO)->PERF_ID == Util::PERFIL_USUARIO_GENERADOR)
				{
					//OBTIENE PRUS_ID DE USUARIO EXISTENTE
					$oProcUspePerf = DB::table('MEPCO_PROC_USPE AS MPU')
		                    ->join('MEPCO_USUA_PERF AS MUP', 'MPU.USPE_ID', '=', 'MUP.USPE_ID')
							->select('PRUS_ID')
							->where('PROC_ID', '=', $proceso->PROC_ID)
							->where('MUP.PERF_ID', '=', Util::PERFIL_USUARIO_GENERADOR)
							->orderBy('PRUS_ID', 'DESC')
							->first();
					$oPrus_id = ($oProcUspePerf) ? $oProcUspePerf->PRUS_ID : null;
					DB::update('UPDATE MEPCO_PROC_USPE SET USPE_ID=? WHERE PRUS_ID=?', [
						session(Util::USUARIO)->USPE_ID,
						$oPrus_id
					]);
					$result = true;
				}
				else
				{				
					\Session::set('ALERT-TYPE', 'ERROR');
					\Session::set('ALERT-MSG', json_encode('NO TIENE ACCESO PARA VER EL PROCESO SOLICITADO'));
				}
            }
        }
        
        return $result;
    }

    /**
     * Envía un comentario del usuario validador al generador por correo electrónico
     *
     * @return Response
     */
    public function rechazar(Request $request)
    {
        $comentario = $request['comentario'];
        $proc_id    = $request['proc_id'];

        $proceso = Proceso::findOrFail($proc_id);

        $lstEmail = DB::table('MEPCO_USUA_PERF AS MUP')
                ->join('MEPCO_PROC_USPE AS MPU', 'MUP.USPE_ID', '=', 'MPU.USPE_ID')
                ->select('MUP.USUA_EMAIL')
                ->where('MPU.PROC_ID', '=', $proc_id)
                ->get();

        foreach ($lstEmail as $item) {
            $param = [
                'generador'  => $item->USUA_EMAIL,
                'validador'  => session(Util::USUARIO)->USUA_EMAIL,
                'comentario' => $comentario,
                'fecha'      => Util::dateFormat($proceso->PROC_FECHA_VIGENCIA, 'd/m/Y')
            ];

            try {
                Mail::send('template.email-generador-rechazar', $param, function ($m) use ($item)
                {
                    $m->to($item->USUA_EMAIL, '')->subject('MEPCO: Notificación generador');
                });
            }
            catch(Exception $e)
            {
                return response()->json([
                        'status' => 'error',
                ]);
            }
        }

        return response()->json([
                'status' => 'success'
        ]);

    }

}

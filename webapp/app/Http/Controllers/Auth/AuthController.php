<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Routing\Controller;
//use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Usuario;
use App\Perfil;
use App\Util;
use App\ContenidoPortal;
use Validator;
use adLDAP;
use Log;
use Config;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'USUA_EMAIL' => 'required|email|max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'USUA_EMAIL' => $data['usua_email'],
        ]);
    }    

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function getLogout()
    {
        // Marcar el usuario como desactivado
        $usuario = session(Util::USUARIO);

        if($usuario != null)
        {
            $usuario->USPE_ACTIVO = 0;
            $usuario->save();
        }

        // Eliminar la variable de sesión
        session_unset();

        return redirect('auth/login');
    }    

    /**
     * Redirect to login page
     *
     * @param  array  $data
     * @return User
     */
    protected function getLogin()
    {
        return view('auth.login');
    } 

    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function chooseProfile(Request $request)
    {
        $usua_email = $request['email'];
        $password   = $request['password'];

        $validator = Validator::make($request->all(), [
            'email'     => 'required|email',
            'password'  => 'required'
        ]);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        //Validar que vengan los dos campos

        $username = explode('@', $usua_email)[0];

        /**
        * Comprobar en LDAP que usuario existe. Si devuelve true, comprobar que existe en bbdd
        */
        $ldap = self::LDAP($username, $password);

        if ($ldap == true) {
            /**
            * Comprobar que usuario existe en bbdd. Obtener sus perfiles
            */
            $perfiles = Usuario::perfiles($usua_email);

            /**
            * Comprobar en bbdd si existe una sesión abierta de usuario generador
            */
            $usuario_generador = Usuario::where('PERF_ID', Util::PERFIL_USUARIO_GENERADOR)
                ->where('USPE_ACTIVO', 1)
                ->first();

            if ($usuario_generador) {
                // Chequear lifetime
                $last_active    = strtotime($usuario_generador->USPE_ULTIMA_CONEXION);
                $max_lifetime   = Config::get('session.lifetime')*60;
                $max_active = $last_active+$max_lifetime;
                $now = strtotime(Util::dateTimeNow());
                // Cerrar la sesión de usuario generador
                if ($now>$max_active) {
                    $usuario_generador->USPE_ACTIVO = 0;
                    $usuario_generador->save();
                    $generador_conectado = 0;
                }
                else{
                    $generador_conectado = 1;
                }
            }
            else{
                $generador_conectado = 0;
            }

            // Usuario no existe en bbdd, redirigir a página de login
            if (!$perfiles->toArray()) {
                $errors[] = 'El usuario introducido no existe en la aplicación MEPCO.';
                return redirect()->back()->withErrors($errors)->withInput();
            }
            else if (count($perfiles->toArray()) == 1) {
                // El usuario seleccionado sólo tiene un perfil asociado
                // por tanto, realiza el login automáticamente sin pasar por chooseProfile

                // Pero primero, se debe tener en cuenta si existe un usuario generador conectado
                $perf_id = $perfiles[0]->PERF_ID;

                if (($perf_id == Util::PERFIL_USUARIO_GENERADOR)&&($generador_conectado == 1)) {
                    // El usuario sólo tiene el perfil generador, y hay un generador conectado 
                    // Mostrar mensaje
                    $errors[] = 'Existe un usuario Generador conectado actualmente. Por favor, intente acceder más tarde o póngase en contacto con el Administrador del sistema';

                    return redirect()->back()->withErrors($errors)->withInput();
                }
                else {
                    // O no tiene el perfil de generador, o no está iniciada la sesión generador
                    $usuario = Usuario::where('PERF_ID', $perf_id)
                                        ->where('USUA_EMAIL', $usua_email)
                                        ->first();

                    $usuario->USPE_ACTIVO=1;
                    $usuario->save();

                    session([Util::USUARIO=>$usuario]);

                    /**
                    * Obtener el contenido del portal asociado al perfil del usuario
                    */
                    $contenidoPortal = Perfil::find($perf_id)->contenidoPortal()->get();

                    // Generar el menú
                    $menu = ContenidoPortal::menu($contenidoPortal);

                    session([Util::MENU=>$menu]);

                    // Generar el menú de perfiles
                    $menuPerfiles = ContenidoPortal::menuPerfiles($perfiles, $perf_id);

                    session([Util::MENU_PERFILES=>$menuPerfiles]);
                
                    return redirect('home');
                }
            }
            else {
                // Bloqueo de la selección de usuario generador si hay uno conectado
                // Redirigir a página para seleccionar perfiles (el usuario tiene más de un perfil)
                return view('perfil.choose', compact('perfiles', 'generador_conectado', 'usua_email'));
            }
        }
        else {
            // Error al autenticar en LDAP
            $errors[] = 'Error de autenticación. Por favor, inténtelo de nuevo o póngase en contacto con el Administrador del sistema';

          return redirect()->back()->withErrors($errors)->withInput();
        }
    }    

    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        /**
        * Cargar el usuario correspondiente con el perfil seleccionado
        */
        $perf_id    = $request['perf_id']; 
        $usua_email = $request['usua_email']; 

        if ($perf_id == Util::PERFIL_USUARIO_GENERADOR) {
            // Comprobar si existe un generador conectado
            if (count(Usuario::where('PERF_ID', Util::PERFIL_USUARIO_GENERADOR)
                                      ->where('USPE_ACTIVO', 1)
                                      ->get()) > 0) {
                // El usuario sólo tiene el perfil generador, y hay un generador conectado 
                // Mostrar mensaje
                $errors = array();
                $errors[] = 'Existe un usuario Generador conectado actualmente. Por favor, intente acceder de nuevo o póngase en contacto con el Administrador del sistema';

                return redirect()->back()->withErrors($errors)->withInput();
            }
        }
        $usuario = Usuario::where('PERF_ID', $perf_id)
                            ->where('USUA_EMAIL', $usua_email)
                            ->first();

        $usuario->USPE_ACTIVO=1;
        $usuario->save();

        session([Util::USUARIO=>$usuario]);

        /**
        * Obtener el contenido del portal asociado al perfil del usuario
        */
        $contenidoPortal = Perfil::find($perf_id)->contenidoPortal()->get();

        // Generar el menú de acceso al contenido del portal
        $menu = ContenidoPortal::menu($contenidoPortal);

        session([Util::MENU=>$menu]);

        /**
        * Obtener el menú de perfiles para cambio 'en caliente'
        */
        $perfiles = $usuario->perfiles($usuario->USUA_EMAIL);

        // Generar el menú de perfiles
        $menuPerfiles = ContenidoPortal::menuPerfiles($perfiles, $perf_id);

        session([Util::MENU_PERFILES=>$menuPerfiles]);

        /**
        * Obtener el menú del home que corresponde
        */
        $home = ContenidoPortal::home($contenidoPortal);

        session([Util::HOME=>$home]);

        return redirect('home');
    }

    /**
     * Comprueba la existencia del usuario en LDAP
     *
     * @return Response
     */
    public function LDAP($username, $password)
    {
        try {
            
            $configuration = array(
                #'user_id_key' => $username,
                'account_suffix' => '@redcne.cne.cl',
                #'person_filter' => array('category' => 'CN=Person,CN=Schema,CN=Configuration,DC=redcne,DC=cne,DC=cl'),
                'domain_controllers' => array('10.0.0.230'),
                #'base_dn' => 'DC=redcne,DC=cne,DC=cl',
                'admin_username' => $username,
                'admin_password' => $password,
                #'real_primarygroup' => true,
                #'use_ssl' => false,
                #'use_tls' => false,
                #'recursive_groups' => true,
                #'ad_port' => '389',
                #'sso' => false,
            );
            Log::debug('Array configuracion');            
            $adldap = new adLDAP($configuration);
            Log::debug('crear $adldap');
            return true;
        }
        catch (adLDAPException $e) {
            Log::debug('adLDAPException: '.$username."-".$e->getMessage());
            error_log($username."-".$e->getMessage());
            return false;
        }
        catch(\Exception $e){
            Log::debug('otras excepciones: '.$username."-".$e->getMessage());
            error_log($username."-".$e->getMessage());
            return false;
        }
    }
}

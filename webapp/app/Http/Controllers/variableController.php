<?php

namespace App\Http\Controllers;

use DB;
use App\Util;
use App\Log;
use App\Hidrocarburo;
use App\Usuario;
use App\Variable;
use App\VariableHistorico;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Validator;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class variableController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('authenticated');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $variablesCalculo           = array();
        $variablesPlantilla         = array();
        $variablesQuery             = array();
        $variablesQueryPlantilla    = array();
        $variablesPredefinidas      = array();
        $variablesProyeccionQuery   = array();
        $variablesProyeccion        = array();     

        $variables = DB::table('MEPCO_PARAMETROS_VARIABLE AS V')
                ->leftJoin('MEPCO_HIDROCARBUROS AS H', 'V.HIDR_ID', '=', 'H.HIDR_ID')
                ->select('V.PAVA_ID', 'V.PAVA_NOMBRE', 'V.PAVA_LLAVE', 'V.PAVA_TIPO_VARIABLE', 'V.PAVA_NOMBRE', 'V.PAVA_VALOR_CALCULO', 'V.PAVA_VALOR_PLANTILLA', 'V.PAVA_DESCRIPCION', 'V.PAVA_FECHA_MODIFICACION', 'H.HIDR_NOMBRE_CHILE')
                ->where('V.PAVA_ELIMINADO', 0)
                ->get();

        foreach ($variables as $key=>$variable) {
            if($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_CALCULO) {
                $variablesCalculo[] = $variable;
            }
            else if ($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_PROYECCION) {
                $variablesProyeccion[] = $variable;
            }            
            else if ($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_PROYECCION_QUERY) {
                $variable->PAVA_VALOR_PLANTILLA=  Util::formatText($variable->PAVA_VALOR_PLANTILLA);
                $variablesProyeccionQuery[] = $variable;
            }
            else if ($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_PLANTILLA) {
                $variablesPlantilla[] = $variable;
            }
            else if ($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_QUERY) {
                $variable->PAVA_VALOR_PLANTILLA=  Util::formatText($variable->PAVA_VALOR_PLANTILLA);
                $variablesQuery[] = $variable;
            }
            else if ($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_QUERY_PLANTILLA) {
                $variable->PAVA_VALOR_PLANTILLA=  Util::formatText($variable->PAVA_VALOR_PLANTILLA);
                $variablesQueryPlantilla[] = $variable;
            }
            else if ($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_INPUT) {
                $variablesPredefinidas[] = $variable;
            }
            if ($variable->HIDR_NOMBRE_CHILE == Null) {
                $variable->HIDR_NOMBRE_CHILE = 'Sin hidrocarburo asociado';
            }
        }

        // Listado de hidrocarburos, para los formularios crear y editar
        $hidrocarburos = Hidrocarburo::select(array('HIDR_ID', 'HIDR_NOMBRE_CHILE'))->get();

        return view('variable.list', compact('variablesCalculo', 'variablesProyeccion', 'variablesProyeccionQuery', 'variablesPlantilla', 'variablesQuery', 'variablesPredefinidas', 'variablesQueryPlantilla', 'hidrocarburos'));  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        parse_str($request['formData'], $formData);

        /* *** VALIDACIÓN *** */
        $validator = Validator::make($formData,[
            'pava_llave'            => 'required|max:50',
            'pava_nombre'           => 'required|max:50',
            'pava_descripcion'      => 'required|max:255',
            'pava_tipo_variable'    => 'required',
            'pava_valor_calculo'    => 'required_without_all:pava_valor_query,pava_valor_plantilla',
            'pava_valor_query'      => 'required_without_all:pava_valor_calculo,pava_valor_plantilla',
            'pava_valor_plantilla'  => 'required_without_all:pava_valor_calculo,pava_valor_query',

        ]);

        if ($validator->fails()){
            return response()->json([
                'status' => $validator->errors()->all()
            ]);
        }

        // Comprobar que la llave de la variable corresponde con el patrón necesario
        // para variable de cálculo o de plantilla
        if (preg_match("(^{{(.*)}}$)", $formData['pava_llave']) != 1) {
            \Session::set('ALERT-TYPE', 'ERROR');
            \Session::set('ALERT-MSG', json_encode('Definición incorrecta de la variable: {{llave}}'));
            return response()->json([
                    'status' => 'success',
            ]);
        }

        // Comprobar si existe variable con la misma llave
        $existe = Variable::where('PAVA_LLAVE', $formData['pava_llave'])->first();
        if ($existe) {
            $errors = new MessageBag;
            $errors->add('0', 'Ya existe una variable con la llave especificada. Por favor, escoja otra llave.');

            return response()->json([
                    'status' => $errors,
            ]);
        }

        $now = Util::dateTimeNow();

        /* *** GUARDAR *** */
        // Si es de tipo variable de cálculo, guardar el valor en PAVA_VALOR_CALCULO
        $variable = new Variable;
        $variable->PAVA_LLAVE               = $formData['pava_llave'];
        $variable->PAVA_NOMBRE              = $formData['pava_nombre'];
        $variable->PAVA_DESCRIPCION         = $formData['pava_descripcion']; 
        $variable->PAVA_TIPO_VARIABLE       = $formData['pava_tipo_variable']; 
        $variable->PAVA_FECHA_MODIFICACION  = $now;

        if (($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_QUERY)||($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_PROYECCION_QUERY)){
            $variable->HIDR_ID              = $formData['hidr_id'];
            $variable->PAVA_VALOR_PLANTILLA = $formData['pava_valor_query'];
        }
        else if (($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_CALCULO)||($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_PROYECCION)){
            $variable->HIDR_ID              = $formData['hidr_id'];
            $variable->PAVA_VALOR_CALCULO   = Util::unFormatNumber($formData['pava_valor_calculo']);
        }
        else if ($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_PLANTILLA) {
            $variable->HIDR_ID              = 0;
            $variable->PAVA_VALOR_PLANTILLA = $formData['pava_valor_plantilla'];
        }
        else {
            $variable->HIDR_ID              = 0;
            $variable->PAVA_VALOR_PLANTILLA = $formData['pava_valor_query'];
        }
        $variable->save();

        \Session::set('ALERT-TYPE', 'SUCCESS');
        \Session::set('ALERT-MSG', json_encode('Variable guardada correctamente'));

        /* Almacenar la acción en el log de acciones */
        Util::crearFilaLog('btnGuardar', 'Crear', 'variable', $variable->PAVA_ID);

        return response()->json([
                'status' => 'success',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $variable = Variable::findOrFail($id);
        return response()->json([
            'status' => 'success',
            'variable' => $variable
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $variable = Variable::findOrFail($id);

        if (($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_CALCULO)||($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_PROYECCION)) {
            // Convertir el campo a float
            $variable->PAVA_VALOR_CALCULO = Util::formatNumber($variable->PAVA_VALOR_CALCULO);
        }

        $variable->PAVA_FECHA_MODIFICACION = Util::dateFormat($variable->PAVA_FECHA_MODIFICACION, 'd/m/Y H:i').' Hrs';

        // Retornar JSON
        return response()->json([
                'status' => 'success',
                'variable' => $variable
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        parse_str($request['formData'], $formData);

        /* *** VALIDACIÓN *** */
        $validator = Validator::make($formData,[
            'pava_llave'        => 'required|max:50',
            'pava_nombre'       => 'required|max:50',
            'pava_descripcion'  => 'required|max:255',
            'pava_valor_calculo'    => 'required_without_all:pava_valor_query,pava_valor_plantilla',
            'pava_valor_query'      => 'required_without_all:pava_valor_calculo,pava_valor_plantilla',
            'pava_valor_plantilla'  => 'required_without_all:pava_valor_calculo,pava_valor_query',
        ]);

        if ($validator->fails()){
            return response()->json([
                'status' => $validator->errors()->all()
            ]);
        }

        // Comprobar que la llave de la variable corresponde con el patrón necesario
        // para variable de cálculo o de plantilla
        if (preg_match("(^{{(.*)}}$)", $formData['pava_llave']) != 1) {
            \Session::set('ALERT-TYPE', 'ERROR');
            \Session::set('ALERT-MSG', json_encode('Definición incorrecta de la variable: {{llave}}'));
            return response()->json([
                    'status' => 'success',
            ]);
        }

        $variable = Variable::findOrFail($id);

        $variableHistorico = new VariableHistorico;

        $variableHistorico->PAVA_ID             = $variable->PAVA_ID;
        $variableHistorico->USPE_ID             = session(Util::USUARIO)->USPE_ID;
        $variableHistorico->VAHI_LLAVE          = $variable->PAVA_LLAVE;
        $variableHistorico->VAHI_NOMBRE         = $variable->PAVA_NOMBRE;
        $variableHistorico->VAHI_DESCRIPCION    = $variable->PAVA_DESCRIPCION;
        $variableHistorico->VAHI_FECHA          = $variable->PAVA_FECHA_MODIFICACION;

        if (($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_CALCULO)||($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_PROYECCION)) {
            $variableHistorico->VAHI_VALOR      = (float)$variable->PAVA_VALOR_CALCULO;
        }
        else {
            $variableHistorico->VAHI_VALOR      = $variable->PAVA_VALOR_PLANTILLA;
        }

        $variableHistorico->save();

        $now = Util::dateTimeNow();
        
        /* *** GUARDAR *** */
        $variable->PAVA_LLAVE                = $formData['pava_llave'];
        $variable->PAVA_NOMBRE               = $formData['pava_nombre'];
        $variable->PAVA_DESCRIPCION          = $formData['pava_descripcion'];
        $variable->PAVA_FECHA_MODIFICACION   = $now;

        if (($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_QUERY)||($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_PROYECCION_QUERY)){
            $variable->HIDR_ID              = $formData['hidr_id'];
            $variable->PAVA_VALOR_PLANTILLA = $formData['pava_valor_query'];
        }
        else if (($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_CALCULO)||($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_PROYECCION)){
            $variable->HIDR_ID              = $formData['hidr_id'];
            $variable->PAVA_VALOR_CALCULO   = Util::unFormatNumber($formData['pava_valor_calculo']);
        }
        else if ($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_PLANTILLA) {
            $variable->HIDR_ID              = 0;
            $variable->PAVA_VALOR_PLANTILLA = $formData['pava_valor_plantilla'];
        }
        else {
            $variable->HIDR_ID              = 0;
            $variable->PAVA_VALOR_PLANTILLA = $formData['pava_valor_query'];
        }
        $variable->save();

        \Session::set('ALERT-TYPE', 'SUCCESS');
        \Session::set('ALERT-MSG', json_encode('Variable editada correctamente'));

        /* Almacenar la acción en el log de acciones */
        Util::crearFilaLog('btnGuardar', 'Editar', 'variable', $variable->PAVA_ID);

        return response()->json([
                'status' => 'success',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function eliminar(Request $request)
    {
        $pava_id = $request['pava_id'];
        $variable = Variable::find($pava_id);
        $variable->PAVA_ELIMINADO = 1;
        $variable->save();

        \Session::set('ALERT-TYPE', 'SUCCESS');
        \Session::set('ALERT-MSG', json_encode('Variable eliminada correctamente'));

        /* Almacenar la acción en el log de acciones */
        Util::crearFilaLog('btnEliminar', 'Eliminar', 'variable', $variable->PAVA_ID);

        return response()->json([
                'status' => 'success',
        ]);
    }

    /**
     * Obtiene las filas del histórico relacioadas con una variable
     *
     * @param  int  $id
     * @return Response
     */
    public function historico(Request $request)
    {

        $historico = VariableHistorico::select('USPE_ID', 'VAHI_LLAVE', 'VAHI_NOMBRE', 'VAHI_VALOR', 'VAHI_DESCRIPCION', 'VAHI_FECHA')
                                    ->where('PAVA_ID', $request['pava_id'])
                                    ->get();
                                    
        foreach ($historico as $key=>$linea) {
            $uspe_id = $linea->USPE_ID;
            unset($linea['USPE_ID']);
            $usuario = Usuario::find($uspe_id);
            $linea['USUA_EMAIL'] = $usuario->USUA_EMAIL;
            $historico[$key] = $linea;
        }

        return response()->json([
                'status'    => 'success',
                'historico' => $historico,
        ]);
    }

    /**
     * Ejecuta una query almacenada en una variable query y devuelve el resultado o los errores
     *
     * @param  int  $id
     * @return Response
     */
    public function ejecutarQuery(Request $request)
    {
        $pava_id = $request['pava_id'];
        $variable = Variable::findOrFail($pava_id);
        $errors = new MessageBag;

        $parametros = Util::obtenerLlaves($variable->PAVA_VALOR_PLANTILLA);
        // Buscar variables dentro de la query
        if ($parametros!=NULL) {
            // Existen variables, se depliega el modal de introducción de variables
            return response()->json([
                    'status'        => 'parameters',
                    'parametros'    => $parametros
            ]);
        }

        try {
            $resultado = DB::select(DB::raw($variable->PAVA_VALOR_PLANTILLA));
            if($resultado != null && count($resultado) > 0)
            {
                $resultado = (float)reset($resultado[0]);
            }
        }
        catch(\PDOException $e) {
            $errors->add('0', 'Error al ejecutar la Query. Detalle del error: '.$e->getMessage());
            return response()->json([
                    'status'    => 'error',
                    'errors'    => $errors
            ]);
        }
        return response()->json([
                'status'    => 'success',
                'resultado' => $resultado
        ]);
    }

    /**
     * Ejecuta una query que contiene parametros
     *
     * @param  int  $id
     * @return Response
     */
    public function ejecutarQueryParametros(Request $request)
    {
        parse_str($request['formData'], $formData);

        $variable = Variable::findOrFail($formData['pava_id']);

        $parametros_entrada = $formData;

        // Eliminamos lo que no sirve de parametros_entrada
        unset($parametros_entrada['pava_id']);
        unset($parametros_entrada['_token']);

        $errors = new MessageBag;

        $resultado = Util::obtenerVariablePorLlave($variable->PAVA_LLAVE, $parametros_entrada);

        if ($resultado!=NULL) {
            return response()->json([
                    'status'    => 'success',
                    'resultado' => $resultado
            ]);
        }
        else {
            $errors->add('0', 'Error al ejecutar la Query. Compruebe que los tipos de los parámetros sean correctos.');
            return response()->json([
                    'status'    => 'error',
                    'errors' => $errors
            ]);
        }
    }

    /**
     * Recibe llamada de ajax y pasa el texto a formatText()
     * @return datetime
     */
    public static function formatText($str)
    {
        $str = Util::formatText($str);
        
        return response()->json([
                'status'        => 'success',
                'str'        => $str
        ]);
    }

    public function exportarAExcel()
    {
        $variables = DB::table('MEPCO_PARAMETROS_VARIABLE AS V')
                ->leftJoin('MEPCO_HIDROCARBUROS AS H', 'V.HIDR_ID', '=', 'H.HIDR_ID')
                ->select('V.PAVA_ID', 'V.PAVA_NOMBRE', 'V.PAVA_LLAVE', 'V.PAVA_TIPO_VARIABLE', 'V.PAVA_NOMBRE', 'V.PAVA_VALOR_CALCULO', 'V.PAVA_VALOR_PLANTILLA', 'V.PAVA_DESCRIPCION', 'V.PAVA_FECHA_MODIFICACION', 'H.HIDR_NOMBRE_CHILE')
                ->get();

        foreach ($variables as $key=>$variable) {
            if(($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_CALCULO)||(($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_PROYECCION))) {
                $variable->PAVA_VALOR_PLANTILLA = Util::formatNumber($variable->PAVA_VALOR_CALCULO);
            }
            $variables[$key] = $variable;
        }

        Excel::create('Variables', function($excel) use(&$variables) {
            $variables_calculo      = array();
            $variables_proyeccion   = array();
            $variables_plantilla    = array();
            $variables_predefinidas = array();

            foreach ($variables as $variable) {
                if (($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_CALCULO) || ($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_QUERY)) {
                    $nombreHoja = 'Cálculo';
                    $tabla[$nombreHoja][] = $variable;
                }
                else if (($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_PROYECCION) || ($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_PROYECCION_QUERY)) {
                    $nombreHoja = 'Proyección';
                    $tabla[$nombreHoja][] = $variable;
                }
                else if (($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_PLANTILLA) || ($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_QUERY_PLANTILLA)) {
                    $nombreHoja = 'Documento';
                    $tabla[$nombreHoja][] = $variable;
                }
                else if ($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_INPUT) {
                    $nombreHoja = 'Predefinidas';
                    $tabla[$nombreHoja][] = $variable;
                }
            }

            foreach($tabla as $nombreHoja=>$t) {
                $excel->sheet($nombreHoja, function($sheet) use(&$t) {
                    $sheet->setWidth(array(
                        'A' => 20,
                        'B' => 20,
                        'C' => 20,
                        'D' => 20,
                        'E' => 20,
                        'F' => 20,
                    ));
                    $sheet->loadView('variable.templatePdf', array('variables' => $t));
                });
            }

        })->export('xls');

    }

    public function exportarAPdf($tipo)
    {
        ini_set('memory_limit', '128M');
        
        if ($tipo == 'calculo') {
            $variables = DB::table('MEPCO_PARAMETROS_VARIABLE AS V')
                    ->leftJoin('MEPCO_HIDROCARBUROS AS H', 'V.HIDR_ID', '=', 'H.HIDR_ID')
                    ->select('V.PAVA_ID', 'V.PAVA_NOMBRE', 'V.PAVA_LLAVE', 'V.PAVA_TIPO_VARIABLE', 'V.PAVA_NOMBRE', 'V.PAVA_VALOR_CALCULO', 'V.PAVA_VALOR_PLANTILLA', 'V.PAVA_DESCRIPCION', 'V.PAVA_FECHA_MODIFICACION', 'H.HIDR_NOMBRE_CHILE')
                    ->where('PAVA_TIPO_VARIABLE', Util::VARIABLE_CALCULO)
                    ->orWhere('PAVA_TIPO_VARIABLE', Util::VARIABLE_QUERY)
                    ->get();
        }
        else if ($tipo == 'proyeccion') {
            $variables = DB::table('MEPCO_PARAMETROS_VARIABLE AS V')
                    ->leftJoin('MEPCO_HIDROCARBUROS AS H', 'V.HIDR_ID', '=', 'H.HIDR_ID')
                    ->select('V.PAVA_ID', 'V.PAVA_NOMBRE', 'V.PAVA_LLAVE', 'V.PAVA_TIPO_VARIABLE', 'V.PAVA_NOMBRE', 'V.PAVA_VALOR_CALCULO', 'V.PAVA_VALOR_PLANTILLA', 'V.PAVA_DESCRIPCION', 'V.PAVA_FECHA_MODIFICACION', 'H.HIDR_NOMBRE_CHILE')
                    ->where('PAVA_TIPO_VARIABLE', Util::VARIABLE_PROYECCION)
                    ->orWhere('PAVA_TIPO_VARIABLE', Util::VARIABLE_PROYECCION_QUERY)
                    ->get();
        }
        else if ($tipo == 'documento') {
            $variables = DB::table('MEPCO_PARAMETROS_VARIABLE AS V')
                    ->leftJoin('MEPCO_HIDROCARBUROS AS H', 'V.HIDR_ID', '=', 'H.HIDR_ID')
                    ->select('V.PAVA_ID', 'V.PAVA_NOMBRE', 'V.PAVA_LLAVE', 'V.PAVA_TIPO_VARIABLE', 'V.PAVA_NOMBRE', 'V.PAVA_VALOR_CALCULO', 'V.PAVA_VALOR_PLANTILLA', 'V.PAVA_DESCRIPCION', 'V.PAVA_FECHA_MODIFICACION', 'H.HIDR_NOMBRE_CHILE')
                    ->where('PAVA_TIPO_VARIABLE', Util::VARIABLE_PLANTILLA)
                    ->orWhere('PAVA_TIPO_VARIABLE', Util::VARIABLE_QUERY_PLANTILLA)
                    ->get();
        }
        else {// predefinidas
            $variables = DB::table('MEPCO_PARAMETROS_VARIABLE AS V')
                    ->leftJoin('MEPCO_HIDROCARBUROS AS H', 'V.HIDR_ID', '=', 'H.HIDR_ID')
                    ->select('V.PAVA_ID', 'V.PAVA_NOMBRE', 'V.PAVA_LLAVE', 'V.PAVA_TIPO_VARIABLE', 'V.PAVA_NOMBRE', 'V.PAVA_VALOR_CALCULO', 'V.PAVA_VALOR_PLANTILLA', 'V.PAVA_DESCRIPCION', 'V.PAVA_FECHA_MODIFICACION', 'H.HIDR_NOMBRE_CHILE')
                    ->where('PAVA_TIPO_VARIABLE', Util::VARIABLE_INPUT)
                    ->get();
        }

        foreach ($variables as $key=>$variable) {
            if(($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_CALCULO)||(($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_PROYECCION))) {
                $variable->PAVA_VALOR_PLANTILLA = Util::formatNumber($variable->PAVA_VALOR_CALCULO);
            }
            $variables[$key] = $variable;
        }

        $pdf =  \PDF::loadView('variable.templatePdf', compact('variables'))->setOrientation('landscape');
        return $pdf->download('variable.pdf');
    }
}

<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\PlantillaDocumento;
use App\Util;
use Validator;
use App\Usuario;
use App\Variable;
use App\Log;
use App\Http\Requests;
use Illuminate\Support\MessageBag;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class plantillaDocumentoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('authenticated');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $plantillas = PlantillaDocumento::where('PLDO_ELIMINADO', 0)
                            ->get();

        return view('plantilla.list', compact('plantillas'));

    }

    /**
     * Obtiene el listado de las plantillas para devolverlo por AJAX
     *
     * @return Response
     */
    public function indexAjax(Request $request)
    {
        $plantillas = PlantillaDocumento::where('PLDO_ELIMINADO', 0)
                            ->get();

        return response()->json([
            'status'        => 'success',
            'plantillas'    => $plantillas
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pldo_nombre'           => 'required|max:50',
            'pldo_descripcion'      => 'required|max:255'
        ]);
        if ($validator->fails()) 
        {
            return response()->json([
                    'status' => 'error',
                    'errors' => $validator->getMessageBag()->toArray()
            ]);
        }
        else
        {
            $plantilla = $request->file('pldo_nombre_archivo');
            $nombre = $plantilla->getClientOriginalName();
            $strNombreAux = Util::formatFileName($nombre);

            if($plantilla == null)
            {
                $errors = new MessageBag;
                $errors->add('0', 'Seleccione un documento para la plantilla');
                return response()->json([
                        'status' => 'error',
                        'errors' => $errors
                ]);
            }
            else {
                $path = Util::PATH_PLANTILLAS.$strNombreAux;
                \Storage::put(
                    $path,
                    \File::get($plantilla)
                );

                $plantillaDocumento = new PlantillaDocumento();
                $plantillaDocumento->PLDO_NOMBRE = $request['pldo_nombre'];
                $plantillaDocumento->PLDO_DESCRIPCION = $request['pldo_descripcion'];
                $plantillaDocumento->PLDO_NOMBRE_ARCHIVO = $strNombreAux;
                $plantillaDocumento->PLDO_MIMETYPE = $plantilla->getMimeType();
                $plantillaDocumento->PLDO_PATH = $path;
                $plantillaDocumento->PLDO_ELIMINADO = 0;
                $plantillaDocumento->save();
            }
        }

        \Session::set('ALERT-TYPE', 'SUCCESS');
        \Session::set('ALERT-MSG', json_encode('Plantilla almacenada correctamente.'));

        // Registrar en el log la creación de la plantilla
        Util::crearFilaLog('btnCrear', 'Crear', 'plantilla', $plantillaDocumento->PLDO_ID);

        return response()->json([
            'status' => 'success',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $plantilla = PlantillaDocumento::findOrFail($id);

        // Retornar JSON
        return response()->json([
                'status'        => 'success',
                'plantilla'     => $plantilla,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pldo_nombre'           => 'required|max:50',
            'pldo_descripcion'      => 'required|max:255'
        ]);
        if ($validator->fails()) 
        {
            return response()->json([
                    'status' => 'error',
                    'errors' => $validator->getMessageBag()->toArray()
            ]);
        }
        else
        {
            $plantilla = $request->file('pldo_nombre_archivo');
            $plantillaDocumento = PlantillaDocumento::findOrFail($request['pldo_id']);

            if($plantilla != null)
            {   
                $nombre = $plantilla->getClientOriginalName();
                $strNombreAux = Util::formatFileName($nombre);

                $path = Util::PATH_PLANTILLAS.$strNombreAux;
                \Storage::put(
                    $path,
                    \File::get($plantilla)
                );
                $plantillaDocumento->PLDO_NOMBRE_ARCHIVO = $strNombreAux;
                $plantillaDocumento->PLDO_MIMETYPE = $plantilla->getMimeType();
                $plantillaDocumento->PLDO_PATH = $path;
            }
        }
        $plantillaDocumento->PLDO_NOMBRE = $request['pldo_nombre'];
        $plantillaDocumento->PLDO_DESCRIPCION = $request['pldo_descripcion'];
        $plantillaDocumento->save();

        \Session::set('ALERT-TYPE', 'SUCCESS');
        \Session::set('ALERT-MSG', json_encode('Plantilla actualizada correctamente.'));

        // Registrar en el log la creación de la plantilla
        Util::crearFilaLog('btnEditar', 'Editar', 'plantilla', $plantillaDocumento->PLDO_ID);

        return response()->json([
            'status' => 'success',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $plantilla = PlantillaDocumento::find($id);
        $archivo = storage_path().'/app/'.$plantilla->PLDO_PATH;
        $plantilla->PLDO_ELIMINADO = 1;
        $plantilla->save();

        unlink($archivo);

        \Session::set('ALERT-TYPE', 'SUCCESS');
        \Session::set('ALERT-MSG', json_encode('Plantilla eliminada correctamente'));

        /* Almacenar la acción en el log de acciones */
        Util::crearFilaLog('btnEliminar', 'Eliminar', 'plantilla', $plantilla->PLDO_ID);

        return response()->json([
                'status' => 'success',
        ]);
    }

    /**
     * Genera un documento a partir de una plantilla
     *
     * @return Response
     */
    public function generarDocumento(Request $request)
    {

        ini_set('memory_limit', '128M');

        $plantillas = PlantillaDocumento::findOrFail($request['pldo']);
        $proc_id = $request['proc_id'];
        $proc_fecha_vigencia = DB::table('MEPCO_PROCESO')
                        ->select('PROC_FECHA_VIGENCIA')
                        ->where('PROC_ID', $proc_id)
                        ->pluck('PROC_FECHA_VIGENCIA');

        $nombre_archivo = $plantillas->PLDO_NOMBRE_ARCHIVO;

        // Abrir documento
        $documento = storage_path().'/app/'.$plantillas->PLDO_PATH;
        $plantilla = new \PhpOffice\PhpWord\TemplateProcessor($documento);

        // Busca tags y los sustituye por su valor
        $variables = Variable::where('PAVA_ELIMINADO', 0)->get();

        foreach ($variables as $variable) {
            // Eliminar las {{}}
            $llave = Util::obtenerValorPorTag($variable->PAVA_LLAVE);

            if (($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_CALCULO)||($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_PLANTILLA)) {
                if ($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_CALCULO) {
                    $valor = Util::formatNumber($variable->PAVA_VALOR_CALCULO);
                }
                else if ($variable->PAVA_TIPO_VARIABLE == Util::VARIABLE_PLANTILLA) {
                    $valor = $variable->PAVA_VALOR_PLANTILLA;
                }
                $plantilla->setValue($llave, $valor);
            }
        }

        // BUSCAR TABLAS DINÁMICAS
        $variables = $plantilla->getTables();
        $columnas = $variables[0];
        $listadoVariables = $variables[1];

        // ELimina valores duplicados consecutivos para dejar 
        // sólo un array con variables, es decir, tablas
        $variables=array();
        $variableAnterior='';
        foreach($listadoVariables as $variable) {
            if ($variable!=$variableAnterior) {
                array_push($variables, $variable);
                $variableAnterior=$variable;
            }
        }

        foreach ($variables as $variable) {
            try {
                $llave = '{{'.$variable.'}}';

                $query = Util::obtenerVariablePorLlave($llave, array('PROC_ID' => $proc_id, 'PROC_FECHA_VIGENCIA' => $proc_fecha_vigencia));

                // Buscar la variable en la tabla
                $clon = 0;
                foreach ($columnas as $key=>$columna) {
                    $columna = Util::obtenerValorPorTag($columna);
                    if (preg_match('/'.$variable.'\.(.*?)/', $columna)) {
                        $columnaTabla = substr($columna, strpos($columna, '.')+1);
                        $tablaColumnas[$variable][$columnaTabla] = $columna;
                        if ($clon == 0) {
                            $plantilla->cloneRow($columna, count($query));
                            // Ya han sido clonadas las filas
                            $clon = 1;
                        }
                    }
                }

                $query = (array)$query;

                // Recorrer los resultados de la query y copiar en las columnas
                foreach($query as $numfila=>$row) { // Cada fila
                    $row = (array)$row;
                    foreach ($row as $nombrecolumna=>$column) { // Cada columna de la fila
                        if (isset($tablaColumnas[$variable][$nombrecolumna])) {
                            $numf = $numfila+1;
                            if (is_numeric($column)) {
                                $column = Util::formatNumberPlantilla($column);
                            }
                            else if ((bool)strtotime($column) != false) {
                                $column = Util::dateFormat($column, 'd-m-Y');
                            }
                            $plantilla->setValue($tablaColumnas[$variable][$nombrecolumna].'#'.$numf, $column);
                        }
                    }
                }
            }
            catch(\PDOException $e)
            {}
            catch(\Exception $e)
            {}
        }

        $tempfile = tempnam(sys_get_temp_dir(), 'Word');
        $plantilla->saveAs($tempfile);

        $fecha_vigencia = date('Ymd', strtotime(DB::table('MEPCO_PROCESO')
                        ->select('PROC_FECHA_VIGENCIA')
                        ->where('PROC_ID', $proc_id)
                        ->pluck('PROC_FECHA_VIGENCIA')));

        // Guardar en server
        $path = Util::PATH_INFORMES. $fecha_vigencia .'/'.$fecha_vigencia.'_MEPCO.docx';
        \Storage::put(
            $path,
            \File::get($tempfile)
        );

        // Obtener el mimetype
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mimetype = finfo_file($finfo, storage_path().'/app/'.Util::PATH_INFORMES. $fecha_vigencia .'/'.$fecha_vigencia.'_MEPCO.docx');
        finfo_close($finfo);

        // Guardar en base de datos
        DB::insert('INSERT INTO MEPCO_DOCUMENTO_CALCULO (PROC_ID, DOCA_NOMBRE, DOCA_DESCRIPCION, DOCA_MINETYPE, DOCA_PATH, DOCA_ELIMINADO) VALUES(?,?,?,?,?,?)', [
            $proc_id,
            $fecha_vigencia.'_MEPCO.docx',
            'Documento de cálculo para fecha de vigencia '.$fecha_vigencia,
            $mimetype,
            storage_path().'/app/'.Util::PATH_INFORMES. $fecha_vigencia .'/'.$fecha_vigencia.'_MEPCO.docx',
            0
        ]);

        $fechaHoy = Util::dateFormat(Util::dateTimeNow(), 'dmY');

        // Crear el descargable para el cliente
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.$fecha_vigencia.'_MEPCO.docx');
        header('Content-Transfer-Encoding: binary');

        readfile($tempfile);
        unlink($tempfile);
    }

    public function exportarAExcel()
    {
        $plantillas = PlantillaDocumento::where('PLDO_ELIMINADO', 0)
                            ->get();

        Excel::create('Plantillas', function($excel) use(&$plantillas) {
            $excel->sheet('Plantillas', function($sheet) use(&$plantillas) {
                $sheet->setWidth(array(
                    'A' => 20,
                    'B' => 20,
                    'C' => 20,
                    'D' => 20,
                ));
                $sheet->loadView('plantilla.templatePdf', array('plantillas' => $plantillas));
            });
        })->export('xls');
    }

    public function exportarAPdf()
    {
        $plantillas = PlantillaDocumento::where('PLDO_ELIMINADO', 0)
                            ->get();
        $pdf =  \PDF::loadView('plantilla.templatePdf', compact('plantillas'))->setOrientation('landscape');
        return $pdf->download('plantilla.pdf');
    }
}
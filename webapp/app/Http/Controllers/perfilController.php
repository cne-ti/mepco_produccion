<?php

namespace App\Http\Controllers;

use App\Log;
use Validator;
use App\Perfil;
use App\Util;
use App\Usuario;
use App\ContenidoPortal;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class perfilController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('authenticated');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $perfiles = Perfil::where('PERF_ELIMINADO', 0)
                            ->get();

        // Enviar todo el contenido del portal
        $contenido = ContenidoPortal::get();

        return view('perfil.list', compact('perfiles', 'contenido'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        parse_str($request['formData'], $formData);

        $validator = Validator::make($formData,[
            'perf_nombre'       => 'required',
            'perf_descripcion'  => 'required',
        ]);

        if ($validator->fails()){
            return response()->json([
                'status' => $validator->errors()->all()
            ]);
        }

        // Comprobamos si se ha asociado contenido del portal al perfil. Si no, mostrar mensaje de error
        if (!array_key_exists('check', $formData)) { 
            \Session::set('ALERT-TYPE', 'ERROR');
            \Session::set('ALERT-MSG', json_encode('Seleccione al menos un contenido asociado al perfil'));
            return response()->json([
                'status' => 'success'
            ]);
        }

        $perfil = Perfil::create([
            'PERF_NOMBRE'       => $formData['perf_nombre'],
            'PERF_DESCRIPCION'  => $formData['perf_descripcion'],
        ]);
        
        // Almacenar las relaciones con Contenido Portal
        foreach ($formData['check'] as $key=>$val) {
            $perfil->contenidoPortal()->attach($key);
        }

        \Session::set('ALERT-TYPE', 'SUCCESS');
        \Session::set('ALERT-MSG', json_encode('Perfil guardado correctamente'));

        /* Almacenar la acción en el log de acciones */
        Log::create([
                'USPE_ID'                   => session(Util::USUARIO)->USPE_ID,
                'LOG_FECHA'                 => Util::dateTimeNow(),
                'LOG_NOMENCLATURA'          => 'btnGuardar Crear Perfil',
                'LOG_NOMBRE_ACCION'         => 'Crear perfil',
                'LOG_DESCRIPCION_ACCION'    => 'Crear perfil con identificador '.$perfil->PERF_ID,
                'LOG_VALOR_ACCION'          => '0',
        ]);

        return response()->json([
                'status' => 'success',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $perfil = Perfil::findOrFail($id);

        // Obtener el contenido asociado
        $contenido = Perfil::find($id)->contenidoPortal()->get();
        // Retornar JSON
        return response()->json([
                'status'        => 'success',
                'perfil'        => $perfil,
                'contenido'     => $contenido
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        parse_str($request['formData'], $formData);

        $validator = Validator::make($formData, [
            'perf_nombre'       => 'required',
            'perf_descripcion'  => 'required',
        ]);

        if ($validator->fails()){
            return response()->json([
                'status' => $validator->errors()->all()
            ]);
        }

        // Comprobamos si se ha asociado contenido del portal al perfil. Si no, mostrar mensaje de error
        if (!array_key_exists('check', $formData)) { 
            \Session::set('ALERT-TYPE', 'ERROR');
            \Session::set('ALERT-MSG', json_encode('Seleccione al menos un contenido asociado al perfil'));
            return response()->json([
                'status' => 'success'
            ]);
        }

        $perfil = Perfil::find($id);
        $perfil->PERF_NOMBRE = $formData['perf_nombre'];
        $perfil->PERF_DESCRIPCION = $formData['perf_descripcion']; 
        $perfil->save();

        // Almacenar las relaciones con Contenido Portal
        // Primero eliminar todas, luego insertar las enviadas
        $perfil->contenidoPortal()->detach();
        foreach ($formData['check'] as $key=>$val) {
            $perfil->contenidoPortal()->attach($key);
        }

        \Session::set('ALERT-TYPE', 'SUCCESS');
        \Session::set('ALERT-MSG', json_encode('Datos del perfil actualizados correctamente'));

        /* Almacenar la acción en el log de acciones */
        Log::create([
                'USPE_ID'                   => session(Util::USUARIO)->USPE_ID,
                'LOG_FECHA'                 => Util::dateTimeNow(),
                'LOG_NOMENCLATURA'          => 'btnGuardar Editar Perfil',
                'LOG_NOMBRE_ACCION'         => 'Editar perfil',
                'LOG_DESCRIPCION_ACCION'    => 'Editar perfil con identificador '.$perfil->PERF_ID,
                'LOG_VALOR_ACCION'          => '0',
        ]);

        return response()->json([
                'status' => 'success',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // Chequear que el usuario no esté eliminando su propio perfil
        if (session(Util::USUARIO)->PERF_ID == $id) {
            // EL usuario está intentando eliminar su propio perfil!! error
            \Session::set('ALERT-TYPE', 'ERROR');
            \Session::set('ALERT-MSG', json_encode('No puede eliminar éste perfil porque es el mismo con el que está conectado. Cambie de perfil e inténtelo de nuevo.'));
            return response()->json([
                    'status' => 'success',
            ]);
        }

        $perfil = Perfil::find($id);
        $perfil->PERF_ELIMINADO = 1;
        $perfil->save();

        // Elimina las relaciones con usuarios asignados a ese perfil
        $usuarios = Perfil::find($id)->usuarios()->delete();

        \Session::set('ALERT-TYPE', 'SUCCESS');
        \Session::set('ALERT-MSG', json_encode('Perfil eliminado correctamente'));

        /* Almacenar la acción en el log de acciones */
        Log::create([
                'USPE_ID'                   => session(Util::USUARIO)->USPE_ID,
                'LOG_FECHA'                 => Util::dateTimeNow(),
                'LOG_NOMENCLATURA'          => 'btnEliminar Eliminar Perfil',
                'LOG_NOMBRE_ACCION'         => 'Eliminar perfil',
                'LOG_DESCRIPCION_ACCION'    => 'Eliminar perfil con identificador '.$perfil->PERF_ID,
                'LOG_VALOR_ACCION'          => '0',
        ]);

        return response()->json([
                'status' => 'success',
        ]);
    }

    /**
     * Obtiene información asociada a los perfiles nuevo y anterior, para el cambio de perfil
     *
     * @return Response
     */
    public function info(Request $request) 
    {
        $perf_id_nuevo = $request['perf_id_nuevo'];
        $perf_id_anterior = session(Util::USUARIO)->PERF_ID;

        // Obtener nombres
        $perf_nombre_anterior = Perfil::where('PERF_ID', $perf_id_anterior)->pluck('PERF_NOMBRE');
        $perf_nombre_nuevo = Perfil::where('PERF_ID', $perf_id_nuevo)->pluck('PERF_NOMBRE');
        // Obtener las imágenes asociadas
        switch($perf_id_nuevo) {
            case Util::PERFIL_USUARIO_GENERADOR:
                    $img_perf_nuevo = (String)view('partials.icon.generador');
                break;
            case Util::PERFIL_USUARIO_ADMINISTRADOR:
                    $img_perf_nuevo = (String)view('partials.icon.administrador');
                break;
            default: 
                    $img_perf_nuevo = (String)view('partials.icon.validador');
        }

        switch($perf_id_anterior) {
            case Util::PERFIL_USUARIO_GENERADOR:
                    $img_perf_anterior = (String)view('partials.icon.generador');
                break;
            case Util::PERFIL_USUARIO_ADMINISTRADOR:
                    $img_perf_anterior = (String)view('partials.icon.administrador');
                break;
            default: 
                    $img_perf_anterior = (String)view('partials.icon.validador');
        }

        return response()->json([
                'status'                    => 'success',
                'perf_nombre_nuevo'         => $perf_nombre_nuevo,
                'perf_nombre_anterior'      => $perf_nombre_anterior,
                'img_perf_nuevo'            => $img_perf_nuevo,
                'img_perf_anterior'         => $img_perf_anterior
        ]);
    }

    /**
     * Realiza el cambio de perfil en caliente
     *
     * @return Response
     */
    public function cambioPerfil(Request $request) 
    {
        $perf_id = $request['perf_id_nuevo'];
        $usuario = session(Util::USUARIO);

        // Buscar primero que el perfil exista
        $perfil = Perfil::find($perf_id);

        if ($perfil->PERF_ELIMINADO==1) {
            // Perfil eliminado
            \Session::set('ALERT-TYPE', 'ERROR');
            \Session::set('ALERT-MSG', json_encode('El perfil seleccionado ha sido eliminado. Si tiene dudas contacte con el Administrador del Sistema.'));
            return redirect('home');
        }

        if ($perf_id == $usuario->PERF_ID ) {
            \Session::set('ALERT-TYPE', 'INFO');
            \Session::set('ALERT-MSG', json_encode('Sesión actualmente iniciada con el perfil seleccionado.'));
            return redirect('home');
        }
        else {
            // Comprobar primero si el perfil nuevo es el de generador
            if ($perf_id == Util::PERFIL_USUARIO_GENERADOR) {
                // En ese caso comprobar si existe un generador conectado
                if (count(Usuario::where('PERF_ID', Util::PERFIL_USUARIO_GENERADOR)
                                          ->where('USPE_ACTIVO', 1)
                                          ->get()) > 0) {
                    // Hay un generador conectado, devolver mensaje de error en el cambio de perfil
                    \Session::set('ALERT-TYPE', 'ERROR');
                    \Session::set('ALERT-MSG', json_encode('No se puede realizar el cambio de perfil porque existe un usuario Generador conectado. Inténtelo de nuevo más tarde.'));
                    return redirect('home');
                }
            }
        }

        // Cerrar la sesión del usuario conectado
        $usuario->USPE_ACTIVO = 0;
        $usuario->save();

        // Cargar el usuario correspondiente (usua_email, perf_id) y marcarlo como activo
        $usuario = Usuario::where('USUA_EMAIL', $usuario->USUA_EMAIL)
                            ->where('PERF_ID', $perf_id)
                            ->first();
        $usuario->USPE_ACTIVO=1;
        $usuario->save();

        // Asignar la variable de sesión
        session([Util::USUARIO=>$usuario]);

        //Obtener el contenido del portal asociado al perfil del usuario
        $contenidoPortal = Perfil::find($perf_id)->contenidoPortal()->get();
        
        // Generar el menú de acceso al contenido del portal
        $menu = ContenidoPortal::menu($contenidoPortal);
        session([Util::MENU=>$menu]);

        // Obtener el contenido del home que corresponde al perfil del usuario
        $home = ContenidoPortal::home($contenidoPortal);
        session([Util::HOME=>$home]);

        // Generar el menú de perfiles
        $perfiles = Usuario::perfiles($usuario->USUA_EMAIL);
        $menuPerfiles = ContenidoPortal::menuPerfiles($perfiles, $perf_id);
        session([Util::MENU_PERFILES=>$menuPerfiles]);

        \Session::set('ALERT-TYPE', 'SUCCESS');
        \Session::set('ALERT-MSG', json_encode('Cambio de perfil realizado correctamente'));
        return redirect('home');
    }

    public function exportarAExcel()
    {
        $perfiles = Perfil::where('PERF_ELIMINADO', 0)
                            ->get();

        Excel::create('Perfiles', function($excel) use(&$perfiles) {
            $excel->sheet('Perfiles', function($sheet) use(&$perfiles) {
                $sheet->setWidth(array(
                    'A' => 20,
                    'B' => 20,
                ));
                $sheet->loadView('perfil.templatePdf', array('perfiles' => $perfiles));
            });
        })->export('xls');
    }

    public function exportarAPdf()
    {
        $perfiles = Perfil::where('PERF_ELIMINADO', 0)
                            ->get();
        $pdf =  \PDF::loadView('perfil.templatePdf', compact('perfiles'));
        return $pdf->download('perfil.pdf');
    }
}
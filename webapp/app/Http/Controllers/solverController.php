<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use App\Http\Requests;
use App\Util;
use App\Solver;
use App\SolverParidad;
use App\SolverReferencia;
use App\Formula;
use DB;
use Rezzza\Formulate\Formula as Form;
use Log;

class solverController extends Controller
{

    // RESULTADOS DE CÁLCULOS REALIZADOS
    //
    // Paridad promedio semanal
    var $paridad_promedio_semanal;

    // FOB GLP Arbitrado promedio
    var $fob_glp_arbitrado_prom;

    // Margen
    var $margen;

    // Paridad
    var $paridad;

    // Crudo
    var $crudo;

    // Referencia
    var $referencia_int;
    var $referencia_sup;
    var $referencia_inf;

    // Listados de variables y fórmulas para evitar ir a BBDD
    var $listadoVariables;
    var $listadoFormulas;

    // Corrección rvp
    var $correccion_rvp;

    /**
    * Almacena valores de formula segun fecha
    */
    public static $arrFormulaValores = array();

    /**
    * Flag para determinar si debe almacenar resultado de formulas sin fecha
    */
    public static $almacenaFormulaValoresSinFecha = false;

    /**
     * Calcula valores de parámetros n, m, s, t, f dependiendo
     * de los datos ingresados en la pantalla Solver
     *
     * @return Response
     */
    public function calcular(Request $request)
    {
        ini_set('memory_limit', '128M');
        ini_set('max_execution_time', '300');

        Util::log('TIEMPO SOLVER INI: '.Util::dateTimeNow());

        $listadoVariables = \App\Variable::where('PAVA_ELIMINADO', 0)->get();
        $listadoVariables = DB::table('MEPCO_PARAMETROS_VARIABLE AS V')
                    ->select('V.PAVA_ID', 'V.PAVA_TIPO_VARIABLE', 'V.PAVA_LLAVE', 'V.PAVA_VALOR_CALCULO', 'V.PAVA_VALOR_PLANTILLA')
                    ->where('V.PAVA_ELIMINADO', '=', 0)   
                    ->whereIn('V.PAVA_TIPO_VARIABLE', array(Util::VARIABLE_CALCULO, Util::VARIABLE_QUERY, Util::VARIABLE_INPUT))
                    ->get();
        foreach($listadoVariables as $variable) {
            $this->listadoVariables[$variable->PAVA_LLAVE] = $variable; 
        }

        $listadoFormulas = \App\Formula::where('PAFO_ELIMINADO', 0)->get();
        $listadoFormulas = DB::table('MEPCO_PARAMETROS_FORMULA AS F')
                    ->select('F.PAFO_ID', 'F.PAFO_FORMULA', 'F.PAFO_LLAVE', 'F.PAFO_CANTIDAD_DECIMALES')
                    ->where('F.PAFO_ELIMINADO', '=', 0)   
                    ->get();
        foreach($listadoFormulas as $formula) {
            $this->listadoFormulas[$formula->PAFO_LLAVE] = $formula; 
        }

        // Inicializar variables
        $this->paridad_promedio_semanal   = null;
        $this->fob_glp_arbitrado_prom     = null;
        $this->margen                     = null;
        $this->paridad                    = null;
        $this->crudo                      = null;
        $this->referencia_int             = null;
        $this->referencia_sup             = null;
        $this->referencia_inf             = null;

        parse_str($request['formData'], $formData);

        // Recoger valores parámetros
        $n                  = isset($formData['n']) ? $formData['n']:null;
        $m                  = isset($formData['m']) ? $formData['m']:null;
        $s                  = isset($formData['s']) ? $formData['s']:null;
        $t                  = isset($formData['t']) ? $formData['t']:null;
        $f                  = isset($formData['f']) ? $formData['f']:null;

        // Máximos y mínimos de paridad y referencia
        $paridad_minimo     = Util::unFormatNumber($formData['paridad_minimo']);
        $paridad_maximo     = Util::unFormatNumber($formData['paridad_maximo']);
        $referencia_minimo  = Util::unFormatNumber($formData['referencia_minimo']);
        $referencia_maximo  = Util::unFormatNumber($formData['referencia_maximo']);

        // Combustible y proceso
        $combustible        = $formData['hidr_id'];
        $proceso            = json_decode($formData['proc_id']);
        $proc_id            = $proceso->PROC_ID;

        // Variables para marcar parámetros fijos y variables
        $fijar_n = 0;
        $fijar_m = 0;
        $fijar_s = 0;
        $fijar_t = 0;
        $fijar_f = 0;

        // Variables para resultados
        $resultadosParidad      = array();
        $resultadosReferencia   = array();

        // Tipo de crudo
        $tipo_crudo = $request['tipo_crudo'];
        if ($tipo_crudo == Util::BRENT)
            $tipo_crudo = 'BRENT';
        else
            $tipo_crudo = 'WTI';

        // Corrección RVP
        $this->correccion_rvp = $request['correccion_rvp'];
        if ($request['correccion_rvp'] == 1)
            $this->correccion_rvp = 1;
        else
            $this->correccion_rvp = 0;

        // Almacenar en la tabla Solver
        $idSolver = DB::table('MEPCO_SOLVER')->insertGetId([
            'PROC_ID'                           => $proc_id, 
            'SOLV_PRECIO_PARIDAD_MIN'           => $paridad_minimo,
            'SOLV_PRECIO_PARIDAD_MAX'           => $paridad_maximo,
            'SOLV_PRECIO_REFERENCIA_MIN'        => $referencia_minimo,
            'SOLV_PRECIO_REFERENCIA_MAX'        => $referencia_maximo,
            'SOLV_CORRECCION_RVP'               => $this->correccion_rvp,
        ]);

        // Obtener las fechas de valores de mercado
        $fechasValoresMercado = DB::table('MEPCO_VALOR_MERCADO AS MVM')
            ->join('MEPCO_VAME_USPE AS MVU', 'MVM.VAUS_ID', '=', 'MVU.VAUS_ID')
            ->join('MEPCO_USUA_PERF AS MUP', 'MVU.USPE_ID', '=', 'MUP.USPE_ID')
            ->select('MVM.VAME_FECHA')
            ->where('MVU.PROC_ID', '=', $proc_id)
            ->where('MUP.PERF_ID', '=', Util::PERFIL_USUARIO_GENERADOR)
            ->where('MVU.VAUS_ELIMINADO', 0)
            ->take(5)
            ->get();

        // Obtener rangos para las variables
        $rango_n = DB::table('MEPCO_RESTRICCION_PARAMETROS AS MRP')
            ->join('MEPCO_PARAMETROS_VARIABLE AS MPV', 'MPV.PAVA_ID', '=', 'MRP.PAVA_ID')
            ->select('MRP.REPA_VALOR_MINIMO', 'MRP.REPA_VALOR_MAXIMO')
            ->where('MPV.PAVA_LLAVE', '=', '{{N}}')
            ->first();
        $rango_m = DB::table('MEPCO_RESTRICCION_PARAMETROS AS MRP')
            ->join('MEPCO_PARAMETROS_VARIABLE AS MPV', 'MPV.PAVA_ID', '=', 'MRP.PAVA_ID')
            ->select('MRP.REPA_VALOR_MINIMO', 'MRP.REPA_VALOR_MAXIMO')
            ->where('MPV.PAVA_LLAVE', '=', '{{M}}')
            ->first();
        $rango_s = DB::table('MEPCO_RESTRICCION_PARAMETROS AS MRP')
            ->join('MEPCO_PARAMETROS_VARIABLE AS MPV', 'MPV.PAVA_ID', '=', 'MRP.PAVA_ID')
            ->select('MRP.REPA_VALOR_MINIMO', 'MRP.REPA_VALOR_MAXIMO')
            ->where('MPV.PAVA_LLAVE', '=', '{{S}}')
            ->first();
        $rango_t = DB::table('MEPCO_RESTRICCION_PARAMETROS AS MRP')
            ->join('MEPCO_PARAMETROS_VARIABLE AS MPV', 'MPV.PAVA_ID', '=', 'MRP.PAVA_ID')
            ->select('MRP.REPA_VALOR_MINIMO', 'MRP.REPA_VALOR_MAXIMO')
            ->where('MPV.PAVA_LLAVE', '=', '{{T}}')
            ->first();
        $rango_f = DB::table('MEPCO_RESTRICCION_PARAMETROS AS MRP')
            ->join('MEPCO_PARAMETROS_VARIABLE AS MPV', 'MPV.PAVA_ID', '=', 'MRP.PAVA_ID')
            ->select('MRP.REPA_VALOR_MINIMO', 'MRP.REPA_VALOR_MAXIMO', 'MRP.REPA_INCREMENTO')
            ->where('MPV.PAVA_LLAVE', '=', '{{F}}')
            ->first();

        // Inicializar valores de parámetros si no son fijos. Preparar rangos de variables
        if (($n == null)||($n=='')) {
            $n_max  = $rango_n->REPA_VALOR_MAXIMO;
            $n_min  = $rango_n->REPA_VALOR_MINIMO;
        } else {
            $fijar_n    = 1;
            $n_max      = $n;
            $n_min      = $n;
        }

        if (($m == null)||($m=='')) {
            $m_max  = $rango_m->REPA_VALOR_MAXIMO;
            $m_min  = $rango_m->REPA_VALOR_MINIMO;
        } else {
            $fijar_m    = 1;
            $m_max      = $m;
            $m_min      = $m;
        }

        if (($s == null)||($s=='')) {
            $s_max  = $rango_s->REPA_VALOR_MAXIMO;
            $s_min  = $rango_s->REPA_VALOR_MINIMO;
        } else {
            $fijar_s    = 1;
            $s_max      = $s;
            $s_min      = $s;
        }

        if (($t == null)||($t=='')) {
            $t = $rango_t->REPA_VALOR_MINIMO;
        } else {
            $fijar_t = 1;
        }

        $f_incremento = (float)$rango_f->REPA_INCREMENTO;

        if (($f == null)||($f=='')){
            $f_max  = (float)$rango_f->REPA_VALOR_MAXIMO;
            $f_min  = (float)$rango_f->REPA_VALOR_MINIMO;
        } else {
            $fijar_f = 1;
            $f_max      = (float)$f;
            $f_min      = (float)$f;
        }


        /*
        * OBTENER PARIDAD
        */
        // Si las variables siguen dentro de su rango, y no se ha sobrepasado el precio de paridad
        if ($fijar_t==0) {
            $t = $rango_t->REPA_VALOR_MINIMO;
            $continuar = 1;
            // Primero se obtiene la paridad promedio semanal, una vez, fuera del bucle
            $this->obtenerParidadPromedioSemanal($combustible, $proc_id, $fechasValoresMercado);
            while (($t < $rango_t->REPA_VALOR_MAXIMO) && ($continuar == 1)) {
                $parametros['t'] = $t;
                // Si el precio de paridad está entre el máximo y el mínimo introducir al array de resultados
                $this->paridad = $this->calcularParidad($combustible, $proc_id, $parametros);

                if ($this->paridad >= $paridad_minimo && $this->paridad <= $paridad_maximo) {
                    $solverParidad = SolverParidad::create([
                        'SOLV_ID'           => $idSolver,
                        'HIDR_ID'           => $combustible,
                        'SOLP_T'            => $t,
                        'SOLP_PARIDAD'      => $this->paridad,
                    ]);

                    // Tabla de resultados para refrescar la página
                    $resultadosParidad[] = array(Util::formatNumber($paridad_minimo), Util::formatNumber($paridad_maximo), (int)$t, Util::formatNumber($this->paridad));
                }
                // Si ya se sobrepasó el precio de paridad, se sale del bucle.
                else if ($this->paridad>$paridad_maximo) {
                    $continuar = 0;
                }
                $t++;
            }
        }
        else {
            // Se calcula la paridad para el valor de t fijo
            $this->obtenerParidadPromedioSemanal($combustible, $proc_id, $fechasValoresMercado);
            $parametros['t'] = $t;
            $this->paridad = $this->calcularParidad($combustible, $proc_id, $parametros);
            $solverParidad = SolverParidad::create([
                'SOLV_ID'           => $idSolver,
                'HIDR_ID'           => $combustible,
                'SOLP_T'            => $t,
                'SOLP_PARIDAD'      => $this->paridad
            ]);

            // Tabla de resultados para refrescar la página
            $resultadosParidad[] = array(Util::formatNumber($paridad_minimo), Util::formatNumber($paridad_maximo), $t, Util::formatNumber($this->paridad));
        }
        /*
        * FIN OBTENER PARIDAD
        */

        /*
        * OBTENER REFERENCIA
        */
        $cont = 0;
        $arrCrudoFuturo = array();
        $crudoHistoricoSemanal = null;

        if($tipo_crudo == 'BRENT')
        {
            $crudoHistoricoSemanal = Util::ejecutarFormula('{{BR_BRENT_PROMEDIO_SEM_$/m3}}', [
                'PROC_ID' => $proc_id
            ]);

            for($i = 1; $i <=6; $i++)
            {
                $arrCrudoFuturo[$i] = Util::ejecutarFormula('{{BR_BRENT_FUTURO}}', [
                    'PROC_ID' => $proc_id, 
                    'M' => $i
                ]);    
            }            
        }else
        {//WTI
            $crudoHistoricoSemanal = Util::ejecutarFormula('{{WT_WTI_PROMEDIO_SEM_$/m3}}', [
                'PROC_ID' => $proc_id
            ]);

            for($i = 1; $i <=6; $i++)
            {
                $arrCrudoFuturo[$i] = Util::ejecutarFormula('{{WT_WTI_FUTURO}}', [
                    'PROC_ID' => $proc_id, 
                    'M' => $i
                ]);    
            }
        }

        for ($n=$n_min; $n<=$n_max; $n++) {

            if($tipo_crudo == 'BRENT')
            {
                $crudoHistorico = Util::ejecutarFormula('{{BR_BRENT_HISTORICO}}', [
                    'PROC_ID' => $proc_id, 
                    'BR_BRENT_PROMEDIO_SEM_$/m3' => $crudoHistoricoSemanal,
                    'N'     => $n, 
                    'LIMIT' => (5*($n - 1))
                ]);
            }else
            {//WTI
                $crudoHistorico = Util::ejecutarFormula('{{WT_WTI_HISTORICO}}', [
                    'PROC_ID' => $proc_id, 
                    'WT_WTI_PROMEDIO_SEM_$/m3' => $crudoHistoricoSemanal,
                    'N'     => $n, 
                    'LIMIT' => (5*($n - 1))
                ]);
            }

            for ($m = $m_min; $m <= $m_max; $m++) {
                $f = $f_min;
                while ($f <= $f_max) {

                    $this->crudo = $arrCrudoFuturo[(int)$m] * $f + $crudoHistorico * (1 - $f);

                    switch ($combustible) {
                        case Util::COMBUSTIBLE_93:
                            if($tipo_crudo == 'BRENT')
                            {
                                $margenSemanal = Util::ejecutarFormula('{{MG_MARGEN_SEMANAL_BRENT_G_87}}', ['PROC_ID' => $proc_id]);

                            }else
                            {//WTI
                                $margenSemanal = Util::ejecutarFormula('{{MG_MARGEN_SEMANAL_WTI_G_87}}', ['PROC_ID' => $proc_id]);
                            }
                            break;
                        case Util::COMBUSTIBLE_97:
                            if($tipo_crudo == 'BRENT')
                            {
                                $margenSemanal = Util::ejecutarFormula('{{MG_MARGEN_SEMANAL_BRENT_G_93}}', ['PROC_ID' => $proc_id]);
                            
                            }else
                            {//WTI
                                $margenSemanal = Util::ejecutarFormula('{{MG_MARGEN_SEMANAL_WTI_G_93}}', ['PROC_ID' => $proc_id]);
                                
                            }
                            break;

                        case Util::COMBUSTIBLE_DIESEL:
                            if($tipo_crudo == 'BRENT')
                            {
                                $margenSemanal = Util::ejecutarFormula('{{MG_MARGEN_SEMANAL_BRENT_DIESEL}}', ['PROC_ID' => $proc_id]);
                            
                            }else
                            {//WTI
                                $margenSemanal = Util::ejecutarFormula('{{MG_MARGEN_SEMANAL_WTI_DIESEL}}', ['PROC_ID' => $proc_id]);
                            
                            }
                            break;

                        case Util::COMBUSTIBLE_GLP:
                            if($tipo_crudo == 'BRENT')
                            {
                                $margenSemanal = Util::ejecutarFormula('{{SO_MARGEN_SEMANAL_BRENT_GLP}}', [
                                    'PROC_ID'               => $proc_id,
                                    'SO_FOB_GLP_ARBITRADO'  => $this->fob_glp_arbitrado_prom
                                ]);
                            
                            }else
                            {//WTI
                                $margenSemanal = Util::ejecutarFormula('{{SO_MARGEN_SEMANAL_WTI_GLP}}', [
                                    'PROC_ID'               => $proc_id,
                                    'SO_FOB_GLP_ARBITRADO'  => $this->fob_glp_arbitrado_prom
                                ]);
                            }
                            break;
                    }

                    for ($s = $s_min; $s <= $s_max; $s++) {

                        switch ($combustible) {
                            case Util::COMBUSTIBLE_93:
                                if($tipo_crudo == 'BRENT')
                                {
                                    $result = Util::ejecutarFormula('{{SO_MARGEN_BRENT_G_87}}', [
                                        'PROC_ID'                       => $proc_id,
                                        'SO_MG_PROMEDIO_SEMANAL_ACTUAL' => $margenSemanal,
                                        'S'                             => $s,
                                        'LIMIT'                         => (5*($s - 1))
                                    ]);

                                }else
                                {//WTI
                                    $result = Util::ejecutarFormula('{{SO_MARGEN_WTI_G_87}}', [
                                        'PROC_ID'                       => $proc_id,
                                        'SO_MG_PROMEDIO_SEMANAL_ACTUAL' => $margenSemanal,
                                        'S'                             => $s,
                                        'LIMIT'                         => (5*($s - 1))
                                    ]);
                                }
                                break;
                            case Util::COMBUSTIBLE_97:
                                if($tipo_crudo == 'BRENT')
                                {
                                    $result = Util::ejecutarFormula('{{SO_MARGEN_BRENT_G_93}}', [
                                        'PROC_ID'                       => $proc_id,
                                        'SO_MG_PROMEDIO_SEMANAL_ACTUAL' => $margenSemanal,
                                        'S'                             => $s,
                                        'LIMIT'                         => (5*($s - 1))
                                    ]);
                                }else
                                {//WTI
                                    $result = Util::ejecutarFormula('{{SO_MARGEN_WTI_G_93}}', [
                                        'PROC_ID'                       => $proc_id,
                                        'SO_MG_PROMEDIO_SEMANAL_ACTUAL' => $margenSemanal,
                                        'S'                             => $s,
                                        'LIMIT'                         => (5*($s - 1))
                                    ]);
                                }
                                break;

                            case Util::COMBUSTIBLE_DIESEL:
                                if($tipo_crudo == 'BRENT')
                                {
                                    $result = Util::ejecutarFormula('{{SO_MARGEN_BRENT_DIESEL}}', [
                                        'PROC_ID'                       => $proc_id,
                                        'SO_MG_PROMEDIO_SEMANAL_ACTUAL' => $margenSemanal,
                                        'S'                             => $s,
                                        'LIMIT'                         => (5*($s - 1))
                                    ]);

                                }else
                                {//WTI
                                    $result = Util::ejecutarFormula('{{SO_MARGEN_WTI_DIESEL}}', [
                                        'PROC_ID'                       => $proc_id,
                                        'SO_MG_PROMEDIO_SEMANAL_ACTUAL' => $margenSemanal,
                                        'S'                             => $s,
                                        'LIMIT'                         => (5*($s - 1))
                                    ]);
                                }
                                break;

                            case Util::COMBUSTIBLE_GLP:
                                if($tipo_crudo == 'BRENT')
                                {
                                    $result = Util::ejecutarFormula('{{SO_MARGEN_BRENT_GLP}}', [
                                        'PROC_ID'                       => $proc_id,
                                        'SO_MG_PROMEDIO_SEMANAL_ACTUAL' => $margenSemanal,
                                        'S'                             => $s,
                                        'LIMIT'                         => (5*($s - 1))
                                    ]);
                                }else
                                {//WTI
                                    $result = Util::ejecutarFormula('{{SO_MARGEN_WTI_GLP}}', [
                                        'PROC_ID'                       => $proc_id,
                                        'SO_MG_PROMEDIO_SEMANAL_ACTUAL' => $margenSemanal,
                                        'S'                             => $s,
                                        'LIMIT'                         => (5*($s - 1))
                                    ]);
                                }
                                break;
                        }

                        $this->margen = $result;

                        // 3- Calcular referencia
                        $this->calcularReferencia($combustible, $proc_id, $tipo_crudo, $fechasValoresMercado);
                        
                        $this->referencia_inf = $this->referencia_int * 0.95;
                        $this->referencia_sup = $this->referencia_int * 1.05;

                        if ($this->referencia_int >= $referencia_minimo && $this->referencia_int <= $referencia_maximo) {
                            // 4- Resultados
                            $solverReferencia = SolverReferencia::create([
                                'SOLV_ID'           => $idSolver,
                                'HIDR_ID'           => $combustible,
                                'SOLR_N'            => $n,
                                'SOLR_M'            => $m,
                                'SOLR_S'            => $s,
                                'SOLR_F'            => (float)$f,
                                'SOLR_CRUDO'        => $this->crudo,
                                'SOLR_MARGEN'       => $this->margen,
                                'SOLR_PREFINF'      => $this->referencia_inf,
                                'SOLR_PREFINT'      => $this->referencia_int,
                                'SOLR_PREFSUP'      => $this->referencia_sup
                             ]);

                            // Tabla de resultados para refrescar la página
                            $resultadosReferencia[] = array(Util::formatNumber($referencia_minimo), Util::formatNumber($referencia_maximo), $n, $m, $s, $f, Util::formatNumber($this->crudo), Util::formatNumber($this->margen), Util::formatNumber($this->referencia_inf), Util::formatNumber($this->referencia_int), Util::formatNumber($this->referencia_sup));
                        }

                        $this->crudo            = null;
                        $this->referencia_int   = null;
                        $this->referencia_sup   = null;
                        $this->referencia_inf   = null;
                        $f = $f + $f_incremento;
                    }
                }
            }
        }
        $this->margen = null;

        /*
        * FIN OBTENER REFERENCIA
        */

        Util::crearFilaLog('Solver', 'Crear cálculo', 'solver', $idSolver);

        Util::log('TIEMPO SOLVER FIN: '.Util::dateTimeNow());
        /*
        * Fin de la ejecución, se devuelve la llamada de AJAX con las tablas de resultados
        */
        return response()->json([
                'status'                => 'success',
                'resultadosParidad'     => json_encode($resultadosParidad),
                'resultadosReferencia'  => json_encode($resultadosReferencia),
                'hidrocarburo'          => $combustible
        ]);
    }


    /**
     * Obtiene el valor de paridad
     *
     * @return Response
     */
    public function obtenerParidadPromedioSemanal($combustible, $proc_id, $fechasValoresMercado)
    {
        $this->paridad_promedio_semanal       = null;
        Util::$almacenaFormulaValoresSinFecha = false;

        // Realizar cálculo paridad promedio semanal
        switch ($combustible) {
            case Util::COMBUSTIBLE_93:
                $sum_paridad_ca = 0;
                foreach ($fechasValoresMercado as $item) {
                    
                    $paridad_ca = Util::ejecutarFormula('{{PA_PARIDAD_CA_G_87}}', [
                        'PROC_ID'           => $proc_id, 
                        'FECHA'             => $item->VAME_FECHA,
                        'CORRECCION_RVP'    => $this->correccion_rvp
                    ]);
                    $sum_paridad_ca = $sum_paridad_ca+$paridad_ca;
                }
                // Obtener el promedio de PARIDAD_CA
                $this->paridad_promedio_semanal = $sum_paridad_ca/count($fechasValoresMercado);
                break;
            case Util::COMBUSTIBLE_97:
                $sum_paridad_ca = 0;
                foreach ($fechasValoresMercado as $item) {
                    
                    $paridad_ca = Util::ejecutarFormula('{{PA_PARIDAD_CA_G_93}}', [
                        'PROC_ID' => $proc_id, 
                        'FECHA' => $item->VAME_FECHA,
                        'CORRECCION_RVP'    => $this->correccion_rvp
                    ]);
                    $sum_paridad_ca = $sum_paridad_ca+$paridad_ca;
                }
                // Obtener el promedio de PARIDAD_CA
                $this->paridad_promedio_semanal = $sum_paridad_ca/count($fechasValoresMercado);
                break;
            case Util::COMBUSTIBLE_DIESEL:
                $sum_paridad_ca = 0;
                foreach ($fechasValoresMercado as $item) {
                    
                    $paridad_ca = Util::ejecutarFormula('{{PA_PARIDAD_CA_D}}', [
                        'PROC_ID' => $proc_id, 
                        'FECHA' => $item->VAME_FECHA,
                    ]);
                    $sum_paridad_ca = $sum_paridad_ca+$paridad_ca;
                }
                // Obtener el promedio de PARIDAD_CA
                $this->paridad_promedio_semanal = $sum_paridad_ca/count($fechasValoresMercado);
                break;
            case Util::COMBUSTIBLE_GLP:
                $sum_paridad_ca     = 0;
                $sum_fob_arbitrado  = 0;
                foreach ($fechasValoresMercado as $item) {
                    // Calcular paridad para GLP
                    
                    $paridad_ca = Util::ejecutarFormula('{{PA_PARIDAD_CA_GLP}}', [
                        'PROC_ID' => $proc_id, 
                        'FECHA' => $item->VAME_FECHA,
                    ]);
                    $sum_paridad_ca = $sum_paridad_ca+$paridad_ca;
                    // Calcular también el fob arbitrado
                    Util::obtenerFormulaValores();
                    $sum_fob_arbitrado = $sum_fob_arbitrado + Util::$arrFormulaValores['GLP_ARBITRADO'][$item->VAME_FECHA];
                }
                // Obtener el promedio de PARIDAD_CA
                $this->paridad_promedio_semanal = $sum_paridad_ca/count($fechasValoresMercado);
                // Obtener glp arbitrado promedio
                $this->fob_glp_arbitrado_prom   = $sum_fob_arbitrado/count($fechasValoresMercado);
                break;
        };
        return null;
    }

    /**
     * Obtiene el valor de paridad
     *
     * @return Response
     */
    public function calcularParidad($combustible, $proc_id, $parametros)
    {
        $t = $parametros['t'];

        // Realizar cálculo solver PARA LA PARIDAD
        $result = null;

        Util::$almacenaFormulaValoresSinFecha = true;
        switch ($combustible) {
            case Util::COMBUSTIBLE_93:
                $result = Util::ejecutarFormula('{{SO_PARIDAD_$/m_G_87}}', [
                    'PROC_ID'                                   => $proc_id, 
                    'SO_PARIDAD_PROMEDIO_SEMANAL_ACTUAL'        => $this->paridad_promedio_semanal,
                    'T'                                         => $t,
                    'LIMIT'                                     => (5*($t - 1))
                ]);
                break;
            case Util::COMBUSTIBLE_97:
                $result = Util::ejecutarFormula('{{SO_PARIDAD_$/m_G_93}}', [
                    'PROC_ID'                                   => $proc_id, 
                    'SO_PARIDAD_PROMEDIO_SEMANAL_ACTUAL'        => $this->paridad_promedio_semanal,
                    'T'                                         => $t,
                    'LIMIT'                                     => (5*($t - 1))
                ]);
                break;
            case Util::COMBUSTIBLE_DIESEL:
                $result = Util::ejecutarFormula('{{SO_PARIDAD_$/m_DIESEL}}', [
                    'PROC_ID'                                   => $proc_id, 
                    'SO_PARIDAD_PROMEDIO_SEMANAL_ACTUAL'        => $this->paridad_promedio_semanal,
                    'T'                                         => $t,
                    'LIMIT'                                     => (5*($t - 1))
                ]);
                break;
            case Util::COMBUSTIBLE_GLP:
                $result = Util::ejecutarFormula('{{SO_PARIDAD_$/m_GLP}}', [
                    'PROC_ID'                                   => $proc_id, 
                    'SO_PARIDAD_PROMEDIO_SEMANAL_ACTUAL'        => $this->paridad_promedio_semanal,
                    'T'                                         => $t,
                    'LIMIT'                                     => (5*($t - 1))
                ]);
                break;
        };
        return $result;
    }

    /**
     * Obtiene el valor para crudo
     *
     * @return Response
     */
    private function calcularCrudo($tipo_crudo, $proc_id, $parametros)
    {

        $this->crudo = null;
        $n = $parametros['n'];
        $m = $parametros['m'];
        $f = $parametros['f'];

        Util::$almacenaFormulaValoresSinFecha = true;
        

        if($tipo_crudo == 'BRENT')
        {
            $result = Util::ejecutarFormula('{{BR_BRENT_CALCULADO}}', [
                'PROC_ID' => $proc_id, 
                'M' => $m,
                'N' => $n, 
                'F' => $f, 
                'LIMIT' => (5*($n - 1))
            ]);
        }else
        {//WTI
            $result = Util::ejecutarFormula('{{WT_WTI_CALCULADO}}', [
                'PROC_ID' => $proc_id,
                'M' => $m,
                'N' => $n,
                'F' => $f,
                'LIMIT' => (5 * ($n - 1))
            ]);
        }

        $this->crudo = $result;
        return null;
    }

    /**
     * Obtiene el valor para margen
     *
     * @return Response
     */
    public function calcularMargen($combustible, $proc_id, $parametros, $tipo_crudo)
    {
        $s = $parametros['s'];
        $this->margen = null;

        Util::$almacenaFormulaValoresSinFecha = true;

        switch ($combustible) {
            case Util::COMBUSTIBLE_93:
                if($tipo_crudo == 'BRENT')
                {
                    $margenSemanal = Util::ejecutarFormula('{{MG_MARGEN_SEMANAL_BRENT_G_87}}', ['PROC_ID' => $proc_id]);

                    //CALCULA PROMEDIO MARGEN
                    $result = Util::ejecutarFormula('{{SO_MARGEN_BRENT_G_87}}', [
                        'PROC_ID'                       => $proc_id,
                        'SO_MG_PROMEDIO_SEMANAL_ACTUAL' => $margenSemanal,
                        'S'                             => $s,
                        'LIMIT'                         => (5*($s - 1))
                    ]);

                }else
                {//WTI
                    $margenSemanal = Util::ejecutarFormula('{{MG_MARGEN_SEMANAL_WTI_G_87}}', ['PROC_ID' => $proc_id]);

                    $result = Util::ejecutarFormula('{{SO_MARGEN_WTI_G_87}}', [
                        'PROC_ID'                       => $proc_id,
                        'SO_MG_PROMEDIO_SEMANAL_ACTUAL' => $margenSemanal,
                        'S'                             => $s,
                        'LIMIT'                         => (5*($s - 1))
                    ]);

                }
                break;
            case Util::COMBUSTIBLE_97:
                if($tipo_crudo == 'BRENT')
                {
                    $margenSemanal = Util::ejecutarFormula('{{MG_MARGEN_SEMANAL_BRENT_G_93}}', ['PROC_ID' => $proc_id]);
                
                    $result = Util::ejecutarFormula('{{SO_MARGEN_BRENT_G_93}}', [
                        'PROC_ID'                       => $proc_id,
                        'SO_MG_PROMEDIO_SEMANAL_ACTUAL' => $margenSemanal,
                        'S'                             => $s,
                        'LIMIT'                         => (5*($s - 1))
                    ]);
                }else
                {//WTI
                    $margenSemanal = Util::ejecutarFormula('{{MG_MARGEN_SEMANAL_WTI_G_93}}', ['PROC_ID' => $proc_id]);
                    
                    $result = Util::ejecutarFormula('{{SO_MARGEN_WTI_G_93}}', [
                        'PROC_ID'                       => $proc_id,
                        'SO_MG_PROMEDIO_SEMANAL_ACTUAL' => $margenSemanal,
                        'S'                             => $s,
                        'LIMIT'                         => (5*($s - 1))
                    ]);
                }
                break;

            case Util::COMBUSTIBLE_DIESEL:
                if($tipo_crudo == 'BRENT')
                {
                    
                    $margenSemanal = Util::ejecutarFormula('{{MG_MARGEN_SEMANAL_BRENT_DIESEL}}', ['PROC_ID' => $proc_id]);
                    
                    
                    $result = Util::ejecutarFormula('{{SO_MARGEN_BRENT_DIESEL}}', [
                        'PROC_ID'                       => $proc_id,
                        'SO_MG_PROMEDIO_SEMANAL_ACTUAL' => $margenSemanal,
                        'S'                             => $s,
                        'LIMIT'                         => (5*($s - 1))
                    ]);

                }else
                {//WTI
                    
                    $margenSemanal = Util::ejecutarFormula('{{MG_MARGEN_SEMANAL_WTI_DIESEL}}', ['PROC_ID' => $proc_id]);
                
                    
                    $result = Util::ejecutarFormula('{{SO_MARGEN_WTI_DIESEL}}', [
                        'PROC_ID'                       => $proc_id,
                        'SO_MG_PROMEDIO_SEMANAL_ACTUAL' => $margenSemanal,
                        'S'                             => $s,
                        'LIMIT'                         => (5*($s - 1))
                    ]);
                }
                break;

            case Util::COMBUSTIBLE_GLP:
                if($tipo_crudo == 'BRENT')
                {
                    
                    $margenSemanal = Util::ejecutarFormula('{{SO_MARGEN_SEMANAL_BRENT_GLP}}', [
                        'PROC_ID'               => $proc_id,
                        'SO_FOB_GLP_ARBITRADO'  => $this->fob_glp_arbitrado_prom
                    ]);
                
                    
                    $result = Util::ejecutarFormula('{{SO_MARGEN_BRENT_GLP}}', [
                        'PROC_ID'                       => $proc_id,
                        'SO_MG_PROMEDIO_SEMANAL_ACTUAL' => $margenSemanal,
                        'S'                             => $s,
                        'LIMIT'                         => (5*($s - 1))
                    ]);
                }else
                {//WTI
                    
                    $margenSemanal = Util::ejecutarFormula('{{SO_MARGEN_SEMANAL_WTI_GLP}}', [
                        'PROC_ID'               => $proc_id,
                        'SO_FOB_GLP_ARBITRADO'  => $this->fob_glp_arbitrado_prom
                    ]);
                
                    
                    $result = Util::ejecutarFormula('{{SO_MARGEN_WTI_GLP}}', [
                        'PROC_ID'                       => $proc_id,
                        'SO_MG_PROMEDIO_SEMANAL_ACTUAL' => $margenSemanal,
                        'S'                             => $s,
                        'LIMIT'                         => (5*($s - 1))
                    ]);
                }
                break;
        }

        $this->margen = $result;
        return null;
    }

    /**
     * Obtiene el valor para referencia
     *
     * @return Response
     */
    public function calcularReferencia($combustible, $proc_id, $tipo_crudo, $fechasValoresMercado)
    {
        $this->referencia_int = null;
        switch ($combustible) {
            case Util::COMBUSTIBLE_93:
                $sum_ref_paridad = 0;
                foreach ($fechasValoresMercado as $item) {
                    Util::$almacenaFormulaValoresSinFecha = false;
                    
                    $sum_ref_paridad = $sum_ref_paridad + Util::ejecutarFormula('{{SO_RE_PARIDAD_CA_G_87}}', [
                        'PROC_ID'       => $proc_id,
                        'SO_MARGEN'     => $this->margen,
                        'SO_CRUDO'      => $this->crudo,
                        'CORRECCION_RVP'=> $this->correccion_rvp,
                        'FECHA'         => $item->VAME_FECHA
                    ]);
                }
                $referenciaInt = $sum_ref_paridad/count($fechasValoresMercado);
                break;
            case Util::COMBUSTIBLE_97:
                $sum_ref_paridad = 0;

                foreach ($fechasValoresMercado as $item) {
                    Util::$almacenaFormulaValoresSinFecha = false;
                    
                    $sum_ref_paridad = $sum_ref_paridad + Util::ejecutarFormula('{{SO_RE_PARIDAD_CA_G_93}}', [
                        'PROC_ID'        => $proc_id,
                        'SO_MARGEN'      => $this->margen,
                        'SO_CRUDO'       => $this->crudo,
                        'CORRECCION_RVP' => $this->correccion_rvp,
                        'FECHA'          => $item->VAME_FECHA
                    ]);
                }
                $referenciaInt = $sum_ref_paridad/count($fechasValoresMercado);
                break;
            case Util::COMBUSTIBLE_DIESEL:
                $sum_ref_paridad = 0;
                foreach ($fechasValoresMercado as $item) {
                    Util::$almacenaFormulaValoresSinFecha = false;

                    $sum_ref_paridad = $sum_ref_paridad + Util::ejecutarFormula('{{SO_RE_PARIDAD_CA_D}}', [
                        'PROC_ID'       => $proc_id,
                        'SO_MARGEN'     => $this->margen,
                        'SO_CRUDO'      => $this->crudo,
                        'FECHA'         => $item->VAME_FECHA
                    ]);
                }
                $referenciaInt = $sum_ref_paridad/count($fechasValoresMercado);
                break;
            case Util::COMBUSTIBLE_GLP:
                $sum_ref_paridad = 0;
                foreach ($fechasValoresMercado as $item) {
                    Util::$almacenaFormulaValoresSinFecha = false;
                    
                    $sum_ref_paridad = $sum_ref_paridad + Util::ejecutarFormula('{{SO_RE_PARIDAD_CA_GLP}}', [
                        'PROC_ID'       => $proc_id,
                        'SO_MARGEN'     => $this->margen,
                        'SO_CRUDO'      => $this->crudo,
                        'FECHA'         => $item->VAME_FECHA
                    ]);
                }
                $referenciaInt = $sum_ref_paridad/count($fechasValoresMercado);
                break;
        }
        $this->referencia_int = $referenciaInt;
        return null;
    }

    public function calcularTiempoEjecucion(Request $request)
    {
        $registros          = 500;
        $tiempoSegundos     = 250;
        $registrosaCalcular = 1;

        $n = $request->input('n');
        $m = $request->input('m');
        $s = $request->input('s');
        $t = $request->input('t');
        $f = $request->input('f');

        if($n == null)
        {
            $registrosaCalcular = $registrosaCalcular * 104;
        }

        if($m == null)
        {
            $registrosaCalcular = $registrosaCalcular * 4;
        }

        if($s == null)
        {
            $registrosaCalcular = $registrosaCalcular * 104;
        }

        if($t == null)
        {
            $registrosaCalcular = $registrosaCalcular * 4;
        }

        if($f == null)
        {
            $registrosaCalcular = $registrosaCalcular * 50;
        }

        $tiempo_en_segundos = (($registrosaCalcular * $tiempoSegundos) / $registros);

        $horas    = floor($tiempo_en_segundos / 3600);
        $horas    = (strlen($horas) < 2) ? '0' . $horas : $horas;

        $minutos  = floor(($tiempo_en_segundos - ($horas * 3600)) / 60);
        $minutos  = (strlen($minutos) < 2) ? '0' . $minutos : $minutos;

        $segundos = round($tiempo_en_segundos - ($horas * 3600) - ($minutos * 60));
        $segundos = (strlen($segundos) < 2) ? '0' . $segundos : $segundos;
     
        $result   =  $horas. ':' . $minutos . ":" . $segundos;

        Util::log('result: ' . $result);


        return response()->json([
            'tiempo' => $result,
            'status' => 'OK'
        ]);
    }
}
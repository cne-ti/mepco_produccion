<?php

namespace App\Http\Controllers;

use Validator;
use App\Reporte;
use App\DatosHistoricos;
use App\Util;
use App\VameUspe;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\MessageBag;
use Maatwebsite\Excel\Facades\Excel;
use ZipArchive;
use Response; 
use Log;
use DB;

class reporteController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('authenticated');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {

        return view('reporte.list');

    }

    public function descargaDocumentos()
    {
        $mensaje = '<div class="panel panel-primary"><div class="panel-heading">Descarga de Documentos</div><div class="panel-body">Seleccione un rango de fechas de vigencia para obtener los documentos asociados.</div></div>';

        $maxDate = DB::table('MEPCO_PROCESO')
                    ->select(DB::raw('MAX(PROC_FECHA_VIGENCIA)'))
                    ->pluck('MAX(PROC_FECHA_VIGENCIA)');
        $maxDate = Util::dateFormat($maxDate, 'd/m/Y');

        return view('reporte.descarga-documentos', compact('mensaje', 'maxDate'));
    }

    /**
     * Obtiene los documentos entre las fechas especificadas y las vuelca a la vista
     *
     * @return Response
     */
    public function getDescargaDocumentos(Request $request)
    {
        $formData = array(
        	'fecha_vigencia_desde'=>$request['fecha_vigencia_desde'], 
        	'fecha_vigencia_hasta'=>$request['fecha_vigencia_hasta']
        );

        /* *** VALIDACIÓN *** */
        $validator = Validator::make($formData,[
            'fecha_vigencia_desde'           => 'required|max:10|min:10',
            'fecha_vigencia_hasta'           => 'required|max:10|min:10',
        ]);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        /* Manejo de fechas */
        $fecha_vigencia_desde = $request['fecha_vigencia_desde'];
        $fecha_vigencia_hasta = $request['fecha_vigencia_hasta'];

        $fecha_vigencia_desde = str_replace('/', '-', $fecha_vigencia_desde);
        $fecha_vigencia_hasta = str_replace('/', '-', $fecha_vigencia_hasta);

        // Convertir las fechas al formato de bbdd
        $fecha_vigencia_hasta = Util::dateFormat($fecha_vigencia_hasta, 'Ymd');
        $fecha_vigencia_desde = Util::dateFormat($fecha_vigencia_desde, 'Ymd');

        /* Creación del array de fechas, con todas las fechas de vigencia
        * desde $fecha_vigencia_desde hasta $fecha_vigencia_hasta
        */
        $fechas = array();
        $fechas[] = $fecha_vigencia_hasta;
        $fechas[] = $fecha_vigencia_desde;
        while (strtotime($fecha_vigencia_desde) < strtotime($fecha_vigencia_hasta)) {
            $fechas[] = date('Ymd', strtotime('next Thursday '.$fecha_vigencia_desde));
            $fecha_vigencia_desde =  date('Ymd', strtotime('next Thursday '.$fecha_vigencia_desde));
        }

        /* Creación del archivo .zip */
        $zip = new ZipArchive();
        $zipFileName = tempnam('tmp', 'zip');
        if ($zip->open($zipFileName, ZipArchive::CREATE) === TRUE) {
            $numero_archivos = 0;
            /* Obtener para cada fecha el contenido de su carpeta de informes */
            foreach ($fechas as $fecha) { 
                foreach ( glob(storage_path('app/'.Util::PATH_INFORMES.$fecha.'/*')) as $fileName) {
                    $file = basename($fileName);  
                    // Añadir los archivos de la carpeta al zip               
                    $zip->addFile($fileName, $file);
                    $numero_archivos++;
                }    
            } 
            /* Al finalizar, cerrar el zip y forzar la descarga */
            $zip->close();
            if ($numero_archivos>0) {
                header('Content-Type: application/zip');
                header('Content-Length: '.filesize($zipFileName));
                header('Content-Disposition: attachment; filename="descargaDocumentos.zip"');
                readfile($zipFileName);
                unlink($zipFileName);   
            }
            else {
                \Session::set('ALERT-TYPE', 'INFO');
                \Session::set('ALERT-MSG', json_encode('No existen informes para las fechas seleccionadas.'));
                return redirect()->back();
            }
        }
        else {
            \Session::set('ALERT-TYPE', 'ERROR');
            \Session::set('ALERT-MSG', json_encode('Ha ocurrido un error al descargar los archivos. Por favor, inténtelo de nuevo más tarde o contacte con el Administrador del Sistema.'));
            return redirect()->back();
        }
       
    }

    public function resultados()
    {
        $tabla = array();

        $maxDate = DB::table('MEPCO_PROCESO')
                    ->select(DB::raw('MAX(PROC_FECHA_VIGENCIA)'))
                    ->pluck('MAX(PROC_FECHA_VIGENCIA)');
        $maxDate = Util::dateFormat($maxDate, 'd/m/Y');

        $rvp = 'SI';

        $mensaje = '<div class="panel panel-primary"><div class="panel-heading">Reporte de Resultados</div><div class="panel-body">Seleccione un rango de fechas de vigencia para obtener el reporte de resultados.</div></div>';

        return view('reporte.resultado', compact('tabla', 'mensaje', 'maxDate', 'rvp'));  
    }

    /**
     * Obtiene los datos de resultados entre las fechas especificadas y las vuelca a la vista
     *
     * @return Response
     */
    public function getResultados(Request $request)
    {

        ini_set('memory_limit', '128M');

        $formData = array(
        	'fecha_vigencia_desde'=>$request['fecha_vigencia_desde'], 
        	'fecha_vigencia_hasta'=>$request['fecha_vigencia_hasta']
        );

        /* *** VALIDACIÓN *** */
        $validator = Validator::make($formData,[
            'fecha_vigencia_desde'           => 'required|max:10|min:10',
            'fecha_vigencia_hasta'           => 'required|max:10|min:10',
        ]);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $fecha_vigencia_desde = $formData['fecha_vigencia_desde'];
        $fecha_vigencia_hasta = $formData['fecha_vigencia_hasta'];

        $fecha_vigencia_desde = str_replace('/', '-', $fecha_vigencia_desde);
        $fecha_vigencia_hasta = str_replace('/', '-', $fecha_vigencia_hasta);

        // Convertir las fechas al formato de bbdd
        $fecha_vigencia_hasta = Util::dateFormat($fecha_vigencia_hasta, 'Y-m-d');
        $fecha_vigencia_desde = Util::dateFormat($fecha_vigencia_desde, 'Y-m-d');

        // Convertir las fechas al periodo de vigencia de datos historicos
        $fecha_vigencia_hasta_new = date('Y-m-d',strtotime('-4 days', strtotime($fecha_vigencia_hasta)));
        $fecha_vigencia_desde_new = date('Y-m-d',strtotime('-10 days', strtotime($fecha_vigencia_desde)));
				
        
        if ($request['tipo_crudo'] == 'on') {
            $tipo_crudo = 'BRENT';
        }
        else {
            $tipo_crudo = 'WTI';
        }

        $tabla = array();

        // Consulta
		/*
        $resultado = DB::table('MEPCO_PROCESO AS P')
                    ->join('MEPCO_ESCENARIO AS E', 'P.PROC_ID', '=', 'E.PROC_ID')
                    ->join('MEPCO_ESCENARIO_CALCULO AS EC', 'EC.ESCE_ID', '=', 'E.ESCE_ID')
                    ->join('MEPCO_HIDROCARBUROS AS H', 'EC.HIDR_ID', '=', 'H.HIDR_ID')
                    ->select('P.PROC_FECHA_VIGENCIA', 'H.HIDR_NOMBRE_CHILE', 'EC.ESCA_CORRECCION_RVP', 'EC.ESCA_N', 'EC.ESCA_M', 'EC.ESCA_S', 'EC.ESCA_T', 'EC.ESCA_F', 'EC.ESCA_PARIDAD', 'EC.ESCA_PREFINF', 'EC.ESCA_PREFINT', 'EC.ESCA_PREFSUP')
                    ->whereBetween('P.PROC_FECHA_VIGENCIA', [$fecha_vigencia_desde, $fecha_vigencia_hasta])
                    ->where('P.PROC_ELIMINADO', 0)
                    ->where('P.PRES_ID', Util::PROCESO_ESTADO_FINALIZADO)
                    ->where('EC.ESCA_PUBLICADO', 1)
                    ->where('E.ESCE_ELIMINADO', 0)
                    ->where('EC.ESCA_TIPO_CRUDO', $tipo_crudo)
                    ->orderBy('H.HIDR_ID')
                    ->get();
		*/
		$resultadoGAS_87 = DB::table('MEPCO_DATOS_HISTORICOS AS DH')
                    ->select(DB::raw('DATE_ADD(DH.DAHI_FECHA, INTERVAL 10 DAY) AS PROC_FECHA_VIGENCIA'), DB::raw('\'Gasolina Automotriz 93 Octanos\' AS HIDR_NOMBRE_CHILE'),DB::raw('\'1\' AS HIDR_ID'), 'DH.DAHI_CORRECCION_RVP AS ESCA_CORRECCION_RVP', 'DH.DAHI_N_GAS_87 AS ESCA_N', 'DH.DAHI_M_GAS_87 AS ESCA_M', 'DH.DAHI_S_GAS_87 AS ESCA_S', 'DH.DAHI_T_GAS_87 AS ESCA_T', 'DH.DAHI_F_GAS_87 AS ESCA_F', 'DH.DAHI_PARIDAD_GAS_87 AS ESCA_PARIDAD', DB::raw('(DH.DAHI_PROM_REFERENCIA_SEMANAL_GAS_87*0.95) AS ESCA_PREFINF'), 'DH.DAHI_PROM_REFERENCIA_SEMANAL_GAS_87 AS ESCA_PREFINT', DB::raw('(DH.DAHI_PROM_REFERENCIA_SEMANAL_GAS_87*1.05) AS ESCA_PREFSUP'))
                    ->whereBetween('DH.DAHI_FECHA', [$fecha_vigencia_desde_new, $fecha_vigencia_hasta_new])
                    ->where('DH.DAHI_TIPO_CRUDO', $tipo_crudo)
                    ->groupBy('ESCA_N', 'ESCA_M', 'ESCA_S', 'ESCA_T', 'ESCA_F', 'ESCA_PARIDAD', 'ESCA_PREFINT');
		$resultadoGAS_93 = DB::table('MEPCO_DATOS_HISTORICOS AS DH')
                    ->select(DB::raw('DATE_ADD(DH.DAHI_FECHA, INTERVAL 10 DAY) AS PROC_FECHA_VIGENCIA'),DB::raw('\'Gasolina Automotriz 97 Octanos\' AS HIDR_NOMBRE_CHILE'),DB::raw('\'2\' AS HIDR_ID'), 'DH.DAHI_CORRECCION_RVP AS ESCA_CORRECCION_RVP', 'DH.DAHI_N_GAS_93 AS ESCA_N', 'DH.DAHI_M_GAS_93 AS ESCA_M', 'DH.DAHI_S_GAS_93 AS ESCA_S', 'DH.DAHI_T_GAS_93 AS ESCA_T', 'DH.DAHI_F_GAS_93 AS ESCA_F', 'DH.DAHI_PARIDAD_GAS_93 AS ESCA_PARIDAD', DB::raw('(DH.DAHI_PROM_REFERENCIA_SEMANAL_GAS_93*0.95) AS ESCA_PREFINF'), 'DH.DAHI_PROM_REFERENCIA_SEMANAL_GAS_93 AS ESCA_PREFINT', DB::raw('(DH.DAHI_PROM_REFERENCIA_SEMANAL_GAS_93*1.05) AS ESCA_PREFSUP'))
                    ->whereBetween('DH.DAHI_FECHA', [$fecha_vigencia_desde_new, $fecha_vigencia_hasta_new])
                    ->where('DH.DAHI_TIPO_CRUDO', $tipo_crudo)
                    ->groupBy('ESCA_N', 'ESCA_M', 'ESCA_S', 'ESCA_T', 'ESCA_F', 'ESCA_PARIDAD', 'ESCA_PREFINT');
		$resultadoDIESEL = DB::table('MEPCO_DATOS_HISTORICOS AS DH')
                    ->select(DB::raw('DATE_ADD(DH.DAHI_FECHA, INTERVAL 10 DAY) AS PROC_FECHA_VIGENCIA'),DB::raw('\'Petróleo Diésel\' AS HIDR_NOMBRE_CHILE'),DB::raw('\'3\' AS HIDR_ID'), 'DH.DAHI_CORRECCION_RVP AS ESCA_CORRECCION_RVP', 'DH.DAHI_N_DIESEL AS ESCA_N', 'DH.DAHI_M_DIESEL AS ESCA_M', 'DH.DAHI_S_DIESEL AS ESCA_S', 'DH.DAHI_T_DIESEL AS ESCA_T', 'DH.DAHI_F_DIESEL AS ESCA_F', 'DH.DAHI_PARIDAD_DIESEL AS ESCA_PARIDAD', DB::raw('(DH.DAHI_PROM_REFERENCIA_SEMANAL_DIESEL*0.95) AS ESCA_PREFINF'), 'DH.DAHI_PROM_REFERENCIA_SEMANAL_DIESEL AS ESCA_PREFINT', DB::raw('(DH.DAHI_PROM_REFERENCIA_SEMANAL_DIESEL*1.05) AS ESCA_PREFSUP'))
                    ->whereBetween('DH.DAHI_FECHA', [$fecha_vigencia_desde_new, $fecha_vigencia_hasta_new])
                    ->where('DH.DAHI_TIPO_CRUDO', $tipo_crudo)
                    ->groupBy('ESCA_N', 'ESCA_M', 'ESCA_S', 'ESCA_T', 'ESCA_F', 'ESCA_PARIDAD', 'ESCA_PREFINT');
		$resultado = DB::table('MEPCO_DATOS_HISTORICOS AS DH')
                    ->select(DB::raw('DATE_ADD(DH.DAHI_FECHA, INTERVAL 10 DAY) AS PROC_FECHA_VIGENCIA'),DB::raw('\'Gas Licuado de Petróleo\' AS HIDR_NOMBRE_CHILE'),DB::raw('\'4\' AS HIDR_ID'), 'DH.DAHI_CORRECCION_RVP AS ESCA_CORRECCION_RVP', 'DH.DAHI_N_GLP AS ESCA_N', 'DH.DAHI_M_GLP AS ESCA_M', 'DH.DAHI_S_GLP AS ESCA_S', 'DH.DAHI_T_GLP AS ESCA_T', 'DH.DAHI_F_GLP AS ESCA_F', 'DH.DAHI_PARIDAD_GLP AS ESCA_PARIDAD', DB::raw('(DH.DAHI_PROM_REFERENCIA_SEMANAL_GLP*0.95) AS ESCA_PREFINF'), 'DH.DAHI_PROM_REFERENCIA_SEMANAL_GLP AS ESCA_PREFINT', DB::raw('(DH.DAHI_PROM_REFERENCIA_SEMANAL_GLP*1.05) AS ESCA_PREFSUP'))
                    ->whereBetween('DH.DAHI_FECHA', [$fecha_vigencia_desde_new, $fecha_vigencia_hasta_new])
                    ->where('DH.DAHI_TIPO_CRUDO', $tipo_crudo)
                    ->groupBy('ESCA_N', 'ESCA_M', 'ESCA_S', 'ESCA_T', 'ESCA_F', 'ESCA_PARIDAD', 'ESCA_PREFINT')
					->union($resultadoGAS_87)
					->union($resultadoGAS_93)
					->union($resultadoDIESEL)
                    ->orderBy('HIDR_ID')
                    ->orderBy('PROC_FECHA_VIGENCIA','ASC')
                    ->get();

        $rvp = null;

        foreach ($resultado as $row) {
            $fila_log = array();
            // Restar 7 días a la fecha de vigencia para obtener la anterior
            $fecha_anterior =  date('Y-m-d', strtotime($row->PROC_FECHA_VIGENCIA. '- 7 days'));
			
			// Restar 7 días a la fecha anterior para obtener la anterior new
        	$fecha_anterior_new = date('Y-m-d',strtotime('-7 days', strtotime($fecha_anterior)));
            
            // Obtener el precio de referencia y de paridad de la fecha anterior
            /*
			$anteriores = DB::table('MEPCO_PROCESO AS P')
                    ->join('MEPCO_ESCENARIO AS E', 'P.PROC_ID', '=', 'E.PROC_ID')
                    ->join('MEPCO_ESCENARIO_CALCULO AS EC', 'EC.ESCE_ID', '=', 'E.ESCE_ID')
                    ->join('MEPCO_HIDROCARBUROS AS H', 'EC.HIDR_ID', '=', 'H.HIDR_ID')
                    ->select('EC.ESCA_PARIDAD', 'EC.ESCA_PREFINT')
                    ->where('P.PROC_FECHA_VIGENCIA', $fecha_anterior)
                    ->where('P.PROC_ELIMINADO', 0)
                    ->where('P.PRES_ID', Util::PROCESO_ESTADO_FINALIZADO)
                    ->where('EC.ESCA_PUBLICADO', 1)
                    ->where('E.ESCE_ELIMINADO', 0)
                    ->where('H.HIDR_NOMBRE_CHILE', $row->HIDR_NOMBRE_CHILE)
                    ->where('EC.ESCA_TIPO_CRUDO', $tipo_crudo)
                    ->first();
			*/
			
			$anterioresGAS_87 = DB::table('MEPCO_DATOS_HISTORICOS AS DH')
                    ->select('DH.DAHI_PARIDAD_GAS_87 AS ESCA_PARIDAD', 'DH.DAHI_PROM_REFERENCIA_SEMANAL_GAS_87 AS ESCA_PREFINT')
                    ->where('DH.DAHI_FECHA', $fecha_anterior_new)
                    ->where(DB::raw('\'Gasolina Automotriz 93 Octanos\''), $row->HIDR_NOMBRE_CHILE)
                    ->where('DH.DAHI_TIPO_CRUDO', $tipo_crudo);
			$anterioresGAS_93 = DB::table('MEPCO_DATOS_HISTORICOS AS DH')
                    ->select('DH.DAHI_PARIDAD_GAS_93 AS ESCA_PARIDAD', 'DH.DAHI_PROM_REFERENCIA_SEMANAL_GAS_93 AS ESCA_PREFINT')
                    ->where('DH.DAHI_FECHA', $fecha_anterior_new)
                    ->where(DB::raw('\'Gasolina Automotriz 97 Octanos\''), $row->HIDR_NOMBRE_CHILE)
                    ->where('DH.DAHI_TIPO_CRUDO', $tipo_crudo);
			$anterioresDIESEL = DB::table('MEPCO_DATOS_HISTORICOS AS DH')
                    ->select('DH.DAHI_PARIDAD_DIESEL AS ESCA_PARIDAD', 'DH.DAHI_PROM_REFERENCIA_SEMANAL_DIESEL AS ESCA_PREFINT')
                    ->where('DH.DAHI_FECHA', $fecha_anterior_new)
                    ->where(DB::raw('\'Petróleo Diésel\''), $row->HIDR_NOMBRE_CHILE)
                    ->where('DH.DAHI_TIPO_CRUDO', $tipo_crudo);
			$anteriores = DB::table('MEPCO_DATOS_HISTORICOS AS DH')
                    ->select('DH.DAHI_PARIDAD_GLP AS ESCA_PARIDAD', 'DH.DAHI_PROM_REFERENCIA_SEMANAL_GLP AS ESCA_PREFINT')
                    ->where('DH.DAHI_FECHA', $fecha_anterior_new)
                    ->where(DB::raw('\'Gas Licuado de Petróleo\''), $row->HIDR_NOMBRE_CHILE)
                    ->where('DH.DAHI_TIPO_CRUDO', $tipo_crudo)
					->union($anterioresGAS_87)
					->union($anterioresGAS_93)
					->union($anterioresDIESEL)
                    ->first();
			

            if ($anteriores) {
                $fila_log['PARIDAD_ANTERIOR']   = Util::formatNumber($anteriores->ESCA_PARIDAD);
                $fila_log['PRI_ANTERIOR']       = Util::formatNumber($anteriores->ESCA_PREFINT);
            }
            else {
                $fila_log['PARIDAD_ANTERIOR']   = 'No disponible';
                $fila_log['PRI_ANTERIOR']       = 'No disponible';
            }
            $rvp = $row->ESCA_CORRECCION_RVP;
            $fila_log['COMBUSTIBLE']        = $row->HIDR_NOMBRE_CHILE;
            $fila_log['N']                  = $row->ESCA_N;
            $fila_log['M']                  = $row->ESCA_M;
            $fila_log['S']                  = $row->ESCA_S;
            $fila_log['T']                  = $row->ESCA_T;
            $fila_log['PESO_FUT']           = Util::formatNumber($row->ESCA_F);
            $fila_log['PRINF']              = Util::formatNumber($row->ESCA_PREFINF);
            $fila_log['PRI']                = Util::formatNumber($row->ESCA_PREFINT);
            $fila_log['PRS']                = Util::formatNumber($row->ESCA_PREFSUP);
            $fila_log['PARIDAD']            = Util::formatNumber($row->ESCA_PARIDAD);
            $tabla[Util::dateFormat($row->PROC_FECHA_VIGENCIA, 'd/m/Y')][] = $fila_log;
        }

        $maxDate = DB::table('MEPCO_PROCESO')
                    ->select(DB::raw('MAX(PROC_FECHA_VIGENCIA)'))
                    ->pluck('MAX(PROC_FECHA_VIGENCIA)');
        $maxDate = Util::dateFormat($maxDate, 'd/m/Y');

        $mensaje = '';
        if (count($tabla)==0) {
            $mensaje = '<div class="panel panel-primary"><div class="panel-heading">Reporte de Resultados</div><div class="panel-body">No existe información para la fecha seleccionada.</div></div>';
        }

        $fechas['fecha_vigencia_desde'] = $request['fecha_vigencia_desde'];
        $fechas['fecha_vigencia_hasta'] = $request['fecha_vigencia_hasta'];
        $rvp = $rvp == '1' ? 'SI' : 'NO';

        return view('reporte.resultado', compact('tabla', 'mensaje', 'maxDate', 'fechas', 'tipo_crudo', 'rvp'));
    }

    public function datosHistoricos()
    {

        $tabla = array();
        $crudos = array();
        $tipo_crudo = 'BRENT';
        $maxDate = DB::table('MEPCO_PROCESO')
                    ->select(DB::raw('MAX(PROC_FECHA_VIGENCIA)'))
                    ->pluck('MAX(PROC_FECHA_VIGENCIA)');
        $maxDate = Util::dateFormat($maxDate, 'd/m/Y');

        $mensaje = '<div class="panel panel-primary"><div class="panel-heading">Datos Históricos</div><div class="panel-body">Seleccione un rango de fechas de vigencia para obtener el reporte de datos históricos.</div></div>';

        return view('reporte.datos-historicos', compact('tabla', 'crudos', 'mensaje', 'maxDate', 'tipo_crudo'));   
    }

    /**
     * Obtiene los datos históricos entre las fechas especificadas y las vuelca a la vista
     *
     * @return Response
     */
    public function getDatosHistoricos(Request $request)
    {

        $formData = array(
        	'fecha_vigencia_desde'=>$request['fecha_vigencia_desde'], 
        	'fecha_vigencia_hasta'=>$request['fecha_vigencia_hasta']
        );

        /* *** VALIDACIÓN *** */
        $validator = Validator::make($formData,[
            'fecha_vigencia_desde'           => 'required|max:10|min:10',
            'fecha_vigencia_hasta'           => 'required|max:10|min:10',
        ]);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $fecha_vigencia_desde = $formData['fecha_vigencia_desde'];
        $fecha_vigencia_hasta = $formData['fecha_vigencia_hasta'];

        $fecha_vigencia_desde = str_replace('/', '-', $fecha_vigencia_desde);
        $fecha_vigencia_hasta = str_replace('/', '-', $fecha_vigencia_hasta);

        // Convertir las fechas al formato de bbdd
        $fecha_vigencia_hasta = Util::dateFormat($fecha_vigencia_hasta, 'Y-m-d');
        $fecha_vigencia_desde = Util::dateFormat($fecha_vigencia_desde, 'Y-m-d');

        if ($request['tipo_crudo'] == 'on') {
            $tipo_crudo = 'BRENT';
        }
        else {
            $tipo_crudo = 'WTI';
        }

        $tabla = array();

        // Consulta
        $resultado = DB::table('MEPCO_PROCESO AS P')
                    ->join('MEPCO_ESCENARIO AS E', 'P.PROC_ID', '=', 'E.PROC_ID')
                    ->join('MEPCO_ESCENARIO_PARIDAD AS ESPA', 'E.ESCE_ID', '=', 'ESPA.ESCE_ID')
                    ->join('MEPCO_ESCENARIO_CALCULO AS ESCA', 'E.ESCE_ID', '=', 'ESCA.ESCE_ID')
                    ->join('MEPCO_HIDROCARBUROS AS H', 'ESPA.HIDR_ID', '=', 'H.HIDR_ID')
                    ->select( DB::Raw('DISTINCT(ESPA.ESPA_ID)'), 'P.PROC_FECHA_VIGENCIA', 'H.HIDR_ID', 'H.HIDR_NOMBRE_CHILE', 'ESPA_FECHA', 'ESPA.ESPA_FOB', 'ESPA.ESPA_FM', 'ESPA.ESPA_SM', 'ESPA.ESPA_CIF', 'ESPA.ESPA_GCC', 'ESPA.ESPA_MERM', 'ESPA.ESPA_DA', 'ESPA.ESPA_PPIBL', 'ESPA.ESPA_IVA', 'ESPA.ESPA_CF', 'ESPA.ESPA_CD', 'ESPA.ESPA_CA', 'ESPA.ESPA_PARIDAD_CA')
                    ->whereBetween('P.PROC_FECHA_VIGENCIA', [$fecha_vigencia_desde, $fecha_vigencia_hasta])
                    ->where('P.PROC_ELIMINADO', 0)
                    ->where('ESCA.ESCA_PUBLICADO', 1)
                    ->where('P.PRES_ID', Util::PROCESO_ESTADO_FINALIZADO)
                    ->where('ESPA.HIDR_ID', '!=', Util::COMBUSTIBLE_GLP)
                    ->where('ESCA.ESCA_TIPO_CRUDO', $tipo_crudo)
                    ->orderBy('P.PROC_FECHA_VIGENCIA')
                    ->orderBy('ESPA_FECHA')
                    ->get();

        $resultadoGLP = DB::table('MEPCO_PROCESO AS P')
                    ->join('MEPCO_ESCENARIO AS E', 'P.PROC_ID', '=', 'E.PROC_ID')
                    ->join('MEPCO_ESCENARIO_PARIDAD AS ESPA', 'E.ESCE_ID', '=', 'ESPA.ESCE_ID')
                    ->join('MEPCO_ESCENARIO_CALCULO AS ESCA', 'E.ESCE_ID', '=', 'ESCA.ESCE_ID')
                    ->join('MEPCO_HIDROCARBUROS AS H', 'ESPA.HIDR_ID', '=', 'H.HIDR_ID')
                    ->select(DB::Raw('DISTINCT(ESPA.ESPA_ID)'), 'P.PROC_FECHA_VIGENCIA', 'H.HIDR_ID', 'H.HIDR_NOMBRE_CHILE', 'ESPA_FECHA', 'ESPA.ESPA_FOB', 'ESPA.ESPA_FM', 'ESPA.ESPA_SM', 'ESPA.ESPA_CIF', 'ESPA.ESPA_GCC', 'ESPA.ESPA_MERM', 'ESPA.ESPA_DA', 'ESPA.ESPA_PPIBL', 'ESPA.ESPA_IVA', 'ESPA.ESPA_CF', 'ESPA.ESPA_CD', 'ESPA.ESPA_CA', 'ESPA.ESPA_PARIDAD_CA', 'ESPA.ESPA_FLETE', 'ESPA.ESPA_ARBITRAJE', 'ESPA.ESPA_NETBACK_US_TON', 'ESPA.ESPA_NETBACK_US_GAL', 'ESPA.ESPA_FOB_GLP_ARBITRADO')
                    ->whereBetween('P.PROC_FECHA_VIGENCIA', [$fecha_vigencia_desde, $fecha_vigencia_hasta])
                    ->where('P.PROC_ELIMINADO', 0)
                    ->where('ESCA.ESCA_PUBLICADO', 1)
                    ->where('P.PRES_ID', Util::PROCESO_ESTADO_FINALIZADO)
                    ->where('ESPA.HIDR_ID', Util::COMBUSTIBLE_GLP)
                    ->where('ESCA.ESCA_TIPO_CRUDO', $tipo_crudo)
                    ->orderBy('P.PROC_FECHA_VIGENCIA')
                    ->orderBy('ESPA_FECHA')
                    ->get();
        
        foreach ($resultado as $row) {
            $fecha_vigencia         = Util::dateFormat($row->PROC_FECHA_VIGENCIA, 'd/m/Y');
            $row->ESPA_FOB          = Util::formatNumber($row->ESPA_FOB);
            $row->ESPA_FM           = Util::formatNumber($row->ESPA_FM);
            $row->ESPA_SM           = Util::formatNumber($row->ESPA_SM);
            $row->ESPA_CIF          = Util::formatNumber($row->ESPA_CIF);
            $row->ESPA_GCC          = Util::formatNumber($row->ESPA_GCC);
            $row->ESPA_MERM         = Util::formatNumber($row->ESPA_MERM);
            $row->ESPA_DA           = Util::formatNumber($row->ESPA_DA);
            $row->ESPA_PPIBL        = Util::formatNumber($row->ESPA_PPIBL);
            $row->ESPA_IVA          = Util::formatNumber($row->ESPA_IVA);
            $row->ESPA_CF           = Util::formatNumber($row->ESPA_CF);
            $row->ESPA_CD           = Util::formatNumber($row->ESPA_CD);
            $row->ESPA_CA           = Util::formatNumber($row->ESPA_CA);
            $row->ESPA_PARIDAD_CA   = Util::formatNumber($row->ESPA_PARIDAD_CA);
            //$tabla[$fecha_vigencia][$row->HIDR_ID][] = $row;
            $tabla[$row->HIDR_ID][$fecha_vigencia][] = $row;
        }

        foreach ($resultadoGLP as $row) {
            $fecha_vigencia         = Util::dateFormat($row->PROC_FECHA_VIGENCIA, 'd/m/Y');
            $row->ESPA_FOB          = Util::formatNumber($row->ESPA_FOB);
            $row->ESPA_FM           = Util::formatNumber($row->ESPA_FM);
            $row->ESPA_SM           = Util::formatNumber($row->ESPA_SM);
            $row->ESPA_CIF          = Util::formatNumber($row->ESPA_CIF);
            $row->ESPA_GCC          = Util::formatNumber($row->ESPA_GCC);
            $row->ESPA_MERM         = Util::formatNumber($row->ESPA_MERM);
            $row->ESPA_DA           = Util::formatNumber($row->ESPA_DA);
            $row->ESPA_PPIBL        = Util::formatNumber($row->ESPA_PPIBL);
            $row->ESPA_IVA          = Util::formatNumber($row->ESPA_IVA);
            $row->ESPA_CF           = Util::formatNumber($row->ESPA_CF);
            $row->ESPA_CD           = Util::formatNumber($row->ESPA_CD);
            $row->ESPA_CA           = Util::formatNumber($row->ESPA_CA);
            $row->ESPA_PARIDAD_CA   = Util::formatNumber($row->ESPA_PARIDAD_CA);
            $row->ESPA_FLETE        = Util::formatNumber($row->ESPA_FLETE);
            $row->ESPA_ARBITRAJE    = Util::formatNumber($row->ESPA_ARBITRAJE);
            $row->ESPA_NETBACK_US_TON        = Util::formatNumber($row->ESPA_NETBACK_US_TON);
            $row->ESPA_NETBACK_US_GAL        = Util::formatNumber($row->ESPA_NETBACK_US_GAL);
            $row->ESPA_FOB_GLP_ARBITRADO     = Util::formatNumber($row->ESPA_FOB_GLP_ARBITRADO);
            //$tabla[$row->HIDR_ID][$fecha_vigencia][$row->HIDR_ID][] = $row;
            $tabla[$row->HIDR_ID][$fecha_vigencia][] = $row;
        }
        
        $crudos = array();
        //foreach ($tabla as $fecha_vigencia=>$combustibles) {
        foreach ($tabla as $combustible=>$fechas_vigencia) {
            //$crudos[$fecha_vigencia] = array();
            //foreach ($combustibles as $hidr_id=>$combustible) {
            foreach ($fechas_vigencia as $fecha_vigencia=>$val) {
                if ($tipo_crudo == 'BRENT') {
                    switch ($combustible) {
                    //switch ($hidr_id) {
                        case Util::COMBUSTIBLE_93:
                            $historico  = 'DAHI_BRENT_PROM_HISTORICO_GAS_87';
                            $futuro     = 'DAHI_BRENT_PROM_FUTUROS_GAS_87';
                            $ponderado  = 'DAHI_BRENT_PROM_PONDERADO_GAS_87';
                            break;
                        case Util::COMBUSTIBLE_97:
                            $historico  = 'DAHI_BRENT_PROM_HISTORICO_GAS_93';
                            $futuro     = 'DAHI_BRENT_PROM_FUTUROS_GAS_93';
                            $ponderado  = 'DAHI_BRENT_PROM_PONDERADO_GAS_93';
                            break;
                        case Util::COMBUSTIBLE_DIESEL:
                            $historico  = 'DAHI_BRENT_PROM_HISTORICO_DIESEL';
                            $futuro     = 'DAHI_BRENT_PROM_FUTUROS_DIESEL';
                            $ponderado  = 'DAHI_BRENT_PROM_PONDERADO_DIESEL';
                            break;
                        case Util::COMBUSTIBLE_GLP:
                            $historico  = 'DAHI_BRENT_PROM_HISTORICO_GLP';
                            $futuro     = 'DAHI_BRENT_PROM_FUTUROS_GLP';
                            $ponderado  = 'DAHI_BRENT_PROM_PONDERADO_GLP';
                            break;
                    }
                    $crudo = ['Brent histórico', 'Brent futuro', 'Brent promedio ponderado'];
                }
                else {
                    //switch ($hidr_id) {
                    switch ($combustible) {
                        case Util::COMBUSTIBLE_93:
                            $historico  = 'DAHI_WTI_PROM_HISTORICO_GAS_87';
                            $futuro     = 'DAHI_WTI_PROM_FUTUROS_GAS_87';
                            $ponderado  = 'DAHI_WTI_PROM_PONDERADO_GAS_87';
                            break;
                        case Util::COMBUSTIBLE_97:
                            $historico  = 'DAHI_WTI_PROM_HISTORICO_GAS_93';
                            $futuro     = 'DAHI_WTI_PROM_FUTUROS_GAS_93';
                            $ponderado  = 'DAHI_WTI_PROM_PONDERADO_GAS_93';
                            break;
                        case Util::COMBUSTIBLE_DIESEL:
                            $historico  = 'DAHI_WTI_PROM_HISTORICO_DIESEL';
                            $futuro     = 'DAHI_WTI_PROM_FUTUROS_DIESEL';
                            $ponderado  = 'DAHI_WTI_PROM_PONDERADO_DIESEL';
                            break;
                        case Util::COMBUSTIBLE_GLP:
                            $historico  = 'DAHI_WTI_PROM_HISTORICO_GLP';
                            $futuro     = 'DAHI_WTI_PROM_FUTUROS_GLP';
                            $ponderado  = 'DAHI_WTI_PROM_PONDERADO_GLP';
                            break;
                    }
                    $crudo = ['WTI histórico', 'WTI futuro', 'WTI promedio ponderado'];
                }

				$fecha_vigencia_new = str_replace('/', '-', $fecha_vigencia);
				$fecha = date('Y-m-d',strtotime('-7 days', strtotime($fecha_vigencia_new)));

                // Buscar los datos en la base de datos
                $datos_crudo = DB::table('MEPCO_DATOS_HISTORICOS')
                            ->select($historico.' AS HISTORICO', $futuro.' AS FUTURO', $ponderado.' AS PONDERADO')
                            ->where('DAHI_FECHA', $fecha)
                            ->first();

                $datos_crudo = (array)$datos_crudo;
                if ($datos_crudo) {
                    foreach($datos_crudo as $key=>$dato) {
                        $datos_crudo[$key] = Util::formatNumber($dato);
                    }
                }
                else {
                    $datos_crudo = array('HISTORICO'=>'No disponible', 'FUTURO'=>'No disponible', 'PONDERADO'=>'No disponible');
                }
                $crudos[$fecha_vigencia][$combustible] = array('CRUDO'=>$crudo, 'DATOS_CRUDO'=>$datos_crudo);
                //$crudos[$fecha_vigencia][$hidr_id] = array('CRUDO'=>$crudo, 'DATOS_CRUDO'=>$datos_crudo);
            }
            //$tabla[$fecha_vigencia] = $combustibles;
            $tabla[$combustible] = $fechas_vigencia;
        }

        $mensaje = '';

        $maxDate = DB::table('MEPCO_PROCESO')
                    ->select(DB::raw('MAX(PROC_FECHA_VIGENCIA)'))
                    ->pluck('MAX(PROC_FECHA_VIGENCIA)');
        $maxDate = Util::dateFormat($maxDate, 'd/m/Y');

        if (count($tabla)==0) {
            $mensaje = '<div class="panel panel-primary"><div class="panel-heading">Datos Históricos</div><div class="panel-body">No existe información para el rango de fechas seleccionado.</div></div>';
        }

        $fechas['fecha_vigencia_desde'] = $request['fecha_vigencia_desde'];
        $fechas['fecha_vigencia_hasta'] = $request['fecha_vigencia_hasta'];

        return view('reporte.datos-historicos', compact('tabla', 'crudos', 'mensaje', 'maxDate', 'fechas', 'tipo_crudo'));
    }

    public function datosEntrada()
    {
        $tabla = array();
        $maxDate = DB::table('MEPCO_PROCESO')
                    ->select(DB::raw('MAX(PROC_FECHA_VIGENCIA)'))
                    ->pluck('MAX(PROC_FECHA_VIGENCIA)');
        $maxDate = Util::dateFormat($maxDate, 'd/m/Y');

        $mensaje = '<div class="panel panel-primary"><div class="panel-heading">Datos de Entrada</div><div class="panel-body">Seleccione una fecha de vigencia para obtener el reporte de datos de entrada.</div></div>';

        return view('reporte.datos-entrada', compact('tabla', 'mensaje', 'maxDate'));
    }

    /**
     * Obtiene los datos de entrada entre las fechas especificadas y las vuelca a la vista
     *
     * @return Response
     */
    public function getDatosEntrada(Request $request)
    {
        $formData = array(
        	'fecha_vigencia_desde'=>$request['fecha_vigencia_desde'], 
        	'fecha_vigencia_hasta'=>$request['fecha_vigencia_hasta']
        );

        /* *** VALIDACIÓN *** */
        $validator = Validator::make($formData,[
            'fecha_vigencia_desde'           => 'required|max:10|min:10',
            'fecha_vigencia_hasta'           => 'required|max:10|min:10',
        ]);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $fecha_vigencia_desde = $formData['fecha_vigencia_desde'];
        $fecha_vigencia_hasta = $formData['fecha_vigencia_hasta'];

        $fecha_vigencia_desde = str_replace('/', '-', $fecha_vigencia_desde);
        $fecha_vigencia_hasta = str_replace('/', '-', $fecha_vigencia_hasta);

        // Convertir las fechas al formato de bbdd
        $fecha_vigencia_hasta = Util::dateFormat($fecha_vigencia_hasta, 'Y-m-d');
        $fecha_vigencia_desde = Util::dateFormat($fecha_vigencia_desde, 'Y-m-d');

        $tabla = array();

        while (strtotime($fecha_vigencia_desde) <= strtotime($fecha_vigencia_hasta)) {
            $fecha_vigencia = $fecha_vigencia_desde;
            $lunes   = date('Y-m-d', strtotime('last Monday - 1 week'.$fecha_vigencia));
            $viernes = date('Y-m-d', strtotime('last Friday'.$fecha_vigencia));
            // Obtener datos historicos
            $tabla[Util::dateFormat($fecha_vigencia_desde, 'd/m/Y')] = DatosHistoricos::whereBetween('DAHI_FECHA', [$lunes, $viernes])->get();
            $fecha_vigencia_desde =  date('Y-m-d', strtotime('next Thursday '.$fecha_vigencia_desde));
        }

        $mensaje = '';

        $maxDate = DB::table('MEPCO_PROCESO')
                    ->select(DB::raw('MAX(PROC_FECHA_VIGENCIA)'))
                    ->pluck('MAX(PROC_FECHA_VIGENCIA)');
        $maxDate = Util::dateFormat($maxDate, 'd/m/Y');

        if (count($tabla)==0) {
            $mensaje = '<div class="panel panel-primary"><div class="panel-heading">Datos de Entrada</div><div class="panel-body">No existe información para la fecha seleccionada.</div></div>';
        }

        $fechas['fecha_vigencia_desde'] = $request['fecha_vigencia_desde'];
        $fechas['fecha_vigencia_hasta'] = $request['fecha_vigencia_hasta'];

        return view('reporte.datos-entrada', compact('tabla', 'mensaje', 'maxDate', 'fechas'));
    }

    public function log()
    {
        $tabla = array();

        $maxDate = DB::table('MEPCO_PROCESO')
                    ->select(DB::raw('MAX(PROC_FECHA_VIGENCIA)'))
                    ->pluck('MAX(PROC_FECHA_VIGENCIA)');
        $maxDate = Util::dateFormat($maxDate, 'd/m/Y');

        $mensaje = '<div class="panel panel-primary"><div class="panel-heading">Log</div><div class="panel-body">Seleccione un rango de fechas para obtener el reporte de Log.</div></div>';

        return view('reporte.log', compact('tabla', 'mensaje', 'maxDate'));
    }

    /**
     * Obtiene los datos de log entre las fechas especificadas y las vuelca a la vista
     *
     * @return Response
     */
    public function getLog(Request $request)
    {
        $formData = array(
        	'fecha_vigencia_desde'=>$request['fecha_vigencia_desde'], 
        	'fecha_vigencia_hasta'=>$request['fecha_vigencia_hasta']
        );

        /* *** VALIDACIÓN *** */
        $validator = Validator::make($formData,[
            'fecha_vigencia_desde'           => 'required|max:10|min:10',
            'fecha_vigencia_hasta'           => 'required|max:10|min:10',
        ]);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $fecha_vigencia_desde = $formData['fecha_vigencia_desde'];
        $fecha_vigencia_hasta = $formData['fecha_vigencia_hasta'];

        $fecha_vigencia_desde = str_replace('/', '-', $fecha_vigencia_desde);
        $fecha_vigencia_hasta = str_replace('/', '-', $fecha_vigencia_hasta);

        // Convertir las fechas al formato de bbdd
        $fecha_vigencia_hasta = Util::dateFormat($fecha_vigencia_hasta, 'Y-m-d');
        $fecha_vigencia_desde = Util::dateFormat($fecha_vigencia_desde, 'Y-m-d');

        $mensaje = '';
        
        // Consulta
        $resultadoCalculo = array();
        $resultadoVausCreacion = array();
        $resultadoVausValidado = array();

        $tabla = array();

        $resultadoCalculo = DB::table('MEPCO_PROCESO AS P')
                    ->join('MEPCO_ESCENARIO AS E', 'P.PROC_ID', '=', 'E.PROC_ID')
                    ->join('MEPCO_ESCENARIO_CALCULO AS C', 'E.ESCE_ID', '=', 'C.ESCE_ID')
                    ->leftJoin('MEPCO_USUA_PERF AS EU1', 'EU1.USPE_ID', '=', 'E.USPE_ID_CREACION')
                    ->leftJoin('MEPCO_USUA_PERF AS EU2', 'EU2.USPE_ID', '=', 'E.USPE_ID_VALIDADO')
                    ->select('P.PROC_FECHA_VIGENCIA', 'E.ESCE_FECHA_CREACION', 'EU1.USUA_EMAIL AS USUA_EMAIL_CREACION', 'E.ESCE_FECHA_VALIDADO', 'EU2.USUA_EMAIL AS USUA_EMAIL_VALIDADO')
                    ->whereBetween('P.PROC_FECHA_VIGENCIA', [$fecha_vigencia_desde, $fecha_vigencia_hasta])
                    ->where('P.PROC_ELIMINADO', 0)
                    ->where('C.ESCA_PUBLICADO', 1)
                    ->get();

        $resultadoVausCreacion = DB::table('MEPCO_PROCESO AS P')
                            ->join('MEPCO_VAME_USPE AS VU', 'VU.PROC_ID', '=', 'P.PROC_ID')
                            ->join('MEPCO_USUA_PERF AS UP', 'UP.USPE_ID', '=', 'VU.USPE_ID')
                            ->select('P.PROC_FECHA_VIGENCIA', 'UP.USUA_EMAIL', 'VU.VAUS_FECHA_CREACION')
                            ->whereBetween('P.PROC_FECHA_VIGENCIA', [$fecha_vigencia_desde, $fecha_vigencia_hasta])
                            ->where('P.PROC_ELIMINADO', 0)
                            ->whereNotNull('VU.VAUS_FECHA_CREACION')
                            ->get();

        $resultadoVausValidado = DB::table('MEPCO_PROCESO AS P')
                            ->join('MEPCO_VAME_USPE AS VU', 'VU.PROC_ID', '=', 'P.PROC_ID')
                            ->join('MEPCO_USUA_PERF AS UP', 'UP.USPE_ID', '=', 'VU.USPE_ID')
                            ->select('P.PROC_FECHA_VIGENCIA', 'UP.USUA_EMAIL', 'VU.VAUS_FECHA_VALIDADO')
                            ->whereBetween('P.PROC_FECHA_VIGENCIA', [$fecha_vigencia_desde, $fecha_vigencia_hasta])
                            ->where('P.PROC_ELIMINADO', 0)
                            ->whereNotNull('VU.VAUS_FECHA_VALIDADO')
                            ->get();

        foreach ($resultadoCalculo as $row) {
            $fecha_vigencia = Util::dateFormat($row->PROC_FECHA_VIGENCIA, 'd/m/Y');
            $tabla[$fecha_vigencia] = array();
            $tabla[$fecha_vigencia]['ESCE_FECHA_CREACION'] = $row->ESCE_FECHA_CREACION;
            $tabla[$fecha_vigencia]['USUA_EMAIL_CREACION'] = $row->USUA_EMAIL_CREACION;
            $tabla[$fecha_vigencia]['ESCE_FECHA_VALIDADO'] = $row->ESCE_FECHA_VALIDADO;
            $tabla[$fecha_vigencia]['USUA_EMAIL_VALIDADO'] = $row->USUA_EMAIL_VALIDADO;
            $tabla[$fecha_vigencia]['VAUS_FECHA_CREACION'] = '';
            $tabla[$fecha_vigencia]['VAUS_EMAIL_CREACION'] = '';
            $tabla[$fecha_vigencia]['VAUS_FECHA_VALIDADO'] = '';
            $tabla[$fecha_vigencia]['VAUS_EMAIL_VALIDADO'] = '';
        }

        foreach ($resultadoVausCreacion as $row) {
            $fecha_vigencia = Util::dateFormat($row->PROC_FECHA_VIGENCIA, 'd/m/Y');
            if (!isset($tabla[$fecha_vigencia])) {
                $tabla[$fecha_vigencia]['ESCE_FECHA_CREACION'] = '';
                $tabla[$fecha_vigencia]['USUA_EMAIL_CREACION'] = '';
                $tabla[$fecha_vigencia]['ESCE_FECHA_VALIDADO'] = '';
                $tabla[$fecha_vigencia]['USUA_EMAIL_VALIDADO'] = '';
                $tabla[$fecha_vigencia]['VAUS_FECHA_VALIDADO'] = '';
                $tabla[$fecha_vigencia]['VAUS_EMAIL_VALIDADO'] = '';
            }
            $tabla[$fecha_vigencia]['VAUS_FECHA_CREACION'] = $row->VAUS_FECHA_CREACION;
            $tabla[$fecha_vigencia]['VAUS_EMAIL_CREACION'] = $row->USUA_EMAIL;

        }

        foreach ($resultadoVausValidado as $row) {
            $fecha_vigencia = Util::dateFormat($row->PROC_FECHA_VIGENCIA, 'd/m/Y');
            if (!isset($tabla[$fecha_vigencia])) {
                $tabla[$fecha_vigencia]['ESCE_FECHA_CREACION'] = '';
                $tabla[$fecha_vigencia]['USUA_EMAIL_CREACION'] = '';
                $tabla[$fecha_vigencia]['ESCE_FECHA_VALIDADO'] = '';
                $tabla[$fecha_vigencia]['USUA_EMAIL_VALIDADO'] = '';
                $tabla[$fecha_vigencia]['VAUS_FECHA_CREACION'] = '';
                $tabla[$fecha_vigencia]['VAUS_EMAIL_CREACION'] = '';
            }
            $tabla[$fecha_vigencia]['VAUS_FECHA_VALIDADO'] = $row->VAUS_FECHA_VALIDADO;
            $tabla[$fecha_vigencia]['VAUS_EMAIL_VALIDADO'] = $row->USUA_EMAIL;        
        }

        if (count($tabla)==0) {
            $mensaje = '<div class="panel panel-primary"><div class="panel-heading">Log de acciones</div><div class="panel-body">No existe información para el rango de fechas seleccionado.</div></div>';
        }

        $maxDate = DB::table('MEPCO_PROCESO')
                    ->select(DB::raw('MAX(PROC_FECHA_VIGENCIA)'))
                    ->pluck('MAX(PROC_FECHA_VIGENCIA)');
        $maxDate = Util::dateFormat($maxDate, 'd/m/Y');

        $fechas['fecha_vigencia_desde'] = $request['fecha_vigencia_desde'];
        $fechas['fecha_vigencia_hasta'] = $request['fecha_vigencia_hasta'];

        return view('reporte.log', compact('tabla', 'mensaje', 'maxDate', 'fechas'));
    }   

    /* FUNCIONES PARA EXPORTAR A EXCEL O A PDF CADA REPORTE */
    public function exportarAExcel(Request $request)
    {
        // Obtener las variables necesarias
        $nombreReporte  = $request['nombreReporte'];
        $vista          = $request['vista'];
        $tabla          = json_decode($request['tabla']);

        Excel::create($nombreReporte, function($excel) use(&$nombreReporte, &$vista, &$tabla) {
            $excel->sheet($nombreReporte, function($sheet) use(&$nombreReporte, &$vista, &$tabla) {
                $sheet->setAutoSize(true);
                $sheet->loadView($vista, array('tabla' => $tabla));
            });
        })->export('xls');

    }

    public function exportarAExcelDatosHistoricos(Request $request)
    {
        // Obtener las variables necesarias
        $tabla          = json_decode($request['tabla']);
        $crudos         = json_decode($request['crudos']);

        $tablaExcel = array();
        /*foreach($tabla as $fecha_vigencia=>$combustibles){
            foreach ($combustibles as $hidr_id=>$combustible) {
                $tablaExcel[$hidr_id][$fecha_vigencia] = $combustible;
            }
        }*/

        foreach($tabla as $combustible=>$fechas_vigencia){
            foreach ($fechas_vigencia as $fecha_vigencia=>$val) {
                $tablaExcel[$combustible][$fecha_vigencia] = $val;
            }
        }

        Excel::create('Datos Históricos', function($excel) use(&$tablaExcel, &$crudos) {
            $nombreHoja = '';
            foreach($tablaExcel as $combustible=>$fecha_vigencia){
                if ($combustible == Util::COMBUSTIBLE_93) {
                    $nombreHoja = 'Gasolina 93';
                }
                else if ($combustible == Util::COMBUSTIBLE_97) {
                    $nombreHoja = 'Gasolina 97';
                }
                else if ($combustible == Util::COMBUSTIBLE_DIESEL) {
                    $nombreHoja = 'Diésel';
                }
                else {
                    $nombreHoja = 'GLP';
                }

                $excel->sheet($nombreHoja, function($sheet) use(&$fecha_vigencia, &$combustible, &$crudos) {
                    $sheet->setAutoSize(true);
                    $sheet->loadView('reporte.datosHistoricosTemplateExcel', array('tabla' => $fecha_vigencia, 'crudos' => $crudos, 'combustible' => $combustible));
                });
            }
        })->export('xls');

    }

    public function exportarAPdf(Request $request)
    {
        ini_set('memory_limit', '128M');

        try {
            $tabla    = json_decode($request['tabla']);
            $template = $request['template'];
            if ($request['crudos']) {
                $crudos = json_decode($request['crudos']);
                $pdf    = \PDF::loadView($template, compact('tabla', 'crudos'))->setOrientation('landscape')->setPaper('A3');
            }
            else {
                $pdf = \PDF::loadView($template, compact('tabla', 'crudos'))->setOrientation('landscape');
            }
            return $pdf->download('descargaReporte.pdf');    
        } catch (\Exception $e) {
            Log::error('EX.reporteController.exportarAPdf: ' . $e->getMessage());
        }
    }

}

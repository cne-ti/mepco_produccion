<?php

namespace App\Http\Controllers;

use DB;
use App\Util;
use App\Log;
use App\Usuario;
use App\Variable;
use App\Formula as Formula;
use App\FormulaHistorico;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\MessageBag;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Rezzza\Formulate\Formula as Form;

class formulaController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('authenticated');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $formulas = Formula::where('PAFO_ELIMINADO', 0)
                             ->get();

        $formulas_calculo       = array();
        $formulas_proyeccion    = array();
        $formulas_solver        = array();

        foreach($formulas as $key=>$formula) {
            $formula->PAFO_FORMULA = Util::formatText($formula->PAFO_FORMULA);
            $formulas[$key] = $formula;

            if ($formula->PAFO_TIPO_FORMULA == Util::FORMULA_CALCULO) {
                $formulas_calculo[] = $formula;
            }
            else if ($formula->PAFO_TIPO_FORMULA == Util::FORMULA_SOLVER) {
                $formulas_solver[] = $formula;
            }
            else if ($formula->PAFO_TIPO_FORMULA == Util::FORMULA_PROYECCION) {
                $formulas_proyeccion[] = $formula;
            }
        }

        return view('formula.list', compact('formulas_calculo', 'formulas_solver', 'formulas_proyeccion'));  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        parse_str($request['formData'], $formData);

        /* *** VALIDACIÓN *** */
        $validator = Validator::make($formData,[
            'pafo_llave'                => 'required|max:50',
            'pafo_nombre'               => 'required|max:50',
            'pafo_formula'              => 'required',
            'pafo_descripcion'          => 'required|max:255',
            'pafo_cantidad_decimales'   => 'numeric',
        ]);

        if ($validator->fails()){
            return response()->json([
                'status' => $validator->errors()->all()
            ]);
        }
        
        $errors = new MessageBag;

        // Comprobar que la llave de la fórmula corresponde con el patrón necesario
        if (preg_match("(^{{(.*)}}$)", $formData['pafo_llave']) != 1) {
            \Session::set('ALERT-TYPE', 'ERROR');
            \Session::set('ALERT-MSG', json_encode('Definición incorrecta de la variable: {{llave}}'));
            return response()->json([
                    'status' => 'success',
            ]);
        }

        // Comprobar que no exista otra fórmula con la misma llave
        if (Formula::where('PAFO_LLAVE', $formData['pafo_llave'])->first()) {
            $errors->add('0', 'Ya existe una fórmula con la llave especificada. Por favor, cambie el valor de la llave para ésta fórmula');
            return response()->json([
                    'status'    => 'error',
                    'errors'    => $errors
            ]);        
        }

        if ($formData['pafo_cantidad_decimales'] == '') {
            $cantidad_decimales = null;
        }
        else {
            $cantidad_decimales = $formData['pafo_cantidad_decimales'];
        }

        $now = Util::dateTimeNow();

        /* *** GUARDAR *** */
        $formula = new Formula;
        $formula->PAFO_TIPO_FORMULA        = $formData['pafo_tipo_formula'];
        $formula->PAFO_LLAVE               = $formData['pafo_llave'];
        $formula->PAFO_NOMBRE              = $formData['pafo_nombre'];
        $formula->PAFO_DESCRIPCION         = $formData['pafo_descripcion']; 
        $formula->PAFO_FORMULA             = $formData['pafo_formula'];
        $formula->PAFO_CANTIDAD_DECIMALES  = $cantidad_decimales;
        $formula->PAFO_FECHA_MODIFICACION  = $now;

        $formula->save();

        \Session::set('ALERT-TYPE', 'SUCCESS');
        \Session::set('ALERT-MSG', json_encode('Fórmula guardada correctamente'));

        /* Almacenar la acción en el log de acciones */
        Util::crearFilaLog('btnGuardar', 'Crear', 'fórmula', $formula->PAFO_ID);

        return response()->json([
                'status' => 'success',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $formula = Formula::findOrFail($id);
        return response()->json([
            'status' => 'success',
            'formula' => $formula
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $formula = Formula::findOrFail($id);

        $formula->PAFO_FECHA_MODIFICACION = Util::dateFormat($formula->PAFO_FECHA_MODIFICACION, 'd/m/Y H:i').' Hrs';

        // Retornar JSON
        return response()->json([
                'status' => 'success',
                'formula' => $formula
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        parse_str($request['formData'], $formData);

        /* *** VALIDACIÓN *** */
        $validator = Validator::make($formData,[
            'pafo_llave'                => 'required|max:50',
            'pafo_nombre'               => 'required|max:50',
            'pafo_formula'              => 'required',
            'pafo_descripcion'          => 'required|max:255',
            'pafo_cantidad_decimales'   => 'numeric',
        ]);

        if ($validator->fails()){
            return response()->json([
                'status' => $validator->errors()->all()
            ]);
        }

        $errors = new MessageBag;
        
        // Comprobar que la llave de la fórmula corresponde con el patrón necesario
        if (preg_match("(^{{(.*)}}$)", $formData['pafo_llave']) != 1) {
            \Session::set('ALERT-TYPE', 'ERROR');
            \Session::set('ALERT-MSG', json_encode('Definición incorrecta de la variable: {{llave}}'));
            return response()->json([
                    'status' => 'success',
            ]);
        }

        if ($formData['pafo_cantidad_decimales'] == '') {
            $cantidad_decimales = null;
        }
        else {
            $cantidad_decimales = $formData['pafo_cantidad_decimales'];
        }

        $formula = Formula::findOrFail($id);

        $FormulaHistorico = new FormulaHistorico;

        $FormulaHistorico->PAFO_ID             = $formula->PAFO_ID;
        $FormulaHistorico->USPE_ID             = session(Util::USUARIO)->USPE_ID;
        $FormulaHistorico->FOHI_LLAVE          = $formula->PAFO_LLAVE;
        $FormulaHistorico->FOHI_NOMBRE         = $formula->PAFO_NOMBRE;
        $FormulaHistorico->FOHI_DESCRIPCION    = $formula->PAFO_DESCRIPCION;
        $FormulaHistorico->FOHI_FECHA          = $formula->PAFO_FECHA_MODIFICACION;
        $FormulaHistorico->FOHI_FORMULA        = $formula->PAFO_FORMULA;
        $FormulaHistorico->save();

        $now = Util::dateTimeNow();

        /* *** GUARDAR *** */
        $formula->PAFO_LLAVE                = $formData['pafo_llave'];
        $formula->PAFO_NOMBRE               = $formData['pafo_nombre'];
        $formula->PAFO_DESCRIPCION          = $formData['pafo_descripcion'];
        $formula->PAFO_FORMULA              = $formData['pafo_formula'];
        $formula->PAFO_CANTIDAD_DECIMALES   = $cantidad_decimales;
        $formula->PAFO_FECHA_MODIFICACION   = $now;

        $formula->save();

        \Session::set('ALERT-TYPE', 'SUCCESS');
        \Session::set('ALERT-MSG', json_encode('Fórmula editada correctamente'));

        /* Almacenar la acción en el log de acciones */
        Util::crearFilaLog('btnGuardar', 'Editar', 'fórmula', $formula->PAFO_ID);

        return response()->json([
                'status' => 'success',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function eliminar(Request $request)
    {
        $pafo_id = $request['pafo_id'];
        $formula = Formula::find($pafo_id);
        $formula->PAFO_ELIMINADO = 1;
        $formula->save();

        \Session::set('ALERT-TYPE', 'SUCCESS');
        \Session::set('ALERT-MSG', json_encode('Fórmula eliminada correctamente'));

        /* Almacenar la acción en el log de acciones */
        Util::crearFilaLog('btnEliminar', 'Eliminar', 'fórmula', $formula->PAFO_ID);

        return response()->json([
                'status' => 'success',
        ]);
    }

    /**
     * Obtiene las filas del histórico relacioadas con una fórmula
     *
     * @param  int  $id
     * @return Response
     */
    public function historico(Request $request)
    {

        $historico = FormulaHistorico::select('USPE_ID', 'FOHI_LLAVE', 'FOHI_NOMBRE', 'FOHI_FORMULA', 'FOHI_DESCRIPCION', 'FOHI_FECHA')
                                    ->where('PAFO_ID', $request['pafo_id'])
                                    ->get();

        foreach ($historico as $key=>$linea) {
            $uspe_id = $linea->USPE_ID;
            unset($linea['USPE_ID']);
            $usuario = Usuario::find($uspe_id);
            $linea['USUA_EMAIL'] = $usuario->USUA_EMAIL;
            $historico[$key] = $linea;
        }

        return response()->json([
                'status'    => 'success',
                'historico' => $historico,
        ]);
    }

    /**
     * Valida que la fórmula escrita sea parseable, es decir, que las variables existan y la
     * definición sea correcta
     *
     * @param  int  $id
     * @return Response
     */
    public function validar(Request $request)
    {
        $formula = $request['formula'];
        $errors = new MessageBag;

        // Comprobar si tiene IF
        preg_match_all('/SI\((.*?)\)/i', $formula, $condicionales);
        $matches = $condicionales[1];
        foreach($matches as $match) {
            try {
                $instruccCondicional = explode('|', $match, 3);
                $condicion  = $instruccCondicional[0];
                $condTrue   = $instruccCondicional[1];
                $condFalse  = $instruccCondicional[2];
                // Buscar si hay variable en $condicion
                $pava_llave = Util::obtenerLlaves($condicion)[0];

                if ($pava_llave) {
                    // Hay variable, asignarles un valor cualquiera, ya que muchas dependen de 
                    // proc_id o esce_id, parámeros que no tenemos
                    $pava_valor = rand(1, 10);
                    $condicion = str_replace('{{'.$pava_llave.'}}', $pava_valor, $condicion);
                }
                // Comprobar que $condicion es seguro para ejecutar eval
                $operators = array('==', '!=', '<', '>', '<=', '>=');
                foreach ($operators as $operator) {
                    if (Util::contains($condicion, $operator)) {
                        // Ejecutar condicional
                        $condicion = eval('return '.$condicion.';');
                        if ($condicion) {
                            $formula = preg_replace('/SI\((.*?)\)/', $condTrue, $formula);
                        }
                        else {
                            $formula = preg_replace('/SI\((.*?)\)/', $condFalse, $formula);
                        }
                        break;
                    }
                }
            }
            catch(\Exception $e) {
                 $errors->add('0','Error en la fórmula'); 
            }
        }
        preg_match_all('/\{\{(.*?)\}\}/i', $formula, $matches);

        // Comprobar que las variables existen
        foreach ($matches[0] as $key=>$variable) {
            $existe = Variable::where('PAVA_LLAVE', $variable)
                            ->first();
            if (!$existe) {
                $existeFormula = Formula::where('PAFO_LLAVE', $variable)
                            ->first();
                if (!$existeFormula) {
                    $errors->add($key,'Error en la fórmula: No existe la llave '.$variable); 
                }
            }
        }
        // Caso de error al validar la existencia de las variables
        if (count($errors)>0) {
            return response()->json([
                    'status' => 'error',
                    'errors' => $errors,
            ]);
        }
        // Éxito al validar existencia de las variables, comprobar definición de la fórmula
        else {
            try {
                $variables = Util::obtenerLlaves($formula);
                $formula = new Form($formula);

                if ($variables) {
                    foreach ($variables as $item) {
                        // Sustituir las variables por números, ya que hay algunas
                        // que requieren de parámetros y sólo queremos comprobar la definición de la fórmula
                        $formula->setParameter($item, rand(1, 10));
                    }
                }
                $formula->setIsCalculable(true);
                $formula->render();
                
            }
            catch(\Exception $e) {
                $errors->add('0', 'Se ha producido un error al procesar la fórmula. Por favor, comprueba que está escrita correctamente.');
                return response()->json([
                        'status'    => 'error',
                        'errors'    => $errors
                ]);
            }
            return response()->json([
                    'status'    => 'success',
            ]);
        }
    }

    public function exportarAExcel()
    {
        $formulas = Formula::where('PAFO_ELIMINADO', 0)
                            ->get();

        Excel::create('Fórmulas', function($excel) use(&$formulas) {
            foreach ($formulas as $formula) {
                if ($formula->PAFO_TIPO_FORMULA == Util::FORMULA_CALCULO) {
                    $nombreHoja             = 'Cálculo';
                    $tabla[$nombreHoja][]   = $formula;
                }
                else if ($formula->PAFO_TIPO_FORMULA == Util::FORMULA_PROYECCION) {
                    $nombreHoja             = 'Proyección';
                    $tabla[$nombreHoja][]   = $formula;
                }
                else if ($formula->PAFO_TIPO_FORMULA == Util::FORMULA_SOLVER) {
                    $nombreHoja             = 'Solver';
                    $tabla[$nombreHoja][]   = $formula;
                }
            }

            foreach($tabla as $nombreHoja=>$t) {
                $excel->sheet($nombreHoja, function($sheet) use(&$t) {
                    $sheet->setWidth(array(
                        'A' => 20,
                        'B' => 20,
                    ));
                    $sheet->loadView('formula.templatePdf', array('formulas' => $t));
                });
            }
        })->export('xls');
    }

    public function exportarAPdf($tipo)
    {
        ini_set('memory_limit', '128M');
        
        if ($tipo == 'calculo') {
            $formulas = Formula::where('PAFO_ELIMINADO', 0)
                            ->where('PAFO_TIPO_FORMULA', Util::FORMULA_CALCULO)
                            ->get();
        }
        else if ($tipo == 'proyeccion') {
            $formulas = Formula::where('PAFO_ELIMINADO', 0)
                            ->where('PAFO_TIPO_FORMULA', Util::FORMULA_PROYECCION)
                            ->get();
        }
        else {
            $formulas = Formula::where('PAFO_ELIMINADO', 0)
                            ->where('PAFO_TIPO_FORMULA', Util::FORMULA_SOLVER)
                            ->get();
        }

        $pdf =  \PDF::loadView('formula.templatePdf', compact('formulas'))->setOrientation('landscape');
        return $pdf->download('formula.pdf');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Validator;
use App\Usuario;
use App\Perfil;
use App\Util;
use App\Log;
use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Controllers\Controller;
use Log as LogSistema;

use Maatwebsite\Excel\Facades\Excel;

class usuarioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('authenticated');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $usuarios = DB::table('MEPCO_USUA_PERF')
                    ->select('USUA_EMAIL')
                    ->where('USPE_ELIMINADO', 0)
                    ->groupBy('USUA_EMAIL')
                    ->get();

        foreach($usuarios as $key=>$usuario) {
            // Obtener sus perfiles
            $perfilesUsuario[$usuario->USUA_EMAIL]=      
                DB::table('MEPCO_USUA_PERF')
                    ->join('MEPCO_PERFIL', 'MEPCO_USUA_PERF.PERF_ID', '=', 'MEPCO_PERFIL.PERF_ID')
                    ->select('MEPCO_PERFIL.PERF_NOMBRE')
                    ->where('MEPCO_USUA_PERF.USUA_EMAIL', '=', $usuario->USUA_EMAIL)
                    ->where('MEPCO_USUA_PERF.USPE_ELIMINADO', '=', 0)
                    ->get();
        }

        // Enviar el listado de perfiles
        $perfiles = Perfil::where('PERF_ELIMINADO', 0)->get();

        return view('usuario.list', compact('usuarios', 'perfilesUsuario', 'perfiles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        parse_str($request['formData'], $formData);

        /* *** VALIDACIONES *** */
        $validator = Validator::make($formData,[
            'usua_email' => 'required|email|max:45',
        ]);

        if ($validator->fails()){
            return response()->json([
                'status' => $validator->errors()->all()
            ]);
        }

        // Comprobamos si se ha asociado perfil al usuario. Si no, mostrar mensaje de error
        if (!array_key_exists('check', $formData)) { 
            \Session::set('ALERT-TYPE', 'ERROR');
            \Session::set('ALERT-MSG', json_encode('Seleccione al menos un perfil asociado al usuario'));
            return response()->json([
                'status' => 'success'
            ]);
        }
        /* *** FIN VALIDACIONES *** */

        foreach ($formData['check'] as $key=>$val) {
            $usuario = Usuario::create([
                'USUA_EMAIL' => $formData['usua_email'],
                'PERF_ID' => $key,
            ]);
        }

        /* Almacenar la acción en el log de acciones */
        Util::crearFilaLog('btnGuardar', 'Crear', 'usuario', $usuario->USUA_EMAIL);

        \Session::set('ALERT-TYPE', 'SUCCESS');
        \Session::set('ALERT-MSG', json_encode('Usuario guardado correctamente'));

        return response()->json([
                'status' => 'success',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function editar(Request $request)
    {
        $usua_email = $request['usua_email'];
        // Obtener perfiles asociados
        $perfiles = Usuario::perfiles($usua_email);
        // Retornar JSON
        return response()->json([
                'status' => 'success',
                'perfiles' => $perfiles
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function actualizar(Request $request)
    {
        parse_str($request['formData'], $formData);

        $usua_email = $formData['usua_email'];

        // Comprobamos si se ha asociado perfil al usuario. Si no, mostrar mensaje de error
        if (!array_key_exists('check', $formData)) { 
            \Session::set('ALERT-TYPE', 'ERROR');
            \Session::set('ALERT-MSG', json_encode('Seleccione al menos un perfil asociado al usuario'));
            return response()->json([
                'status' => 'success'
            ]);
        }

        $usuario = Usuario::where('USUA_EMAIL', $usua_email)->get();

        // Eliminar relaciones que se encuentran en usuario y no en la lista de periles enviada
        foreach ($usuario as $key=>$linea) {
            $esta = 0;
            foreach ($formData['check'] as $perf_id=>$val) {
                if ($linea->PERF_ID == $perf_id) {
                    $esta = 1;
                }
            }
            if ($esta==0) {
                Usuario::find($linea->USPE_ID)->delete();
            }
        }
        // Añadir relaciones que se encuentran en la lista de perfiles enviada y no en usuario
        foreach ($formData['check'] as $perf_id=>$val) {
            $esta = 0;
            foreach ($usuario as $k=>$linea) {
                if ($linea->PERF_ID == $perf_id) {
                    $esta = 1;
                }
            }
            if ($esta==0) {
                // Añadir las relaciones que faltan
                Usuario::create([
                    'USUA_EMAIL' => $usua_email,
                    'PERF_ID' => $perf_id,
                ]); 
            }
        }

        /* Almacenar la acción en el log de acciones */
        Util::crearFilaLog('btnGuardar', 'Editar', 'usuario', $usua_email);

        \Session::set('ALERT-TYPE', 'SUCCESS');
        \Session::set('ALERT-MSG', json_encode('Datos del usuario actualizados correctamente'));

        return response()->json([
                'status' => 'success',
        ]);
    }

    /**
     * Realiza el eliminado lógico de un usuario
     *
     * @param  int  $id
     * @return Response
     */
    public function eliminar(Request $request)
    {
        $usua_email = $request['usua_email'];
        $usuario = Usuario::where('USUA_EMAIL', $usua_email)->get();

        foreach($usuario as $linea) {
            $item = Usuario::find($linea->USPE_ID);
            $item->USPE_ELIMINADO = 1;
            $item->save();
        }

        \Session::set('ALERT-TYPE', 'SUCCESS');
        \Session::set('ALERT-MSG', json_encode('Usuario eliminado correctamente'));

        /* Almacenar la acción en el log de acciones */
        Util::crearFilaLog('btnEliminar', 'Eliminar', 'usuario', $usua_email);

        return response()->json([
                'status' => 'success',
        ]);
    }

    /**
     * Comprueba si existe una sesión de usuario generador activa en el sistema
     *
     * @param  int  $id
     * @return Response
     */
    public function comprobarSesionGenerador()
    {
        $generadorConectado = Usuario::where('PERF_ID', Util::PERFIL_USUARIO_GENERADOR)
            ->where('USPE_ACTIVO', '=', 1)
            ->where('USPE_ELIMINADO', '=', 0)
            ->count();

        LogSistema::debug('$generadorConectado: ' . $generadorConectado);

        if ($generadorConectado == 0) {
            // NO hay ningún generador conectado. Se carga el modal informando de dicha situación
            $STATUS               = 'NO_EXISTE_GENERADOR';
            $USUA_EMAIL           = null;
            $USPE_ULTIMA_CONEXION = null;
        }
        else {
            // Existe un usuario generador conectado. Se carga el modal correspondiente
            $generador = Usuario::where('PERF_ID', Util::PERFIL_USUARIO_GENERADOR)
                ->where('USPE_ACTIVO', '=', 1)
                ->where('USPE_ELIMINADO', '=', 0)
                ->first();

            $STATUS               = 'EXISTE_GENERADOR';
            $USUA_EMAIL           = $generador->USUA_EMAIL;
            $USPE_ULTIMA_CONEXION = Util::dateFormat($generador->USPE_ULTIMA_CONEXION, 'd/m/Y H:i:s') . ' Hrs';
        }
        return response()->json([
            'STATUS'               => $STATUS,
            'USUA_EMAIL'           => $USUA_EMAIL,
            'USPE_ULTIMA_CONEXION' => $USPE_ULTIMA_CONEXION
        ]);
    }

    /**
     * Cierra la sesión activa de un usuario generador a petición del admin
     *
     * @param  int  $id
     * @return Response
     */
    public function cerrarSesionGenerador()
    {
        try {
            $generadorConectado = Usuario::where('PERF_ID', Util::PERFIL_USUARIO_GENERADOR)
                ->where('USPE_ACTIVO', '=', 1)
                ->where('USPE_ELIMINADO', '=', 0)
                ->first();
            $generadorConectado->USPE_ACTIVO = 0;
            $generadorConectado->save();

            /* Almacenar la acción en el log de acciones */
            Util::crearFilaLog('btnCerrarSesion', 'Cerrar sesión', 'usuario', $generadorConectado->USUA_EMAIL);

            return response()->json([
                'STATUS' => 'SUCCESS',
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'STATUS' => 'ERROR',
                'MSJ'    => ['Error al intentar desloguear usuario generador, favor reintentar']
            ]);
        }
        
    }

    public function exportarAExcel()
    {
        $usuarios = DB::table('MEPCO_USUA_PERF AS U')
                ->join('MEPCO_PERFIL AS P', 'U.PERF_ID', '=', 'P.PERF_ID')
                ->select('U.USUA_EMAIL', 'P.PERF_NOMBRE')
                ->where('USPE_ELIMINADO', 0)
                ->where('PERF_ELIMINADO', 0)
                ->get();

        Excel::create('Usuarios', function($excel) use(&$usuarios) {
            $excel->sheet('Usuarios', function($sheet) use(&$usuarios) {
                $sheet->setWidth(array(
                    'A' => 20,
                    'B' => 20,
                ));
                $sheet->loadView('usuario.templatePdf', array('usuarios' => $usuarios));
            });
        })->export('xls');
    }

    public function exportarAPdf()
    {
        $usuarios = DB::table('MEPCO_USUA_PERF AS U')
                ->join('MEPCO_PERFIL AS P', 'U.PERF_ID', '=', 'P.PERF_ID')
                ->select('U.USUA_EMAIL', 'P.PERF_NOMBRE')
                ->where('USPE_ELIMINADO', 0)
                ->where('PERF_ELIMINADO', 0)
                ->get();

        $pdf =  \PDF::loadView('usuario.templatePdf', compact('usuarios', 'perfilesUsuario'));

        return $pdf->download('usuario.pdf');
    }
}


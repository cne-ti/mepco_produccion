<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Log;
use App\DatosHistoricos;
use App\Util;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_Shared_Date;
use PHPExcel_Style_Fill;
use PHPExcel_Style_NumberFormat;
use PHPExcel_IOFactory;
use Illuminate\Support\MessageBag;
use DB;

class cargaHistoricaController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('authenticated');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('cargaHistorica.carga');  
    }

    /**
     * Listado de registros de datos históricos
     *
     * @return Response
     */
    public function historico()
    {
        ini_set('memory_limit', '128M');

        $historico = DatosHistoricos::get();

        // Proceso para que el Datatable pueda hacer sort por fecha correctamente
        foreach($historico as $key=>$row) {
            $row->DAHI_FECHA_SORT = date('Ymd', strtotime($row->DAHI_FECHA));
            $historico[$key] = $row;
        }

        return view('cargaHistorica.historico', compact('historico'));  
    }

    /**
     * Ver el detalle de un registro 
     *
     * @return Response
     */
    public function show($id)
    {
        $row = DatosHistoricos::findOrFail($id)->toArray();

        // Formatear
        foreach($row as $key=>$value) {
            if (($key!='DAHI_ID')&&($key!='DAHI_FECHA')) {
                $row[$key] = Util::formatNumber($row[$key]);
            }
        }
        // Retornar JSON
        return response()->json([
                'status'        => 'success',
                'row'        => $row
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ficheroCarga' => 'required'
        ]);

        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else
        {
            $cargaHistorica = $request->file('ficheroCarga');
            if($cargaHistorica == null)
            {
                return redirect()->back()->withErrors('Seleccione un fichero de carga')->withInput();
            }
        }

        ini_set('memory_limit', '128M');
        
        // Lectura del fichero de carga, Sólo leer datos para evitar problemas de memoria
        $reader = PHPExcel_IOFactory::createReader('Excel2007');
        $reader->setReadDataOnly(true);

        $reader = Excel::load($cargaHistorica);

        // Preprocesar carga masiva
        $resultado = Util::procesarDatos($reader, Util::PROCESAR_CARGA_HISTORICA);

        $valores = $resultado[0];
        $errores = $resultado[1];
        $advertencias = $resultado[2];

        $mensajeErrores = '';
        $mensajeAdvertencias = '';
        $errors = new MessageBag;
        $warnings = new MessageBag;
        
        // Comprobar si existen errores de preprocesamiento
        // SI NO EXISTEN errores en esta fase, comienza la inserción en BBDD
        if (count($errores) != 0) {
            // Devolver el array con errores al usuario
            $mensajeErrores = 'Se han encontrado los siguientes errores. Las siguientes filas no han sido insertadas en la base de datos.';
            foreach($errores as $key=>$error) {
                $errors->add($key, $error);
            }
        }
        if (count($advertencias) != 0) {
            // Devolver el array con warnings al usuario
            $mensajeAdvertencias = 'Se han encontrado las siguientes advertencias.';
            foreach($advertencias as $key=>$warning) {
                $warnings->add($key, $warning);
            }
        }

        /* Almacenar la acción en el log de acciones */
        Util::crearFilaLog('btnCargar', 'Carga histórica añadir', 'datos históricos', '', 'Añadir datos históricos. Número de errores: '.count($errores));

        return view('cargaHistorica.informeErrores', compact('errors', 'mensajeErrores', 'warnings', 'mensajeAdvertencias'));
    }

    /**
     * Descarga la plantilla de Excel con todos los datos de carga masiva
     *
     * @param  Request  $request
     * @return Response
     */
    public function descargarExcel()
    {
        ini_set('memory_limit', '128M');

        // Abrir documento
        $documento = public_path().'/docs/plantilla_carga_masiva.xlsx';
        $reader = Excel::load($documento, function($reader) {

            $sheet = $reader->getActiveSheet();

            // Colorear las celdas
            $sheet->getStyle(Util::EXCEL_FECHA.'2:'.Util::EXCEL_PROM_REFERENCIA_SEMANAL_GLP.'3')
                  ->getFill()
                  ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                  ->getStartColor()
                  ->setARGB('808080');

            $sheet->getStyle(Util::EXCEL_FECHA.'2:'.Util::EXCEL_PROM_REFERENCIA_SEMANAL_GLP.'3')
                  ->getFont()
                  ->getColor()
                  ->setRGB('FFFFFF');

            $highestRow = $sheet->getHighestDataRow();
            $highestRow = $highestRow + 1;
            // Obtener todas las filas de la base de datos
            $datosHistoricos = DB::table('MEPCO_DATOS_HISTORICOS')->get();

            // Recorrer todas las filas y rellenar la fila en el Excel
            foreach($datosHistoricos as $row){
                $fecha = $row->DAHI_FECHA;
                // Formatear la fecha
                $t_year   = substr($fecha,0,4);
                $t_month  = substr($fecha,5,2);// Fixed problems with offsets
                $t_day    = substr($fecha,8,2);
                $fecha    = PHPExcel_Shared_Date::FormattedPHPToExcel($t_year, $t_month, $t_day);
                $sheet->setCellValue(Util::EXCEL_FECHA.$highestRow, $fecha);        
                $sheet->getStyle(Util::EXCEL_FECHA.$highestRow)
                      ->getNumberFormat()
                      ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
                $sheet->setCellValue(Util::EXCEL_GAS_UNL_87.$highestRow, $row->DAHI_GAS_UNL_87 );      
                $sheet->setCellValue(Util::EXCEL_GAS_UNL_93.$highestRow, $row->DAHI_GAS_UNL_93 );      
                $sheet->setCellValue(Util::EXCEL_DIESEL.$highestRow, $row->DAHI_LD_DIESEL );          
                $sheet->setCellValue(Util::EXCEL_USG_BUTANO.$highestRow, $row->DAHI_USG_BUTANO );      
                $sheet->setCellValue(Util::EXCEL_UKC_USG.$highestRow, $row->DAHI_UKC_USG );         
                $sheet->setCellValue(Util::EXCEL_BRENT.$highestRow, $row->DAHI_BRENT );           
                $sheet->setCellValue(Util::EXCEL_BRENT_M1.$highestRow, $row->DAHI_BRENT_M1 );        
                $sheet->setCellValue(Util::EXCEL_BRENT_M2.$highestRow, $row->DAHI_BRENT_M2 );        
                $sheet->setCellValue(Util::EXCEL_BRENT_M3.$highestRow, $row->DAHI_BRENT_M3 );        
                $sheet->setCellValue(Util::EXCEL_BRENT_M4.$highestRow, $row->DAHI_BRENT_M4 );        
                $sheet->setCellValue(Util::EXCEL_BRENT_M5.$highestRow, $row->DAHI_BRENT_M5 );        
                $sheet->setCellValue(Util::EXCEL_BRENT_M6.$highestRow, $row->DAHI_BRENT_M6 );        
                $sheet->setCellValue(Util::EXCEL_PPT_USG.$highestRow, $row->DAHI_PPT_USG );         
                $sheet->setCellValue(Util::EXCEL_MONT_BELVIEU_CALCULADO.$highestRow, $row->DAHI_MONT_BELVIEU ); 
                $sheet->setCellValue(Util::EXCEL_CIF_ARA_CALCULADO.$highestRow, $row->DAHI_CIF_ARA ); 
                $sheet->setCellValue(Util::EXCEL_WTI.$highestRow, $row->DAHI_WTI );             
                $sheet->setCellValue(Util::EXCEL_WTI_M1.$highestRow, $row->DAHI_WTI_M1 );          
                $sheet->setCellValue(Util::EXCEL_WTI_M2.$highestRow, $row->DAHI_WTI_M2 );          
                $sheet->setCellValue(Util::EXCEL_WTI_M3.$highestRow, $row->DAHI_WTI_M3 );          
                $sheet->setCellValue(Util::EXCEL_WTI_M4.$highestRow, $row->DAHI_WTI_M4 );          
                $sheet->setCellValue(Util::EXCEL_WTI_M5.$highestRow, $row->DAHI_WTI_M5 );          
                $sheet->setCellValue(Util::EXCEL_WTI_M6.$highestRow, $row->DAHI_WTI_M6 );          
                $sheet->setCellValue(Util::EXCEL_TC_PUBLICADO.$highestRow, $row->DAHI_TC_PUBLICADO );    
                $sheet->setCellValue(Util::EXCEL_UTM.$highestRow, $row->DAHI_UTM );             
                $sheet->setCellValue(Util::EXCEL_LIBOR.$highestRow, $row->DAHI_LIBOR );           
                $sheet->setCellValue(Util::EXCEL_TARIFA_DIARIA_82000.$highestRow, $row->DAHI_TARIFA_DIARIA_82000 ); 
                $sheet->setCellValue(Util::EXCEL_TARIFA_DIARIA_59000.$highestRow, $row->DAHI_TARIFA_DIARIA_59000 ); 
                $sheet->setCellValue(Util::EXCEL_IFO_380_HOUSTON.$highestRow, $row->DAHI_IFO_380_HOUSTON );     
                $sheet->setCellValue(Util::EXCEL_MDO_HOUSTON.$highestRow, $row->DAHI_MDO_HOUSTON );         
                $sheet->setCellValue(Util::EXCEL_IFO_380_CRISTOBAL.$highestRow, $row->DAHI_IFO_380_CRISTOBAL );   
                $sheet->setCellValue(Util::EXCEL_MDO_CRISTOBAL.$highestRow, $row->DAHI_MDO_CRISTOBAL );       
                $sheet->setCellValue(Util::EXCEL_N_GAS_87.$highestRow, $row->DAHI_N_GAS_87 );            
                $sheet->setCellValue(Util::EXCEL_M_GAS_87.$highestRow, $row->DAHI_M_GAS_87 );            
                $sheet->setCellValue(Util::EXCEL_S_GAS_87.$highestRow, $row->DAHI_S_GAS_87 );            
                $sheet->setCellValue(Util::EXCEL_T_GAS_87.$highestRow, $row->DAHI_T_GAS_87 );            
                $sheet->setCellValue(Util::EXCEL_F_GAS_87.$highestRow, $row->DAHI_F_GAS_87 );            
                $sheet->setCellValue(Util::EXCEL_N_GAS_93.$highestRow, $row->DAHI_N_GAS_93 );            
                $sheet->setCellValue(Util::EXCEL_M_GAS_93.$highestRow, $row->DAHI_M_GAS_93 );            
                $sheet->setCellValue(Util::EXCEL_S_GAS_93.$highestRow, $row->DAHI_S_GAS_93 );            
                $sheet->setCellValue(Util::EXCEL_T_GAS_93.$highestRow, $row->DAHI_T_GAS_93 );            
                $sheet->setCellValue(Util::EXCEL_F_GAS_93.$highestRow, $row->DAHI_F_GAS_93 );            
                $sheet->setCellValue(Util::EXCEL_N_DIESEL.$highestRow, $row->DAHI_N_DIESEL );            
                $sheet->setCellValue(Util::EXCEL_M_DIESEL.$highestRow, $row->DAHI_M_DIESEL );            
                $sheet->setCellValue(Util::EXCEL_S_DIESEL.$highestRow, $row->DAHI_S_DIESEL );            
                $sheet->setCellValue(Util::EXCEL_T_DIESEL.$highestRow, $row->DAHI_T_DIESEL );            
                $sheet->setCellValue(Util::EXCEL_F_DIESEL.$highestRow, $row->DAHI_F_DIESEL );            
                $sheet->setCellValue(Util::EXCEL_N_GLP.$highestRow, $row->DAHI_N_GLP );               
                $sheet->setCellValue(Util::EXCEL_M_GLP.$highestRow, $row->DAHI_M_GLP );               
                $sheet->setCellValue(Util::EXCEL_S_GLP.$highestRow, $row->DAHI_S_GLP );               
                $sheet->setCellValue(Util::EXCEL_T_GLP.$highestRow, $row->DAHI_T_GLP );               
                $sheet->setCellValue(Util::EXCEL_F_GLP.$highestRow, $row->DAHI_F_GLP );                           
                $sheet->setCellValue(Util::EXCEL_PROM_PARIDAD_SEMANAL_GAS_87.$highestRow, $row->DAHI_PROM_PARIDAD_SEMANAL_GAS_87 ); 
                $sheet->setCellValue(Util::EXCEL_PROM_PARIDAD_SEMANAL_GAS_93.$highestRow, $row->DAHI_PROM_PARIDAD_SEMANAL_GAS_93 ); 
                $sheet->setCellValue(Util::EXCEL_PROM_PARIDAD_SEMANAL_DIESEL.$highestRow, $row->DAHI_PROM_PARIDAD_SEMANAL_DIESEL ); 
                $sheet->setCellValue(Util::EXCEL_PROM_PARIDAD_SEMANAL_GLP.$highestRow, $row->DAHI_PROM_PARIDAD_SEMANAL_GLP );    
                $sheet->setCellValue(Util::EXCEL_PARIDAD_GAS_87.$highestRow, $row->DAHI_PARIDAD_GAS_87 );              
                $sheet->setCellValue(Util::EXCEL_PARIDAD_GAS_93.$highestRow, $row->DAHI_PARIDAD_GAS_93 );              
                $sheet->setCellValue(Util::EXCEL_PARIDAD_DIESEL.$highestRow, $row->DAHI_PARIDAD_DIESEL );              
                $sheet->setCellValue(Util::EXCEL_PARIDAD_GLP.$highestRow, $row->DAHI_PARIDAD_GLP );                 
                $sheet->setCellValue(Util::EXCEL_PROM_BRENT_SEMANAL.$highestRow, $row->DAHI_PROM_BRENT_SEMANAL );          
                $sheet->setCellValue(Util::EXCEL_BRENT_PROM_HISTORICO_GAS_87.$highestRow, $row->DAHI_BRENT_PROM_HISTORICO_GAS_87 ); 
                $sheet->setCellValue(Util::EXCEL_BRENT_PROM_HISTORICO_GAS_93.$highestRow, $row->DAHI_BRENT_PROM_HISTORICO_GAS_93 ); 
                $sheet->setCellValue(Util::EXCEL_BRENT_PROM_HISTORICO_DIESEL.$highestRow, $row->DAHI_BRENT_PROM_HISTORICO_DIESEL ); 
                $sheet->setCellValue(Util::EXCEL_BRENT_PROM_HISTORICO_GLP.$highestRow, $row->DAHI_BRENT_PROM_HISTORICO_GLP );    
                $sheet->setCellValue(Util::EXCEL_BRENT_PROM_FUTUROS_GAS_87.$highestRow, $row->DAHI_BRENT_PROM_FUTUROS_GAS_87 );   
                $sheet->setCellValue(Util::EXCEL_BRENT_PROM_FUTUROS_GAS_93.$highestRow, $row->DAHI_BRENT_PROM_FUTUROS_GAS_93 );   
                $sheet->setCellValue(Util::EXCEL_BRENT_PROM_FUTUROS_DIESEL.$highestRow, $row->DAHI_BRENT_PROM_FUTUROS_DIESEL );   
                $sheet->setCellValue(Util::EXCEL_BRENT_PROM_FUTUROS_GLP.$highestRow, $row->DAHI_BRENT_PROM_FUTUROS_GLP );      
                $sheet->setCellValue(Util::EXCEL_BRENT_PROM_PONDERADO_GAS_87.$highestRow, $row->DAHI_BRENT_PROM_PONDERADO_GAS_87 ); 
                $sheet->setCellValue(Util::EXCEL_BRENT_PROM_PONDERADO_GAS_93.$highestRow, $row->DAHI_BRENT_PROM_PONDERADO_GAS_93 ); 
                $sheet->setCellValue(Util::EXCEL_BRENT_PROM_PONDERADO_DIESEL.$highestRow, $row->DAHI_BRENT_PROM_PONDERADO_DIESEL ); 
                $sheet->setCellValue(Util::EXCEL_BRENT_PROM_PONDERADO_GLP.$highestRow, $row->DAHI_BRENT_PROM_PONDERADO_GLP );    
                $sheet->setCellValue(Util::EXCEL_PROM_WTI.$highestRow, $row->DAHI_PROM_WTI_SEMANAL );                    
                $sheet->setCellValue(Util::EXCEL_WTI_PROM_HISTORICO_GAS_87.$highestRow, $row->DAHI_WTI_PROM_HISTORICO_GAS_87 );   
                $sheet->setCellValue(Util::EXCEL_WTI_PROM_HISTORICO_GAS_93.$highestRow, $row->DAHI_WTI_PROM_HISTORICO_GAS_93 );   
                $sheet->setCellValue(Util::EXCEL_WTI_PROM_HISTORICO_DIESEL.$highestRow, $row->DAHI_WTI_PROM_HISTORICO_DIESEL );   
                $sheet->setCellValue(Util::EXCEL_WTI_PROM_HISTORICO_GLP.$highestRow, $row->DAHI_WTI_PROM_HISTORICO_GLP );      
                $sheet->setCellValue(Util::EXCEL_WTI_PROM_FUTUROS_GAS_87.$highestRow, $row->DAHI_WTI_PROM_FUTUROS_GAS_87 );     
                $sheet->setCellValue(Util::EXCEL_WTI_PROM_FUTUROS_GAS_93.$highestRow, $row->DAHI_WTI_PROM_FUTUROS_GAS_93 );     
                $sheet->setCellValue(Util::EXCEL_WTI_PROM_FUTUROS_DIESEL.$highestRow, $row->DAHI_WTI_PROM_FUTUROS_DIESEL );     
                $sheet->setCellValue(Util::EXCEL_WTI_PROM_FUTUROS_GLP.$highestRow, $row->DAHI_WTI_PROM_FUTUROS_GLP );        
                $sheet->setCellValue(Util::EXCEL_WTI_PROM_PONDERADO_GAS_87.$highestRow, $row->DAHI_WTI_PROM_PONDERADO_GAS_87 );   
                $sheet->setCellValue(Util::EXCEL_WTI_PROM_PONDERADO_GAS_93.$highestRow, $row->DAHI_WTI_PROM_PONDERADO_GAS_93 );   
                $sheet->setCellValue(Util::EXCEL_WTI_PROM_PONDERADO_DIESEL.$highestRow, $row->DAHI_WTI_PROM_PONDERADO_DIESEL );   
                $sheet->setCellValue(Util::EXCEL_WTI_PROM_PONDERADO_GLP.$highestRow, $row->DAHI_WTI_PROM_PONDERADO_GLP );      
                $sheet->setCellValue(Util::EXCEL_PROM_MARGEN_SEMANAL_BRENT_GAS_87.$highestRow, $row->DAHI_PROM_MARGEN_SEMANAL_BRENT_GAS_87);
                $sheet->setCellValue(Util::EXCEL_PROM_MARGEN_SEMANAL_BRENT_GAS_93.$highestRow, $row->DAHI_PROM_MARGEN_SEMANAL_BRENT_GAS_93);
                $sheet->setCellValue(Util::EXCEL_PROM_MARGEN_SEMANAL_BRENT_DIESEL.$highestRow, $row->DAHI_PROM_MARGEN_SEMANAL_BRENT_DIESEL);
                $sheet->setCellValue(Util::EXCEL_PROM_MARGEN_SEMANAL_BRENT_GLP.$highestRow, $row->DAHI_PROM_MARGEN_SEMANAL_BRENT_GLP);
                $sheet->setCellValue(Util::EXCEL_PROM_MARGEN_SEMANAL_WTI_GAS_87.$highestRow, $row->DAHI_PROM_MARGEN_SEMANAL_WTI_GAS_87);
                $sheet->setCellValue(Util::EXCEL_PROM_MARGEN_SEMANAL_WTI_GAS_93.$highestRow, $row->DAHI_PROM_MARGEN_SEMANAL_WTI_GAS_93);
                $sheet->setCellValue(Util::EXCEL_PROM_MARGEN_SEMANAL_WTI_DIESEL.$highestRow, $row->DAHI_PROM_MARGEN_SEMANAL_WTI_DIESEL);
                $sheet->setCellValue(Util::EXCEL_PROM_MARGEN_SEMANAL_WTI_GLP.$highestRow, $row->DAHI_PROM_MARGEN_SEMANAL_WTI_GLP);
                $sheet->setCellValue(Util::EXCEL_MARGEN_PROMEDIO_GAS_87.$highestRow, $row->DAHI_MARGEN_PROMEDIO_GAS_87 );      
                $sheet->setCellValue(Util::EXCEL_MARGEN_PROMEDIO_GAS_93.$highestRow, $row->DAHI_MARGEN_PROMEDIO_GAS_93 );      
                $sheet->setCellValue(Util::EXCEL_MARGEN_PROMEDIO_DIESEL.$highestRow, $row->DAHI_MARGEN_PROMEDIO_DIESEL );      
                $sheet->setCellValue(Util::EXCEL_MARGEN_PROMEDIO_GLP.$highestRow, $row->DAHI_MARGEN_PROMEDIO_GLP );  
                $sheet->setCellValue(Util::EXCEL_TIPO_CRUDO.$highestRow, $row->DAHI_TIPO_CRUDO );       
                $sheet->setCellValue(Util::EXCEL_FOB_TEORICO_GAS_87.$highestRow, $row->DAHI_FOB_TEORICO_GAS_87 );          
                $sheet->setCellValue(Util::EXCEL_FOB_TEORICO_GAS_93.$highestRow, $row->DAHI_FOB_TEORICO_GAS_93 );          
                $sheet->setCellValue(Util::EXCEL_FOB_TEORICO_DIESEL.$highestRow, $row->DAHI_FOB_TEORICO_DIESEL );          
                $sheet->setCellValue(Util::EXCEL_FOB_TEORICO_GLP.$highestRow, $row->DAHI_FOB_TEORICO_GLP );             
                $sheet->setCellValue(Util::EXCEL_PROM_REFERENCIA_SEMANAL_GAS_87.$highestRow, $row->DAHI_PROM_REFERENCIA_SEMANAL_GAS_87 );  
                $sheet->setCellValue(Util::EXCEL_PROM_REFERENCIA_SEMANAL_GAS_93.$highestRow, $row->DAHI_PROM_REFERENCIA_SEMANAL_GAS_93 );  
                $sheet->setCellValue(Util::EXCEL_PROM_REFERENCIA_SEMANAL_DIESEL.$highestRow, $row->DAHI_PROM_REFERENCIA_SEMANAL_DIESEL );  
                $sheet->setCellValue(Util::EXCEL_PROM_REFERENCIA_SEMANAL_GLP.$highestRow, $row->DAHI_PROM_REFERENCIA_SEMANAL_GLP );     
                $sheet->setCellValue(Util::EXCEL_CORRECCION_RVP.$highestRow, $row->DAHI_CORRECCION_RVP ); 
                $highestRow = $highestRow + 1;
            }
        })->download('xlsx');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolverParidad extends Model 
{
    protected $table='MEPCO_SOLVER_PARIDAD';

    protected $primaryKey='SOLP_ID';

    public $timestamps=false;

	protected $fillable=[
        'SOLV_ID',
        'HIDR_ID',
        'SOLP_T',
        'SOLP_PARIDAD',
    ];

    /**
    * Relación 1:N con Solver
    */
    public function solver() {
        return $this->hasOne('App\Solver', 'SOLV_ID');
    }
}

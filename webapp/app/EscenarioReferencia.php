<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EscenarioReferencia extends Model 
{
    protected $table='MEPCO_ESCENARIO_REFERENCIA';

    protected $primaryKey='ESRE_ID';

    public $timestamps=false;

	protected $fillable=[
        'ESCE_ID',
        'HIDR_ID', 
        'ESRE_FECHA',
        'ESRE_MARGEN',
        'ESRE_BRENT_CORREGIDO',
        'ESRE_FOB_TEORICO',
        'ESRE_FM',
        'ESRE_SM',
        'ESRE_CIF',
        'ESRE_GCC',
        'ESRE_MERM',
        'ESRE_DA',
        'ESRE_PPIBL',
        'ESRE_IVA',
        'ESRE_CF',
        'ESRE_CD',
        'ESRE_CA',
        'ESRE_PARIDAD_CA',
    ];

    /**
    * Relación 1:N con hidrocarburos
    */
    public function hidrocarburo() {
        return $this->belongsTo('App\Hidrocarburo');
    }

    /**
    * Relación 1:N con Escenario
    */
    public function escenario() {
        return $this->belongsTo('App\Escenario', 'ESCE_ID');
    }
}

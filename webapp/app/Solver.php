<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Solver extends Model 
{
    protected $table='MEPCO_SOLVER';

    protected $primaryKey='SOLV_ID';

    public $timestamps=false;

	protected $fillable=[
        'PROC_ID',
        'SOLV_PRECIO_PARIDAD_MIN',
        'SOLV_PRECIO_PARIDAD_MAX',
        'SOLV_PRECIO_REFERENCIA_MIN',
        'SOLV_PRECIO_REFERENCIA_MAX',
        'SOLV_CORRECCION_RVP',
    ];


    /**
    * Relación 1:N con Proceso
    */
    public function proceso() {
        return $this->hasOne('App\Proceso', 'PROC_ID');
    }

    /**
    * Relación 1:N con la tabla de Solver Paridad
    */
    public function solverParidad() {
        return $this->hasMany('App\SolverParidad', 'SOLV_ID');
    }

    /**
    * Relación 1:N con la tabla de Solver Referencia
    */
    public function solverReferencia() {
        return $this->hasMany('App\SolverReferencia', 'SOLV_ID');
    }

    /**
    * Obtiene los últimos n, m, s, t, f utilizados para precargar en solver
    */
    public static function obtenerParametros($combustible)
    {
        switch($combustible) {
            case Util::COMBUSTIBLE_93:
                $parametros = DB::table('MEPCO_DATOS_HISTORICOS AS D')
                    ->select('D.DAHI_N_GAS_87 AS N', 'D.DAHI_M_GAS_87 AS M', 'D.DAHI_S_GAS_87 AS S', 'D.DAHI_T_GAS_87 AS T', 'D.DAHI_F_GAS_87 AS F')
                    ->orderBy('D.DAHI_FECHA', 'DESC')
                    ->first();
                break;
            case Util::COMBUSTIBLE_97:
                $parametros = DB::table('MEPCO_DATOS_HISTORICOS AS D')
                    ->select('D.DAHI_N_GAS_93 AS N', 'D.DAHI_M_GAS_93 AS M', 'D.DAHI_S_GAS_93 AS S', 'D.DAHI_T_GAS_93 AS T', 'D.DAHI_F_GAS_93 AS F')
                    ->orderBy('D.DAHI_FECHA', 'DESC')
                    ->first();
                break;
            case Util::COMBUSTIBLE_DIESEL:
                $parametros = DB::table('MEPCO_DATOS_HISTORICOS AS D')
                    ->select('D.DAHI_N_DIESEL AS N', 'D.DAHI_M_DIESEL AS M', 'D.DAHI_S_DIESEL AS S', 'D.DAHI_T_DIESEL AS T', 'D.DAHI_F_DIESEL AS F')
                    ->orderBy('D.DAHI_FECHA', 'DESC')
                    ->first();
                break;
            case Util::COMBUSTIBLE_GLP:
                $parametros = DB::table('MEPCO_DATOS_HISTORICOS AS D')
                    ->select('D.DAHI_N_GLP AS N', 'D.DAHI_M_GLP AS M', 'D.DAHI_S_GLP AS S', 'D.DAHI_T_GLP AS T', 'D.DAHI_F_GLP AS F')
                    ->orderBy('D.DAHI_FECHA', 'DESC')
                    ->first();
                break;
        }
        return $parametros;
    }
}

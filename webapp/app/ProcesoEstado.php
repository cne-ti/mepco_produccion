<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProcesoEstado extends Model
{

	const static ESTADO_NUEVO = 1;

    protected $table='MEPCO_PROCESO_ESTADO';

    protected $primaryKey='PRES_ID';

    public $timestamps=false;

	protected $fillable=[
        'PRES_NOMBRE', 
        'PRES_DESCRIPCION'
    ];

}

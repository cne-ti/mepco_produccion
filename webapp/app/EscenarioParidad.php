<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EscenarioParidad extends Model 
{
    protected $table='MEPCO_ESCENARIO_PARIDAD';

    protected $primaryKey='ESPA_ID';

    public $timestamps=false;

	protected $fillable=[
        'ESCE_ID',
        'HIDR_ID', 
        'ESPA_FECHA',
        'ESPA_FOB',
        'ESPA_FM',
        'ESPA_SM',
        'ESPA_CIF',
        'ESPA_GCC',
        'ESPA_MERM',
        'ESPA_DA',
        'ESPA_PPIBL',
        'ESPA_IVA',
        'ESPA_CF',
        'ESPA_CD',
        'ESPA_CA',
        'ESPA_PARIDAD_CA',
    ];

    /**
    * Relación 1:N con hidrocarburos
    */
    public function hidrocarburo() {
        return $this->belongsTo('App\Hidrocarburo');
    }

    /**
    * Relación 1:N con Escenario
    */
    public function escenario() {
        return $this->belongsTo('App\Escenario', 'ESCE_ID');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hidrocarburo extends Model
{
    protected $table = 'MEPCO_HIDROCARBUROS';

    protected $primaryKey = 'HIDR_ID';

    public $timestamps = false;

    /**
    * Relación 1:N con variables
    */
    public function variables() {
        return $this->hasMany('App\Variable', 'HIDR_ID');
    }

    /**
    * Relación 1:N con Escenario Calculo
    */
    public function escenariosCalculo() {
        return $this->hasMany('App\EscenarioCalculo', 'HIDR_ID');
    }

    /**
    * Relación 1:N con Escenario Paridad
    */
    public function escenariosParidad() {
        return $this->hasMany('App\EscenarioParidad', 'HIDR_ID');
    }

    /**
    * Relación 1:N con Escenario Referencia
    */
    public function escenariosReferencia() {
        return $this->hasMany('App\EscenarioReferencia', 'HIDR_ID');
    }
}

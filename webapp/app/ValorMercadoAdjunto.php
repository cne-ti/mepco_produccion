<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ValorMercadoAdjunto extends Model
{
    protected $table = 'MEPCO_VALOR_MERCADO_ADJUNTO';

    protected $primaryKey = 'ARCH_ID';

    public $timestamps = false;

	protected $fillable = [
        'VAUS_ID', 
        'ARCH_NOMBRE', 
        'ARCH_DESCRIPCION', 
        'ARCH_MIMETYPE',
        'ARCH_PATH'
    ];

    public function scopeProceso($query, $proc_id)
    {
        return $query->where('VAUS_ID', $proc_id);
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Escenario extends Model 
{
    protected $table='MEPCO_ESCENARIO';

    protected $primaryKey='ESCE_ID';

    public $timestamps=false;

	protected $fillable=[
        'ESCE_FECHA_CREACION',
        'USPE_ID_CREACION',
        'ESCE_FECHA_VALIDADO',
        'USPE_ID_VALIDADO',
        'ESCE_FECHA_ELIMINADO',
        'ESCE_DESCRIPCION_ELIMINADO',
    ];

    protected $attributes = [
        'ESCE_ELIMINADO' => 0,
    ];

    /**
    * Relación 1:N con Proceso
    */
    public function hidrocarburo() {
        return $this->hasOne('App\Hidrocarburo', 'HIDR_ID');
    }

    /**
    * Relación 1:N con la tabla de Escenario paridad
    */
    public function escenariosParidad() {
        return $this->hasMany('App\EscenarioParidad', 'ESCE_ID');
    }

    /**
    * Relación 1:N con la tabla de Escenario referencia
    */
    public function escenariosReferencia() {
        return $this->hasMany('App\EscenarioReferencia', 'ESCE_ID');
    }

    /**
    * Relación 1:N con la tabla de Escenario Cálculo
    */
    public function escenarioCalculo() {
        return $this->hasOne('App\EscenarioCalculo', 'ESCE_ID');
    }

}

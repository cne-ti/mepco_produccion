<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlantillaDocumento extends Model 
{
    protected $table='MEPCO_PLANTILLA_DOCUMENTO';

    protected $primaryKey='PLDO_ID';

    public $timestamps=false;

	protected $fillable=[
        'PLDO_ID',
        'PLDO_NOMBRE', 
        'PLDO_DESCRIPCION',
        'PLDO_MENSAJE', 
        'PLDO_TIPO_PLANTILLA',
    ];
}

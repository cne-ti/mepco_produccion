<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formula extends Model 
{
    protected $table='MEPCO_PARAMETROS_FORMULA';

    protected $primaryKey='PAFO_ID';

    public $timestamps=false;

	protected $fillable=[
        'PAFO_LLAVE',
        'PAFO_NOMBRE', 
        'PAFO_FORMULA',
        'PAFO_DESCRIPCION',
        'PAFO_FECHA_MODIFICACION',
        'PAFO_CANTIDAD_DECIMALES'
    ];

    protected $attributes = [
        'PAFO_ELIMINADO' => 0,
    ];

    public function scopeLlave($query, $llave)
    {
        return $query->where('PAFO_LLAVE', $llave);
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormulaHistorico extends Model 
{
    protected $table='MEPCO_FORMULA_HISTORICO';

    protected $primaryKey='FOHI_ID';

    public $timestamps=false;

    protected $fillable=[
        'PAFO_ID',
        'USPE_ID', 
        'FOHI_LLAVE', 
        'FOHI_NOMBRE',
        'FOHI_FORMULA',
        'FOHI_DESCRIPCION',
        'FOHI_FECHA'
    ];

    /**
    * Relación 1:N con Usuarios
    */
    public function usuario() {
        return $this->hasOne('App\Usuario', 'USPE_ID');
    }

    /**
    * Relación 1:N con Usuarios
    */
    public function formula() {
        return $this->hasOne('App\Formula', 'PAFO_ID');
    }
}

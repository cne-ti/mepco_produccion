<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model 
{
    protected $table='MEPCO_USUA_PERF';

    protected $primaryKey='USPE_ID';

    public $timestamps=false;

	protected $fillable=[
        'USPE_ID',
        'PERF_ID', 
        'USUA_EMAIL', 
        'USPE_ACTIVO',
        'USPE_ULTIMA_CONEXION',
    ];


    protected $attributes = [
        'USPE_ACTIVO' => 0,
        'USPE_ELIMINADO' => 0,
    ];

    public function procesos() 
    {
        return $this->belongsToMany('App\Proceso', 'MEPCO_PROC_USPE', 'USPE_ID', 'PROC_ID')
                    ->withPivot('PRUS_FECHA_CREACION', 
                                'PRUS_FECHA_MODIFICADO',
                                'PRUS_FECHA_VALIDADO', 
                                'PRUS_FECHA_ELIMINADO');
    }

    /**
    * Relación con perfiles a través del email del usuario
    */
    public static function perfiles($usua_email) 
    {
        $perfiles = Usuario::join('MEPCO_PERFIL', 'MEPCO_USUA_PERF.PERF_ID', '=', 'MEPCO_PERFIL.PERF_ID')
                            ->where('USUA_EMAIL', $usua_email)
                            ->where('USPE_ELIMINADO', '=', 0)
                            ->get();
        return $perfiles;
    }

    public function scopePerfil($query, $perfil)
    {
        return $query->where('PERF_ID', $perfil);
    }

    public function scopeVigente($query)
    {
        return $query->where('USPE_ELIMINADO', '=', 0);
    }

    /**
    * Relación 1:N con VAME USPE
    */
    public function vameUspe() {
        return $this->hasMany('App\VameUspe', 'USPE_ID', 'USPE_ID');
    }

    /**
    * Relación 1:N con Log
    */
    public function log() {
        return $this->belongsToMany('App\Log', 'USPE_ID');
    }

    /**
    * Relación 1:N con Variable Histórico (log de variable)
    */
    public function variableHistorico() {
        return $this->belongsToMany('App\VariableHistorico', 'USPE_ID');
    }

    /**
    * Relación 1:N con Fórmula Histórico (log de fórmula)
    */
    public function formulaHistorico() {
        return $this->belongsToMany('App\FormulaHistorico', 'USPE_ID');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VariableHistorico extends Model 
{
    protected $table='MEPCO_VARIABLE_HISTORICO';

    protected $primaryKey='VAHI_ID';

    public $timestamps=false;

	protected $fillable=[
        'PAVA_ID',
        'USPE_ID', 
        'VAHI_LLAVE', 
        'VAHI_NOMBRE',
        'VAHI_VALOR',
        'VAHI_DESCRIPCION',
        'VAHI_FECHA'
    ];

    /**
    * Relación 1:N con Usuarios
    */
    public function usuario() {
        return $this->hasOne('App\Usuario', 'USPE_ID');
    }

    /**
    * Relación 1:N con Usuarios
    */
    public function variable() {
        return $this->hasOne('App\Variable', 'PAVA_ID');
    }
}

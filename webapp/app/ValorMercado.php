<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ValorMercado extends Model 
{
    protected $table='MEPCO_VALOR_MERCADO';

    protected $primaryKey='VAME_ID';

    public $timestamps=false;

    protected $fillable=[
        'VAUS_ID',
        'VAME_FECHA',
        'VAME_GAS_UNL_87',
        'VAME_GAS_UNL_93',
        'VAME_LD_DIESEL',
        'VAME_USG_BUTANO',
        'VAME_UKC_USG',
        'VAME_BRENT',
        'VAME_BRENT_CORREGIDO',
        'VAME_BRENT_M1',
        'VAME_BRENT_M2',
        'VAME_BRENT_M3',
        'VAME_BRENT_M4',
        'VAME_BRENT_M5',
        'VAME_BRENT_M6',
        'VAME_BRENT_CORREGIDO_M1',
        'VAME_BRENT_CORREGIDO_M2',
        'VAME_BRENT_CORREGIDO_M3',
        'VAME_BRENT_CORREGIDO_M4',
        'VAME_BRENT_CORREGIDO_M5',
        'VAME_BRENT_CORREGIDO_M6',
        'VAME_PPT_USG',
        'VAME_MONT_BELVIEU_1',
        'VAME_MONT_BELVIEU_2',
        'VAME_MONT_BELVIEU',
        'VAME_CIF_ARA_1',
        'VAME_CIF_ARA_2',
        'VAME_CIF_ARA',
        'VAME_WTI',
        'VAME_WTI_M1',
        'VAME_WTI_M2',
        'VAME_WTI_M3',
        'VAME_WTI_M4',
        'VAME_WTI_M5',
        'VAME_WTI_M6',
        'VAME_TC_DOLAR',
        'VAME_TC_PUBLICADO',
        'VAME_LIBOR',
        'VAME_TARIFA_DIARIA_82000',
        'VAME_TARIFA_DIARIA_59000',
        'VAME_IFO_380_HOUSTON',
        'VAME_MDO_HOUSTON',
        'VAME_IFO_380_CRISTOBAL',
        'VAME_MDO_CRISTOBAL',
        'VAME_UTM'
    ];

    public function scopeProceso($query, $proc_id)
    {
        return $query->where('PROC_ID', $proc_id);
    }
}

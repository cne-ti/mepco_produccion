@extends('public-layout')
@section('content')

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-10 col-xs-10">
                                <h4>Perfiles
                                <button type="button" class="btn btn-primary" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalNew">
                                Nuevo <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                </button></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table id="list" class="table table-hover table-bordered dataTable">
                        <thead>
                            <tr>
                                <th>
                                    Nombre
                                </th>
                                <th>
                                    Descripción
                                </th>
                                <th style="min-width:100px;">
                                    Opciones
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($perfiles as $perfil)
                                <tr>
                                    <td>{{ $perfil->PERF_NOMBRE }}</td>
                                    <td>{{ $perfil->PERF_DESCRIPCION }}</td>
                                    <td>
                                        <button type="button" id="editar" name="editar" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#modalEdit" data-id={{ $perfil->PERF_ID }}>
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </button>
                                        <button type="button" id="eliminar" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalDelete" data-id={{ $perfil->PERF_ID }}>
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- NUEVO -->
    @include('perfil.partials.modal-new')

    <!-- EDITAR-->
    @include('perfil.partials.modal-edit')

    <!-- ELIMINAR -->
    @include('perfil.partials.modal-delete')

@endsection                  

@section('script')
    <script>
        $(document).ready(function() {
            // Marcar la pestaña en el menú superior
            activeMenuItem('perfil', 'configuracion');
            
            var targetPdf = "{{ route('perfil/exportarAPdf')}}";
            var targetXls = "{{ route('perfil/exportarAExcel')}}";

            @include('partials.btn-exportar');
        }); 
    </script>
@endsection

<div class="form-group">
    <div class="col-md-5 lblObligatorio"></div>
</div>
<div class="form-group required">
    <label class="col-md-4 control-label">Nombre</label>
    <div class="col-md-6">
        <input type="text" class="form-control" id="perf_nombre" name="perf_nombre" />
    </div>
</div>
<div class="form-group required">
    <label class="col-md-4 control-label">Descripción</label>
    <div class="col-md-6">
        <textarea class="form-control" id="perf_descripcion" name="perf_descripcion" maxlength="255"></textarea>
        <div class="numChar"></div>
    </div>
</div>

<table class="table table-bordered table-hover">
	<thead>
	    <tr>
	        <th>Nombre del Contenido</th>
	        <th>¿Asignado a perfil?</th>
	    </tr>
    </thead>
    <tbody>
		@foreach ($contenido as $item)
			<tr>
	       		<td>{{ $item->COPO_NOMENCLATURA }}</td>
	       		<td><input type="checkbox" name="check[{{ $item->COPO_ID }}]" class="checkSiNo" target-input="{{ $item->COPO_NOMENCLATURA }}"></td>
			</tr>
		@endforeach
	</tbody>
</table>

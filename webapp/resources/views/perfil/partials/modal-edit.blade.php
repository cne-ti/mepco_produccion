<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar Perfil</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-edit">
                    <!-- Identificador del perfil -->
                    <input type="hidden" name="perf_id" id="perf_id">
                    <!-- Errores en formulario -->
                    @include('partials.error')
                    <!-- Añadir campos -->
                    @include('perfil.partials.fields')
                </form>
                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary loading-message" id="btnGuardar">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#modalEdit .checkSiNo').on('switchChange.bootstrapSwitch', function() {
        var seleccionados = 0;
        $('#modalEdit .checkSiNo').each(function(i) {
            if ($(this).bootstrapSwitch('state') == true) {
                seleccionados++;   
            }
        });
        if (seleccionados>0) {
            $('#modalEdit #btnGuardar').attr('disabled', false);
        }
        else {
            $('#modalEdit #btnGuardar').attr('disabled', true);
        }
    });

    $('#modalEdit').on('show.bs.modal', function (e) {

        $("#form-edit input:checkbox").bootstrapSwitch('state',false);
        $('.numChar').text('');
        $('#form-edit #perf_descripcion').on('keyup', numChar);

        $.ajax({
            url: 'perfil/' + $(e.relatedTarget).attr('data-id')+'/edit',
            type: 'GET',
            data: { 
                '_token' : $('#_token').val() 
            },
            success: function(data) {
                loadingMessageHide();
                if(data.status == "success"){

                    var perfil = data.perfil;
                    var contenido = data.contenido;

                    $('#form-edit #errors').hide();
                    $('#form-edit #perf_id').val($(e.relatedTarget).attr('data-id'));
                    $('#form-edit #perf_nombre').val(perfil.PERF_NOMBRE);
                    $('#form-edit #perf_descripcion').val(perfil.PERF_DESCRIPCION);

                    for (var i=0;i<contenido.length;i++) {
                        var copo_id = contenido[i].COPO_ID;
                        $("[name='check["+copo_id+"]']").bootstrapSwitch('state',true);
                    }
                }else{
                    errorProcess(data.status);
                }
            },
            error: function(e) {
                console.log(e.message);
            }
        });
    });

    $('#modalEdit #btnGuardar').click(function(){
        var formData = $('#form-edit').serialize();
        $.ajax({
            url: 'perfil/' + $('#form-edit #perf_id').val(),
            type: 'PUT',
            data: { 
                'formData' : formData,
                '_token' : $('#modalEdit #_token').val()
            },
            success: function(data) {
                loadingMessageHide();
                if(data.status == 'success'){
                    $('#form-edit #errors').hide();
                    $('#modalEdit').modal('hide');
                    location.reload();
                }else{
                    errorProcess('form-edit', data.status);
                }
            },
            error: function(e) {
                console.log(e.message);
            }
        });
    });
  </script>
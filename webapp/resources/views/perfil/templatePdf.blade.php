<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<img src='images/Logo_Oficinal_CNE.png'>
<br/>

<h3>Listado de Perfiles</h3>
<table class="table table-hover col-xs-10 table-bordered" id="perfiles" cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center">
    <thead style="background-color:#f5f5f5;">
        <tr>
            <th>
                Identificador
            </th>
            <th>
                Nombre
            </th>
            <th>
                Descripción
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($perfiles as $perfil)
            <tr>
                <td style="border:1px solid #ddd;">{{ $perfil->PERF_ID }}</td>
                <td style="border:1px solid #ddd;">{{ $perfil->PERF_NOMBRE }}</td>
                <td style="border:1px solid #ddd;">{{ $perfil->PERF_DESCRIPCION }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

<br/>
<br/>
<h5>Fecha del reporte: {!! date('d-m-Y') !!}</h5>


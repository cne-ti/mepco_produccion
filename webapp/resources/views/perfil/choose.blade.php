<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Comisión Nacional de Energía</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=9" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" type="image/x-icon" href={{ asset('images/gobcl-favicon.ico') }}  />
        <link href={{ asset('css/bootstrap.min.css') }} rel="stylesheet" media="screen">
        <link href={{ asset('css/app.css') }} rel="stylesheet" media="screen">
        <link href={{ asset('css/font-awesome.min.css') }} rel="stylesheet" media="screen">
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->   
        <script src={{ asset('js/jquery.js') }}></script>
        <script src={{ asset('js/jquery-ui.min.js') }}></script>
        <script src={{ asset('js/bootstrap.min.js') }}></script>
        <script src={{ asset('js/bootstrap-switch.min.js') }}></script>
        <script src={{ asset('js/datatables.min.js') }}></script>
        <script src={{ asset('js/jquery.formatNumber.js') }}></script>
        <script src={{ asset('js/init.js') }}></script>
    </head>
    <body>
        <div class="container">
            <div class="row">

                <!-- LOADING MESSAGE -->
                @include('partials.loading-message')
                
                <div class="col-md-offset-3 col-md-5 col-xs-offset-1 col-xs-10 login">
                    <h2>Seleccionar Perfil</h2>
                    <hr>
                    <form id="choose-profile" method="POST" action={{route('auth/authenticate')}}>
                        <div class="list-group">
                            @foreach ($perfiles as $item)

                                @if ($item->PERF_ID == Util::PERFIL_USUARIO_ADMINISTRADOR)
                                    <button type="submit" class="btn btn-default loading-message list-group-item" id="{{ $item->PERF_ID }}" style="width:100%; text-align:left">
                                        @include('partials.icon.administrador')
                                        {{ $item->PERF_NOMBRE }}
                                    </button>
                                @elseif ($item->PERF_ID == Util::PERFIL_USUARIO_GENERADOR)
                                    @if ($generador_conectado == 0)
                                        <button type="submit" class="btn btn-default loading-message list-group-item" aria-label="Left Align" id="{{ $item->PERF_ID }}" style="width:100%; text-align:left">
                                            @include('partials.icon.generador')
                                            {{ $item->PERF_NOMBRE }}
                                        </button>
                                    @else
                                        <button type="button" disabled class="btn btn-default loading-message list-group-item" aria-label="Left Align" id="{{ $item->PERF_ID }}" style="width:100%; text-align:left; background:#fcf8e3; white-space:normal">
                                                @include('partials.icon.generador')
                                                {{ $item->PERF_NOMBRE }}
                                                <br/>
                                                No se puede iniciar sesión como usuario Generador ya que actualmente hay un usuario generador conectado. Si tiene dudas póngase en contacto con el Administrador del sistema.
                                        </button>
                                    @endif
                                @elseif ($item->PERF_ID == Util::PERFIL_USUARIO_VALIDADOR)
                                    <button type="submit" class="btn btn-default loading-message list-group-item" id="{{ $item->PERF_ID }}" style="width:100%; text-align:left">
                                        @include('partials.icon.validador')
                                        {{ $item->PERF_NOMBRE }}
                                    </button>
                                @else
                                    <button type="submit" class="btn btn-default loading-message list-group-item" aria-label="Left Align" id="{{ $item->PERF_ID }}" style="width:100%; text-align:left">
                                        @include('partials.icon.otros')
                                        {{ $item->PERF_NOMBRE }}
                                    </button>
                                @endif

                            @endforeach

                                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="perf_id" id="perf_id">
                                <input type="hidden" name="usua_email" id="usua_email" value="{{ $usua_email }}">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>


<script type="text/javascript">

$('#choose-profile').submit(function ( event ) {
    $('#perf_id').val($('#choose-profile').attr('submited'));
});

$(':submit').click(function ( event ) {
    $('#choose-profile').attr('submited', this.id);
});

</script>

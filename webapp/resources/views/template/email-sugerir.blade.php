<center>
	<table style="font-size: 9pt; background-color: #e6e6e6; width:100%; font-family: \'Open Sans\',sans-serif; margin:0; border:0;color: #606060">
		<thead>
			<tr>
				<th style="text-align: left;">
					<img src="http://www.cne.cl/cdn/logo-final.png" style="float: left;margin-left: 5%;">
					<h1 style="margin:0;"></h1>
				</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="padding-top:30px;" colspan="2">
					<table style="background-color:#ffffff;border-collapse:separate!important;border-radius:4px;width:90%;margin:auto;">
						<tr>
							<td style="padding-top: 5px;padding-bottom: 50px;">
								<h4>Estimad@s:</h4>
								<p>Se han sugerido los calculos para cada combustible, favor revisar
								<p>Saludos cordiales.</p>
								<div style="font-size: 9pt;">
									<img src="http://www.cne.cl/cdn/pie_firma.png"><br>
									<span style="font-weight: 700">Comisi&oacute;n Nacional de Energ&iacute;a I Gobierno de Chile</span><br>
									<span>(T) +562 27972600</span><br>
									<span>(T) +562 27972655</span><br>
									<span><a href="http://www.cne.cl">www.cne.cl</a></span>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td style="text-align:center; font-family: Helvetica,Arial,sans-serif;font-size: 13px;line-height: 125%;color: #606060;">
					<p style="margin-top: 3%;">Comisi&oacute;n Nacional de Energ&iacute;a - Tel. (2) 2797 2600, Fax. (2) 2797 2627, Miraflores 222 - Piso 10, Santiago - Chile<br>Gobierno de Chile</p>
				</td>
			</tr>
		</tfoot>
	</table>
</center>
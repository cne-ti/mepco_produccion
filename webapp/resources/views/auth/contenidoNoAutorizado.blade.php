@extends('public-layout')
@section('content')

	<div class="row">
	    <div class="col-md-12 col-xs-12">
	        <div class="panel panel-default">
	        	<div class="panel-heading">
			        <h4>Acceso no autorizado</h4>
	        	</div>
	        	<div class="panel-body">
					<div class="alert alert-danger col-md-offset-3 col-md-6" style="text-align: justify">
						<center><i class='fa fa-exclamation-triangle fa-4x'></i></center><br><br>
						No tiene permiso para acceder a la página solicitada. Si posee más de un perfil intente cambiar al perfil correspondiente o póngase en contacto con el administrador del sistema.
					</div>
	        	</div>
	        </div>
        </div>
    </div>

@endsection
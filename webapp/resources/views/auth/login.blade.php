<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Comisión Nacional de Energía</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=9" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" type="image/x-icon" href={{ asset('images/gobcl-favicon.ico') }}  />
        <link href={{ asset('css/bootstrap.min.css') }} rel="stylesheet" media="screen">
        <link href={{ asset('css/app.css') }} rel="stylesheet" media="screen">
        <!-- <link href="css/login.css" rel="stylesheet" media="screen"> -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->   
        <script src={{ asset('js/jquery.js') }}></script>
        <script src={{ asset('js/bootstrap.min.js') }}></script>

        <script type="text/javascript">
            $(document).ready(function() {
                // $('#email').val('demo@mepco.com');
                // $('#password').val('demodemo');

                var email = $('#email').val();
                if(email !== '')
                {
                    $('#password').focus();

                }else
                {
                    $('#email').focus();
                }
            });
        </script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                
                <div class="col-md-offset-3 col-md-6 col-xs-offset-1 col-xs-10 login">
                    <h2>Ingreso de usuarios</h2>
                    <hr>
                    <form  method="POST" action={{ route('auth/chooseProfile') }}>
                        {!! csrf_field() !!}
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="col-md-4" style="background-color: #fff">
                            <img src={{ asset('images/Logo_Oficinal_CNE.png') }} class="img-responsive">
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="inputEmail3">Usuario</label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email" value={{ old('email') }}>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3">Contraseña</label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                            </div>
                            <div class="checkbox">
                                <label>
                                <input type="checkbox" name="remember"> Recordarme
                                </label>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Ingresar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
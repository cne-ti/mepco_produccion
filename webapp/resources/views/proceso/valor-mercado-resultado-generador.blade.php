@extends('public-layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <script type="text/javascript">
                        var btnHabilitar='ValorDeMercado';
                    </script>
                    @include('proceso.partials.menu-proceso')
                </div>
                <div class="panel-body">
                    <div class="col-md-8 col-md-offset-2">
                    	<div class="alert alert-success">
                    	    Valores de mercado cargados exitosamente, se ha enviado email de notificación a 
                    	    los siguientes validadores: 
                    	    <br><ul>
                                @foreach ($listEmailValidadores as $item)
                                    <li>{{ $item }}</li>
                                @endforeach
                            </ul>
                    	</div>
                    	
	                    @if (count($warnings) > 0)
	                        <div class="alert alert-warning">
	                            Se han encontrado algunas advertencias, pero el documento fue cargado con <b>éxito</b>
	                            <br><ul>
	                                @foreach ($warnings as $item)
	                                    <li>{{ $item }}</li>
	                                @endforeach
	                            </ul>
	                        </div>
	                    @endif
                    </div>
                    <div class="col-md-2 col-md-offset-8" style="text-align: right">
                        <a href="{{ route('proceso/solver', ['id' => $proceso->PROC_ID]) }}" class="btn btn-primary loading-message">
                            Aceptar
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection  

@section('script')
    <script type="text/javascript">

        activeMenuItem('proceso');

    </script>
@endsection

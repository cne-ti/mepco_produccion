@extends('public-layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <script type="text/javascript">
                        var btnHabilitar='ValorDeMercado';
                    </script>
                    @include('proceso.partials.menu-proceso')
                </div>
                <div class="panel-body">

                    @if($valores['valoresMercadoValidadoOk'])
                        
                        <div class="col-md-12">
                            <div class="alert alert-success " style="text-align: center">
                                <center><i class='fa fa-hand-paper-o  fa-4x'></i></center><br>
                                Valores de mercado validados correcamente 
                            </div>
                        </div>
                        
                        <!-- MOSTRAR VM INGRESADOS -->
                        <div id="holderListValorMercadoAdjunto">
                            <div class="col-md-12">
                                <h4>Archivos asociados al proceso</h4>
                                <div id="listValorMercadoAdjunto" class="list-group"></div>
                            </div>
                            <div class="col-md-2">
                                @if($valores['perfilUsuario'] == Util::PERFIL_USUARIO_GENERADOR &&
                                    $valores['ESTADO_ACTUAL'] != Util::PROCESO_ESTADO_FINALIZADO)
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalConfirmReingresoVm" data-id={{ $proceso->PROC_ID }}>
                                        Reingresar valores de mercado
                                    </button>
                                @endif
                            </div>
                            <div class="col-md-4 col-md-offset-6" style="text-align: right">
                                <a href="{{ route('proceso/validadores', ['id' => $proceso->PROC_ID]) }}" class="btn btn-default loading-message">
                                    Anterior
                                </a>
                                <a href="{{ route('proceso/solver', ['id' => $proceso->PROC_ID]) }}" class="btn btn-primary loading-message">
                                    Siguiente
                                </a>
                            </div>
                        </div>
                    @else
                        @if((count($valores['listValorMercadoAdjunto']) > 0) && ($valores['perfilUsuario'] == Util::PERFIL_USUARIO_GENERADOR))
                            <div id="holderListValorMercadoAdjunto">
                                <div class="col-md-12">
                                    <h4>Archivos asociados al proceso</h4>
                                    <div id="listValorMercadoAdjunto" class="list-group"></div>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalConfirmReingresoVm" data-id={{ $proceso->PROC_ID }}>
                                        Reingresar valores de mercado
                                    </button>
                                </div>
                                <div class="col-md-4 col-md-offset-6" style="text-align: right">
                                    <a href="{{ route('proceso/validadores', ['id' => $proceso->PROC_ID]) }}" class="btn btn-default loading-message">
                                        Anterior
                                    </a>
                                    <a href="{{ route('proceso/solver', ['id' => $proceso->PROC_ID]) }}" class="btn btn-primary loading-message">
                                        Siguiente
                                    </a>
                                </div>
                            </div>
                        @elseif($valores['perfilUsuario'] == Util::PERFIL_USUARIO_GENERADOR)
                            <!-- PERMITIR INGRESO DE VM -->
                            <div class="alert alert-warning" style="text-align:center">
                                <span>Por favor, recuerde introducir las fechas de valores de mercado en orden ascendente.</span>
                            </div>

                            <form id="form-vm" class="form-horizontal lblObligatorio" role="form"  method="POST" action="{{route('proceso/valorMercadoRequest')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="proc_id" value="{{ $proceso->PROC_ID }}">

                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        Por favor corrige los siguientes errores:<br><br>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <div class="form-group required">
                                    <label for="valorMercado" class="col-md-3 control-label">Cargar Valores de Mercado (.xls, .xlsx) </label>
                                    <div class="col-md-9">
                                        <input type="file"  class="form-control" id="valorMercado" name="valorMercado" style="height: auto;">
                                        <!-- accept=".xls,.xlsx" -->
                                        <span style="font-style: italic; font-size: 0.9em;color: #a5a4a4;">Tamaño máximo de archivo 2MB</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Documentos de respaldo</label>
                                    <div class="col-md-9">
                                        <input type="file" class="form-control" name="documentos[]" style="height: auto;" multiple="multiple">
                                        <span style="font-style: italic; font-size: 0.9em;color: #a5a4a4;">Tamaño máximo de archivo 2MB</span>
                                    </div>
                                </div>
                            
                                <div class="form-group" style="padding-top: 20px">
                                    <div class="col-md-4 col-md-offset-8" style="text-align: right">
                                        <a href="{{ route('proceso/validadores', ['id' => $proceso->PROC_ID]) }}"class="btn btn-default loading-message">
                                            Anterior
                                        </a>
                                        <button type="submit" class="btn btn-primary loading-message">
                                            Siguiente
                                        </button>
                                    </div>
                                </div>
                            </form>
                        @else
                            @if($valores['perfilUsuario'] == Util::PERFIL_USUARIO_VALIDADOR)

                                <!-- MOSTRAR MENSAJE ALERTA -->

                                @if(count($valores['listValorMercadoAdjunto']) == 0)
                                    <div class="col-md-12">
                                        <div class="alert alert-danger " style="text-align: center">
                                            <center><i class='fa fa-hand-paper-o  fa-4x'></i></center><br>
                                            Generador aún no ha cargado valores de mercado
                                        </div>
                                    </div>
                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                Se econtraron los siguientes errores:<br><br>
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                @else
                                    <div class="col-md-12">
                                        <div class="alert alert-danger " style="text-align: center">
                                            <center><i class='fa fa-hand-paper-o  fa-4x'></i></center><br>
                                            Favor cargar valores de mercado para validar contra generador
                                        </div>
                                    </div>
                                    <!-- PERMITIR INGRESO DE VM -->

                                    <form id="form-vm" class="form-horizontal" role="form"  method="POST" action="{{route('proceso/valorMercadoRequest')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="proc_id" value="{{ $proceso->PROC_ID }}">

                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                Por favor corrige los siguientes errores:<br><br>
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif

                                        <div class="form-group required">
                                            <label for = 'carga' class="col-md-3 control-label">Cargar Valores de Mercado (.xls, .xlsx) </label>
                                            <div class="col-md-9">
                                                <input type="file" accept=".xls,.xlsx" class="form-control" name="valorMercado" style="height: auto;">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Documentos de respaldo</label>
                                            <div class="col-md-9">
                                                <input type="file" class="form-control" name="documentos[]" style="height: auto;" multiple="multiple">
                                            </div>
                                        </div>
                                    
                                        <div class="form-group" style="padding-top: 20px">
                                            <div class="col-md-4 col-md-offset-8" style="text-align: right">
                                                <a href="{{ route('proceso/validadores', ['id' => $proceso->PROC_ID]) }}"class="btn btn-default loading-message">
                                                    Anterior
                                                </a>
                                                <button type="submit" class="btn btn-primary loading-message">
                                                    Siguiente
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                @endif
                            @endif
                        @endif
                    @endif
                    <div style="padding-top: 10px; padding-left: 16px; clear: both; font-style: italic">
                        <a  href="{{ asset('docs/PLANTILLA_VALORES_MERCADO_VACIA.xlsx') }}"><i class="fa fa-download"></i>&nbsp;Descargar plantilla valores de mercado</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalConfirmReingresoVm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Advertencia</h4>
                </div>
                <form class="form-horizontal" action="{{ route('proceso/reingresoVmRequest') }}" method="POST">
                    <div class="modal-body">
                        Al reingresar los valores de mercado se eliminarán todos los escenarios creados anteriormente. <br><br>
                        ¿Desea reingresar los valores de mercado?
                        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="proc_id" value="{{ $proceso->PROC_ID }}">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Reingresar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection  

@section('script')
    <script type="text/javascript">

        activeMenuItem('proceso');

        /***
        * SI EXISTE VALOR LLENA CONTROLES
        */
        @if(isset($valores) && $valores['listValorMercadoAdjunto'] != null)
            var list = {!! $valores['listValorMercadoAdjunto'] !!};
            if(list.length > 0)
            {
                $('#holderListValorMercadoAdjunto').show();
                $.each(list, function(i, item) {
                     $('#listValorMercadoAdjunto').append('<a href="' + list[i].ARCH_LINK + '" class="list-group-item">' + list[i].ARCH_NOMBRE + '</a>'); 
                });
            }
        @endif

    </script>
@endsection

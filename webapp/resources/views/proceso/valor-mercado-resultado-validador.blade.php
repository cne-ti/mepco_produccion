@extends('public-layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <script type="text/javascript">
                        var btnHabilitar='ValorDeMercado';
                    </script>
                    @include('proceso.partials.menu-proceso')
                </div>
                <div class="panel-body">
                    @if ($validadoOk)
                        <div class="col-md-8 col-md-offset-2">
                            <div class="alert alert-success">
                                Valores de mercado cargados son los mismos que fueron ingresados por el generador : <b>{{ $emailGenerador->USUA_EMAIL }}</b>
                            </div>  
                        </div>
                        <div class="col-md-2 col-md-offset-8" style="text-align: right">
                            <a href="{{ route('proceso/solver', ['id' => $proceso->PROC_ID]) }}" class="btn btn-primary loading-message">
                                Aceptar
                            </a>
                        </div>
                    @else
                        <div class="col-md-8 col-md-offset-2">
                            <div class="alert alert-danger">
                                Los valores de mercado ingresados no corresponden
                                 a los ingresador por generador: <b>{{ $emailGenerador->USUA_EMAIL }}</b>
                                <br>
                                <br>
                                Se ha eliminado todos los escenarios creados anteriormente con los valores de mercado
                                <br>
                                <br>
                                Celdas con diferencia en valor son las siguientes: <br>
                                <ul>
                                    @foreach ($listaError as $key => $value)
                                        @if ($value != null)
                                            <li>Fecha: {{ $key }}</li>
                                            <ul>
                                            @foreach ($value as $item)  

                                                <li>{!! $item !!}</li>

                                            @endforeach
                                            </ul>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-2 col-md-offset-8" style="text-align: right">
                            <a href="{{ route('proceso/valorMercado', ['id' => $proceso->PROC_ID]) }}" class="btn btn-primary loading-message">
                                Aceptar
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection  

@section('script')
    <script type="text/javascript">

        activeMenuItem('proceso');

    </script>
@endsection

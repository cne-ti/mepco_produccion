@extends('public-layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <script type="text/javascript">
                        var btnHabilitar='SeleccionarValidadores';
                    </script>
                    @include('proceso.partials.menu-proceso')
                </div>
                <div class="panel-body">   
                    <form class="form-horizontal lblObligatorio" role="form"  method="POST" action = {{route('proceso/validadoresRequest')}}>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="proc_id" value="{{ $proceso->PROC_ID }}">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                Por favor corrige los siguientes errores:<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group required">
                            <label class="col-md-3 control-label">Fecha vigencia</label>
                            <div class="col-md-2">
                                <input name="fecha" id="fecha" type="text" class="form-control fecha-vigencia" value=" {{ old('fecha') }}" style="width: 106px">
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-md-3 control-label">Seleccion de validadores</label>
                            <div class="col-md-6">
                                <select class="form-control" multiple="multiple" name="validadores[]" id="validadores">
                                    @foreach ($usuariosValidadores as $item)
                                        <option value="{{ $item->USPE_ID }}">{{ $item->USUA_EMAIL }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group" style="padding-top: 20px" id="holderBtnSiguiente">
                            <div class="col-md-offset-10 col-md-2" style="text-align: right">
                                <button type="submit" class="btn btn-primary loading-message"> Siguiente</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection 

@section('script')
    <script type="text/javascript">
        activeMenuItem('proceso');

        /***
        * SI EXISTE VALOR LLENA CONTROLES
        */
        @if(isset($valores) && $valores['fecha'] != null)
            var fecha = '{!! $valores['fecha'] !!}';
            var validadoresSeleccionados = {!! $valores['validadoresSeleccionados'] !!};

            $('#fecha').val(fecha);
            $('#fecha').prop('readonly','readonly');
            $('#fecha').removeClass('fecha-vigencia');
            $("#validadores > option").each(function() {
                var obj = this;
                $.each(validadoresSeleccionados, function(i, item) {
                    if(!obj.selected)
                    {
                        obj.selected = (obj.value == validadoresSeleccionados[i].USPE_ID);
                    }
                });
            });

            var esValidador = {!! $valores['esValidador'] !!};
            var estadoActual = {!! $valores['ESTADO_ACTUAL'] !!};

            if(esValidador || estadoActual == {{Util::PROCESO_ESTADO_FINALIZADO}})
            {
                $("#validadores").attr("readonly", "readonly");
                $("#validadores").attr("disabled", "disabled");
                $("#holderBtnSiguiente").hide();
            }


        @endif

        $(document).ready(function() {
            $('#fecha').datepicker('option', 'minDate', '{{ $fechaHistorico }}');
        });

    </script>
@endsection

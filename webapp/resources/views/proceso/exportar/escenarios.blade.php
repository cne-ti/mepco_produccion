<table>
    <thead>
        <tr>
            <th><span class="text-lowercase">n</span></th>
            <th><span class="text-lowercase">m</span></th>
            <th><span class="text-lowercase">s</span></th>
            <th><span class="text-lowercase">t</span></th>
            <th><span class="text-lowercase">f</span></th>
            <th>P.Paridad</th>
            <th>Tipo crudo</th>
            <th>Crudo BRENT</th>
            <th>Crudo WTI</th>
            <th>Margen</th>
            <th>P.Ref Inf</th>
            <th>P.Ref Int</th>
            <th>P.Ref Sup</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($listEscenarios as $item)
            <tr>
                <td>{{$item->ESCA_N}}</td>
                <td>{{$item->ESCA_M}}</td>
                <td>{{$item->ESCA_S}}</td>
                <td>{{$item->ESCA_T}}</td>
                <td>{{Util::formatNumber($item->ESCA_F)}}</td>
                <td>{{Util::formatNumber($item->ESCA_PARIDAD)}}</td>
                <td>{{$item->ESCA_TIPO_CRUDO}}</td>
                <td>{{Util::formatNumber($item->ESCA_CRUDO_BRENT)  }}</td>
                <td>{{Util::formatNumber($item->ESCA_CRUDO_WTI)  }}</td>
                <td>{{Util::formatNumber($item->ESCA_MARGEN) }}</td>
                <td>{{Util::formatNumber($item->ESCA_PREFINF)}}</td>
                <td>{{Util::formatNumber($item->ESCA_PREFINT)}}</td>
                <td>{{Util::formatNumber($item->ESCA_PREFSUP)}}</td>
            </tr>
        @endforeach
    </tbody>
</table>
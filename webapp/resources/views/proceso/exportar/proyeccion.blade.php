<table>
    <thead>
        <tr>
            <th>Fecha</th>
            <th>P.REF INF</th>
            <th>P.REF INT</th>
            <th>P.REF SUP</th>
            <th>Paridad</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($valores as $item)
            <tr>
                <td>{{Util::dateformat($item->PROY_FECHA, 'd/m/Y')}}</td>
                <td>{{Util::formatNumber(($item->PROY_RE_PARIDAD * 0.95))}}</td>
                <td>{{Util::formatNumber($item->PROY_RE_PARIDAD)}}</td>
                <td>{{Util::formatNumber(($item->PROY_RE_PARIDAD * 1.05))}}</td>
                <td>{{Util::formatNumber($item->PROY_PA_PARIDAD)}}</td>
            </tr>
        @endforeach
    </tbody>
</table>
@extends('public-layout')
@section('content')

            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="container-fluid">
                                <div class="row">
                                    <form action={{ route('proceso/nuevoRequest') }} method="POST" id="form-search">
                                        {!! csrf_field() !!}
                                        <div class="col-md-6 col-xs-6">
                                            <h4>Lista de Procesos de Cálculo
                                            <button type="submit" class="btn btn-primary loading-message">
                                            Nuevo <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                            </button></h4>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover col-xs-10 table-bordered dataTable" id="tablaProceso">
                                <thead>
                                    <tr>
                                        <th>
                                            Identificador de Proceso
                                        </th>
                                        <th>
                                            Fecha de Inicio
                                        </th>
                                        <th>
                                            Fecha de Fin
                                        </th>
                                        <th>
                                            Fecha de Vigencia
                                        </th>
                                        <th>
                                            Estado
                                        </th>
                                        <th>
                                            Opciones
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($procesos as $item)
                                        <tr>
                                            <td>{{ $item->PROC_ID }}</td>
                                            <td>{{ \App\Util::dateFormat($item->PROC_FECHA_INICIO_DATO_ORIGINAL, 'd/m/Y') }}</td>
                                            <td>{{ \App\Util::dateFormat($item->PROC_FECHA_FIN_DATO_ORIGINAL, 'd/m/Y') }}</td>
                                            <td>{{ \App\Util::dateFormat($item->PROC_FECHA_VIGENCIA, 'd/m/Y') . ' Hrs' }}</td>
                                            <td>{{ $item->PRES_NOMBRE }}</td>
                                            @if($item->PRES_ID == Util::PROCESO_ESTADO_FINALIZADO)
                                                <td>
                                                    <a href="{{ route('proceso/editar', ['id' => $item->PROC_ID]) }}" class="btn btn-default loading-message">
                                                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                                    </a>
                                                </td>
                                            @else
                                                <td>
                                                    <a href="{{ route('proceso/editar', ['id' => $item->PROC_ID]) }}" class="btn btn-default loading-message">
                                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                                    </a>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
@endsection                  

@section('script')
    <script>
        $(document).ready(function() {
            
            var targetPdf = "";
            var targetXls = "";

            activeMenuItem('proceso');

        });
    </script>
@endsection

@extends('public-layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <script type="text/javascript">
                        var btnHabilitar='Solver';
                    </script>
                    @include('proceso.partials.menu-proceso')
                </div>
                <div class="panel-body" style="padding: 15px 0 15px 0">
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="proc_id" id="proc_id" value="{{ $proceso->PROC_ID }}">

                    @if(!$puedeCalcularPublicar)
                        <div class="alert alert-danger" style="margin: 0 5px 14px 5px;clear: both">
                            Debe ingresar valores de mercado antes de generar cálculos
                        </div>
                    @endif

                    <form class="form-inline" style="padding-left: 10px">
                        <div class="form-group">
                            <label for="exampleInputEmail3">Fecha Vigencia</label>
                            <input id="fechaVigencia" name="fechaVigencia" type="text" disabled="disabled" class="fecha form-control" style="width: 106px">
                        </div>
                        <div class="form-group" style="padding-left: 10px">
                            <label for="tipo">Tipo crudo</label>
                            <input type="checkbox" id="tipoCrudo" name="tipoCrudo" checked="true" class="checkBrentWti">
                        </div>
                    </form>
                    <div class="panel panel-primary panel-mod" style="margin: 4px;">
                        <div class="panel-heading">
                            Gasolina 93 
                        </div>
                        <div class="panel-body" style="padding: 4px">
                            <div class="col-md-12 col-xs-12" style="padding: 2px 0 4px;">
                                <form id="form_gas_87">
                                {!! csrf_field() !!}

                                    <!-- WARNING TIEMPO -->
                                    <div class="alert alert-warning" id="form_gas_87-warning" style="text-align: center; display: none">
                                        <div id="form_gas_87-spin">
                                            <i class="fa fa-refresh fa-spin"></i>
                                            Calculando tiempo ejecución...
                                        </div>
                                        <span id="form_gas_87-lblTiempo"></span>
                                    </div>
                                    <!-- FIN WARNING TIEMPO -->

                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th><strong>Valores</strong></th>
                                                <td>Mínimo</td>
                                                <td>Máximo</td>
                                                <th><strong>Parámetros</strong></th>
                                                <td>n</td>
                                                <td>m</td>
                                                <td>s</td>
                                                <td>t</td>
                                                <td>f</td>
                                                <th><strong>Corrección RVP</strong></th>
                                                <th><strong>Calcular</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Precio paridad</td>
                                                <td class="col-md-3"><input type="text" name="paridad_minimo" class="form-control formatNumber"></td>
                                                <td class="col-md-3"><input type="text" name="paridad_maximo" class="form-control formatNumber"></td>
                                                <td>¿Fijar parámetro?</td>
                                                <td><input type="checkbox" id="habilitar_n_gas_87" class="habilitarInput checkSiNo" target-input="n"></td>
                                                <td><input type="checkbox" id="habilitar_m_gas_87" class="habilitarInput checkSiNo" target-input="m"></td>
                                                <td><input type="checkbox" id="habilitar_s_gas_87" class="habilitarInput checkSiNo" target-input="s"></td>
                                                <td><input type="checkbox" id="habilitar_t_gas_87" class="habilitarInput checkSiNo" target-input="t"></td>
                                                <td><input type="checkbox" id="habilitar_f_gas_87" class="habilitarInput checkSiNo" target-input="f"></td>
                                                <td rowspan="2" style="vertical-align:middle;">
                                                    <input type="checkbox" id="correccionRvp_gas_87" name="correccionRvp" checked="true" class="rvpSiNo">
                                                </td>
                                                <td rowspan="2" style="vertical-align:middle;">
                                                    @if(!$puedeCalcularPublicar)
                                                        <button class="calcular btn btn-default" type="button" disabled="true">                         
                                                            <i class="fa fa-cog fa-1x"></i>
                                                        </button>
                                                    @else
                                                        <button class="calcular btn btn-default" type="button" id="'btn_gas_93'" onclick="calcular('form_gas_87')">
                                                            <i class="fa fa-cog fa-1x"></i>
                                                        </button>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Precio referencia</td>
                                                <td class="col-md-3"><input type="text" name="referencia_minimo" class="form-control formatNumber"></td>
                                                <td class="col-md-3"><input type="text" name="referencia_maximo" class="form-control formatNumber"></td>
                                                <td>Valor</td>
                                                <td class="col-md-3">
                                                    <select id="valor_n_gas_87" name="n" disabled="disabled" class="form-control" param="{{ $parametros_93->N }}">
                                                        @for($i=$rango_n->REPA_VALOR_MINIMO; $i<=$rango_n->REPA_VALOR_MAXIMO; $i=$i+$rango_n->REPA_INCREMENTO)
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </td>
                                                <td class="col-md-3">
                                                    <select id="valor_m_gas_87" name="m" disabled="disabled" class="form-control" param="{{ $parametros_93->M }}">
                                                        @for($i=$rango_m->REPA_VALOR_MINIMO; $i<=$rango_m->REPA_VALOR_MAXIMO; $i=$i+$rango_m->REPA_INCREMENTO)
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </td>
                                                <td class="col-md-3">
                                                    <select id="valor_s_gas_87" name="s" disabled="disabled" class="form-control" param="{{ $parametros_93->S }}">
                                                        @for($i=$rango_s->REPA_VALOR_MINIMO; $i<=$rango_s->REPA_VALOR_MAXIMO; $i=$i+$rango_s->REPA_INCREMENTO)
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </td>
                                                <td class="col-md-3">
                                                    <select id="valor_t_gas_87" name="t" disabled="disabled" class="form-control" param="{{ $parametros_93->T }}">
                                                        @for($i=$rango_t->REPA_VALOR_MINIMO; $i<=$rango_t->REPA_VALOR_MAXIMO; $i=$i+$rango_t->REPA_INCREMENTO)
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </td>
                                                <td class="col-md-3">
                                                   <select id="valor_f_gas_87" name="f" disabled="disabled" class="form-control" param="{{ (float)$parametros_93->F }}">
                                                        @for($i=$rango_f->REPA_VALOR_MINIMO; $i<=$rango_f->REPA_VALOR_MAXIMO; $i=$i+$rango_f->REPA_INCREMENTO)
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <input type="hidden" name="n">
                                    <input type="hidden" name="m">
                                    <input type="hidden" name="f">
                                    <input type="hidden" name="s">
                                    <input type="hidden" name="t">

                                    <input type="hidden" class="tipo_crudo" name="tipo_crudo">
                                    <input type="hidden" class="correccion_rvp" id="correccion_rvp_gas_87" name="correccion_rvp">
                                    <input type="hidden" name="hidr_id" value='{{ Util::COMBUSTIBLE_93 }}'>
                                    <input type="hidden" name="proc_id" value='{{ $proceso }}'>
                                </form>
                            </div>
                            <div class="col-md-12 col-xs-12 tablaParidad" style="padding: 2px 0 4px; overflow-x:auto">
                                <table class="table table-hover table-bordered solverDataTable"  cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center; table-layout:auto; display:table" id="paridades_gas_87">
                                    <thead>
                                        <tr>
                                            <th>Precio paridad mínimo</th>
                                            <th>Precio paridad máximo</th>
                                            <th>Valor T</th>
                                            <th>Precio Paridad</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($paridad as $p)
                                            @if ($p->HIDR_ID == Util::COMBUSTIBLE_93)
                                                <tr>
                                                    <td>{{ Util::formatNumber($p->SOLV_PRECIO_PARIDAD_MIN) }}</td>
                                                    <td>{{ Util::formatNumber($p->SOLV_PRECIO_PARIDAD_MAX) }}</td>
                                                    <td>{{ $p->SOLP_T }}</td>
                                                    <td>{{ Util::formatNumber($p->SOLP_PARIDAD) }}</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12 col-xs-12 tablaReferencia" style="padding: 2px 0 4px; min-width:100%; overflow-x:auto">
                                <table class="table table-hover table-bordered solverDataTable"  cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center; table-layout:auto; display:table" id="referencias_gas_87">
                                    <thead>
                                        <tr>
                                            <th>P.Ref mín</th>
                                            <th>P.Ref máx</th>
                                            <th>N</th>
                                            <th>M</th>
                                            <th>S</th>
                                            <th>F</th>
                                            <th>Crudo</th>
                                            <th>Margen</th>
                                            <th>P.Ref Inf</th>
                                            <th>P.Ref Int</th>
                                            <th>P.Ref Sup</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($referencia as $r)
                                            @if ($r->HIDR_ID == Util::COMBUSTIBLE_93)
                                                <tr>
                                                    <td>{{ Util::formatNumber($r->SOLV_PRECIO_REFERENCIA_MIN) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLV_PRECIO_REFERENCIA_MAX) }}</td>
                                                    <td>{{ $r->SOLR_N }}</td>
                                                    <td>{{ $r->SOLR_M }}</td>
                                                    <td>{{ $r->SOLR_S }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_F) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_CRUDO) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_MARGEN) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_PREFINF) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_PREFINT) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_PREFSUP) }}</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-primary panel-mod" style="margin: 4px;">
                        <div class="panel-heading">
                            Gasolina 97
                        </div>
                        <div class="panel-body" style="padding: 4px">
                            <div class="col-md-12 col-xs-12" style="padding: 2px 0 4px;">
                                <form id="form_gas_93">
                                {!! csrf_field() !!}
                                    <!-- WARNING TIEMPO -->
                                    <div class="alert alert-warning" id="form_gas_93-warning" style="text-align: center; display: none">
                                        <div id="form_gas_93-spin">
                                            <i class="fa fa-refresh fa-spin"></i>
                                            Calculando tiempo ejecución...
                                        </div>
                                        <span id="form_gas_93-lblTiempo"></span>
                                    </div>
                                    <!-- FIN WARNING TIEMPO -->
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th><strong>Valores</strong></th>
                                                <td>Mínimo</td>
                                                <td>Máximo</td>
                                                <th><strong>Parámetros</strong></th>
                                                <td>n</td>
                                                <td>m</td>
                                                <td>s</td>
                                                <td>t</td>
                                                <td>f</td>
                                                <th><strong>Corrección RVP</strong></th>
                                                <th><strong>Calcular</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Precio paridad</td>
                                                <td class="col-md-3"><input type="text" name="paridad_minimo" class="form-control formatNumber"></td>
                                                <td class="col-md-3"><input type="text" name="paridad_maximo" class="form-control formatNumber"></td>
                                                <td>¿Fijar parámetro?</td>
                                                <td><input type="checkbox" id="habilitar_n_gas_93" class="habilitarInput checkSiNo" target-input="n"></td>
                                                <td><input type="checkbox" id="habilitar_m_gas_93" class="habilitarInput checkSiNo" target-input="m"></td>
                                                <td><input type="checkbox" id="habilitar_s_gas_93" class="habilitarInput checkSiNo" target-input="s"></td>
                                                <td><input type="checkbox" id="habilitar_t_gas_93" class="habilitarInput checkSiNo" target-input="t"></td>
                                                <td><input type="checkbox" id="habilitar_f_gas_93" class="habilitarInput checkSiNo" target-input="f"></td>
                                                <td rowspan="2" style="vertical-align:middle;">
                                                    <input type="checkbox" id="correccionRvp_gas_93" name="correccionRvp" checked="true" class="rvpSiNo">
                                                </td>
                                                <td rowspan="2" style="vertical-align:middle;">
                                                    @if(!$puedeCalcularPublicar)
                                                        <button class="calcular btn btn-default" type="button" disabled="true">                         
                                                            <i class="fa fa-cog fa-1x"></i>
                                                        </button>
                                                    @else
                                                        <button class="calcular btn btn-default" type="button" id="'btn_gas_93'" onclick="calcular('form_gas_93')">
                                                            <i class="fa fa-cog fa-1x"></i>
                                                        </button>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Precio referencia</td>
                                                <td class="col-md-3"><input type="text" name="referencia_minimo" class="form-control formatNumber"></td>
                                                <td class="col-md-3"><input type="text" name="referencia_maximo" class="form-control formatNumber"></td>
                                                <td>Valor</td>
                                                <td class="col-md-3">
                                                    <select id="valor_n_gas_93" name="n" disabled="disabled" class="form-control" param="{{ $parametros_97->N }}">
                                                        @for($i=$rango_n->REPA_VALOR_MINIMO; $i<=$rango_n->REPA_VALOR_MAXIMO; $i=$i+$rango_n->REPA_INCREMENTO)
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </td>
                                                <td class="col-md-3">
                                                    <select id="valor_m_gas_93" name="m" disabled="disabled" class="form-control" param="{{ $parametros_97->M }}">
                                                        @for($i=$rango_m->REPA_VALOR_MINIMO; $i<=$rango_m->REPA_VALOR_MAXIMO; $i=$i+$rango_m->REPA_INCREMENTO)
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </td>
                                                <td class="col-md-3">
                                                    <select id="valor_s_gas_93" name="s" disabled="disabled" class="form-control" param="{{ $parametros_97->S }}">
                                                        @for($i=$rango_s->REPA_VALOR_MINIMO; $i<=$rango_s->REPA_VALOR_MAXIMO; $i=$i+$rango_s->REPA_INCREMENTO)
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </td>
                                                <td class="col-md-3">
                                                    <select id="valor_t_gas_93" name="t" disabled="disabled" class="form-control" param="{{ $parametros_97->T }}">
                                                        @for($i=$rango_t->REPA_VALOR_MINIMO; $i<=$rango_t->REPA_VALOR_MAXIMO; $i=$i+$rango_t->REPA_INCREMENTO)
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </td>
                                                <td class="col-md-3">
                                                   <select id="valor_f_gas_93" name="f" disabled="disabled" class="form-control" param="{{ (float)$parametros_97->F }}">
                                                        @for($i=$rango_f->REPA_VALOR_MINIMO; $i<=$rango_f->REPA_VALOR_MAXIMO; $i=$i+$rango_f->REPA_INCREMENTO)
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <input type="hidden" name="n">
                                    <input type="hidden" name="m">
                                    <input type="hidden" name="f">
                                    <input type="hidden" name="s">
                                    <input type="hidden" name="t">

                                    <input type="hidden" class="tipo_crudo" name="tipo_crudo">
                                    <input type="hidden" class="correccion_rvp" id="correccion_rvp_gas_93" name="correccion_rvp">
                                    <input type="hidden" name="hidr_id" value='{{ Util::COMBUSTIBLE_97 }}'>
                                    <input type="hidden" name="proc_id" value='{{ $proceso }}'>
                                </form>
                            </div>
                            <div class="col-md-6 col-xs-6 tablaParidad" style="padding: 2px 0 4px; min-width:100%; overflow-x:auto">
                                <table class="table table-hover table-bordered solverDataTable"  cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center; table-layout:auto; display:table" id="paridades_gas_93">
                                    <thead>
                                        <tr>
                                            <th>Precio paridad mínimo</th>
                                            <th>Precio paridad máximo</th>
                                            <th>Valor T</th>
                                            <th>Precio Paridad</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($paridad as $p)
                                            @if ($p->HIDR_ID == Util::COMBUSTIBLE_97)
                                                <tr>
                                                    <td>{{ Util::formatNumber($p->SOLV_PRECIO_PARIDAD_MIN) }}</td>
                                                    <td>{{ Util::formatNumber($p->SOLV_PRECIO_PARIDAD_MAX) }}</td>
                                                    <td>{{ $p->SOLP_T }}</td>
                                                    <td>{{ Util::formatNumber($p->SOLP_PARIDAD) }}</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6 col-xs-6 tablaReferencia" style="padding: 2px 0 4px; min-width:100%; overflow-x:auto">
                                <table class="table table-hover table-bordered solverDataTable"  cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center; table-layout:auto; display:table" id="referencias_gas_93">
                                    <thead>
                                        <tr>
                                            <th>P.Ref mín</th>
                                            <th>P.Ref máx</th>
                                            <th>N</th>
                                            <th>M</th>
                                            <th>S</th>
                                            <th>F</th>
                                            <th>Crudo</th>
                                            <th>Margen</th>
                                            <th>P.Ref Inf</th>
                                            <th>P.Ref Int</th>
                                            <th>P.Ref Sup</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($referencia as $r)
                                            @if ($r->HIDR_ID == Util::COMBUSTIBLE_97)
                                                <tr>
                                                    <td>{{ Util::formatNumber($r->SOLV_PRECIO_REFERENCIA_MIN) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLV_PRECIO_REFERENCIA_MAX) }}</td>
                                                    <td>{{ $r->SOLR_N }}</td>
                                                    <td>{{ $r->SOLR_M }}</td>
                                                    <td>{{ $r->SOLR_S }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_F) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_CRUDO) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_MARGEN) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_PREFINF) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_PREFINT) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_PREFSUP) }}</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-primary panel-mod" style="margin: 4px;">
                        <div class="panel-heading">
                            Diésel
                        </div>
                        <div class="panel-body" style="padding: 4px">
                            <div class="col-md-12 col-xs-12" style="padding: 2px 0 4px;">
                                <form id="form_diesel">
                                {!! csrf_field() !!}
                                    <!-- WARNING TIEMPO -->
                                    <div class="alert alert-warning" id="form_diesel-warning" style="text-align: center; display: none">
                                        <div id="form_diesel-spin">
                                            <i class="fa fa-refresh fa-spin"></i>
                                            Calculando tiempo ejecución...
                                        </div>
                                        <span id="form_diesel-lblTiempo"></span>
                                    </div>
                                    <!-- FIN WARNING TIEMPO -->
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th><strong>Valores</strong></th>
                                                <td>Mínimo</td>
                                                <td>Máximo</td>
                                                <th><strong>Parámetros</strong></th>
                                                <td>n</td>
                                                <td>m</td>
                                                <td>s</td>
                                                <td>t</td>
                                                <td>f</td>
                                                <th><strong>Calcular</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Precio paridad</td>
                                                <td class="col-md-3"><input type="text" name="paridad_minimo" class="form-control formatNumber"></td>
                                                <td class="col-md-3"><input type="text" name="paridad_maximo" class="form-control formatNumber"></td>
                                                <td>¿Fijar parámetro?</td>
                                                <td><input type="checkbox" id="habilitar_n_diesel" class="habilitarInput checkSiNo" target-input="n"></td>
                                                <td><input type="checkbox" id="habilitar_m_diesel" class="habilitarInput checkSiNo" target-input="m"></td>
                                                <td><input type="checkbox" id="habilitar_s_diesel" class="habilitarInput checkSiNo" target-input="s"></td>
                                                <td><input type="checkbox" id="habilitar_t_diesel" class="habilitarInput checkSiNo" target-input="t"></td>
                                                <td><input type="checkbox" id="habilitar_f_diesel" class="habilitarInput checkSiNo" target-input="f"></td>
                                                <td rowspan="2" style="vertical-align:middle;">
                                                    @if(!$puedeCalcularPublicar)
                                                        <button class="calcular btn btn-default" type="button" disabled="true">                         
                                                            <i class="fa fa-cog fa-1x"></i>
                                                        </button>
                                                    @else
                                                        <button class="calcular btn btn-default" type="button" id="'btn_gas_93'" onclick="calcular('form_diesel')">
                                                            <i class="fa fa-cog fa-1x"></i>
                                                        </button>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Precio referencia</td>
                                                <td class="col-md-3"><input type="text" name="referencia_minimo" class="form-control formatNumber"></td>
                                                <td class="col-md-3"><input type="text" name="referencia_maximo" class="form-control formatNumber"></td>
                                                <td>Valor</td>
                                                <td class="col-md-3">
                                                    <select id="valor_n_diesel" name="n" disabled="disabled" class="form-control" param="{{ $parametros_diesel->N }}">
                                                        @for($i=$rango_n->REPA_VALOR_MINIMO; $i<=$rango_n->REPA_VALOR_MAXIMO; $i=$i+$rango_n->REPA_INCREMENTO)
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </td>
                                                <td class="col-md-3">
                                                    <select id="valor_m_diesel" name="m" disabled="disabled" class="form-control" param="{{ $parametros_diesel->M }}">
                                                        @for($i=$rango_m->REPA_VALOR_MINIMO; $i<=$rango_m->REPA_VALOR_MAXIMO; $i=$i+$rango_m->REPA_INCREMENTO)
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </td>
                                                <td class="col-md-3">
                                                    <select id="valor_s_diesel" name="s" disabled="disabled" class="form-control" param="{{ $parametros_diesel->S }}">
                                                        @for($i=$rango_s->REPA_VALOR_MINIMO; $i<=$rango_s->REPA_VALOR_MAXIMO; $i=$i+$rango_s->REPA_INCREMENTO)
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </td>
                                                <td class="col-md-3">
                                                    <select id="valor_t_diesel" name="t" disabled="disabled" class="form-control" param="{{ $parametros_diesel->T }}">
                                                        @for($i=$rango_t->REPA_VALOR_MINIMO; $i<=$rango_t->REPA_VALOR_MAXIMO; $i=$i+$rango_t->REPA_INCREMENTO)
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </td>
                                                <td class="col-md-3">
                                                   <select id="valor_f_diesel" name="f" disabled="disabled" class="form-control" param="{{ (float)$parametros_diesel->F }}">
                                                        @for($i=$rango_f->REPA_VALOR_MINIMO; $i<=$rango_f->REPA_VALOR_MAXIMO; $i=$i+$rango_f->REPA_INCREMENTO)
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <input type="hidden" name="n">
                                    <input type="hidden" name="m">
                                    <input type="hidden" name="f">
                                    <input type="hidden" name="s">
                                    <input type="hidden" name="t">

                                    <input type="hidden" class="tipo_crudo" name="tipo_crudo">
                                    <input type="hidden" name="hidr_id" value='{{ Util::COMBUSTIBLE_DIESEL }}'>
                                    <input type="hidden" name="proc_id" value='{{ $proceso }}'>
                                </form>
                            </div>
                            <div class="col-md-6 col-xs-6 tablaParidad" style="padding: 2px 0 4px; min-width:100%; overflow-x:auto">
                                <table class="table table-hover table-bordered solverDataTable"  cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center; table-layout:auto; display:table" id="paridades_diesel">
                                    <thead>
                                        <tr>
                                            <th>Precio paridad mínimo</th>
                                            <th>Precio paridad máximo</th>
                                            <th>Valor T</th>
                                            <th>Precio Paridad</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($paridad as $p)
                                            @if ($p->HIDR_ID == Util::COMBUSTIBLE_DIESEL)
                                                <tr>
                                                    <td>{{ Util::formatNumber($p->SOLV_PRECIO_PARIDAD_MIN) }}</td>
                                                    <td>{{ Util::formatNumber($p->SOLV_PRECIO_PARIDAD_MAX) }}</td>
                                                    <td>{{ $p->SOLP_T }}</td>
                                                    <td>{{ Util::formatNumber($p->SOLP_PARIDAD) }}</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6 col-xs-6 tablaReferencia" style="padding: 2px 0 4px; min-width:100%; overflow-x:auto">
                                <table class="table table-hover table-bordered solverDataTable"  cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center; table-layout:auto; display:table" id="referencias_diesel">
                                    <thead>
                                        <tr>
                                            <th>P.Ref mín</th>
                                            <th>P.Ref máx</th>
                                            <th>N</th>
                                            <th>M</th>
                                            <th>S</th>
                                            <th>F</th>
                                            <th>Crudo</th>
                                            <th>Margen</th>
                                            <th>P.Ref Inf</th>
                                            <th>P.Ref Int</th>
                                            <th>P.Ref Sup</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($referencia as $r)
                                            @if ($r->HIDR_ID == Util::COMBUSTIBLE_DIESEL)
                                                <tr>
                                                    <td>{{ Util::formatNumber($r->SOLV_PRECIO_REFERENCIA_MIN) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLV_PRECIO_REFERENCIA_MAX) }}</td>
                                                    <td>{{ $r->SOLR_N }}</td>
                                                    <td>{{ $r->SOLR_M }}</td>
                                                    <td>{{ $r->SOLR_S }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_F) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_CRUDO) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_MARGEN) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_PREFINF) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_PREFINT) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_PREFSUP) }}</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-primary panel-mod" style="margin: 4px;">
                        <div class="panel-heading">
                            GLP 
                        </div>
                        <div class="panel-body" style="padding: 4px">
                            <div class="col-md-12 col-xs-12" style="padding: 2px 0 4px;">
                                <form id="form_glp">
                                {!! csrf_field() !!}
                                    <!-- WARNING TIEMPO -->
                                    <div class="alert alert-warning" id="form_glp-warning" style="text-align: center; display: none">
                                        <div id="form_glp-spin">
                                            <i class="fa fa-refresh fa-spin"></i>
                                            Calculando tiempo ejecución...
                                        </div>
                                        <span id="form_glp-lblTiempo"></span>
                                    </div>
                                    <!-- FIN WARNING TIEMPO -->
                                     <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th><strong>Valores</strong></th>
                                                <td>Mínimo</td>
                                                <td>Máximo</td>
                                                <th><strong>Parámetros</strong></th>
                                                <td>n</td>
                                                <td>m</td>
                                                <td>s</td>
                                                <td>t</td>
                                                <td>f</td>
                                                <th><strong>Calcular</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Precio paridad</td>
                                                <td class="col-md-3"><input type="text" name="paridad_minimo" class="form-control formatNumber"></td>
                                                <td class="col-md-3"><input type="text" name="paridad_maximo" class="form-control formatNumber"></td>
                                                <td>¿Fijar parámetro?</td>
                                                <td><input type="checkbox" id="habilitar_n_glp" class="habilitarInput checkSiNo" target-input="n"></td>
                                                <td><input type="checkbox" id="habilitar_m_glp" class="habilitarInput checkSiNo" target-input="m"></td>
                                                <td><input type="checkbox" id="habilitar_s_glp" class="habilitarInput checkSiNo" target-input="s"></td>
                                                <td><input type="checkbox" id="habilitar_t_glp" class="habilitarInput checkSiNo" target-input="t"></td>
                                                <td><input type="checkbox" id="habilitar_f_glp" class="habilitarInput checkSiNo" target-input="f"></td>
                                                <td rowspan="2" style="vertical-align:middle;">
                                                    @if(!$puedeCalcularPublicar)
                                                        <button class="calcular btn btn-default" type="button" disabled="true">                         
                                                            <i class="fa fa-cog fa-1x"></i>
                                                        </button>
                                                    @else
                                                        <button class="calcular btn btn-default" type="button" id="'btn_gas_93'" onclick="calcular('form_glp')">
                                                            <i class="fa fa-cog fa-1x"></i>
                                                        </button>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Precio referencia</td>
                                                <td class="col-md-3"><input type="text" name="referencia_minimo" class="form-control formatNumber"></td>
                                                <td class="col-md-3"><input type="text" name="referencia_maximo" class="form-control formatNumber"></td>
                                                <td>Valor</td>
                                                <td class="col-md-3">
                                                    <select id="valor_n_glp" name="n" disabled="disabled" class="form-control" param="{{ $parametros_glp->N }}">
                                                        @for($i=$rango_n->REPA_VALOR_MINIMO; $i<=$rango_n->REPA_VALOR_MAXIMO; $i=$i+$rango_n->REPA_INCREMENTO)
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </td>
                                                <td class="col-md-3">
                                                    <select id="valor_m_glp" name="m" disabled="disabled" class="form-control" param="{{ $parametros_glp->M }}">
                                                        @for($i=$rango_m->REPA_VALOR_MINIMO; $i<=$rango_m->REPA_VALOR_MAXIMO; $i=$i+$rango_m->REPA_INCREMENTO)
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </td>
                                                <td class="col-md-3">
                                                    <select id="valor_s_glp" name="s" disabled="disabled" class="form-control" param="{{ $parametros_glp->S }}">
                                                        @for($i=$rango_s->REPA_VALOR_MINIMO; $i<=$rango_s->REPA_VALOR_MAXIMO; $i=$i+$rango_s->REPA_INCREMENTO)
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </td>
                                                <td class="col-md-3">
                                                    <select id="valor_t_glp" name="t" disabled="disabled" class="form-control" param="{{ $parametros_glp->T }}">
                                                        @for($i=$rango_t->REPA_VALOR_MINIMO; $i<=$rango_t->REPA_VALOR_MAXIMO; $i=$i+$rango_t->REPA_INCREMENTO)
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </td>
                                                <td class="col-md-3">
                                                   <select id="valor_f_glp" name="f" disabled="disabled" class="form-control" param="{{ (float)$parametros_glp->F }}">
                                                        @for($i=$rango_f->REPA_VALOR_MINIMO; $i<=$rango_f->REPA_VALOR_MAXIMO; $i=$i+$rango_f->REPA_INCREMENTO)
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <input type="hidden" name="n">
                                    <input type="hidden" name="m">
                                    <input type="hidden" name="f">
                                    <input type="hidden" name="s">
                                    <input type="hidden" name="t">

                                    <input type="hidden" class="tipo_crudo" name="tipo_crudo">
                                    <input type="hidden" name="hidr_id" value='{{ Util::COMBUSTIBLE_GLP }}'>
                                    <input type="hidden" name="proc_id" value='{{ $proceso }}'>
                                </form>
                            </div>
                            <div class="col-md-6 col-xs-6 tablaParidad" style="padding: 2px 0 4px; min-width:100%; overflow-x:auto">
                                <table class="table table-hover table-bordered solverDataTable"  cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center; table-layout:auto; display:table" id="paridades_glp">
                                    <thead>
                                        <tr>
                                            <th>Precio paridad mínimo</th>
                                            <th>Precio paridad máximo</th>
                                            <th>Valor T</th>
                                            <th>Precio Paridad</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($paridad as $p)
                                            @if ($p->HIDR_ID == Util::COMBUSTIBLE_GLP)
                                                <tr>
                                                    <td>{{ Util::formatNumber($p->SOLV_PRECIO_PARIDAD_MIN) }}</td>
                                                    <td>{{ Util::formatNumber($p->SOLV_PRECIO_PARIDAD_MAX) }}</td>
                                                    <td>{{ $p->SOLP_T }}</td>
                                                    <td>{{ Util::formatNumber($p->SOLP_PARIDAD) }}</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6 col-xs-6 tablaReferencia" style="padding: 2px 0 4px; min-width:100%; overflow-x:auto">
                                <table class="table table-hover table-bordered solverDataTable"  cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center; table-layout:auto; display:table" id="referencias_glp">
                                    <thead>
                                        <tr>
                                            <th>P.Ref mín</th>
                                            <th>P.Ref máx</th>
                                            <th>N</th>
                                            <th>M</th>
                                            <th>S</th>
                                            <th>F</th>
                                            <th>Crudo</th>
                                            <th>Margen</th>
                                            <th>P.Ref Inf</th>
                                            <th>P.Ref Int</th>
                                            <th>P.Ref Sup</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($referencia as $r)
                                            @if ($r->HIDR_ID == Util::COMBUSTIBLE_GLP)
                                                <tr>
                                                    <td>{{ Util::formatNumber($r->SOLV_PRECIO_REFERENCIA_MIN) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLV_PRECIO_REFERENCIA_MAX) }}</td>
                                                    <td>{{ $r->SOLR_N }}</td>
                                                    <td>{{ $r->SOLR_M }}</td>
                                                    <td>{{ $r->SOLR_S }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_F) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_CRUDO) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_MARGEN) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_PREFINF) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_PREFINT) }}</td>
                                                    <td>{{ Util::formatNumber($r->SOLR_PREFSUP) }}</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <div class="modal fade" id="solverStatus" keyboard="false" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="procesoStatusTitle">Calculando ...</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered" id="calculando">
                        <tr>
                            <td style="background-color: #F5F5F5; font-weight: bold">Calculando valores de parámetros para los rangos de Paridad y Referencia seleccionados...<br/> Si desea detener el cálculo, pulse el botón 'Detener'.</td>
                        </tr>
                        <tr>
                            <td><span id="lblSolverStatusPPARIDAD"></span>&nbsp;<i id="spinProcesoStatusPPARIDAD" class="fa fa-refresh fa-spin"></i></td>
                        </tr>
                    </table>
                    <div class="alert alert-danger" id="lblErrorProceso" style="display: none">Ha ocurrido un error al procesar los datos.<br>Por favor, inténtelo de nuevo. Si el problema persiste contacte a al Administrador del sistema.</div>
                </div>
                <div class="modal-footer">
                    <button id="btnSolverCancel" type="button" class="btn btn-warning" data-dismiss="modal">Detener</button>
                    <button id="btnSolverOK" type="button" class="btn btn-primary" data-dismiss="modal" style="display: none;">Aceptar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="solverAdv" keyboard="false" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Información</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger">
                        Para realizar cálculo solver debe fijar a lo menos 3 parametros<br><br> favor corregir y reintentar
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>
@endsection  

@section('script')
    <script type="text/javascript">

    var request;

    $('#solverStatus').on('hidden.bs.modal', function() {
            $('#btnSolverCancel').show();
            $('#calculando').show();
            $('#btnSolverOK').hide();
            $('#lblErrorProceso').hide();
    });

    $('.habilitarInput').on('switchChange.bootstrapSwitch', function( event, state ) {
        var id_switch = this.id;
        var id_input  = null;
        var indice_combustible = null;

        indice_combustible = id_switch.indexOf('_');
        id_input = 'valor'+id_switch.substr(indice_combustible);

        if (state == true) {
            $('#'+id_input).attr('disabled', false);
            $('#'+id_input).val($('#'+id_input).attr('param'));
        }
        else 
        {
            $('#'+id_input).attr('disabled', true);
            $('#'+id_input).val('');
        }

        console.log('id_switch: ' + id_switch);
        var aux = id_switch.split('_');

        calcularTiempoEjecucion(aux[aux.length - 1]);
    });

    $('#tipoCrudo').on('switchChange.bootstrapSwitch', function( event, state ) {
        // Estado true - BRENT
        if (state == true) {
            $('.tipo_crudo').val('{{ Util::BRENT}} ');
        }
        // Estado false - WTI
        else {
            $('.tipo_crudo').val('{{ Util::WTI}} ');
        }
    });

    $('#correccionRvp_gas_93').on('switchChange.bootstrapSwitch', function( event, state ) {
        if (state == true) {
            $('#correccion_rvp_gas_93').val('1');
        }
        else {
            $('#correccion_rvp_gas_93').val('0');
        }
    });

    $('#correccionRvp_gas_87').on('switchChange.bootstrapSwitch', function( event, state ) {
        if (state == true) {
            $('#correccion_rvp_gas_87').val('1');
        }
        else {
            $('#correccion_rvp_gas_87').val('0');
        }
    });

    $('select').val('');

    function calcular(form_id) {

        if(!validaFijacionParametro(form_id)) return false;

        $('#'+form_id + " input[name='n']").val($('#'+form_id + " select[name='n']").val());
        $('#'+form_id + " input[name='m']").val($('#'+form_id + " select[name='m']").val());
        $('#'+form_id + " input[name='s']").val($('#'+form_id + " select[name='s']").val());
        $('#'+form_id + " input[name='t']").val($('#'+form_id + " select[name='t']").val());
        $('#'+form_id + " input[name='f']").val($('#'+form_id + " select[name='f']").val());

        var formData = $('#'+form_id).serialize();

        if ((form_id == 'form_gas_87') || (form_id == 'form_gas_93')) {
            correccion_rvp = $('#'+form_id + " input[name='correccion_rvp']").val();
        }
        else {
            correccion_rvp = '';
        }

        $('#solverStatus').modal('show');

        // Llamada a AJAX para realizar el cálculo SOLVER
        request = $.ajax({
            url: '{{ route("solver/calcular") }}',
            type: 'POST',
            data: { 
                'formData'        : formData,
                'tipo_crudo'      : $('.tipo_crudo').val(),
                'correccion_rvp'  : correccion_rvp,
                '_token'          : $('#_token').val() 
            },
            success: function(data) {

                if(data.status == 'success'){

                    // Actualizará datatable para añadir las filas nuevas
                    $('#solverStatus').modal('hide');

                    resultadosParidad       = JSON.parse(data.resultadosParidad);
                    resultadosReferencia    = JSON.parse(data.resultadosReferencia);
                    hidrocarburo            = data.hidrocarburo;

                    if (hidrocarburo == '{{ Util::COMBUSTIBLE_93 }}') {
                        $('#paridades_gas_87').DataTable().rows.add(resultadosParidad).draw().nodes().to$().effect("highlight", {color: '#428BCA'}, 3000);
                        $('#referencias_gas_87').DataTable().rows.add(resultadosReferencia).draw().nodes().to$().effect("highlight", {color: '#428BCA'}, 3000);
                    }
                    else if (hidrocarburo == '{{ Util::COMBUSTIBLE_97 }}') {
                        $('#paridades_gas_93').DataTable().rows.add(resultadosParidad).draw().nodes().to$().effect("highlight", {color: '#428BCA'}, 3000);
                        $('#referencias_gas_93').DataTable().rows.add(resultadosReferencia).draw().nodes().to$().effect("highlight", {color: '#428BCA'}, 3000);
                    }
                    else if (hidrocarburo == '{{ Util::COMBUSTIBLE_DIESEL }}') {
                        $('#paridades_diesel').DataTable().rows.add(resultadosParidad).draw().nodes().to$().effect("highlight", {color: '#428BCA'}, 3000);
                        $('#referencias_diesel').DataTable().rows.add(resultadosReferencia).draw().nodes().to$().effect("highlight", {color: '#428BCA'}, 3000);
                    }
                    else if (hidrocarburo == '{{ Util::COMBUSTIBLE_GLP }}') {
                        $('#paridades_glp').DataTable().rows.add(resultadosParidad).draw().nodes().to$().effect("highlight", {color: '#428BCA'}, 3000);
                        $('#referencias_glp').DataTable().rows.add(resultadosReferencia).draw().nodes().to$().effect("highlight", {color: '#428BCA'}, 3000);
                    }
                }
                else {
                    $('#btnSolverCancel').hide();
                    $('#btnSolverOK').show();
                    $('#calculando').hide();
                    $('#lblErrorProceso').show();
                }
            },
            error: function(e) {
                $('#btnSolverCancel').hide();
                $('#btnSolverOK').show();
                $('#calculando').hide();
                $('#lblErrorProceso').show();
            }
        });
    }

    // Detener la ejecución del script, por ejemplo, si tarda demasiado en responder
    $('#btnSolverCancel').click(function() {
        request.abort();
        $('#solverStatus').modal('hide');
        location.reload();
    });

    $(".rvpSiNo").bootstrapSwitch({
        onText :'SI',
        offText:'NO',
        size   :'mini'
    });
    
    $(document).ready(function() {
        $('.tipo_crudo').val({{ Util::BRENT}});

        if ('{{ count($paridad)>0 }}') {
            $('.tablaParidad').show();
        }
        if ('{{ count($referencia)>0 }}') {
            $('.tablaReferencia').show();
        }

        // Inicializar correcciones RVP
        $('#correccionRvp_gas_87').bootstrapSwitch('state', false);
        $('#correccionRvp_gas_93').bootstrapSwitch('state', false);
        $('#correccion_rvp_gas_87').val('0');
        $('#correccion_rvp_gas_93').val('0');

        $('#fechaVigencia').val('{!! $fecha_vigencia !!}');

        /* DESHABILITAR Y MARCAR LOS CHECKBOX QUE TIENEN RESTRICCIÓN SEMANAL */
        $('#habilitar_n_gas_87').bootstrapSwitch('state', {!! $restriccion_semanas !!}['n_gas_87']);
        $('#habilitar_m_gas_87').bootstrapSwitch('state', {!! $restriccion_semanas !!}['m_gas_87']);
        $('#habilitar_s_gas_87').bootstrapSwitch('state', {!! $restriccion_semanas !!}['s_gas_87']);
        $('#habilitar_t_gas_87').bootstrapSwitch('state', {!! $restriccion_semanas !!}['t_gas_87']);
        $('#habilitar_f_gas_87').bootstrapSwitch('state', {!! $restriccion_semanas !!}['f_gas_87']);

        $('#habilitar_n_gas_93').bootstrapSwitch('state', {!! $restriccion_semanas !!}['n_gas_93']);
        $('#habilitar_m_gas_93').bootstrapSwitch('state', {!! $restriccion_semanas !!}['m_gas_93']);
        $('#habilitar_s_gas_93').bootstrapSwitch('state', {!! $restriccion_semanas !!}['s_gas_93']);
        $('#habilitar_t_gas_93').bootstrapSwitch('state', {!! $restriccion_semanas !!}['t_gas_93']);
        $('#habilitar_f_gas_93').bootstrapSwitch('state', {!! $restriccion_semanas !!}['f_gas_93']);

        $('#habilitar_n_diesel').bootstrapSwitch('state', {!! $restriccion_semanas !!}['n_diesel']);
        $('#habilitar_m_diesel').bootstrapSwitch('state', {!! $restriccion_semanas !!}['m_diesel']);
        $('#habilitar_s_diesel').bootstrapSwitch('state', {!! $restriccion_semanas !!}['s_diesel']);
        $('#habilitar_t_diesel').bootstrapSwitch('state', {!! $restriccion_semanas !!}['t_diesel']);
        $('#habilitar_f_diesel').bootstrapSwitch('state', {!! $restriccion_semanas !!}['f_diesel']);
        
        $('#habilitar_n_glp').bootstrapSwitch('state', {!! $restriccion_semanas !!}['n_glp']);
        $('#habilitar_m_glp').bootstrapSwitch('state', {!! $restriccion_semanas !!}['m_glp']);
        $('#habilitar_s_glp').bootstrapSwitch('state', {!! $restriccion_semanas !!}['s_glp']);
        $('#habilitar_t_glp').bootstrapSwitch('state', {!! $restriccion_semanas !!}['t_glp']);
        $('#habilitar_f_glp').bootstrapSwitch('state', {!! $restriccion_semanas !!}['f_glp']);

        $('#habilitar_n_gas_87').bootstrapSwitch('disabled', {!! $restriccion_semanas !!}['n_gas_87']);
        $('#habilitar_m_gas_87').bootstrapSwitch('disabled', {!! $restriccion_semanas !!}['m_gas_87']);
        $('#habilitar_s_gas_87').bootstrapSwitch('disabled', {!! $restriccion_semanas !!}['s_gas_87']);
        $('#habilitar_t_gas_87').bootstrapSwitch('disabled', {!! $restriccion_semanas !!}['t_gas_87']);
        $('#habilitar_f_gas_87').bootstrapSwitch('disabled', {!! $restriccion_semanas !!}['f_gas_87']);

        $('#habilitar_n_gas_93').bootstrapSwitch('disabled', {!! $restriccion_semanas !!}['n_gas_93']);
        $('#habilitar_m_gas_93').bootstrapSwitch('disabled', {!! $restriccion_semanas !!}['m_gas_93']);
        $('#habilitar_s_gas_93').bootstrapSwitch('disabled', {!! $restriccion_semanas !!}['s_gas_93']);
        $('#habilitar_t_gas_93').bootstrapSwitch('disabled', {!! $restriccion_semanas !!}['t_gas_93']);
        $('#habilitar_f_gas_93').bootstrapSwitch('disabled', {!! $restriccion_semanas !!}['f_gas_93']);

        $('#habilitar_n_diesel').bootstrapSwitch('disabled', {!! $restriccion_semanas !!}['n_diesel']);
        $('#habilitar_m_diesel').bootstrapSwitch('disabled', {!! $restriccion_semanas !!}['m_diesel']);
        $('#habilitar_s_diesel').bootstrapSwitch('disabled', {!! $restriccion_semanas !!}['s_diesel']);
        $('#habilitar_t_diesel').bootstrapSwitch('disabled', {!! $restriccion_semanas !!}['t_diesel']);
        $('#habilitar_f_diesel').bootstrapSwitch('disabled', {!! $restriccion_semanas !!}['f_diesel']);
        
        $('#habilitar_n_glp').bootstrapSwitch('disabled', {!! $restriccion_semanas !!}['n_glp']);
        $('#habilitar_m_glp').bootstrapSwitch('disabled', {!! $restriccion_semanas !!}['m_glp']);
        $('#habilitar_s_glp').bootstrapSwitch('disabled', {!! $restriccion_semanas !!}['s_glp']);
        $('#habilitar_t_glp').bootstrapSwitch('disabled', {!! $restriccion_semanas !!}['t_glp']);
        $('#habilitar_f_glp').bootstrapSwitch('disabled', {!! $restriccion_semanas !!}['f_glp']);

        /* DESHABILITAR LOS CAMPOS QUE NO SE PUEDEN VARIAR SEGÚN LA RESTRICCIÓN DE SEMANAS */
        $('#valor_n_gas_87').attr('disabled', {!! $restriccion_semanas !!}['n_gas_87']);
        $('#valor_m_gas_87').attr('disabled', {!! $restriccion_semanas !!}['m_gas_87']);
        $('#valor_s_gas_87').attr('disabled', {!! $restriccion_semanas !!}['s_gas_87']);
        $('#valor_t_gas_87').attr('disabled', {!! $restriccion_semanas !!}['t_gas_87']);
        $('#valor_f_gas_87').attr('disabled', {!! $restriccion_semanas !!}['f_gas_87']);

        $('#valor_n_gas_93').attr('disabled', {!! $restriccion_semanas !!}['n_gas_93']);
        $('#valor_m_gas_93').attr('disabled', {!! $restriccion_semanas !!}['m_gas_93']);
        $('#valor_s_gas_93').attr('disabled', {!! $restriccion_semanas !!}['s_gas_93']);
        $('#valor_t_gas_93').attr('disabled', {!! $restriccion_semanas !!}['t_gas_93']);
        $('#valor_f_gas_93').attr('disabled', {!! $restriccion_semanas !!}['f_gas_93']);

        $('#valor_n_diesel').attr('disabled', {!! $restriccion_semanas !!}['n_diesel']);
        $('#valor_m_diesel').attr('disabled', {!! $restriccion_semanas !!}['m_diesel']);
        $('#valor_s_diesel').attr('disabled', {!! $restriccion_semanas !!}['s_diesel']);
        $('#valor_t_diesel').attr('disabled', {!! $restriccion_semanas !!}['t_diesel']);
        $('#valor_f_diesel').attr('disabled', {!! $restriccion_semanas !!}['f_diesel']);

        $('#valor_n_glp').attr('disabled', {!! $restriccion_semanas !!}['n_glp']);
        $('#valor_m_glp').attr('disabled', {!! $restriccion_semanas !!}['m_glp']);
        $('#valor_s_glp').attr('disabled', {!! $restriccion_semanas !!}['s_glp']);
        $('#valor_t_glp').attr('disabled', {!! $restriccion_semanas !!}['t_glp']);
        $('#valor_f_glp').attr('disabled', {!! $restriccion_semanas !!}['f_glp']);

        $('select').each(function (i, obj) {
            var param = $(this).attr('param');
            if ($(this).attr('disabled') == true) {
                $(this).val(param);
            }
            else {
                $(this).attr('disabled', true);
            }
        });

    });

    function validaFijacionParametro(form_id)
    {
        var totalNull = 0;
        var n         = $('#'+form_id + " select[name='n']").val();
        var m         = $('#'+form_id + " select[name='m']").val();
        var s         = $('#'+form_id + " select[name='s']").val();
        var t         = $('#'+form_id + " select[name='t']").val();
        var f         = $('#'+form_id + " select[name='f']").val();

        if(n == null)
        {
            totalNull++;
        }
        if(m == null)
        {
            totalNull++;
        }
        if(s == null)
        {
            totalNull++;
        }
        if(t == null)
        {
            totalNull++;
        }
        if(f == null)
        {
            totalNull++;
        }

        if (totalNull > 2)
        {
            $('#solverAdv').modal('show');
            return false;
        }

        return true;

    }

    function calcularTiempoEjecucion(commbustible)
    {
        var form_id = null;

        switch(commbustible)
        {
            case '87':
                form_id = 'form_gas_87';
                break;
            case '93':
                form_id = 'form_gas_93';
                break;
            case 'diesel':
                form_id = 'form_diesel';
                break;
            case 'glp':
                form_id = 'form_glp';
                break;
        }

        var totalNull = 0;
        var n         = $('#'+form_id + " select[name='n']").val();
        var m         = $('#'+form_id + " select[name='m']").val();
        var s         = $('#'+form_id + " select[name='s']").val();
        var t         = $('#'+form_id + " select[name='t']").val();
        var f         = $('#'+form_id + " select[name='f']").val();

        // console.log('n: ' + n);
        // console.log('m: ' + m);
        // console.log('s: ' + s);
        // console.log('t: ' + t);
        // console.log('f: ' + f);

        $.ajax({
            url: '{{ route("solver/calcularTiempoEjecucion") }}',
            type: 'POST',
            data: { 
                'n' : n,
                'm' : m,
                's' : s,
                't' : t,
                'f' : f,
                '_token' : $('#_token').val()
            },
            beforeSend: function(){
                $('#'+form_id + '-lblTiempo').hide();
                $('#'+form_id + '-warning').show();
                $('#'+form_id + '-spin').show();
            },
            success: function(data) {
                $('#'+form_id + '-lblTiempo').html('Tiempo aproximado: ' + data.tiempo + ' hrs');
            },
            complete: function(){
                $('#'+form_id + '-spin').hide();
                $('#'+form_id + '-lblTiempo').show();
            },
            error: function(e)
            {
                console.log(e);
            }
        });
    }

    </script>
@endsection
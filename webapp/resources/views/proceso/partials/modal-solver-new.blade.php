<div class="modal fade" id="modalNew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h4>Definición valores</h4><hr>
                <table class="table table-striped table-bordered table-hover">
                    <tr>
                        <td></td>
                        <td>Mí­nimo</td>
                        <td>Máximo</td>
                    </tr>
                    <tr>
                        <td>Precio paridad</td>
                        <td class="col-md-3"><input type="number" class="form-control"></td>
                        <td class="col-md-3"><input type="number" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>Precio referencia</td>
                        <td class="col-md-3"><input type="number" class="form-control"></td>
                        <td class="col-md-3"><input type="number" class="form-control"></td>
                    </tr>
                </table>
                <h4>Definición variables</h4><hr>
                <table class="table table-striped table-bordered table-hover">
                    <tr>
                        <td>Variable</td>
                        <td>Fijar</td>
                        <td>Valor</td>
                    </tr>
                    <tr>
                        <td>n</td>
                        <td><input type="checkbox" class="habilitarInput checkOnOff" target-input="n"></td>
                        <td class="col-md-3"><input type="number" id="n" disabled="disabled" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>m</td>
                        <td><input type="checkbox" class="habilitarInput checkOnOff" target-input="m"></td>
                        <td class="col-md-3"><input type="number" id="m" disabled="disabled" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>s</td>
                        <td><input type="checkbox" class="habilitarInput checkOnOff" target-input="s"></td>
                        <td class="col-md-3"><input type="number" id="s" disabled="disabled" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>t</td>
                        <td><input type="checkbox" class="habilitarInput checkOnOff" target-input="t"></td>
                        <td class="col-md-3"><input type="number" id="t" disabled="disabled" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>f</td>
                        <td><input type="checkbox" class="habilitarInput checkOnOff" target-input="f"></td>
                        <td class="col-md-3"><input type="number" id="f" disabled="disabled" class="form-control"></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btnGrabarSolver" class="btn btn-primary loading-message">Aceptar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    //OCULTA DIV ERROR
    $('#modalNew').on('show.bs.modal', function () {
      $('#form-new #errors').hide();
    });

    $(".checkOnOff").bootstrapSwitch({
        onText :'SI',
        offText:'NO',
        size   :'mini',
        onSwitchChange: function(){
            var target = $(this).attr('target-input');
            if($(this).bootstrapSwitch('state'))
            {
                $('#' + target).removeAttr('disabled');
            }else
            {
                $('#' + target).attr('disabled', 'disabled');
            }
        }
    });
    
    $('#btnGrabarSolver').click(function(){
        setTimeout(function(){
            loadingMessageHide();
            $('#modalNew').modal('hide');
            $.notify('Calculo realizado',{position:'top center', className:'success'});
        }, 1500);
    });
</script>
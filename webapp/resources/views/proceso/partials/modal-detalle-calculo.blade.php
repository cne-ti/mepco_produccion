<div class="modal fade" id="modalDetalleCalculo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Fecha cálculo 06/07/2015 - Fecha vigencia 09/07/2015</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" method="POST">
                    <table class="table table-hover table-bordered" id="tablaCalculo">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Pardid Gasolina</th>
                                <th><span class="text-lowercase">n</span> WTI <small>(Semanal)</small></th>
                                <th><span class="text-lowercase">m</span> Futuros <small>(Meses)</small></th>
                                <th>Pesos Futuros</th>
                                <th><span class="text-lowercase">s</span> margen <small>(Semanal)</small></th>
                                <th><span class="text-lowercase">t</span> paridad <small>(Semanal)</small></th>
                                <th>Determinado</th>
                                <th>RVP</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Gasolina 93</td>
                                <td><input type="checkbox" class="checkUsgUsac"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="checkbox" class="checkSiNo"></td>
                                <td><input type="checkbox" class="checkSiNo"></td>
                            </tr>
                            <tr>
                                <td>Gasolina 97</td>
                                <td><input type="checkbox" class="checkUsgUsac"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="checkbox" class="checkSiNo"></td>
                                <td><input type="checkbox" class="checkSiNo"></td>
                            </tr>
                            <tr>
                                <td>Diesel</td>
                                <td><input type="checkbox" class="checkUsgUsac"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="checkbox" class="checkSiNo"></td>
                                <td><input type="checkbox" class="checkSiNo"></td>
                            </tr>
                            <tr>
                                <td>GLP</td>
                                <td><input type="checkbox" class="checkUsgUsac"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="number" class="form-control"></td>
                                <td><input type="checkbox" class="checkSiNo"></td>
                                <td><input type="checkbox" class="checkSiNo"></td>
                            </tr>
                        </tbody>
                    </table>
                    <div>
                        <a href="#" id="btnCalcular" class="btn btn-primary loading-message">Calcular</a>
                    </div>
                </form>
                <div id="tablas-resultado" style="display: none; clear: both; padding-top: 10px">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <th></th>
                            <th>n</th>
                            <th>m</th>
                            <th>s</th>
                            <th>t</th>
                            <th>mercado</th>
                            <th>Peso Fut</th>
                            <th>Vigencia n</th>
                            <th>Vigencia m</th>
                            <th>Vigencia s</th>
                            <th>Corrección</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Gasolina 93</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Gasolina 97</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Diesel</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>GLP</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th colspan="2">Crudo promedio</th>
                                <th colspan="3">Margen</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th>Anterior $/m3</th>
                                <th>Actual $/m3</th>
                                <th>Actual $/m3</th>
                                <th>Actual $/m3</th>
                                <th>Cambio %</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Gasolina 93</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Gasolina 97</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Diesel</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>GLP</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Crudo semanal US $/bbl</th>
                                <th>Crudo promedio US $/bbl</th>
                                <th>Margen anterior US $/bbl</th>
                                <th>Margen actual US $/bbl</th>
                                <th>Futuros semana anterior US $/m3</th>
                                <th>Futuros semana actual US $/m3</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Gasolina 93</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Gasolina 97</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Diesel</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>GLP</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Paridad anterior US $/m3</th>
                                <th>PRI anterior US $/m3</th>
                                <th>Prinf $/m3</th>
                                <th>Pri $/m3</th>
                                <th>Prs $/m3</th>
                                <th>Paridad $/m3</th>
                                <th>Fracción cred/imp %</th>
                                <th>Crédito/Impuesto $/m3</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Gasolina 93</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Gasolina 97</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Diesel</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>GLP</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Prinf $/m3</th>
                                <th>Pri $/m3</th>
                                <th>Prs $/m3</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Gasolina 93</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Gasolina 97</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Diesel</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>GLP</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer" id="footerCalculo">
                <a href="#" class="btn btn-primary" id="btnGuardarCalculo">Guardar</a>
                <a href="#" class="btn btn-default" data-dismiss="modal">Cerrar</a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#modalDetalleCalculo').on('show.bs.modal', function (e) {
        var type = $(e.relatedTarget).attr('data-type');
        if(type == "NUEVO")
        {
            $('#tablas-resultado').hide();
            $('#btnGuardarCalculo').show();
            $('#btnCalcular').show();
            $('#footerCalculo').show();
            $('#tablaCalculo :input').each(function(){
                $(this).removeAttr('disabled');
            });
        }else
        {
            $('#tablas-resultado').show();
            $('#btnGuardarCalculo').hide();
            $('#footerCalculo').hide();
            $('#btnCalcular').hide();
            $('#tablaCalculo :input').each(function(){
                $(this).attr('disabled', 'disabled');
            });
        }
    });

    $(".checkSiNo").bootstrapSwitch({
        onText :'SI',
        offText:'NO',
        size   :'mini'
    });

    $(".checkUsgUsac").bootstrapSwitch({
        onText :'USG',
        offText:'P-USAC',
        size   :'mini'
    });

    $('#btnCalcular').click(function(){
        setTimeout(function(){
            loadingMessageHide();
            $('#tablas-resultado').fadeIn('2000');
        }, 1000);
    });
</script>
				<div class="container-fluid">
				    <div class="row text-center">
				        <div class="col-md-2 col-xs-3">
				            <div style="padding: 10px">
				                <a id="SeleccionarValidadores" href="{{ route('proceso/validadores', ['id' => $proceso->PROC_ID]) }}" class="loading-message" style="color: #ccc;"><i class="fa fa-hand-paper-o fa-2x"></i><br>Seleccion validadores</a>
				            </div>
				        </div>
				        <div class="col-md-2 col-xs-3">
				            <div style="padding: 10px">
				                <a id="ValorDeMercado" href="{{ route('proceso/valorMercado', ['id' => $proceso->PROC_ID]) }}" class="loading-message" style="color: #ccc;"><i class="fa fa-upload fa-2x"></i><br>Valores de mercado</a>
				            </div>
				        </div>
				        <div class="col-md-2 col-xs-3">
				            <div style="padding: 10px">
				                <a id="Solver" href="{{ route('proceso/solver', ['id' => $proceso->PROC_ID]) }}" class="loading-message" style="color: #ccc"><i class="fa fa-calculator fa-2x"></i><br>Solver</a>
				            </div>
				        </div>
				        <div class="col-md-2 col-xs-3">
				            <div style="padding: 10px">
				                <a id="Escenarios" href="{{ route('proceso/escenarios', ['id' => $proceso->PROC_ID]) }}" class="loading-message" style="color: #ccc"><i class="fa fa-cog fa-2x"></i><br>Crea Escenarios</a>
				            </div>
				        </div>
				        <div class="col-md-2 col-xs-3">
				            <div style="padding: 10px">
				                <a  id="Proyeccion" href="{{ route('proceso/proyeccion', ['id' => $proceso->PROC_ID]) }}" class="loading-message" style="color: #ccc"><i class="fa fa-line-chart fa-2x"></i><br>Proyección</a>
				            </div>
				        </div>
				        <div class="col-md-2 col-xs-3">
				            <div style="padding: 10px">
				                <a id="Documentos" href="{{ route('proceso/documentos', ['id' => $proceso->PROC_ID]) }}" class="loading-message" style="color: #ccc"><i class="fa fa-file-text fa-2x"></i><br>Documentos</a>
				            </div>
				        </div>
				    </div>
				</div>
				<script type="text/javascript">
					$('#' + btnHabilitar).css('color', '');
				</script>
@extends('public-layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <script type="text/javascript">
                        var btnHabilitar='Escenarios';
                    </script>
                    @include('proceso.partials.menu-proceso')
                </div>
                <div class="panel-body" style="padding: 15px 0 15px 0">

                    @if(!$valores['PUEDE_CALCULAR_PUBLICAR'])
                        <div class="alert alert-danger" style="margin: 0 5px 14px 5px;clear: both">
                            Debe ingresar valores de mercado antes de poder crear/publicar escenarios
                        </div>
                    @endif

                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="proc_id" id="proc_id" value="{{ $proceso->PROC_ID }}">
                    <form class="form-inline" style="padding-left: 10px">
                        <div class="form-group">
                            <label for="exampleInputEmail3">Fecha Vigencia</label>
                            <input id="fechaVigencia" name="fechaVigencia" type="text" disabled="disabled" class="fecha form-control" style="width: 106px">
                        </div>
                        <div class="form-group" style="padding-left: 10px">
                            <label for="tipo">Tipo crudo</label>
                            <input type="checkbox" id="tipoCrudo" name="tipoCrudo" checked="true" class="checkBrentWti">
                        </div>
                    </form>
                    <div class="panel panel-primary panel-mod" style="margin: 4px;">
                        <div class="panel-heading">
                            Gasolina 93 
                        </div>
                        <div class="panel-body" style="padding: 4px">
                            <div class="col-md-12 col-xs-12" style="padding: 2px 0 4px">
                                <table class="table table-bordered" id="tablaValores-{{ Util::COMBUSTIBLE_93 }}">
                                    <thead>
                                        <th>Corrección RVP</th>
                                        <th><span class="text-lowercase">n</span></th>
                                        <th><span class="text-lowercase">m</span></th>
                                        <th><span class="text-lowercase">s</span></th>
                                        <th><span class="text-lowercase">t</span></th>
                                        <th><span class="text-lowercase">f</span></th>
                                        <th>Calcular</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                            	@if($valores['PARAM_93']->ESCA_CORRECCION_RVP == 1)
                                                    <input type="checkbox" id="{{ Util::COMBUSTIBLE_93 }}correccionRVP" name="correccionRvp" checked="true" class="rvpSiNo">
                                                @else
                                                    <input type="checkbox" id="{{ Util::COMBUSTIBLE_93 }}correccionRVP" name="correccionRvp" class="rvpSiNo">
                                                @endif
                                            </td>
                                            <td>
                                                @if ($valores['N_93_DISABLED'])
                                                    <select id="{{ Util::COMBUSTIBLE_93 }}n" class="form-control" disabled="true">
                                                @else
                                                    <select id="{{ Util::COMBUSTIBLE_93 }}n" class="form-control">
                                                @endif
                                                    @for ($i = $valores['N_93_MIN']; $i <= $valores['N_93_MAX']; $i = $i + $valores['N_93_INC'])
                                                        @if($valores['PARAM_93'] != null)
                                                            @if($valores['PARAM_93']->ESCA_N == $i)
                                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                                            @else
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endif
                                                    @endfor
                                                </select>
                                            </td>
                                            <td>
                                                @if ($valores['M_93_DISABLED'])
                                                    <select id="{{ Util::COMBUSTIBLE_93 }}m" class="form-control" disabled="true">
                                                @else
                                                    <select id="{{ Util::COMBUSTIBLE_93 }}m" class="form-control">
                                                @endif
                                                    @for ($i = $valores['M_93_MIN']; $i <= $valores['M_93_MAX']; $i = $i + $valores['M_93_INC'])
                                                        @if($valores['PARAM_93'] != null)
                                                            @if($valores['PARAM_93']->ESCA_M == $i)
                                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                                            @else
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endif
                                                    @endfor
                                                </select>
                                            </td>
                                            <td>
                                                @if ($valores['S_93_DISABLED'])
                                                    <select id="{{ Util::COMBUSTIBLE_93 }}s" class="form-control" disabled="true">
                                                @else
                                                    <select id="{{ Util::COMBUSTIBLE_93 }}s" class="form-control">
                                                @endif
                                                    @for ($i = $valores['S_93_MIN']; $i <= $valores['S_93_MAX']; $i = $i + $valores['S_93_INC'])
                                                        @if($valores['PARAM_93'] != null)
                                                            @if($valores['PARAM_93']->ESCA_S == $i)
                                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                                            @else
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endif
                                                    @endfor
                                                </select>
                                            </td>
                                            <td>
                                                @if ($valores['T_93_DISABLED'])
                                                    <select id="{{ Util::COMBUSTIBLE_93 }}t" class="form-control" disabled="true">
                                                @else
                                                    <select id="{{ Util::COMBUSTIBLE_93 }}t" class="form-control">
                                                @endif
                                                    @for ($i = $valores['T_93_MIN']; $i <= $valores['T_93_MAX']; $i = $i + $valores['T_93_INC'])
                                                        @if($valores['PARAM_93'] != null)
                                                            @if($valores['PARAM_93']->ESCA_T == $i)
                                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                                            @else
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endif
                                                    @endfor
                                                </select>
                                            </td>
                                            <td>
                                                @if ($valores['F_93_DISABLED'])
                                                    <select id="{{ Util::COMBUSTIBLE_93 }}f" class="form-control" disabled="true">
                                                @else
                                                    <select id="{{ Util::COMBUSTIBLE_93 }}f" class="form-control">
                                                @endif
                                                    @for ($i = $valores['F_93_MIN']; $i <= $valores['F_93_MAX']; $i = $i + $valores['F_93_INC'])
                                                        @if($valores['PARAM_93'] != null)
                                                            @if($valores['PARAM_93']->ESCA_F == $i)
                                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                                            @else
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endif
                                                    @endfor
                                                </select>
                                            </td>
                                            <td>
                                                @if($valores['FINALIZADO'] || !$valores['PUEDE_CALCULAR_PUBLICAR'])
                                                    <button class="btn btn-default" disabled="true">
                                                        <i class="fa fa-cog fa-1x"></i>
                                                    </button>
                                                @else
                                                    @if ($valores['PERFIL'] == Util::PERFIL_USUARIO_VALIDADOR)
                                                        <button class="btn btn-default" disabled="true">
                                                            <i class="fa fa-cog fa-1x"></i>
                                                        </button>
                                                    @else
                                                        <button onclick="addRow('{{ Util::COMBUSTIBLE_93 }}');calcular(this)" combustible="{{ Util::COMBUSTIBLE_93 }}" linea="1" class="btn btn-default">
                                                            <i class="fa fa-cog fa-1x"></i>
                                                        </button>
                                                    @endif
                                                @endif
                                                
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12 col-xs-12" style="padding: 2px 0 4px">
                                @if ($valores['PERFIL'] == Util::PERFIL_USUARIO_VALIDADOR)
                                    <div style="float: right; padding-right: 4px">
                                        <svg width="10" height="10">
                                            <rect width="10" height="10" style="fill:rgb(128, 179, 221)" />
                                        </svg>
                                        <span style="color: #a5a4a4;font-style: italic;font-size: 0.9em;">Línea sugerida por generador</span>
                                    </div>
                                @endif
                                <table class="table table-bordered lblResultado" id="tablaResultado-{{ Util::COMBUSTIBLE_93 }}">
                                    <thead>
                                        <tr>
                                            <th width="40"><span>RVP</span></th>
                                            <th width="40"><span class="text-lowercase">n</span></th>
                                            <th width="40"><span class="text-lowercase">m</span></th>
                                            <th width="40"><span class="text-lowercase">s</span></th>
                                            <th width="40"><span class="text-lowercase">t</span></th>
                                            <th width="40"><span class="text-lowercase">f</span></th>
                                            <th title="PRECIO PARIDAD">P.Paridad</th>
                                            <th title="TCRUDO">Tipo crudo</th>
                                            <th title="CRUDO">Crudo</th>
                                            <th title="MARGEN">Margen</th>
                                            <th title="PRECIO REFERENCIA INFERIOR">P.Ref Inf</th>
                                            <th title="PRECIO REFERENCIA INTERMEDIO">P.Ref Int</th>
                                            <th title="PRECIO REFERENCIA SUPERIOR">P.Ref Sup</th>
                                            @if($valores['PERFIL'] == Util::PERFIL_USUARIO_GENERADOR)
                                                <th width="70">Sugerir</th>
                                            @else
                                                <th width="80">Publicar</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($valores) && $valores[Util::COMBUSTIBLE_93] != null)
                                            @for ($i = 0; $i < count($valores[Util::COMBUSTIBLE_93]); $i++)
                                                @if($valores['PERFIL'] == Util::PERFIL_USUARIO_GENERADOR)
                                                    <tr>
                                                @else
                                                    @if($valores[Util::COMBUSTIBLE_93][$i]->ESCA_SUGERIR == 1)
                                                        <tr class="filaDestacada">
                                                    @else
                                                        <tr>
                                                    @endif
                                                @endif
                                                    <td><label id="{{ Util::COMBUSTIBLE_93 }}rvp{{$i + 1}}"         >{{$valores[Util::COMBUSTIBLE_93][$i]->ESCA_CORRECCION_RVP}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_93 }}n{{$i + 1}}"           >{{$valores[Util::COMBUSTIBLE_93][$i]->ESCA_N}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_93 }}m{{$i + 1}}"           >{{$valores[Util::COMBUSTIBLE_93][$i]->ESCA_M}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_93 }}s{{$i + 1}}"           >{{$valores[Util::COMBUSTIBLE_93][$i]->ESCA_S}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_93 }}t{{$i + 1}}"           >{{$valores[Util::COMBUSTIBLE_93][$i]->ESCA_T}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_93 }}f{{$i + 1}}"           >{{Util::formatNumber($valores[Util::COMBUSTIBLE_93][$i]->ESCA_F)}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_93 }}pparidad{{$i + 1}}"    >{{Util::formatNumber($valores[Util::COMBUSTIBLE_93][$i]->ESCA_PARIDAD)}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_93 }}tcrudo{{$i + 1}}"      >{{$valores[Util::COMBUSTIBLE_93][$i]->ESCA_TIPO_CRUDO}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_93 }}crudo{{$i + 1}}"       >{{Util::formatNumber($valores[Util::COMBUSTIBLE_93][$i]->ESCA_CRUDO)  }}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_93 }}margen{{$i + 1}}"      >{{Util::formatNumber($valores[Util::COMBUSTIBLE_93][$i]->ESCA_MARGEN) }}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_93 }}prinferior{{$i + 1}}"  >{{Util::formatNumber($valores[Util::COMBUSTIBLE_93][$i]->ESCA_PREFINF)}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_93 }}printermedio{{$i + 1}}">{{Util::formatNumber($valores[Util::COMBUSTIBLE_93][$i]->ESCA_PREFINT)}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_93 }}prsuperior{{$i + 1}}"  >{{Util::formatNumber($valores[Util::COMBUSTIBLE_93][$i]->ESCA_PREFSUP)}}</label></td>
                                                    @if($valores['PERFIL'] == Util::PERFIL_USUARIO_GENERADOR)

                                                        @if($valores[Util::COMBUSTIBLE_93][$i]->ESCA_SUGERIR == 1)
                                                            <td><input id="{{ Util::COMBUSTIBLE_93 }}chkSugerir{{$i + 1}}" type="checkbox" tipo="SUGERIR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_93][$i]->ESCA_ID}}" checked="true" class="checkSiNoDinamico"></td>
                                                        @else
                                                            <td><input id="{{ Util::COMBUSTIBLE_93 }}chkSugerir{{$i + 1}}" type="checkbox" tipo="SUGERIR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_93][$i]->ESCA_ID}}" class="checkSiNoDinamico"></td>
                                                        @endif

                                                    @else
                                                        @if($valores[Util::COMBUSTIBLE_93][$i]->ESCA_SUGERIR == 1)
                                                            @if($valores[Util::COMBUSTIBLE_93][$i]->ESCA_PUBLICADO == 1)
                                                                <td><input id="{{ Util::COMBUSTIBLE_93 }}chkPublicar{{$i + 1}}" type="checkbox" tipo="PUBLICAR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_93][$i]->ESCA_ID}}" checked="true" class="checkSiNoDinamico"></td>
                                                            @else
                                                                <td><input id="{{ Util::COMBUSTIBLE_93 }}chkPublicar{{$i + 1}}" type="checkbox" tipo="PUBLICAR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_93][$i]->ESCA_ID}}" class="checkSiNoDinamico"></td>
                                                            @endif
                                                        @else
                                                            @if($valores[Util::COMBUSTIBLE_93][$i]->ESCA_PUBLICADO == 1)
                                                                <td><input disabled="true" id="{{ Util::COMBUSTIBLE_93 }}chkPublicar{{$i + 1}}" type="checkbox" tipo="PUBLICAR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_93][$i]->ESCA_ID}}" checked="true" class="checkSiNoDinamico"></td>
                                                            @else
                                                                <td><input disabled="true" id="{{ Util::COMBUSTIBLE_93 }}chkPublicar{{$i + 1}}" type="checkbox" tipo="PUBLICAR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_93][$i]->ESCA_ID}}" class="checkSiNoDinamico"></td>
                                                            @endif
                                                        @endif
                                                    @endif                                                    
                                                </tr>
                                            @endfor
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-primary panel-mod" style="margin: 4px;">
                        <div class="panel-heading">
                            Gasolina 97
                        </div>
                        <div class="panel-body" style="padding: 4px">
                            <div class="col-md-12 col-xs-12" style="padding: 2px 0 4px">
                                <table class="table table-bordered" id="tablaValores-{{ Util::COMBUSTIBLE_97 }}">
                                    <thead>
                                        <th>Corrección RVP</th>
                                        <th><span class="text-lowercase">n</span></th>
                                        <th><span class="text-lowercase">m</span></th>
                                        <th><span class="text-lowercase">s</span></th>
                                        <th><span class="text-lowercase">t</span></th>
                                        <th><span class="text-lowercase">f</span></th>
                                        <th>Calcular</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                @if($valores['PARAM_97']->ESCA_CORRECCION_RVP == 1)
                                                    <input type="checkbox" id="{{ Util::COMBUSTIBLE_97 }}correccionRVP" name="correccionRvp" checked="true" class="rvpSiNo">
                                                @else
                                                    <input type="checkbox" id="{{ Util::COMBUSTIBLE_97 }}correccionRVP" name="correccionRvp" class="rvpSiNo">
                                                @endif
                                            </td>
                                            <td>
                                                @if ($valores['N_97_DISABLED'])
                                                    <select id="{{ Util::COMBUSTIBLE_97 }}n" class="form-control" disabled="true">
                                                @else
                                                    <select id="{{ Util::COMBUSTIBLE_97 }}n" class="form-control">
                                                @endif
                                                    @for ($i = $valores['N_97_MIN']; $i <= $valores['N_97_MAX']; $i = $i + $valores['N_97_INC'])
                                                        @if($valores['PARAM_97'] != null)
                                                            @if($valores['PARAM_97']->ESCA_N == $i)
                                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                                            @else
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endif
                                                    @endfor
                                                </select>
                                            </td>
                                            <td>
                                                @if ($valores['M_97_DISABLED'])
                                                    <select id="{{ Util::COMBUSTIBLE_97 }}m" class="form-control" disabled="true">
                                                @else
                                                    <select id="{{ Util::COMBUSTIBLE_97 }}m" class="form-control">
                                                @endif
                                                    @for ($i = $valores['M_97_MIN']; $i <= $valores['M_97_MAX']; $i = $i + $valores['M_97_INC'])
                                                        @if($valores['PARAM_97'] != null)
                                                            @if($valores['PARAM_97']->ESCA_M == $i)
                                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                                            @else
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endif
                                                    @endfor
                                                </select>
                                            </td>
                                            <td>
                                                @if ($valores['S_97_DISABLED'])
                                                    <select id="{{ Util::COMBUSTIBLE_97 }}s" class="form-control" disabled="true">
                                                @else
                                                    <select id="{{ Util::COMBUSTIBLE_97 }}s" class="form-control">
                                                @endif
                                                    @for ($i = $valores['S_97_MIN']; $i <= $valores['S_97_MAX']; $i = $i + $valores['S_97_INC'])
                                                        @if($valores['PARAM_97'] != null)
                                                            @if($valores['PARAM_97']->ESCA_S == $i)
                                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                                            @else
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endif
                                                    @endfor
                                                </select>
                                            </td>
                                            <td>
                                                @if ($valores['T_97_DISABLED'])
                                                    <select id="{{ Util::COMBUSTIBLE_97 }}t" class="form-control" disabled="true">
                                                @else
                                                    <select id="{{ Util::COMBUSTIBLE_97 }}t" class="form-control">
                                                @endif
                                                    @for ($i = $valores['T_97_MIN']; $i <= $valores['T_97_MAX']; $i = $i + $valores['T_97_INC'])
                                                        @if($valores['PARAM_97'] != null)
                                                            @if($valores['PARAM_97']->ESCA_T == $i)
                                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                                            @else
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endif
                                                    @endfor
                                                </select>
                                            </td>
                                            <td>
                                                @if ($valores['F_97_DISABLED'])
                                                    <select id="{{ Util::COMBUSTIBLE_97 }}f" class="form-control" disabled="true">
                                                @else
                                                    <select id="{{ Util::COMBUSTIBLE_97 }}f" class="form-control">
                                                @endif
                                                    @for ($i = $valores['F_97_MIN']; $i <= $valores['F_97_MAX']; $i = $i + $valores['F_97_INC'])
                                                        @if($valores['PARAM_97'] != null)
                                                            @if($valores['PARAM_97']->ESCA_F == $i)
                                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                                            @else
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endif
                                                    @endfor
                                                </select>
                                            </td>
                                            <td>
                                                @if($valores['FINALIZADO'] || !$valores['PUEDE_CALCULAR_PUBLICAR'])
                                                    <button class="btn btn-default" disabled="true">
                                                        <i class="fa fa-cog fa-1x"></i>
                                                    </button>
                                                @else
                                                    @if ($valores['PERFIL'] == Util::PERFIL_USUARIO_VALIDADOR)
                                                        <button class="btn btn-default" disabled="true">
                                                            <i class="fa fa-cog fa-1x"></i>
                                                        </button>
                                                    @else
                                                        <button onclick="addRow('{{ Util::COMBUSTIBLE_97 }}');calcular(this)" combustible="{{ Util::COMBUSTIBLE_97 }}" linea="1" class="btn btn-default">
                                                            <i class="fa fa-cog fa-1x"></i>
                                                        </button>
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12 col-xs-12" style="padding: 2px 0 4px">
                                @if ($valores['PERFIL'] == Util::PERFIL_USUARIO_VALIDADOR)
                                    <div style="float: right; padding-right: 4px">
                                        <svg width="10" height="10">
                                            <rect width="10" height="10" style="fill:rgb(128, 179, 221)" />
                                        </svg>
                                        <span style="color: #a5a4a4;font-style: italic;font-size: 0.9em;">Línea sugerida por generador</span>
                                    </div>
                                @endif
                                <table class="table table-bordered lblResultado" id="tablaResultado-{{ Util::COMBUSTIBLE_97 }}">
                                    <thead>
                                        <tr>
                                            <th width="40"><span>RVP</span></th>
                                            <th width="40"><span class="text-lowercase">n</span></th>
                                            <th width="40"><span class="text-lowercase">m</span></th>
                                            <th width="40"><span class="text-lowercase">s</span></th>
                                            <th width="40"><span class="text-lowercase">t</span></th>
                                            <th width="40"><span class="text-lowercase">f</span></th>
                                            <th title="PRECIO PARIDAD">P.Paridad</th>
                                            <th title="TCRUDO">Tipo crudo</th>
                                            <th title="CRUDO">Crudo</th>
                                            <th title="MARGEN">Margen</th>
                                            <th title="PRECIO REFERENCIA INFERIOR">P.Ref Inf</th>
                                            <th title="PRECIO REFERENCIA INTERMEDIO">P.Ref Int</th>
                                            <th title="PRECIO REFERENCIA SUPERIOR">P.Ref Sup</th>
                                            @if($valores['PERFIL'] == Util::PERFIL_USUARIO_GENERADOR)
                                                <th width="70">Sugerir</th>
                                            @else
                                                <th width="80">Publicar</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($valores) && $valores[Util::COMBUSTIBLE_97] != null)
                                            @for ($i = 0; $i < count($valores[Util::COMBUSTIBLE_97]); $i++)
                                                @if($valores['PERFIL'] == Util::PERFIL_USUARIO_GENERADOR)
                                                    <tr>
                                                @else
                                                    @if($valores[Util::COMBUSTIBLE_97][$i]->ESCA_SUGERIR == 1)
                                                        <tr class="filaDestacada">
                                                    @else
                                                        <tr>
                                                    @endif
                                                @endif
                                                    <td><label id="{{ Util::COMBUSTIBLE_97 }}rvp{{$i + 1}}"         >{{$valores[Util::COMBUSTIBLE_97][$i]->ESCA_CORRECCION_RVP}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_97 }}n{{$i + 1}}"           >{{$valores[Util::COMBUSTIBLE_97][$i]->ESCA_N}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_97 }}m{{$i + 1}}"           >{{$valores[Util::COMBUSTIBLE_97][$i]->ESCA_M}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_97 }}s{{$i + 1}}"           >{{$valores[Util::COMBUSTIBLE_97][$i]->ESCA_S}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_97 }}t{{$i + 1}}"           >{{$valores[Util::COMBUSTIBLE_97][$i]->ESCA_T}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_97 }}f{{$i + 1}}"           >{{Util::formatNumber($valores[Util::COMBUSTIBLE_97][$i]->ESCA_F)}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_97 }}pparidad{{$i + 1}}"    >{{Util::formatNumber($valores[Util::COMBUSTIBLE_97][$i]->ESCA_PARIDAD)}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_93 }}tcrudo{{$i + 1}}"      >{{$valores[Util::COMBUSTIBLE_97][$i]->ESCA_TIPO_CRUDO}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_97 }}crudo{{$i + 1}}"       >{{Util::formatNumber($valores[Util::COMBUSTIBLE_97][$i]->ESCA_CRUDO)  }}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_97 }}margen{{$i + 1}}"      >{{Util::formatNumber($valores[Util::COMBUSTIBLE_97][$i]->ESCA_MARGEN) }}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_97 }}prinferior{{$i + 1}}"  >{{Util::formatNumber($valores[Util::COMBUSTIBLE_97][$i]->ESCA_PREFINF)}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_97 }}printermedio{{$i + 1}}">{{Util::formatNumber($valores[Util::COMBUSTIBLE_97][$i]->ESCA_PREFINT)}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_97 }}prsuperior{{$i + 1}}"  >{{Util::formatNumber($valores[Util::COMBUSTIBLE_97][$i]->ESCA_PREFSUP)}}</label></td>
                                                    @if($valores['PERFIL'] == Util::PERFIL_USUARIO_GENERADOR)

                                                        @if($valores[Util::COMBUSTIBLE_97][$i]->ESCA_SUGERIR == 1)
                                                            <td><input id="{{ Util::COMBUSTIBLE_97 }}chkSugerir{{$i + 1}}" type="checkbox" tipo="SUGERIR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_97][$i]->ESCA_ID}}" checked="true" class="checkSiNoDinamico"></td>
                                                        @else
                                                            <td><input id="{{ Util::COMBUSTIBLE_97 }}chkSugerir{{$i + 1}}" type="checkbox" tipo="SUGERIR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_97][$i]->ESCA_ID}}" class="checkSiNoDinamico"></td>
                                                        @endif

                                                    @else

                                                        @if($valores[Util::COMBUSTIBLE_97][$i]->ESCA_SUGERIR == 1)
                                                            @if($valores[Util::COMBUSTIBLE_97][$i]->ESCA_PUBLICADO == 1)
                                                                <td><input id="{{ Util::COMBUSTIBLE_97 }}chkPublicar{{$i + 1}}" type="checkbox" tipo="PUBLICAR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_97][$i]->ESCA_ID}}" checked="true" class="checkSiNoDinamico"></td>
                                                            @else
                                                                <td><input id="{{ Util::COMBUSTIBLE_97 }}chkPublicar{{$i + 1}}" type="checkbox" tipo="PUBLICAR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_97][$i]->ESCA_ID}}" class="checkSiNoDinamico"></td>
                                                            @endif
                                                        @else
                                                            @if($valores[Util::COMBUSTIBLE_97][$i]->ESCA_PUBLICADO == 1)
                                                                <td><input disabled="true" id="{{ Util::COMBUSTIBLE_97 }}chkPublicar{{$i + 1}}" type="checkbox" tipo="PUBLICAR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_97][$i]->ESCA_ID}}" checked="true" class="checkSiNoDinamico"></td>
                                                            @else
                                                                <td><input disabled="true" id="{{ Util::COMBUSTIBLE_97 }}chkPublicar{{$i + 1}}" type="checkbox" tipo="PUBLICAR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_97][$i]->ESCA_ID}}" class="checkSiNoDinamico"></td>
                                                            @endif

                                                        @endif
                                                    @endif 
                                                </tr>
                                            @endfor
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-primary panel-mod" style="margin: 4px;">
                        <div class="panel-heading">
                            DIESEL
                        </div>
                        <div class="panel-body" style="padding: 4px">
                            <div class="col-md-12 col-xs-12" style="padding: 2px 0 4px">
                                <table class="table table-bordered" id="tablaValores-{{ Util::COMBUSTIBLE_DIESEL }}">
                                    <thead>
                                        <th><span class="text-lowercase">n</span></th>
                                        <th><span class="text-lowercase">m</span></th>
                                        <th><span class="text-lowercase">s</span></th>
                                        <th><span class="text-lowercase">t</span></th>
                                        <th><span class="text-lowercase">f</span></th>
                                        <th>Calcular</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                @if ($valores['N_DIESEL_DISABLED'])
                                                    <select id="{{ Util::COMBUSTIBLE_DIESEL }}n" class="form-control" disabled="true">
                                                @else
                                                    <select id="{{ Util::COMBUSTIBLE_DIESEL }}n" class="form-control">
                                                @endif
                                                    @for ($i = $valores['N_DIESEL_MIN']; $i <= $valores['N_DIESEL_MAX']; $i = $i + $valores['N_DIESEL_INC'])
                                                        @if($valores['PARAM_DIESEL'] != null)
                                                            @if($valores['PARAM_DIESEL']->ESCA_N == $i)
                                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                                            @else
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endif
                                                    @endfor
                                                </select>
                                            </td>
                                            <td>
                                                @if ($valores['M_DIESEL_DISABLED'])
                                                    <select id="{{ Util::COMBUSTIBLE_DIESEL }}m" class="form-control" disabled="true">
                                                @else
                                                    <select id="{{ Util::COMBUSTIBLE_DIESEL }}m" class="form-control">
                                                @endif
                                                    @for ($i = $valores['M_DIESEL_MIN']; $i <= $valores['M_DIESEL_MAX']; $i = $i + $valores['M_DIESEL_INC'])
                                                        @if($valores['PARAM_DIESEL'] != null)
                                                            @if($valores['PARAM_DIESEL']->ESCA_M == $i)
                                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                                            @else
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endif
                                                    @endfor
                                                </select>                                            
                                            </td>
                                            <td>
                                                @if ($valores['S_DIESEL_DISABLED'])
                                                    <select id="{{ Util::COMBUSTIBLE_DIESEL }}s" class="form-control" disabled="true">
                                                @else
                                                    <select id="{{ Util::COMBUSTIBLE_DIESEL }}s" class="form-control">
                                                @endif
                                                    @for ($i = $valores['S_DIESEL_MIN']; $i <= $valores['S_DIESEL_MAX']; $i = $i + $valores['S_DIESEL_INC'])
                                                        @if($valores['PARAM_DIESEL'] != null)
                                                            @if($valores['PARAM_DIESEL']->ESCA_S == $i)
                                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                                            @else
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endif
                                                    @endfor
                                                </select>
                                            </td>
                                            <td>
                                                @if ($valores['T_DIESEL_DISABLED'])
                                                    <select id="{{ Util::COMBUSTIBLE_DIESEL }}t" class="form-control" disabled="true">
                                                @else
                                                    <select id="{{ Util::COMBUSTIBLE_DIESEL }}t" class="form-control">
                                                @endif
                                                    @for ($i = $valores['T_DIESEL_MIN']; $i <= $valores['T_DIESEL_MAX']; $i = $i + $valores['T_DIESEL_INC'])
                                                        @if($valores['PARAM_DIESEL'] != null)
                                                            @if($valores['PARAM_DIESEL']->ESCA_T == $i)
                                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                                            @else
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endif
                                                    @endfor
                                                </select>
                                            </td>
                                            <td>
                                                @if ($valores['F_DIESEL_DISABLED'])
                                                    <select id="{{ Util::COMBUSTIBLE_DIESEL }}f" class="form-control" disabled="true">
                                                @else
                                                    <select id="{{ Util::COMBUSTIBLE_DIESEL }}f" class="form-control">
                                                @endif
                                                    @for ($i = $valores['F_DIESEL_MIN']; $i <= $valores['F_DIESEL_MAX']; $i = $i + $valores['F_DIESEL_INC'])
                                                        @if($valores['PARAM_DIESEL'] != null)
                                                            @if($valores['PARAM_DIESEL']->ESCA_F == $i)
                                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                                            @else
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endif
                                                    @endfor
                                                </select>
                                            </td>
                                            <td>
                                                @if($valores['FINALIZADO'] || !$valores['PUEDE_CALCULAR_PUBLICAR'])
                                                    <button class="btn btn-default" disabled="true">
                                                        <i class="fa fa-cog fa-1x"></i>
                                                    </button>
                                                @else
                                                    @if ($valores['PERFIL'] == Util::PERFIL_USUARIO_VALIDADOR)
                                                        <button class="btn btn-default" disabled="true">
                                                            <i class="fa fa-cog fa-1x"></i>
                                                        </button>
                                                    @else
                                                        <button onclick="addRow('{{ Util::COMBUSTIBLE_DIESEL }}');calcular(this)" combustible="{{ Util::COMBUSTIBLE_DIESEL }}" linea="1" class="btn btn-default">
                                                            <i class="fa fa-cog fa-1x"></i>
                                                        </button>
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12 col-xs-12" style="padding: 2px 0 4px">
                                @if ($valores['PERFIL'] == Util::PERFIL_USUARIO_VALIDADOR)
                                    <div style="float: right; padding-right: 4px">
                                        <svg width="10" height="10">
                                            <rect width="10" height="10" style="fill:rgb(128, 179, 221)" />
                                        </svg>
                                        <span style="color: #a5a4a4;font-style: italic;font-size: 0.9em;">Línea sugerida por generador</span>
                                    </div>
                                @endif
                                <table class="table table-bordered lblResultado" id="tablaResultado-{{ Util::COMBUSTIBLE_DIESEL }}">
                                    <thead>
                                        <tr>
                                            <th width="40"><span class="text-lowercase">n</span></th>
                                            <th width="40"><span class="text-lowercase">m</span></th>
                                            <th width="40"><span class="text-lowercase">s</span></th>
                                            <th width="40"><span class="text-lowercase">t</span></th>
                                            <th width="40"><span class="text-lowercase">f</span></th>
                                            <th title="PRECIO PARIDAD">P.Paridad</th>
                                            <th title="TCRUDO">Tipo crudo</th>
                                            <th title="CRUDO">Crudo</th>
                                            <th title="MARGEN">Margen</th>
                                            <th title="PRECIO REFERENCIA INFERIOR">P.Ref Inf</th>
                                            <th title="PRECIO REFERENCIA INTERMEDIO">P.Ref Int</th>
                                            <th title="PRECIO REFERENCIA SUPERIOR">P.Ref Sup</th>
                                            @if($valores['PERFIL'] == Util::PERFIL_USUARIO_GENERADOR)
                                                <th width="70">Sugerir</th>
                                            @else
                                                <th width="80">Publicar</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($valores) && $valores[Util::COMBUSTIBLE_DIESEL] != null)
                                            @for ($i = 0; $i < count($valores[Util::COMBUSTIBLE_DIESEL]); $i++)
                                                @if($valores['PERFIL'] == Util::PERFIL_USUARIO_GENERADOR)
                                                    <tr>
                                                @else
                                                    @if($valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_SUGERIR == 1)
                                                        <tr class="filaDestacada">
                                                    @else
                                                        <tr>
                                                    @endif
                                                @endif
                                                    <td><label id="{{ Util::COMBUSTIBLE_DIESEL }}n{{$i + 1}}"           >{{$valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_N}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_DIESEL }}m{{$i + 1}}"           >{{$valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_M}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_DIESEL }}s{{$i + 1}}"           >{{$valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_S}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_DIESEL }}t{{$i + 1}}"           >{{$valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_T}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_DIESEL }}f{{$i + 1}}"           >{{Util::formatNumber($valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_F)}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_DIESEL }}pparidad{{$i + 1}}"    >{{Util::formatNumber($valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_PARIDAD)}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_DIESEL }}tcrudo{{$i + 1}}"      >{{$valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_TIPO_CRUDO}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_DIESEL }}crudo{{$i + 1}}"       >{{Util::formatNumber($valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_CRUDO)  }}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_DIESEL }}margen{{$i + 1}}"      >{{Util::formatNumber($valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_MARGEN) }}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_DIESEL }}prinferior{{$i + 1}}"  >{{Util::formatNumber($valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_PREFINF)}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_DIESEL }}printermedio{{$i + 1}}">{{Util::formatNumber($valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_PREFINT)}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_DIESEL }}prsuperior{{$i + 1}}"  >{{Util::formatNumber($valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_PREFSUP)}}</label></td>
                                                    @if($valores['PERFIL'] == Util::PERFIL_USUARIO_GENERADOR)

                                                        @if($valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_SUGERIR == 1)
                                                            <td><input id="{{ Util::COMBUSTIBLE_DIESEL }}chkSugerir{{$i + 1}}" type="checkbox" tipo="SUGERIR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_ID}}" checked="true" class="checkSiNoDinamico"></td>
                                                        @else
                                                            <td><input id="{{ Util::COMBUSTIBLE_DIESEL }}chkSugerir{{$i + 1}}" type="checkbox" tipo="SUGERIR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_ID}}" class="checkSiNoDinamico"></td>
                                                        @endif

                                                    @else

                                                        @if($valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_SUGERIR == 1)
                                                            @if($valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_PUBLICADO == 1)
                                                                <td><input id="{{ Util::COMBUSTIBLE_DIESEL }}chkPublicar{{$i + 1}}" type="checkbox" tipo="PUBLICAR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_ID}}" checked="true" class="checkSiNoDinamico"></td>
                                                            @else
                                                                <td><input id="{{ Util::COMBUSTIBLE_DIESEL }}chkPublicar{{$i + 1}}" type="checkbox" tipo="PUBLICAR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_ID}}" class="checkSiNoDinamico"></td>
                                                            @endif
                                                        @else
                                                            @if($valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_PUBLICADO == 1)
                                                                <td><input disabled="true" id="{{ Util::COMBUSTIBLE_DIESEL }}chkPublicar{{$i + 1}}" type="checkbox" tipo="PUBLICAR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_ID}}" checked="true" class="checkSiNoDinamico"></td>
                                                            @else
                                                                <td><input disabled="true" id="{{ Util::COMBUSTIBLE_DIESEL }}chkPublicar{{$i + 1}}" type="checkbox" tipo="PUBLICAR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_DIESEL][$i]->ESCA_ID}}" class="checkSiNoDinamico"></td>
                                                            @endif

                                                        @endif
                                                    @endif 
                                                </tr>
                                            @endfor
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-primary panel-mod" style="margin: 4px;">
                        <div class="panel-heading">
                            GLP
                        </div>
                        <div class="panel-body" style="padding: 4px">
                            <div class="col-md-12 col-xs-12" style="padding: 2px 0 4px">
                                <table class="table table-bordered" id="tablaValores-{{ Util::COMBUSTIBLE_GLP }}">
                                    <thead>
                                        <th><span class="text-lowercase">n</span></th>
                                        <th><span class="text-lowercase">m</span></th>
                                        <th><span class="text-lowercase">s</span></th>
                                        <th><span class="text-lowercase">t</span></th>
                                        <th><span class="text-lowercase">f</span></th>
                                        <th>Calcular</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                @if ($valores['N_GLP_DISABLED'])
                                                    <select id="{{ Util::COMBUSTIBLE_GLP }}n" class="form-control" disabled="true">
                                                @else
                                                    <select id="{{ Util::COMBUSTIBLE_GLP }}n" class="form-control">
                                                @endif
                                                    @for ($i = $valores['N_GLP_MIN']; $i <= $valores['N_GLP_MAX']; $i = $i + $valores['N_GLP_INC'])
                                                        @if($valores['PARAM_GLP'] != null)
                                                            @if($valores['PARAM_GLP']->ESCA_N == $i)
                                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                                            @else
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endif
                                                    @endfor
                                                </select>
                                            </td>
                                            <td>
                                                @if ($valores['M_GLP_DISABLED'])
                                                    <select id="{{ Util::COMBUSTIBLE_GLP }}m" class="form-control" disabled="true">
                                                @else
                                                    <select id="{{ Util::COMBUSTIBLE_GLP }}m" class="form-control">
                                                @endif
                                                    @for ($i = $valores['M_GLP_MIN']; $i <= $valores['M_GLP_MAX']; $i = $i + $valores['M_GLP_INC'])
                                                        @if($valores['PARAM_GLP'] != null)
                                                            @if($valores['PARAM_GLP']->ESCA_M == $i)
                                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                                            @else
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endif
                                                    @endfor
                                                </select> 
                                            </td>
                                            <td>
                                                @if ($valores['S_GLP_DISABLED'])
                                                    <select id="{{ Util::COMBUSTIBLE_GLP }}s" class="form-control" disabled="true">
                                                @else
                                                    <select id="{{ Util::COMBUSTIBLE_GLP }}s" class="form-control">
                                                @endif
                                                    @for ($i = $valores['S_GLP_MIN']; $i <= $valores['S_GLP_MAX']; $i = $i + $valores['S_GLP_INC'])
                                                        @if($valores['PARAM_GLP'] != null)
                                                            @if($valores['PARAM_GLP']->ESCA_S == $i)
                                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                                            @else
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endif
                                                    @endfor
                                                </select>
                                            </td>
                                            <td>
                                                @if ($valores['T_GLP_DISABLED'])
                                                    <select id="{{ Util::COMBUSTIBLE_GLP }}t" class="form-control" disabled="true">
                                                @else
                                                    <select id="{{ Util::COMBUSTIBLE_GLP }}t" class="form-control">
                                                @endif
                                                    @for ($i = $valores['T_GLP_MIN']; $i <= $valores['T_GLP_MAX']; $i = $i + $valores['T_GLP_INC'])
                                                        @if($valores['PARAM_GLP'] != null)
                                                            @if($valores['PARAM_GLP']->ESCA_T == $i)
                                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                                            @else
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endif
                                                    @endfor
                                                </select>
                                            </td>
                                            <td>
                                                @if ($valores['F_GLP_DISABLED'])
                                                    <select id="{{ Util::COMBUSTIBLE_GLP }}f" class="form-control" disabled="true">
                                                @else
                                                    <select id="{{ Util::COMBUSTIBLE_GLP }}f" class="form-control">
                                                @endif
                                                    @for ($i = $valores['F_GLP_MIN']; $i <= $valores['F_GLP_MAX']; $i = $i + $valores['F_GLP_INC'])
                                                        @if($valores['PARAM_GLP'] != null)
                                                            @if($valores['PARAM_GLP']->ESCA_F == $i)
                                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                                            @else
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endif
                                                    @endfor
                                                </select>
                                            </td>
                                            <td>
                                                @if($valores['FINALIZADO'] || !$valores['PUEDE_CALCULAR_PUBLICAR'])
                                                    <button linea="1" class="btn btn-default" disabled="true">
                                                        <i class="fa fa-cog fa-1x"></i>
                                                    </button>
                                                @else
                                                    @if ($valores['PERFIL'] == Util::PERFIL_USUARIO_VALIDADOR)
                                                        <button class="btn btn-default" disabled="true">
                                                            <i class="fa fa-cog fa-1x"></i>
                                                        </button>
                                                    @else
                                                        <button onclick="addRow('{{ Util::COMBUSTIBLE_GLP }}');calcular(this)" combustible="{{ Util::COMBUSTIBLE_GLP }}" linea="1" class="btn btn-default">
                                                            <i class="fa fa-cog fa-1x"></i>
                                                        </button>
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12 col-xs-12" style="padding: 2px 0 4px">
                                @if ($valores['PERFIL'] == Util::PERFIL_USUARIO_VALIDADOR)
                                    <div style="float: right; padding-right: 4px">
                                        <svg width="10" height="10">
                                            <rect width="10" height="10" style="fill:rgb(128, 179, 221)" />
                                        </svg>
                                        <span style="color: #a5a4a4;font-style: italic;font-size: 0.9em;">Línea sugerida por generador</span>
                                    </div>
                                @endif
                                <table class="table table-bordered lblResultado" id="tablaResultado-{{ Util::COMBUSTIBLE_GLP }}">
                                    <thead>
                                        <tr>
                                            <th width="40"><span class="text-lowercase">n</span></th>
                                            <th width="40"><span class="text-lowercase">m</span></th>
                                            <th width="40"><span class="text-lowercase">s</span></th>
                                            <th width="40"><span class="text-lowercase">t</span></th>
                                            <th width="40"><span class="text-lowercase">f</span></th>
                                            <th title="PRECIO PARIDAD">P.Paridad</th>
                                            <th title="TCRUDO">Tipo Crudo</th>
                                            <th title="CRUDO">Crudo</th>
                                            <th title="MARGEN">Margen</th>
                                            <th title="PRECIO REFERENCIA INFERIOR">P.Ref Inf</th>
                                            <th title="PRECIO REFERENCIA INTERMEDIO">P.Ref Int</th>
                                            <th title="PRECIO REFERENCIA SUPERIOR">P.Ref Sup</th>
                                            @if($valores['PERFIL'] == Util::PERFIL_USUARIO_GENERADOR)
                                                <th width="70">Sugerir</th>
                                            @else
                                                <th width="80">Publicar</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($valores) && $valores[Util::COMBUSTIBLE_GLP] != null)
                                            @for ($i = 0; $i < count($valores[Util::COMBUSTIBLE_GLP]); $i++)
                                                @if($valores['PERFIL'] == Util::PERFIL_USUARIO_GENERADOR)
                                                    <tr>
                                                @else
                                                    @if($valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_SUGERIR == 1)
                                                        <tr class="filaDestacada">
                                                    @else
                                                        <tr>
                                                    @endif
                                                @endif
                                                    <td><label id="{{ Util::COMBUSTIBLE_GLP }}n{{$i + 1}}"           >{{$valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_N}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_GLP }}m{{$i + 1}}"           >{{$valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_M}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_GLP }}s{{$i + 1}}"           >{{$valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_S}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_GLP }}t{{$i + 1}}"           >{{$valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_T}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_GLP }}f{{$i + 1}}"           >{{Util::formatNumber($valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_F)}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_GLP }}pparidad{{$i + 1}}"    >{{Util::formatNumber($valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_PARIDAD)}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_GLP }}tcrudo{{$i + 1}}"      >{{$valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_TIPO_CRUDO}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_GLP }}crudo{{$i + 1}}"       >{{Util::formatNumber($valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_CRUDO)  }}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_GLP }}margen{{$i + 1}}"      >{{Util::formatNumber($valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_MARGEN) }}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_GLP }}prinferior{{$i + 1}}"  >{{Util::formatNumber($valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_PREFINF)}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_GLP }}printermedio{{$i + 1}}">{{Util::formatNumber($valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_PREFINT)}}</label></td>
                                                    <td><label id="{{ Util::COMBUSTIBLE_GLP }}prsuperior{{$i + 1}}"  >{{Util::formatNumber($valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_PREFSUP)}}</label></td>
                                                    @if($valores['PERFIL'] == Util::PERFIL_USUARIO_GENERADOR)

                                                        @if($valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_SUGERIR == 1)
                                                            <td><input id="{{ Util::COMBUSTIBLE_GLP }}chkSugerir{{$i + 1}}" type="checkbox" tipo="SUGERIR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_ID}}" checked="true" class="checkSiNoDinamico"></td>
                                                        @else
                                                            <td><input id="{{ Util::COMBUSTIBLE_GLP }}chkSugerir{{$i + 1}}" type="checkbox" tipo="SUGERIR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_ID}}" class="checkSiNoDinamico"></td>
                                                        @endif

                                                    @else

                                                        @if($valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_SUGERIR == 1)
                                                            @if($valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_PUBLICADO == 1)
                                                                <td><input id="{{ Util::COMBUSTIBLE_GLP }}chkPublicar{{$i + 1}}" type="checkbox" tipo="PUBLICAR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_ID}}" checked="true" class="checkSiNoDinamico"></td>
                                                            @else
                                                                <td><input id="{{ Util::COMBUSTIBLE_GLP }}chkPublicar{{$i + 1}}" type="checkbox" tipo="PUBLICAR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_ID}}" class="checkSiNoDinamico"></td>
                                                            @endif
                                                        @else
                                                            @if($valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_PUBLICADO == 1)
                                                                <td><input disabled="true" id="{{ Util::COMBUSTIBLE_GLP }}chkPublicar{{$i + 1}}" type="checkbox" tipo="PUBLICAR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_ID}}" checked="true" class="checkSiNoDinamico"></td>
                                                            @else
                                                                <td><input disabled="true" id="{{ Util::COMBUSTIBLE_GLP }}chkPublicar{{$i + 1}}" type="checkbox" tipo="PUBLICAR" idescenariocalculo="{{$valores[Util::COMBUSTIBLE_GLP][$i]->ESCA_ID}}" class="checkSiNoDinamico"></td>
                                                            @endif

                                                        @endif
                                                    @endif 
                                                </tr>
                                            @endfor
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-offset-8 col-md-4" style="text-align: right">
                        <a href="{{ route('proceso/escenarios/expo', ['id' => $proceso->PROC_ID]) }}" class="btn btn-success">Descargar XLS</a>
                        @if($valores['PERFIL'] == Util::PERFIL_USUARIO_GENERADOR)
                            @if($valores['FINALIZADO'] || !$valores['PUEDE_CALCULAR_PUBLICAR'])
                                <button class="btn btn-primary" disabled="true">Sugerir</button>
                            @else
                                <button class="btn btn-primary" onclick="sugerirCalculo()">Sugerir</button>
                            @endif
                        @else
                            @if($valores['FINALIZADO'] || !$valores['PUEDE_CALCULAR_PUBLICAR'])
                                <button class="btn btn-primary" disabled="true">Publicar</button>
                                @if($valores['ROLLBACK_VISIBLE'])
                                    <button class="btn btn-danger" onclick="rollbackModal()">RollBack</button>
                                @endif
                            @else
                                @if($valores['PUEDE_CALCULAR_PUBLICAR'])
                                    <button class="btn btn-primary" onclick="publicarCalculo()">Publicar</button>
                                    <button class="btn btn-warning" onclick="rechazarModal()">Enviar comentario</button>
                                @endif
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- LOADING MESSAGE PROCESO -->
    <div class="modal fade" id="procesoStatus" keyboard="false" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="procesoStatusTitle">Calculando ...</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered">
                        <tr>
                            <td style="background-color: #F5F5F5; font-weight: bold">P.PARIDAD</td>
                            <td><span id="lblProcesoStatusPPARIDAD"></span>&nbsp;<i id="spinProcesoStatusPPARIDAD" class="fa fa-refresh fa-spin"></i></td>
                        </tr>
                        <tr>
                            <td style="background-color: #F5F5F5; font-weight: bold">CRUDO</td>
                            <td><span id="lblProcesoStatusCRUDO"></span>&nbsp;<i id="spinProcesoStatusCRUDO" class="fa fa-refresh fa-spin"></i></td>
                        </tr>
                        <tr>
                            <td style="background-color: #F5F5F5; font-weight: bold">MARGEN</td>
                            <td><span id="lblProcesoStatusMARGEN"></span>&nbsp;<i id="spinProcesoStatusMARGEN" class="fa fa-refresh fa-spin"></i></td>
                        </tr>
                        <tr>
                            <td style="background-color: #F5F5F5; font-weight: bold">P.REF INF</td>
                            <td><span id="lblProcesoStatusPREFINF"></span>&nbsp;<i id="spinProcesoStatusPREFINF" class="fa fa-refresh fa-spin"></i></td>
                        </tr>
                        <tr>
                            <td style="background-color: #F5F5F5; font-weight: bold">P.REF INT</td>
                            <td><span id="lblProcesoStatusPREFINT"></span>&nbsp;<i id="spinProcesoStatusPREFINT" class="fa fa-refresh fa-spin"></i></td>
                        </tr>
                        <tr>
                            <td style="background-color: #F5F5F5; font-weight: bold">P.REF SUP</td>
                            <td><span id="lblProcesoStatusPREFSUP"></span>&nbsp;<i id="spinProcesoStatusPREFSUP" class="fa fa-refresh fa-spin"></i></td>
                        </tr>
                    </table>
                    <div class="alert alert-danger" id="lblErrorProceso" style="display: none">Ha ocurrido un error al procesar calculo, favor reintentar.<br>Si el problema persiste contacte a Administrador</div>
                </div>
                <div class="modal-footer">
                    <button id="btnProcesoStatus" type="button" onclick="destacarFila()" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- SUGERIR -->
    <div class="modal fade" id="modalSugerir" keyboard="false" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalSugerir-titulo"></h4>
                </div>
                <div class="modal-body">
                    <div id="modalSugerir-error" class="alert alert-danger"><ul id="modalSugerir-listError"></ul></div>
                    <div id="modalSugerir-success" class="alert alert-success">
                    <span id="modalSugerir-success-msg"></span>
                    </div>
                </div>
                <div class="modal-footer" id="modalSugerir-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- ROLLBACK -->
    <div class="modal fade" id="modalRollBack" keyboard="false" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">RollBack</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning">¿Desea realizar rollback del proceso?</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" onclick="rollBackLink()">Si</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>

    <!-- RECHAZAR ESCENARIO -->
    <div class="modal fade" id="modalRechazar" keyboard="false" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Enviar comentario</h4>
                </div>
                <div class="modal-body">
                    <div class="contenido_modal">
                        <div class="form-group">
                            <div class="alert alert-warning">Escriba un comentario. Éste comentario se enviará por correo electrónico al Usuario Generador</div>
                        </div>
                        <div class="form-group">
                                <textarea class="form-control" id="comentario_rechazar" name="comentario_rechazar"></textarea>
                        </div>
                    </div>
                    <div id="exito" style="display:none;">
                        <div class="form-group">
                            <div class="alert alert-success">Comentario enviado con éxito.</div>
                        </div>
                    </div>
                    <div id="error" style="display:none;">
                        <div class="form-group">
                            <div class="alert alert-danger">Error al envíar el correo electrónico. Inténtelo de nuevo o contacte con el Administrador del Sistema</div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="rechazarEscenario" class="btn btn-warning" onclick="rechazarEscenario()">Enviar comentario</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- TIME OUT -->
    <div class="modal fade" id="modalTimeOut" keyboard="false" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Sesión expirada</h4>
                </div>
                <div class="modal-body">
                    <div class="contenido_modal">
                        <div class="form-group">
                            <div class="alert alert-warning">
                                Su sesión ha expirado, favor vuelva a ingresar
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="{{ route('auth/logout') }}" class="btn btn-primary loading-message">Aceptar</a>
                </div>
            </div>
        </div>
    </div>

@endsection  

@section('script')
    <script type="text/javascript">

        var P_REF_INF = '{{Util::P_REF_INF}}';
        var P_REF_INT = '{{Util::P_REF_INT}}';
        var P_REF_SUP = '{{Util::P_REF_SUP}}';
        var P_PARIDAD = '{{Util::P_PARIDAD}}';
        var MARGEN    = '{{Util::MARGEN}}';
        var CRUDO     = '{{Util::CRUDO}}';
        var filaParaDescatar = null;

        $(document).ready(function() {
            activeMenuItem('proceso');

            initializeSwitch('.checkSiNoDinamico');

            $('.rvpSiNo').bootstrapSwitch({
                onText :'SI',
                offText:'NO',
                size   :'mini'
            }); 

            $('.rvpSiNo').on('switchChange.bootstrapSwitch', function(event, state) {
            	$('.rvpSiNo').bootstrapSwitch('state', state);
			});

            $('#btnProcesoStatus').click(function (){
                procesoStatusReset();
            });

            iniciarChkReadOnly({{Util::COMBUSTIBLE_93}});
            iniciarChkReadOnly({{Util::COMBUSTIBLE_97}});
            iniciarChkReadOnly({{Util::COMBUSTIBLE_DIESEL}});
            iniciarChkReadOnly({{Util::COMBUSTIBLE_GLP}});

        }); 

        function sugerirCalculo()
        {
            var chkTable93     = $('#tablaResultado-{{Util::COMBUSTIBLE_93}}').find('input[type="checkbox"]:checked');
            var chkTable97     = $('#tablaResultado-{{Util::COMBUSTIBLE_97}}').find('input[type="checkbox"]:checked');
            var chkTableDIESEL = $('#tablaResultado-{{Util::COMBUSTIBLE_DIESEL}}').find('input[type="checkbox"]:checked');
            var chkTableGLP    = $('#tablaResultado-{{Util::COMBUSTIBLE_GLP}}').find('input[type="checkbox"]:checked');
            var lstError       = [];

            if(chkTable93.length == 0)
            {
                lstError.push('combustible 93');
            }
            if(chkTable97.length == 0)
            {
                lstError.push('combustible 97');
            }
            if(chkTableDIESEL.length == 0)
            {
                lstError.push('combustible DIESEL');
            }
            if(chkTableGLP.length == 0)
            {
                lstError.push('combustible GLP');
            }

            if(lstError.length != 0)
            {
                $('#modalSugerir-error').show();
                $('#modalSugerir-success').hide();
                $('#modalSugerir-listError').empty();
                $('#modalSugerir-titulo').html('Falta sugerir cálculo para:');
                for (var i=0;i < lstError.length; i++) {
                    $('#modalSugerir-listError').append('<li>' + lstError[i] +'</li>');
                };
                $('#modalSugerir').modal('show');
            }else{
                $.ajax({
                    url: '{{ route("proceso/sugerirCalculo") }}',
                    type: 'POST',
                    data: { 
                        proc_id  : $('#proc_id').val(),
                        '_token' : $('#_token').val() 
                    },
                    beforeSend: function(){
                        $('#loading-message').show();
                    },
                    success: function(data) {
                        $('#loading-message').hide();
                        if(data.result == 'SUCCESS')
                        {
                            $('#modalSugerir-error').hide();
                            $('#modalSugerir-success').show();
                            $('#modalSugerir-listError').empty();
                            $('#modalSugerir-titulo').html('Cálculo sugerido exitosamente');
                            $('#modalSugerir-success-msg').html('Se han sugerido los calculos para los combustibles de forma exitosa');
                            $('#modalSugerir-footer').hide();
                            $('#modalSugerir').modal('show');
                            setTimeout(function(){
                                // window.location.href = '../../proceso';
                                location.reload();
                            }, 3000);
                        }else if(data.result == 'TOUT')
                        {
                            $('#loading-message').hide();
                            $('#modalTimeOut').modal({
                                'keyboard' : false,
                                'backdrop' : 'static'
                            });
                        }
                    },
                    error: function(e) {
                        $('#loading-message').hide();
                        console.log(e);
                    }
                }); 
            }
            
        }

        function publicarCalculo()
        {
            var chkTable93     = $('#tablaResultado-{{Util::COMBUSTIBLE_93}}').find('input[type="checkbox"]:checked');
            var chkTable97     = $('#tablaResultado-{{Util::COMBUSTIBLE_97}}').find('input[type="checkbox"]:checked');
            var chkTableDIESEL = $('#tablaResultado-{{Util::COMBUSTIBLE_DIESEL}}').find('input[type="checkbox"]:checked');
            var chkTableGLP    = $('#tablaResultado-{{Util::COMBUSTIBLE_GLP}}').find('input[type="checkbox"]:checked');
            var lstError       = [];

            if(chkTable93.length == 0)
            {
                lstError.push('combustible 93');
            }
            if(chkTable97.length == 0)
            {
                lstError.push('combustible 97');
            }
            if(chkTableDIESEL.length == 0)
            {
                lstError.push('combustible DIESEL');
            }
            if(chkTableGLP.length == 0)
            {
                lstError.push('combustible GLP');
            }

            if(lstError.length != 0)
            {
                $('#modalSugerir-error').show();
                $('#modalSugerir-success').hide();
                $('#modalSugerir-listError').empty();
                $('#modalSugerir-titulo').html('Falta publicar cálculo para:');
                for (var i=0;i < lstError.length; i++) {
                    $('#modalSugerir-listError').append('<li>' + lstError[i] +'</li>');
                };
                $('#modalSugerir').modal('show');
            }else{

                $.ajax({
                    url: '{{ route("proceso/publicarCalculo") }}',
                    type: 'POST',
                    data: { 
                        proc_id   : $('#proc_id').val(),
                        tipoCrudo : $('#tipoCrudo').bootstrapSwitch('state'),
                        '_token'  : $('#_token').val() 
                    },
                    beforeSend: function(){
                        $('#loading-message').show();
                    },
                    success: function(data) {
                        $('#loading-message').hide();
                        if(data.result == 'SUCCESS')
                        {
                            $('#modalSugerir-error').hide();
                            $('#modalSugerir-success').show();
                            $('#modalSugerir-listError').empty();
                            $('#modalSugerir-titulo').html('Cálculo publicado exitosamente');
                            $('#modalSugerir-success-msg').html('Se han publicado los cálculos para los combustibles de forma exitosa');
                            $('#modalSugerir-footer').hide();
                            $('#modalSugerir').modal('show');
                            setTimeout(function(){
                                // window.location.href = '../../proceso';
                                location.reload();
                            }, 3000);
                            
                        }else if(data.result == 'TOUT')
                        {
                            $('#loading-message').hide();
                            $('#modalTimeOut').modal({
                                'keyboard' : false,
                                'backdrop' : 'static'
                            });
                        }else{
                            $('#modalSugerir-error').show();
                            $('#modalSugerir-success').hide();
                            $('#modalSugerir-listError').empty();
                            $('#modalSugerir-titulo').html('Error al publicar proceso');
                            $('#modalSugerir-listError').append('<li>' + data.msg + '</li>');
                            $('#modalSugerir').modal('show');
                        }
                    },
                    error: function(e) {
                        $('#loading-message').hide();
                    }
                }); 
            }
        }

        function rollbackModal()
        {
            $('#modalRollBack').modal('show');
        }

        function rollBackLink()
        {
            window.location.href = '../rollback/' + $('#proc_id').val();
        }

        function destacarFila(){
            filaParaDescatar.effect("highlight", {color: '#428BCA'}, 3000);
        }

        function rechazarModal(){
            $('#modalRechazar').modal('show');
        }

        $('#modalRechazar').on('hidden.bs.modal', function() {
            $('.contenido_modal').show();
            $('#rechazarEscenario').show();
            $('#exito').hide();
            $('#error').hide();
        });

        function rechazarEscenario(){

            $.ajax({
                url: '{{ route("proceso/rechazar") }}',
                type: 'POST',
                data: { 
                    comentario        : $('#comentario_rechazar').val(),
                    proc_id           : $('#proc_id').val(),
                    '_token'          : $('#_token').val() 
                },
                success: function(data) {
                    if(data.status == 'success')
                    {
                        $('.contenido_modal').hide();
                        $('#rechazarEscenario').hide();
                        $('#exito').show();
                        $('#loading-message').hide();
                    }
                    else{
                        $('.contenido_modal').hide();
                        $('#rechazarEscenario').hide();
                        $('#error').show();
                        $('#loading-message').hide();  
                    }
                },
                error: function(e) {
                    console.log(e.message);
                }
            });
        }

        function iniciarChkReadOnly(combustible)
        {

            @if($valores['FINALIZADO'])
                $('#tablaResultado-' + combustible).find('input[type="checkbox"]').each(function () {
                    $(this).bootstrapSwitch('readonly', true);
                });
            @else

                var chkSugerido = false;
                $('#tablaResultado-' + combustible).find('input[type="checkbox"]').each(function () {
                    if($(this).is(':checked'))
                    {
                        chkSugerido = true;
                    }
                });

                if(chkSugerido)
                {
                    $('#tablaResultado-' + combustible).find('input[type="checkbox"]').each(function () {
                        if(!$(this).is(':checked'))
                        {
                            $(this).bootstrapSwitch('readonly', true);
                        }
                    });
                }
            @endif
        }

        function initializeSwitch(selector) {
            $(selector).bootstrapSwitch({
                onText :'SI',
                offText:'NO',
                size   :'mini'
            },false, true); 

            $('.checkSiNoDinamico').on('switchChange.bootstrapSwitch', function(event, state) {
                chk = event.currentTarget;
                var esca_id     = chk.getAttribute('idescenariocalculo');
                var combustible = chk.id[0];
                var tipo        = chk.getAttribute('tipo');
                
                $.ajax({
                    url: '{{ route("proceso/sugerir") }}',
                    type: 'POST',
                    data: { 
                        idEscenarioCalculo: esca_id,
                        state             : state,
                        tipo              : tipo,
                        '_token'          : $('#_token').val() 
                    },
                    success: function(data) {
                        if(data.result == 'SUCCESS')
                        {
                            if(state){
                                $('#tablaResultado-' + combustible).find('input[type="checkbox"]').each(function () {
                                    if(!$(this).is(':checked'))
                                    {
                                        $(this).bootstrapSwitch('readonly', true);
                                    }
                                });    
                            }
                        }
                    },
                    error: function(e) {
                        console.log(e.message);
                    }
                }); 
                
                if(!state)
                {
                    $('#tablaResultado-' + combustible).find('input[type="checkbox"]').each(function () {
                        $(this).bootstrapSwitch('readonly', false);
                    });  
                } 
            });
        }


        function calcular(elem, variable, idEscenario, idEscenarioCalculo)
        {
            $('#procesoStatus').modal({
                'keyboard' : false,
                'backdrop' : 'static'
            });

            $('#btnProcesoStatus').attr('disabled', 'disabled');

            if(!variable) variable = '{{ Util::P_PARIDAD }}';
            if(!idEscenario) idEscenario = '-1';
            if(!idEscenarioCalculo) idEscenarioCalculo = '-1';

            var combustible   = elem.getAttribute('combustible');
            var linea         = $('#tablaResultado-' + combustible + ' tr').length - 1;
            var tipoCrudo     = $('#tipoCrudo').bootstrapSwitch('state');
            var n             = $('#' + combustible + 'n').val();
            var m             = $('#' + combustible + 'm').val();
            var s             = $('#' + combustible + 's').val();
            var t             = $('#' + combustible + 't').val();
            var f             = $('#' + combustible + 'f').val();
            var correccionRvp = $('#' + {{Util::COMBUSTIBLE_93}} + 'correccionRVP').bootstrapSwitch('state');

            if(combustible == {{Util::COMBUSTIBLE_93}} || combustible == {{Util::COMBUSTIBLE_97}}){
	            $('#' + combustible + 'rvp' + linea).html(correccionRvp == 1 ? 'SI' : 'NO');
            }

            $('#' + combustible + 'n' + linea).html(n);
            $('#' + combustible + 'm' + linea).html(m);
            $('#' + combustible + 's' + linea).html(s);
            $('#' + combustible + 't' + linea).html(t);
            $('#' + combustible + 'f' + linea).html(f);
            $('#' + combustible + 'tcrudo' + linea).html((tipoCrudo) ? 'BRENT' : 'WTI');

            $.ajax({
                url: '{{ route("proceso/crearEscenarioRequest") }}',
                type: 'POST',
                data: { 
                    n: n,
                    m: m,
                    s: s,
                    t: t,
                    f: f,
                    proc_id           : $('#proc_id').val(),
                    combustible       : combustible,
                    tipoCrudo         : tipoCrudo,
                    correccionRvp     : correccionRvp,
                    variable          : variable,
                    idEscenario       : idEscenario,
                    idEscenarioCalculo: idEscenarioCalculo,
                    '_token'          : $('#_token').val() 
                },
                success: function(data) {
                    switch(variable){
                        case '{{ Util::P_PARIDAD }}':
                            $('#lblProcesoStatusPPARIDAD').html(data.result);
                            $('#spinProcesoStatusPPARIDAD').hide();
                            $('#' + combustible + 'pparidad' + linea).html(data.result);
                            calcular(elem, '{{ Util::CRUDO }}', data.idEscenario, data.idEscenarioCalculo);
                            break;
                        case '{{ Util::CRUDO }}':
                            $('#lblProcesoStatusCRUDO').html(data.result);
                            $('#spinProcesoStatusCRUDO').hide();
                            $('#' + combustible + 'crudo' + linea).html(data.result);
                            calcular(elem, '{{ Util::MARGEN }}', data.idEscenario, data.idEscenarioCalculo);
                            break;
                        case '{{ Util::MARGEN }}':
                            $('#lblProcesoStatusMARGEN').html(data.result);
                            $('#spinProcesoStatusMARGEN').hide();
                            $('#' + combustible + 'margen' + linea).html(data.result);
                            calcular(elem, '{{ Util::P_REF_INF }}', data.idEscenario, data.idEscenarioCalculo);
                            break;
                        case '{{ Util::P_REF_INF }}':
                            $('#lblProcesoStatusPREFINF').html(data.result);
                            $('#spinProcesoStatusPREFINF').hide();
                            $('#' + combustible + 'prinferior' + linea).html(data.result);
                            calcular(elem, '{{ Util::P_REF_INT }}', data.idEscenario, data.idEscenarioCalculo);
                            break;
                        case '{{ Util::P_REF_INT }}':
                            $('#lblProcesoStatusPREFINT').html(data.result);
                            $('#spinProcesoStatusPREFINT').hide();
                            $('#' + combustible + 'printermedio' + linea).html(data.result);
                            calcular(elem, '{{ Util::P_REF_SUP }}', data.idEscenario, data.idEscenarioCalculo);
                            break;
                        case '{{ Util::P_REF_SUP }}':
                            $('#lblProcesoStatusPREFSUP').html(data.result);
                            $('#spinProcesoStatusPREFSUP').hide();
                            $('#' + combustible + 'prsuperior' + linea).html(data.result);

                            //SETEA ATRIBUTOS CHK
                            $('#' + combustible + 'chkSugerir' + linea).attr('idescenariocalculo', data.idEscenarioCalculo);

                            @if($valores['PERFIL'] == Util::PERFIL_USUARIO_GENERADOR)
                                var auxTpo = 'SUGERIR';
                            @else
                                var auxTpo = 'PUBLICAR';
                            @endif

                            $('#' + combustible + 'chkSugerir' + linea).attr('tipo', auxTpo);

                            //VALIDA SI YA EXISTE OPCION SUGERIDA O PUBLICADA
                            var listAux = $('#tablaResultado-' + combustible).find('input[type="checkbox"]:checked');
                            if(listAux.length > 0)
                            {
                                $('#' + combustible + 'chkSugerir' + linea).attr('readonly', true);
                            }

                            $('#procesoStatusTitle').html('Resultado proceso');//FIN
                            $('#btnProcesoStatus').removeAttr('disabled');
                            initializeSwitch('.checkSiNoDinamico');
                            break;
                    }                    
                },
                complete: function(){
                    filaParaDescatar = $('#' + combustible + 'n' + linea).parent().parent();
                },
                error: function(e) {
                    $('#procesoStatusTitle').html('Resultado proceso');//FIN
                    $('#lblErrorProceso').show();
                    $('#btnProcesoStatus').removeAttr('disabled');
                    console.log(e.message);
                }
            });    
        }

        function procesoStatusReset()
        {
            $('#procesoStatusTitle').html('Calculando ...');
            $('#lblErrorProceso').hide();
            $('#lblProcesoStatusPPARIDAD').html('');
            $('#spinProcesoStatusPPARIDAD').show();
            $('#lblProcesoStatusCRUDO').html('');
            $('#spinProcesoStatusCRUDO').show();
            $('#lblProcesoStatusMARGEN').html('');
            $('#spinProcesoStatusMARGEN').show();
            $('#lblProcesoStatusPREFINF').html('');
            $('#spinProcesoStatusPREFINF').show();
            $('#lblProcesoStatusPREFINT').html('');
            $('#spinProcesoStatusPREFINT').show();
            $('#lblProcesoStatusPREFSUP').html('');
            $('#spinProcesoStatusPREFSUP').show();
        }

        function addRow(combustible)
        {
            var tabla  = document.getElementById('tablaResultado-' + combustible).getElementsByTagName('tbody')[0];
            var newRow = tabla.insertRow(0);
            var len    = tabla.rows.length;

            var tipo7      = document.createAttribute("tipo"); 
            var idescenariocalculo7 = document.createAttribute("idescenariocalculo"); 
            var inp7       = document.createElement('input');
            inp7.id        = combustible + 'chkSugerir' + len; 
            inp7.setAttributeNode(tipo7);
            inp7.setAttributeNode(idescenariocalculo7);
            inp7.type      = 'checkbox';
            inp7.className = 'checkSiNoDinamico';
            var newCell    = newRow.insertCell(0);
            newCell.appendChild(inp7);

            var ro6        = document.createAttribute("readonly"); 
            var lbl6       = document.createElement('label');
            lbl6.id        = combustible + 'prsuperior' + len; 
            lbl6.setAttributeNode(ro6);
            var newCell    = newRow.insertCell(0);
            newCell.appendChild(lbl6);

            var ro5 = document.createAttribute("readonly"); 
            var lbl5       = document.createElement('label');
            lbl5.id        = combustible + 'printermedio' + len; 
            lbl5.setAttributeNode(ro5);
            var newCell    = newRow.insertCell(0);
            newCell.appendChild(lbl5);

            var ro4 = document.createAttribute("readonly"); 
            var lbl4       = document.createElement('label');
            lbl4.id        = combustible + 'prinferior' + len; 
            lbl4.setAttributeNode(ro4);
            var newCell    = newRow.insertCell(0);
            newCell.appendChild(lbl4);

            var ro3 = document.createAttribute("readonly"); 
            var lbl3       = document.createElement('label');
            lbl3.id        = combustible + 'margen' + len; 
            lbl3.setAttributeNode(ro3);
            var newCell    = newRow.insertCell(0);
            newCell.appendChild(lbl3);

            var ro2 = document.createAttribute("readonly"); 
            var lbl2       = document.createElement('label');
            lbl2.id        = combustible + 'crudo' + len; 
            lbl2.setAttributeNode(ro2);
            var newCell    = newRow.insertCell(0);
            newCell.appendChild(lbl2);

            var rotc = document.createAttribute("readonly"); 
            var lbltc       = document.createElement('label');
            lbltc.id        = combustible + 'tcrudo' + len; 
            lbltc.setAttributeNode(rotc);
            var newCell    = newRow.insertCell(0);
            newCell.appendChild(lbltc);

            var ro1 = document.createAttribute("readonly"); 
            var lbl1       = document.createElement('label');
            lbl1.id        = combustible + 'pparidad' + len; 
            lbl1.setAttributeNode(ro1);
            var newCell    = newRow.insertCell(0);
            newCell.appendChild(lbl1);

            var rof = document.createAttribute("readonly"); 
            var lblf       = document.createElement('label');
            lblf.id        = combustible + 'f' + len; 
            lblf.setAttributeNode(rof);
            var newCell    = newRow.insertCell(0);
            newCell.appendChild(lblf);

            var rot = document.createAttribute("readonly"); 
            var lblt       = document.createElement('label');
            lblt.id        = combustible + 't' + len; 
            lblt.setAttributeNode(rot);
            var newCell    = newRow.insertCell(0);
            newCell.appendChild(lblt);

            var ros = document.createAttribute("readonly"); 
            var lbls       = document.createElement('label');
            lbls.id        = combustible + 's' + len; 
            lbls.setAttributeNode(ros);
            var newCell    = newRow.insertCell(0);
            newCell.appendChild(lbls);

            var rom = document.createAttribute("readonly"); 
            var lblm       = document.createElement('label');
            lblm.id        = combustible + 'm' + len; 
            lblm.setAttributeNode(rom);
            var newCell    = newRow.insertCell(0);
            newCell.appendChild(lblm);

            var ron = document.createAttribute("readonly"); 
            var lbln       = document.createElement('label');
            lbln.id        = combustible + 'n' + len; 
            lbln.setAttributeNode(ron);
            var newCell    = newRow.insertCell(0);
            newCell.appendChild(lbln);

            if(combustible == {{Util::COMBUSTIBLE_93}} || combustible == {{Util::COMBUSTIBLE_97}}){
            	var roRvp = document.createAttribute("readonly"); 
            	var lblRvp       = document.createElement('label');
            	lblRvp.id        = combustible + 'rvp' + len; 
            	lblRvp.setAttributeNode(roRvp);
            	var newCell    = newRow.insertCell(0);
            	newCell.appendChild(lblRvp);	
            }

            tabla.appendChild(newRow);

        }

        /***
        * SI EXISTE VALOR LLENA CONTROLES
        */
        @if(isset($valores) && $valores['FECHA'] != null)
            var fecha = '{!! $valores['FECHA'] !!}';

            $('#fechaVigencia').val(fecha);
        @endif
    </script>
@endsection

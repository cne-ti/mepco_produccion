@extends('public-layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <script type="text/javascript">
                        var btnHabilitar='Proyeccion';
                    </script>
                    @include('proceso.partials.menu-proceso')
                </div>
                <div class="panel-body">   
                    @if(!$valores['PUEDE_CALCULAR_PUBLICAR'])
                        <div class="alert alert-danger" style="margin: 0 5px 14px 5px;clear: both">
                            Debe publicar el proceso antes de ingresar a proyección
                        </div>
                    @endif

                    <ul class="nav nav-tabs nav-justified" id="tabs">
                        <li class="active">
                            <a href="#93" data-toggle="tab" class="tab-mod">GASOLINA 93 OCTANOS</a>
                        </li>
                        <li><a href="#97" data-toggle="tab" class="tab-mod">GASOLINA 97 OCTANOS</a></li>
                        <li><a href="#Petroleo" data-toggle="tab" class="tab-mod">PRETRÓLEO DIÉSEL</a></li>
                        <li><a href="#GLP" data-toggle="tab" class="tab-mod">GAS LIQUADO DE PETRÓLEO</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="93">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <th>Fecha</th>
                                    <th>P.REF INF</th>
                                    <th>P.REF INT</th>
                                    <th>P.REF SUP</th>
                                    <th>Paridad</th>
                                </thead>
                                <tbody>
                                    @foreach ($valores['PROYECCION_93'] as $item)
                                        <tr>
                                            <td>{{Util::dateformat($item->PROY_FECHA, 'd/m/Y')}}</td>
                                            <td>{{Util::formatNumber(($item->PROY_RE_PARIDAD * 0.95))}}</td>
                                            <td>{{Util::formatNumber($item->PROY_RE_PARIDAD)}}</td>
                                            <td>{{Util::formatNumber(($item->PROY_RE_PARIDAD * 1.05))}}</td>
                                            <td>{{Util::formatNumber($item->PROY_PA_PARIDAD)}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="97">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <th>Fecha</th>
                                    <th>P.REF INF</th>
                                    <th>P.REF INT</th>
                                    <th>P.REF SUP</th>
                                    <th>Paridad</th>
                                </thead>
                                <tbody>
                                    @foreach ($valores['PROYECCION_97'] as $item)
                                        <tr>
                                            <td>{{Util::dateformat($item->PROY_FECHA, 'd/m/Y')}}</td>
                                            <td>{{Util::formatNumber(($item->PROY_RE_PARIDAD * 0.95))}}</td>
                                            <td>{{Util::formatNumber($item->PROY_RE_PARIDAD)}}</td>
                                            <td>{{Util::formatNumber(($item->PROY_RE_PARIDAD * 1.05))}}</td>
                                            <td>{{Util::formatNumber($item->PROY_PA_PARIDAD)}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="Petroleo">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <th>Fecha</th>
                                    <th>P.REF INF</th>
                                    <th>P.REF INT</th>
                                    <th>P.REF SUP</th>
                                    <th>Paridad</th>
                                </thead>
                                <tbody>
                                    @foreach ($valores['PROYECCION_DIESEL'] as $item)
                                        <tr>
                                            <td>{{Util::dateformat($item->PROY_FECHA, 'd/m/Y')}}</td>
                                            <td>{{Util::formatNumber(($item->PROY_RE_PARIDAD * 0.95))}}</td>
                                            <td>{{Util::formatNumber($item->PROY_RE_PARIDAD)}}</td>
                                            <td>{{Util::formatNumber(($item->PROY_RE_PARIDAD * 1.05))}}</td>
                                            <td>{{Util::formatNumber($item->PROY_PA_PARIDAD)}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="GLP">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <th>Fecha</th>
                                    <th>P.REF INF</th>
                                    <th>P.REF INT</th>
                                    <th>P.REF SUP</th>
                                    <th>Paridad</th>
                                </thead>
                                <tbody>
                                    @foreach ($valores['PROYECCION_GLP'] as $item)
                                        <tr>
                                            <td>{{Util::dateformat($item->PROY_FECHA, 'd/m/Y')}}</td>
                                            <td>{{Util::formatNumber(($item->PROY_RE_PARIDAD * 0.95))}}</td>
                                            <td>{{Util::formatNumber($item->PROY_RE_PARIDAD)}}</td>
                                            <td>{{Util::formatNumber(($item->PROY_RE_PARIDAD * 1.05))}}</td>
                                            <td>{{Util::formatNumber($item->PROY_PA_PARIDAD)}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-offset-10 col-md-2" style="text-align: right">
                        @if(!$valores['PUEDE_CALCULAR_PUBLICAR'])
                            <a class="btn btn-success" disabled="true">Descargar XLS</a>
                        @else
                            <a href="{{ route('proceso/proyeccion/expo', ['id' => $proceso->PROC_ID]) }}" class="btn btn-success">Descargar XLS</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection 

@section('script')
    <script type="text/javascript">
        activeMenuItem('proceso');
    </script>
@endsection

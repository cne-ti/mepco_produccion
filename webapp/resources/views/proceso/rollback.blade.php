@extends('public-layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <script type="text/javascript">
                        var btnHabilitar='Escenarios';
                    </script>
                    @include('proceso.partials.menu-proceso')
                </div>
                <div class="panel-body">
                    <div class="alert alert-warning" style="clear: both">
                        Al realizar la operación RollBack se eliminarán los datos históricos asociados al calculo actual y el proceso quedara en estado "Recalcular"
                        <br><br>
                        Si desea continuar ingrese documentos de respaldo y presione aceptar
                    </div>
                    <form class="form-horizontal lblObligatorio" role="form"  method="POST" action="{{route('proceso/rollbackRequest')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="proc_id" value="{{ $proceso->PROC_ID }}">


                        @if (count($errors) > 0)
                            <div class="alert alert-danger" style="clear: both">
                                Por favor corrige los siguientes errores:<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group required">
                            <label class="col-md-3 control-label">Documentos de respaldo</label>
                            <div class="col-md-9">
                                <input type="file" class="form-control" name="documentos[]" style="height: auto;" multiple="multiple">
                            </div>
                        </div>
                    
                        <div class="form-group" style="padding-top: 20px">
                            <div class="col-md-4 col-md-offset-8" style="text-align: right">
                                <button type="submit" class="btn btn-primary loading-message">
                                    Aceptar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection  

@section('script')
    <script type="text/javascript">

        activeMenuItem('proceso');

    </script>
@endsection

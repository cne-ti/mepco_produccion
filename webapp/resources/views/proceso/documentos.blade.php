@extends('public-layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <script type="text/javascript">
                        var btnHabilitar='Documentos';
                    </script>
                    @include('proceso.partials.menu-proceso')
                </div>
                <div class="panel-body">   
                    @if(!$valores['PUEDE_CALCULAR_PUBLICAR'])
                        <div class="alert alert-danger" style="margin: 0 5px 14px 5px;clear: both">
                            Debe publicar el proceso antes generar documento
                        </div>
                    @else
                        <form id="form-new" class="form-horizontal" role="form"  method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <div class="col-md-3 col-md-offset-5">
                                    @if($valores['BTN'] == 'GENERAR')
                                        <button type="button" id="selectCalculo" class="btn btn-primary" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalSelectTemplate">
                                            <i class="fa fa-download"></i>&nbsp;Generar documento de cálculo
                                        </button>
                                    @else
                                        <a href="{{ route('proceso/documentosGet', ['fecha' => $valores['FECHA_VIGENCIA'], 'doc' => $valores['DOCUMENTO']]) }}" class="btn btn-success">
                                            <i class="fa fa-download"></i>&nbsp;Descargar
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL PARA SELECCIONAR LA PLANTILLA PARA GENERAR EL DOCUMENTO -->
    @include('plantilla.partials.modal-select-template')
@endsection 

@section('script')
    <script type="text/javascript">
        activeMenuItem('proceso');
    </script>
@endsection

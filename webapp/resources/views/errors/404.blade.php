<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Comisión Nacional de Energía</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=9" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" type="image/x-icon" href={{ asset('images/gobcl-favicon.ico') }}  />

        <link href={{ asset('css/bootstrap.min.css') }} rel="stylesheet" media="screen">
        <link href={{ asset('css/app.css') }} rel="stylesheet" media="screen">
        <link href={{ asset('css/nav.css') }} rel="stylesheet" media="screen">
        <link href={{ asset('css/font-awesome.min.css') }} rel="stylesheet" media="screen">
        
    </head>
    <body>
        <div class="container">
           <div class="col-md-6 col-md-offset-3 col-xs-12">
                <div class="panel panel-default" style="margin-top: 10%">
                    <div class="panel-heading">
                        <h4>404</h4>
                    </div>
                    <div class="panel-body">
                        <div class="alert alert-danger " style="text-align: center">
                            <center><i class='fa fa-exclamation-triangle fa-4x'></i></center><br><br>
                            La página que está intentando acceder no existe
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
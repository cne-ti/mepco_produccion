<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Comisión Nacional de Energía</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=9" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" type="image/x-icon" href={{ asset('images/gobcl-favicon.ico') }}  />

        <link href={{ asset('css/bootstrap.min.css') }} rel="stylesheet" media="screen">
        <link href={{ asset('css/app.css') }} rel="stylesheet" media="screen">
        <link href={{ asset('css/nav.css') }} rel="stylesheet" media="screen">
        <link href={{ asset('css/font-awesome.min.css') }} rel="stylesheet" media="screen">
        <link href={{ asset('css/jquery-ui.min.css') }} rel="stylesheet" media="screen">
        
        <!-- datatable -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/datatables.min.css') }}"/>

        <!-- bootstrap switch -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-switch.min.css') }}"/>

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->   
        <script src={{ asset('js/jquery.js') }}></script>
        <script src={{ asset('js/jquery-ui.min.js') }}></script>
        <script src={{ asset('js/bootstrap.min.js') }}></script>
        <script src={{ asset('js/notify-custom.min.js') }}></script>
        <script src={{ asset('js/datatables.min.js') }}></script>
        <script src={{ asset('js/bootstrap-switch.min.js') }}></script>
        <script src={{ asset('js/jquery.formatNumber.js') }}></script>
        <script src={{ asset('js/init.js') }}></script>
    </head>
    <body>
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active" id="home"><a href={{ route('home') }} class="loading-message">Home</a></li>
                        {!! session(Util::MENU) !!}

                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ session(Util::USUARIO)->USUA_EMAIL }}
                            {!! session(Util::MENU_PERFILES) !!}
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container" id="wrapper">

            @yield('content')

            <!-- CAMBIO PERFIL -->
            @include('partials.modal-cambio-perfil')

            <!-- LOADING MESSAGE -->
            @include('partials.loading-message')

            <!-- DESLOGUEAR GENERADOR -->
            @include('partials.modal-deslog')

        </div>
        <!-- <div class="container-fluid footer">
            <div class="row" style="background-color: #E6E6E6;">
                <div class="col-md-3 col-xs-3 blue-line"></div>
                <div class="col-md-9 col-xs-9 red-line"></div>
                <div class="col-md-offset-1 col-md-2 col-xs-3" style="background-color: #E6E6E6; color: #58585B;">
                    <ul style="font-weight: bold;margin-top: 20px;" class="footer-left">
                        <li><a href={{ route('home') }}>Home</a></li>
                        <li><a href={{ route('construccion') }}>Cálculo</a></li>
                        <li><a href={{ route('construccion') }}>Reportes</a></li>
                        <li><a href={{ route('usuario') }}>Administración</a></li>
                        <li><a href={{ route('variable') }}>Configuración</a></li>
                    </ul>
                    <div class="row col-md-12">
                        <div class="col-md-3 blue-line" style="padding-top: 20px"></div>
                        <div class="col-md-3 red-line" style="padding-top: 20px"></div>
                    </div>
                </div>
                <div class="col-md-9 col-xs-9" style="background-color: #818386; color: #FFF">
                    <div class="col-md-4 col-xs-4" >
                        <ul class="footer-right">
                            <li>
                                <h4>Sitios Relacionados</h4>
                            </li>
                            <li><a href="http://www.gob.cl" target="_blank">Gobierno de Chile</a></li>
                            <li><a href="http://www.minenergia.cl" target="_blank">Ministerio de Energía</a></li>
                            <li><a href="http://www.bencinaenlinea.cl" target="_blank">Bencina en línea</a></li>
                            <li><a href="http://gasenlinea.gob.cl" target="_blank">Gas en línea</a></li>
                            <li><a href="http://parafinaenlinea.gob.cl" target="_blank">Parafína en línea</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-xs-4">
                        <address style="padding-top: 20px">
                            Miraflores 222 - Piso 10, Santiago centro, Chile<br>
                            Teléfono: +56-2-2797 2600<br>
                            Fax: +56-2-2797 2627
                        </address>
                    </div>
                    <div class="col-md-4 col-xs-4">
                        <img src={{ asset('images/Logo_Oficinal_CNE.png') }} class="img-responsive">
                    </div>
                </div>
            </div>
        </div> -->
        
        @yield('script')

        @if (Session::has('ALERT-TYPE'))

            <script type="text/javascript">
                @if (Session::get('ALERT-TYPE') == 'INFO')
                    $.notify('{{Session::get('ALERT-MSG')}}'.match("&quot;(.*)&quot")[1],{position:'top right', className:'info'});
                @elseif (Session::get('ALERT-TYPE') == 'SUCCESS')
                    $.notify('{{Session::get('ALERT-MSG')}}'.match("&quot;(.*)&quot")[1],{position:'top right', className:'success'});
                @elseif (Session::get('ALERT-TYPE') == 'ERROR')
                    $.notify('{{Session::get('ALERT-MSG')}}'.match("&quot;(.*)&quot")[1],{position:'top right', className:'error'});
                @endif
            </script>

            {{ Session::forget('ALERT-TYPE') }}
            {{ Session::forget('ALERT-MSG') }}
            
        @endif
    </body>
</html>


<script type="text/javascript">

    $(document).ready(function() {
        $('.link').click(function (e){
           $('.link').removeAttr('onclick');
        });
    });


    function cambiarPerfil(perf_id_nuevo) {
        // Id del perfil anterior y el nuevo
        $("#perf_id_nuevo").val(perf_id_nuevo);

        // Obtener los nombres por ajax, así como las imágenes que corresponden
        $.ajax({
            url: '{{ route('perfil/info') }}',
            type: 'POST',
            data: { 
                'perf_id_nuevo': perf_id_nuevo,
                '_token' : '{{ csrf_token() }}'
            },
            beforeSend: function(){
                $('#loading-message').show();
            },
            success: function(data) {
                if(data.status == 'success'){
                    $('#loading-message').fadeOut(1000);
                    $('#perf_nombre_anterior').html(data.perf_nombre_anterior);
                    $('#perf_nombre_nuevo').html(data.perf_nombre_nuevo);
                    $('#img_perf_anterior').append(data.img_perf_anterior);
                    $('#img_perf_nuevo').append(data.img_perf_nuevo);

                    $("#modalCambioPerfil").modal('show');
                    $("#cambioPerfil").trigger('submit');
                }else{
                    console.log(data.status);
                }
            },
            error: function(e) {
                console.log(e.message);
            }
        });
    };

</script>
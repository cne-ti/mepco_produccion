@extends('public-layout')
@section('content')

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-10 col-xs-10">
                                <h4>Fórmulas
                                <button type="button" class="btn btn-primary" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalNew">
                                Nuevo <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                </button></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active">
                            <a href="#formulas_calculo" data-toggle="tab">
                            <i class="fa fa-cog fa-2x"></i><br>
                            CÁLCULO
                            </a>
                        </li>
                        <li>
                            <a href="#formulas_solver" data-toggle="tab"><i class="fa fa-calculator fa-2x"></i><br>SOLVER</a>
                        </li>
                        <li>
                            <a href="#formulas_proyeccion" data-toggle="tab"><i class="fa fa-line-chart fa-2x"></i><br>PROYECCIÓN</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="formulas_calculo" class="tab-pane fade in active">
                            <br/>
                            <table class="table table-hover table-bordered dataTable">
                                <thead>
                                    <tr>
                                        <th>
                                            Llave
                                        </th>
                                        <th>
                                            Nombre
                                        </th>
                                        <th>
                                            Descripcion
                                        </th>
                                        <th style="width:300px;">
                                            Fórmula
                                        </th >
                                        <th>
                                            Fecha de modificación
                                        </th>
                                        <th style="width:150px;">
                                            Opciones
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($formulas_calculo as $formula)
                                        <tr>
                                            <td>{{ $formula->PAFO_LLAVE }}</td>
                                            <td>{{ $formula->PAFO_NOMBRE }}</td>
                                            <td>{{ $formula->PAFO_DESCRIPCION }}</td>
                                            <td>{!! $formula->PAFO_FORMULA !!}</td>
                                            <td>{{ \App\Util::dateFormat($formula->PAFO_FECHA_MODIFICACION, 'd/m/Y H:i ') . ' Hrs' }}</td>
                                            <td style="vertical-align:middle;">
                                                <button type="button" id="editar" name="editar" class="btn btn-default" aria-label="Left Align" title="Modificar Fórmula" data-toggle="modal" data-target="#modalEdit" data-id={{ $formula->PAFO_ID }}>
                                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                                </button>
                                                <button type="button" id="eliminar" class="btn btn-default" aria-label="Left Align" title="Eliminar" data-toggle="modal" data-target="#modalDelete" data-id={{ $formula->PAFO_ID }}>
                                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                </button>
                                                <button type="button" id="historico" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#modalHistory" title="Histórico" data-id={{ $formula->PAFO_ID }}>
                                                    <i class="fa fa-history"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div id="formulas_solver" class="tab-pane fade">
                            <br/>
                            <table class="table table-hover table-bordered dataTable">
                                <thead>
                                    <tr>
                                        <th>
                                            Llave
                                        </th>
                                        <th>
                                            Nombre
                                        </th>
                                        <th>
                                            Descripcion
                                        </th>
                                        <th style="width:300px;">
                                            Fórmula
                                        </th >
                                        <th>
                                            Fecha de modificación
                                        </th>
                                        <th style="width:150px;">
                                            Opciones
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($formulas_solver as $formula)
                                        <tr>
                                            <td>{{ $formula->PAFO_LLAVE }}</td>
                                            <td>{{ $formula->PAFO_NOMBRE }}</td>
                                            <td>{{ $formula->PAFO_DESCRIPCION }}</td>
                                            <td>{!! $formula->PAFO_FORMULA !!}</td>
                                            <td>{{ \App\Util::dateFormat($formula->PAFO_FECHA_MODIFICACION, 'd/m/Y H:i ') . ' Hrs' }}</td>
                                            <td style="vertical-align:middle;">
                                                <button type="button" id="editar" name="editar" class="btn btn-default" aria-label="Left Align" title="Modificar Fórmula" data-toggle="modal" data-target="#modalEdit" data-id={{ $formula->PAFO_ID }}>
                                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                                </button>
                                                <button type="button" id="eliminar" class="btn btn-default" aria-label="Left Align" title="Eliminar" data-toggle="modal" data-target="#modalDelete" data-id={{ $formula->PAFO_ID }}>
                                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                </button>
                                                <button type="button" id="historico" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#modalHistory" title="Histórico" data-id={{ $formula->PAFO_ID }}>
                                                    <i class="fa fa-history"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div id="formulas_proyeccion" class="tab-pane fade">
                            <br/>
                            <table class="table table-hover table-bordered dataTable">
                                <thead>
                                    <tr>
                                        <th>
                                            Llave
                                        </th>
                                        <th>
                                            Nombre
                                        </th>
                                        <th>
                                            Descripcion
                                        </th>
                                        <th style="width:300px;">
                                            Fórmula
                                        </th >
                                        <th>
                                            Fecha de modificación
                                        </th>
                                        <th style="width:150px;">
                                            Opciones
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($formulas_proyeccion as $formula)
                                        <tr>
                                            <td>{{ $formula->PAFO_LLAVE }}</td>
                                            <td>{{ $formula->PAFO_NOMBRE }}</td>
                                            <td>{{ $formula->PAFO_DESCRIPCION }}</td>
                                            <td>{!! $formula->PAFO_FORMULA !!}</td>
                                            <td>{{ \App\Util::dateFormat($formula->PAFO_FECHA_MODIFICACION, 'd/m/Y H:i ') . ' Hrs' }}</td>
                                            <td style="vertical-align:middle;">
                                                <button type="button" id="editar" name="editar" class="btn btn-default" aria-label="Left Align" title="Modificar Fórmula" data-toggle="modal" data-target="#modalEdit" data-id={{ $formula->PAFO_ID }}>
                                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                                </button>
                                                <button type="button" id="eliminar" class="btn btn-default" aria-label="Left Align" title="Eliminar" data-toggle="modal" data-target="#modalDelete" data-id={{ $formula->PAFO_ID }}>
                                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                </button>
                                                <button type="button" id="historico" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#modalHistory" title="Histórico" data-id={{ $formula->PAFO_ID }}>
                                                    <i class="fa fa-history"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- CREAR FÓRMULA -->
    @include('formula.partials.modal-new')

    <!-- EDITAR FÓRMULA -->
    @include('formula.partials.modal-edit')

    <!-- ELIMINAR FÓRMULA -->
    @include('formula.partials.modal-delete')

    <!-- HISTORICO -->
    @include('formula.partials.modal-history')

    <!-- EJECUTAR FÓRMULA -->
    @include('formula.partials.modal-formula')

@endsection                  

@section('script')
    <script>
        $(document).ready(function() {
            // Marcar la pestaña en el menú superior
            activeMenuItem('formula', 'configuracion');
            
            var targetXls = "{{ route('formula/exportarAExcel')}}";

            var targetPdfCalculo = "{{ route('formula/exportarAPdf', ['tipo' => 'calculo'])}}";
            var targetPdfProyeccion = "{{ route('formula/exportarAPdf', ['tipo' => 'proyeccion'])}}";
            var targetPdfSolver = "{{ route('formula/exportarAPdf', ['tipo' => 'solver'])}}";

            $('#formulas_calculo .btn-exportar').append('<a title="Exportar a PDF" class="datatable-link" href="' + targetPdfCalculo + '"><i class="fa fa-file-pdf-o fa-2x"></i></a><a title="Exportar a Excel" class="datatable-link" style="margin-left:7px" href="' + targetXls + '" ><i class="fa fa-file-excel-o fa-2x"></i></a>');

            $('#formulas_proyeccion .btn-exportar').append('<a title="Exportar a PDF" class="datatable-link" href="' + targetPdfProyeccion + '"><i class="fa fa-file-pdf-o fa-2x"></i></a><a title="Exportar a Excel" class="datatable-link" style="margin-left:7px" href="' + targetXls + '" ><i class="fa fa-file-excel-o fa-2x"></i></a>');

            $('#formulas_solver .btn-exportar').append('<a title="Exportar a PDF" class="datatable-link" href="' + targetPdfSolver + '"><i class="fa fa-file-pdf-o fa-2x"></i></a><a title="Exportar a Excel" class="datatable-link" style="margin-left:7px" href="' + targetXls + '" ><i class="fa fa-file-excel-o fa-2x"></i></a>');

            $('.dataTables_filter :input').removeClass('input-sm');

            $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
                    $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
            });
        }); 

    </script>
@endsection

<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar Fórmula</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-edit">
                    <!-- Errores en formulario -->
                    @include('partials.error')
                    <input type="hidden" name="pafo_id" id="pafo_id">
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                    <!-- Añadir campos -->
                    @include('formula.partials.fields')
                    <div class="form-group">
                        <label class="col-md-3 control-label">Fecha de modificación</label>
                        <div class="col-md-3">
                            <input type="input" class="form-control" name="pafo_fecha_modificacion" id="pafo_fecha_modificacion" disabled>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary loading-message" id="btnGuardar">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#modalEdit').on('show.bs.modal', function (e) {
        $('#form-edit #errors').hide();
        $('#form-edit #errors ul').html('');
        $('#form-edit')[0].reset();
        $('#form-edit #pafo_tipo_formula').attr('disabled', true);

        $('.numChar').text('');
        $('#form-edit #pafo_descripcion').on('keyup', numChar);
        $.ajax({
            url: 'formula/'+$(e.relatedTarget).attr('data-id')+'/edit',
            type: 'GET',
            data: { 
                '_token'        : $('#_token').val() 
            },
            success: function(data) {
                loadingMessageHide();
                if(data.status == "success"){
                    var formula = data.formula;
                    $('#form-edit #pafo_id').val(formula.PAFO_ID);
                    $('#form-edit #pafo_llave').val(formula.PAFO_LLAVE);
                    $('#form-edit #pafo_nombre').val(formula.PAFO_NOMBRE);
                    $('#form-edit #pafo_formula').val(formula.PAFO_FORMULA);
                    $('#form-edit #pafo_descripcion').val(formula.PAFO_DESCRIPCION);
                    $('#form-edit #pafo_fecha_modificacion').val(formula.PAFO_FECHA_MODIFICACION);
                    $('#form-edit #pafo_tipo_formula option[value="'+formula.PAFO_TIPO_FORMULA+'"]').prop('selected', true);
                    $('#form-edit #pafo_cantidad_decimales').val(formula.PAFO_CANTIDAD_DECIMALES);
                }else{
                    errorProcess(data.status);
                }
            },
            error: function(e) {
                console.log(e.message);
            }
        });           
    });

    $('#modalEdit #btnGuardar').click(function(){
        var formData = $('#form-edit').serialize();

        $('#form-edit #errors ul').html('');
        $('#form-edit #errors').hide();

        // Validar primero la fórmula 
        $.ajax({
            url: 'formula/validar',
            type: 'POST',
            data: { 
                'formula' : $('#form-edit #pafo_formula').val(),
                '_token' : $('#modalNew #_token').val() 
            },
            success: function(data) {
                loadingMessageHide();
                if(data.status == 'success'){
                    $.ajax({
                        url: 'formula/'+$("#form-edit #pafo_id").val(),
                        type: 'PUT',
                        data: { 
                            'formData'  : formData,
                            '_token'    : $('#modalEdit #_token').val() 
                        },
                        success: function(data) {
                            loadingMessageHide();
                            if(data.status == 'success'){
                                $('#form-edit #errors').hide();
                                $('#modalEdit').modal('hide');
                                location.reload();
                            }else{
                                errorProcess('form-edit', data.errors);
                            }
                        },
                        error: function(e) {
                            console.log(e.message);
                        }
                    });
                }else{
                    errorProcess('form-edit', data.errors);
                }
            },
            error: function(e) {
                console.log(e.message);
            }
        });
    });

  </script>
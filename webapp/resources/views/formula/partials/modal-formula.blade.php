<div class="modal fade" id="modalQuery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Resultado de la Fórmula</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-query">
                    <!-- Errores en formulario -->
                    @include('partials.error')
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Resultado: </label>
                        <div class="col-md-6">
                            <input type="text" id="resultado" name="resultado" class="form-control" />
                        </div>
                    </div>                   
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#modalQuery').on('show.bs.modal', function (e) {
        $('#loading-message').show();
        // Errores
        $('#form-query #errors').hide();
        $('#form-query')[0].reset();

        $.ajax({
            url: 'formula/ejecutarFormula',
            type: 'POST',
            data: { 
                'pafo_id'       : $(e.relatedTarget).attr('data-id'),
                '_token'        : $('#_token').val() 
            },
            success: function(data) {
                loadingMessageHide();
                if(data.status == "success"){
                    $('#resultado').val(data.resultado);
                }else{
                    errorProcess('form-query', data.errors);
                }
                $('#loading-message').hide();
            },
            error: function(e) {
                console.log(e.message);
            }
        });           
    });


</script>
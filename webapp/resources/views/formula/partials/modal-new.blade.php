<div class="modal fade" id="modalNew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Nueva fórmula</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-new">
                    <!-- Errores en formulario -->
                    @include('partials.error')
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                    <!-- Añadir campos -->
                    @include('formula.partials.fields')
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btnGuardar" class="btn btn-primary loading-message">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#modalNew').on('show.bs.modal', function () {
        $('#form-new #errors').hide();
        $('#form-new #errors ul').html('');
        $('#form-new')[0].reset();
        $('.numChar').text('');
        $('#form-new #pafo_descripcion').on('keyup', numChar);
    });

    $('#modalNew #btnGuardar').click(function(){
        var formData = $('#form-new').serialize();
        $('#form-new #errors ul').html('');
        $('#form-new #errors').hide();

        // Validar primero la fórmula 
        $.ajax({
            url: 'formula/validar',
            type: 'POST',
            data: { 
                'formula' : $('#form-new #pafo_formula').val(),
                '_token' : $('#modalNew #_token').val() 
            },
            success: function(data) {
                loadingMessageHide();
                if(data.status == 'success'){
                    $.ajax({
                        url: 'formula',
                        type: 'POST',
                        data: { 
                            'formData' : formData,
                            '_token' : $('#modalNew #_token').val() 
                        },
                        success: function(data) {
                            loadingMessageHide();
                            if(data.status == 'success'){
                                $('#form-new #errors').hide();
                                $('#modalNew').modal('hide');
                                location.reload();
                            }else{
                                errorProcess('form-new', data.status);
                            }
                        },
                        error: function(e) {
                            console.log(e.message);
                        }
                    });
                }else{
                    errorProcess('form-new', data.errors);
                }
            },
            error: function(e) {
                console.log(e.message);
            }
        });
    });
  </script>
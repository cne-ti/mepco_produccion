<div class="form-group">
    <div class="col-md-5 lblObligatorio"></div>
</div>
<div class="form-group required">
    <label class="col-md-3 control-label">Tipo de fórmula</label>
    <div class="col-md-4">
        <select id="pafo_tipo_formula" name="pafo_tipo_formula" class="form-control" >
            <option id="{{ Util::FORMULA_CALCULO }}" value="{{ Util::FORMULA_CALCULO }}">Fórmula para el cálculo</option>
            <option id="{{ Util::FORMULA_SOLVER }}" value="{{ Util::FORMULA_SOLVER }}">Fórmula para el solver</option>
            <option id="{{ Util::FORMULA_PROYECCION }}" value="{{ Util::FORMULA_PROYECCION }}">Fórmula para la proyección</option>
        </select>
    </div>
</div>
<div class="form-group required">
    <label class="col-md-3 control-label">Llave</label>
    <div class="col-md-4">
        <input type="text" class="form-control" id="pafo_llave" name="pafo_llave" />
    </div>
</div>
<div class="form-group required">
    <label class="col-md-3 control-label">Nombre</label>
    <div class="col-md-7">
        <input type="text" class="form-control" id="pafo_nombre" name="pafo_nombre" />
    </div>
</div>
<div class="form-group required">
    <label class="col-md-3 control-label">Fórmula</label>
    <div class="col-md-7">
        <textarea class="form-control" id="pafo_formula" name="pafo_formula" rows="6"></textarea>
    </div>
</div>
<div class="form-group required">
    <label class="col-md-3 control-label">Descripción</label>
    <div class="col-md-7">
        <textarea class="form-control" id="pafo_descripcion" name="pafo_descripcion" maxlength="255"></textarea>
        <div class="numChar"></div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 control-label">Número de decimales (si no se especifica se tomará el número máximo de decimales)</label>
    <div class="col-md-2">
        <input type="number" class="form-control" id="pafo_cantidad_decimales" name="pafo_cantidad_decimales" />
    </div>
</div>
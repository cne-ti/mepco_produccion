<div class="form-group">
    <label class="col-md-4 control-label">Campo 1</label>
    <div class="col-md-6">
        <input type="text" class="form-control" id="campo1" name="campo1" />
    </div>
</div>
<div class="form-group">
    <label class="col-md-4 control-label">Campo 2</label>
    <div class="col-md-6">
        <input type="text" class="form-control" id="campo2" name="campo2" />
    </div>
</div>
<div class="form-group">
    <label class="col-md-4 control-label">Campo 3</label>
    <div class="col-md-6">
        <input type="text" class="form-control" id="campo3" name="campo3" />
    </div>
</div>
<div class="form-group">
    <label class="col-md-4 control-label">Fecha de Vigencia</label>
    <div class="col-md-4">
        <input type="text" class="form-control fecha-vigencia" id="fecha" name="fecha" />
    </div>
</div>
<div class="modal fade" id="modalHistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id='modal-content'>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Tabla Histórico</h4>
            </div>
            <div class="modal-body">
                <table class="table table-hover table-condensed table-bordered dataTable-xs">
                    <thead>
                        <tr>
                            <!-- La columna 'Campo Modificado' sólo aplica para Variables y Fórmulas -->
                            <th>Fecha de modificación</th>
                            <th>Campo modificado</th>
                            <th>Valor anterior</th>
                            <th>Valor nuevo</th>
                            <th>Usuario</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>                    
            </div>
        </div>
    </div>
</div>
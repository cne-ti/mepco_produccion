<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Formulario Editar</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-edit">
                    <!-- Errores en formulario -->
                    @include('partials.error')
          
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                    <!-- Añadir campos -->
                    @include('mantenedor.partials.fields')
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary loading-message" id="btnGuardar">Guardar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Confirmar</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-delete">
                    ¿Desea eliminar?
                    <input type="hidden" id="txtId" name="txtId">
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="btnEliminar">Eliminar</button>
            </div>
        </div>
    </div>
</div>

<!-- modal delete end  -->

<script type="text/javascript">
    //OCULTA DIV ERROR
    /*
    $('#modalDelete').on('show.bs.modal', function (e) {
        $('#form-delete #txtId').val($(e.relatedTarget).attr('data-id'));   
    });
    $('#btnEliminarUsuario').click(function(){
        console.log($('#form-delete #txtId').val());
        $.ajax({
            url: 'user/' + $('#form-delete #txtId').val(),
            type: 'DELETE',
            data: { 
              '_token' : $('#form-edit #_token').val() 
            },
            success: function(data) {
                if(data.status == 'success'){
                    $('#form-new #errors').hide();
                    $('#modalNew').modal('hide');
                    $('#form-search').submit();
                }else{
                    errorProcess('form-edit', data.status);
                }
            },
            error: function(e) {
                console.log(e.message);
            }
        });
    });
    */
</script>
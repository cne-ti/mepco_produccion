<div class="modal fade" id="modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Formulario Ver</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-view">
                    <!-- Añadir campos -->
                    @include('mantenedor.partials.fields')
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#modalView').on('show.bs.modal', function (e) {
      // Rellenar los datos del formulario
      $('#form-view #campo1').val('Contenido campo 1');
      $('#form-view #campo2').val('Contenido campo 2');
      $('#form-view #campo3').val('Contenido campo 3');
      $('#form-view #fecha-vigencia').val('12/09/2015');
      // BLoquear los campos
      $('#form-view #campo1').prop('disabled', true);
      $('#form-view #campo2').prop('disabled', true);
      $('#form-view #campo3').prop('disabled', true);
      $('#form-view #fecha').prop('disabled', true);
    
    });
</script>
@extends('public-layout')
@section('content')

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-10 col-xs-10">
                                <h4>Mantenedor (Listado)
                                <button type="button" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalNew">
                                Nuevo <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                </button></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-hover table-bordered dataTable">
                        <thead>
                            <tr>
                                <th>
                                    Columna 1
                                </th>
                                <th>
                                    Columna 2
                                </th>
                                <th>
                                    Opciones
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    Contenido columna 1
                                </td>
                                <td>
                                    Contenido columna 2
                                </td>
                                <td>
                                    <button type="button" id="ver" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalView">
                                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                    </button>
                                    <button type="button" id="editar" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalEdit">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </button>
                                    <button type="button" id="eliminar" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalDelete">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </button>
                                    <button type="button" id="historuco" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#modalHistory">
                                        <i class="fa fa-history"></i>
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- NUEVO -->
    @include('mantenedor.partials.modal-new')

    <!-- VER -->
    @include('mantenedor.partials.modal-view')

    <!-- EDITAR-->
    @include('mantenedor.partials.modal-edit')

    <!-- ELIMINAR -->
    @include('mantenedor.partials.modal-delete')

    <!-- HISTORICO -->
    @include('mantenedor.partials.modal-history')

@endsection                  

@section('script')
    <script>
        $(document).ready(function() {
            // Marcar la pestaña en el menú superior
            activeMenuItem('mantenedor');
            var targetPdf = "";
            var targetXls = "";
            
            @include('partials.btn-exportar');
        }); 
    </script>
@endsection

<!-- modal delete init -->
<div class="modal fade" id="modalDeslog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirmar</h4>
            </div>
            <div class="modal-body">
                <div id="holderLoading" style="text-align: center">Cargando <i class="fa fa-refresh fa-spin"></i></div>
                <form class="form-horizontal" id="form-generador-out" style="display:none">

                    <div class="alert alert-danger" id="lblErrorDeslogueo" style="display: none"></div>

                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                    <table id="datosGenerador" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Email</th>
                                <th>Conectado desde</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                    <div class="alert alert-warning" style="text-align: center" id="lblConfirmarDeslogueo">Si desea desconectar al usuario generador favor presione confirmar</div>
                    <div class="alert alert-warning" style="text-align: center" id="lblNoExisteGeneradorConectado">Actualmente no existe ningún usuario Generador conectado al sistema</div>
                </form>
            </div>
            <div class="modal-footer" id="holderCerrar" style="display:none">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            </div>
            <div class="modal-footer" id="holderCerrarSession" style="display:none">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="btnCerrarSesion">Confirmar</button>
            </div>
        </div>
    </div>
</div>

<!-- modal delete end  -->
<script type="text/javascript">

    $('#modalDeslog').on('show.bs.modal', function (e) {
        $('#lblErrorDeslogueo').empty();
        $('#lblErrorDeslogueo').hide();
        // Ajax, preguntar si hay un usuario generador conectado
        $.ajax({
            url: '{{ route('usuario/comprobarSesionGenerador') }}',
            type: 'GET',
            data: {
                '_token' : $('#_token').val()
            },
            beforeSend: function(){
                $('#holderLoading').show();
                $('#form-generador-out').hide();
            },
            success: function(data) {
                if(data.STATUS == 'EXISTE_GENERADOR'){
                    var html  = '<tr>';
                    html += '<td>' + data.USUA_EMAIL + '</td>';
                    html += '<td>' + data.USPE_ULTIMA_CONEXION + '</td>';
                    html += '</tr>';
                    $('#datosGenerador tbody').empty();
                    $('#datosGenerador tbody').append(html)

                    $('#datosGenerador').show();
                    $('#holderCerrarSession').show();
                    $('#lblConfirmarDeslogueo').show();
                    $('#lblNoExisteGeneradorConectado').hide();
                    $('#holderCerrar').hide();

                }else{
                    $('#datosGenerador').hide();
                    $('#holderCerrarSession').hide();
                    $('#lblConfirmarDeslogueo').hide();
                    $('#lblNoExisteGeneradorConectado').show();
                    $('#holderCerrar').show();
                }
            },
            complete: function () {
                $('#holderLoading').hide();
                $('#form-generador-out').show();
            },
            error: function(e) {
                console.log(e.message);
            }
        });
    });

    $(document).on('click', '#btnCerrarSesion', function() {
        // Cierra la sesión del generador conectado
        $.ajax({
            url: '{{ route('usuario/cerrarSesionGenerador') }}',
            type: 'GET',
            data: {
                '_token' : $('#_token').val()
            },
            beforeSend: function(){
                $('#loading-message').show();
            },
            success: function(data) {
                if(data.STATUS == 'SUCCESS'){
                    $('#modalDeslog').modal('hide');
                    $('#loading-message').hide();
                    $.notify(
                        'Sesión de usuario generador cerrada correctamente'
                        ,{
                            position:'top right', 
                            className:'success'
                        }
                    );
                }
                else{
                    $('#loading-message').hide();
                    $('#lblErrorDeslogueo').show();
                    $('#lblErrorDeslogueo').html(data.MSJ);
                }

            },
            error: function(e) {
                console.log(e.message);
            }
        });
    });

</script>
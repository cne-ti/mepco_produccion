<!-- modal cambioPerfil init -->
<div class="modal fade" id="modalCambioPerfil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  	<div class="modal-dialog" role="document" style="width:400px;">
		<div class="modal-content">
	  		<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Cambio de perfil <i id="loading" class="fa fa-refresh fa-spin"></i></h4>
	 		 </div>
	  		<div class="modal-body">
				<div class="row">
					<div id="cambioPerfilExitoso">
						<div class="col-md-5 col-xs-5">
							<div class="panel panel-default">
						  		<div class="panel-heading text-center" id="perf_nombre_anterior"></div>
						  		<div class="panel-body text-center" id='img_perf_anterior'></div>
							</div>
						</div>
					<div class="col-md-2 col-xs-2" style="margin-top: 30px">
						<i class="fa fa-arrow-circle-right fa-2x"></i>
					</div>
					<div class="col-md-5 col-xs-5">
						<div class="panel panel-default">
						  	<div class="panel-heading text-center" id="perf_nombre_nuevo"></div>
						  	<div class="panel-body text-center" id="img_perf_nuevo"></div>
						</div>
					</div>
				</div>	
				<div id="cambioPerfilError" style="display: none">
					<div class="col-md-12 col-xs-12 alert alert-danger">
						Actualmente se encuentra otro usuario con perfil generador
					</div>
				</div>
			</div>
		  	<input type="hidden" id="txtId" name="txtId">
		  	<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
		</form>
	  	</div>
	  	<div class="modal-footer" id="holderErrorCambioPerfil" style="display: none">
            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
        </div>
	</div>
  </div>
</div>
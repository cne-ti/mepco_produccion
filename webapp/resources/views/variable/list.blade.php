@extends('public-layout')
@section('content')

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-10 col-xs-10">
                                <h4>Variables
                                <button type="button" id="btnNueva" class="btn btn-primary" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalNew">
                                Nueva <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                </button></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active">
                            <a href="#variables_calculo" data-toggle="tab" id="calculo">
                            <i class="fa fa-calculator fa-2x"></i><br>
                            VARIABLES DEL CÁLCULO
                            </a>
                        </li>
                        <li>
                            <a href="#variables_proyeccion" data-toggle="tab" id="proyeccion"><i class="fa fa-line-chart fa-2x"></i><br>VARIABLES DE PROYECCIÓN</a>
                        </li>
                        <li>
                            <a href="#variables_plantilla" data-toggle="tab" id="plantilla"><i class="fa fa-file-text fa-2x"></i><br>VARIABLES DE DOCUMENTO</a>
                        </li>
                        <li>
                            <a href="#variables_predefinidas" data-toggle="tab" id="predefinidas"><i class="fa fa-cog fa-2x"></i><br>VARIABLES PREDEFINIDAS</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="variables_calculo" class="tab-pane fade in active">
                            <br/>
                            <table class="table table-hover table-bordered dataTable">
                                <thead>
                                    <tr>
                                        <th>
                                            Llave
                                        </th>
                                        <th>
                                            Hidrocarburo
                                        </th>
                                        <th>
                                            Nombre
                                        </th>
                                        <th style="width:25%">
                                            Valor
                                        </th>
                                        <th>
                                            Descripcion
                                        </th>
                                        <th>
                                            Fecha de modificación
                                        </th>
                                        <th>
                                            Opciones
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($variablesCalculo as $variable)
                                        <tr>
                                            <td>{{ $variable->PAVA_LLAVE }}</td>
                                            <td>{{ $variable->HIDR_NOMBRE_CHILE }}</td>
                                            <td>{{ $variable->PAVA_NOMBRE }}</td>
                                            <td>{{ Util::formatNumber($variable->PAVA_VALOR_CALCULO) }}</td>
                                            <td>{{ $variable->PAVA_DESCRIPCION }}</td>
                                            <td>{{ \App\Util::dateFormat($variable->PAVA_FECHA_MODIFICACION, 'd/m/Y H:i ') . ' Hrs' }}</td>
                                            <td>
                                                <button type="button" id="editar" title="Modificar Variable" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalEdit" data-id={{ $variable->PAVA_ID }}>
                                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                                </button>
                                                <button type="button" id="eliminar" title="Eliminar" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalDelete" data-id={{ $variable->PAVA_ID }}>
                                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                </button>
                                                <button type="button" id="historico" title="Histórico" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#modalHistory" data-id={{ $variable->PAVA_ID }}>
                                                    <i class="fa fa-history"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    @foreach ($variablesQuery as $variable)
                                        <tr>
                                            <td>{{ $variable->PAVA_LLAVE }}</td>
                                            <td>{{ $variable->HIDR_NOMBRE_CHILE }}</td>
                                            <td>{{ $variable->PAVA_NOMBRE }}</td>
                                            <td>{!! $variable->PAVA_VALOR_PLANTILLA !!}</td>
                                            <td>{{ $variable->PAVA_DESCRIPCION }}</td>
                                            <td>{{ \App\Util::dateFormat($variable->PAVA_FECHA_MODIFICACION, 'd/m/Y H:i ') . ' Hrs' }}</td>
                                            <td>
                                                <button type="button" id="editar" title="Modificar Variable" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalEdit" data-id={{ $variable->PAVA_ID }}>
                                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                                </button>
                                                <button type="button" id="eliminar" title="Eliminar" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalDelete" data-id={{ $variable->PAVA_ID }}>
                                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                </button>
                                                <button type="button" id="historico" title="Histórico" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#modalHistory" data-id={{ $variable->PAVA_ID }}>
                                                    <i class="fa fa-history"></i>
                                                </button>
                                                <button type="button" id="ejecutar" title="Ejecutar Query" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#modalQuery" data-id={{ $variable->PAVA_ID }}>
                                                    <span class="glyphicon glyphicon-play-circle" aria-hidden="true"></span>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div id="variables_proyeccion" class="tab-pane fade">
                            <br />
                            <table class="table table-hover table-bordered dataTable">
                                <thead>
                                    <tr>
                                        <th>
                                            Llave
                                        </th>
                                        <th>
                                            Hidrocarburo
                                        </th>
                                        <th>
                                            Nombre
                                        </th>
                                        <th style="width:25%">
                                            Valor
                                        </th>
                                        <th>
                                            Descripcion
                                        </th>
                                        <th>
                                            Fecha de modificación
                                        </th>
                                        <th>
                                            Opciones
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($variablesProyeccion as $variable)
                                        <tr>
                                            <td>{{ $variable->PAVA_LLAVE }}</td>
                                            <td>{{ $variable->HIDR_NOMBRE_CHILE }}</td>
                                            <td>{{ $variable->PAVA_NOMBRE }}</td>
                                            <td>{{ Util::formatNumber($variable->PAVA_VALOR_CALCULO) }}</td>
                                            <td>{{ $variable->PAVA_DESCRIPCION }}</td>
                                            <td>{{ \App\Util::dateFormat($variable->PAVA_FECHA_MODIFICACION, 'd/m/Y H:i ') . ' Hrs' }}</td>
                                            <td>
                                                <button type="button" id="editar" title="Modificar Variable" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalEdit" data-id={{ $variable->PAVA_ID }}>
                                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                                </button>
                                                <button type="button" id="eliminar" title="Eliminar" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalDelete" data-id={{ $variable->PAVA_ID }}>
                                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                </button>
                                                <button type="button" id="historico" title="Histórico" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#modalHistory" data-id={{ $variable->PAVA_ID }}>
                                                    <i class="fa fa-history"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    @foreach ($variablesProyeccionQuery as $variable)
                                        <tr>
                                            <td>{{ $variable->PAVA_LLAVE }}</td>
                                            <td>{{ $variable->HIDR_NOMBRE_CHILE }}</td>
                                            <td>{{ $variable->PAVA_NOMBRE }}</td>
                                            <td>{!! $variable->PAVA_VALOR_PLANTILLA !!}</td>
                                            <td>{{ $variable->PAVA_DESCRIPCION }}</td>
                                            <td>{{ \App\Util::dateFormat($variable->PAVA_FECHA_MODIFICACION, 'd/m/Y H:i ') . ' Hrs' }}</td>
                                            <td>
                                                <button type="button" id="editar" title="Modificar Variable" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalEdit" data-id={{ $variable->PAVA_ID }}>
                                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                                </button>
                                                <button type="button" id="eliminar" title="Eliminar" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalDelete" data-id={{ $variable->PAVA_ID }}>
                                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                </button>
                                                <button type="button" id="historico" title="Histórico" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#modalHistory" data-id={{ $variable->PAVA_ID }}>
                                                    <i class="fa fa-history"></i>
                                                </button>
                                                <button type="button" id="ejecutar" title="Ejecutar Query" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#modalQuery" data-id={{ $variable->PAVA_ID }}>
                                                    <span class="glyphicon glyphicon-play-circle" aria-hidden="true"></span>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div id="variables_plantilla" class="tab-pane fade">
                            <br/>
                            <table class="table table-hover table-bordered dataTable">
                                <thead>
                                    <tr>
                                        <th>
                                            Llave
                                        </th>
                                        <th>
                                            Nombre
                                        </th>
                                        <th>
                                            Valor
                                        </th>
                                        <th>
                                            Descripcion
                                        </th>
                                        <th>
                                            Fecha de modificación
                                        </th>
                                        <th>
                                            Opciones
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($variablesPlantilla as $variable)
                                        <tr>
                                            <td>{{ $variable->PAVA_LLAVE }}</td>
                                            <td>{{ $variable->PAVA_NOMBRE }}</td>
                                            <td>{{ $variable->PAVA_VALOR_PLANTILLA }}</td>
                                            <td>{{ $variable->PAVA_DESCRIPCION }}</td>
                                            <td>{{ \App\Util::dateFormat($variable->PAVA_FECHA_MODIFICACION, 'd/m/Y H:i ') . ' Hrs' }}</td>
                                            <td>
                                                <button type="button" id="editar" title="Modificar Variable" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalEdit" data-id={{ $variable->PAVA_ID }}>
                                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                                </button>
                                                <button type="button" id="eliminar" title="Eliminar" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalDelete" data-id={{ $variable->PAVA_ID }}>
                                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                </button>
                                                <button type="button" id="historico" title="Histórico" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#modalHistory" data-id={{ $variable->PAVA_ID }}>
                                                    <i class="fa fa-history"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    @foreach ($variablesQueryPlantilla as $variable)
                                        <tr>
                                            <td>{{ $variable->PAVA_LLAVE }}</td>
                                            <td>{{ $variable->PAVA_NOMBRE }}</td>
                                            <td>{!! $variable->PAVA_VALOR_PLANTILLA !!}</td>
                                            <td>{{ $variable->PAVA_DESCRIPCION }}</td>
                                            <td>{{ \App\Util::dateFormat($variable->PAVA_FECHA_MODIFICACION, 'd/m/Y H:i ') . ' Hrs' }}</td>
                                            <td>
                                                <button type="button" id="editar" title="Modificar Variable" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalEdit" data-id={{ $variable->PAVA_ID }}>
                                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                                </button>
                                                <button type="button" id="eliminar" title="Eliminar" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalDelete" data-id={{ $variable->PAVA_ID }}>
                                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                </button>
                                                <button type="button" id="historico" title="Histórico" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#modalHistory" data-id={{ $variable->PAVA_ID }}>
                                                    <i class="fa fa-history"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div id="variables_predefinidas" class="tab-pane fade">
                            <br/>
                            <div class="col-md-6 col-md-offset-3">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        Variables predefinidas
                                    </div>
                                    <div class="panel-body">
                                        Las variables mostradas en esta tabla son variables predefinidas utilizadas en el proceso de cálculo y solver. Su valor se modifica desde la aplicación o durante el proceso. Éstas variables no se pueden eliminar. Se muestran de forma informativa.
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <table class="table table-hover table-bordered dataTable">
                                <thead>
                                    <tr>
                                        <th>
                                            Llave
                                        </th>
                                        <th>
                                            Nombre
                                        </th>
                                        <th>
                                            Descripcion
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($variablesPredefinidas as $variable)
                                        <tr>
                                            <td>{{ $variable->PAVA_LLAVE }}</td>
                                            <td>{{ $variable->PAVA_NOMBRE }}</td>
                                            <td>{{ $variable->PAVA_DESCRIPCION }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- NUEVO -->
    @include('variable.partials.modal-new')

    <!-- EDITAR-->
    @include('variable.partials.modal-edit')

    <!-- ELIMINAR -->
    @include('variable.partials.modal-delete')

    <!-- HISTORICO -->
    @include('variable.partials.modal-history')

    <!-- EJECUTAR QUERY -->
    @include('variable.partials.modal-query')
    
    <!-- iNTRODUCIR PARÁMETROS DE LA QUERY -->
    @include('variable.partials.modal-query-parametros')

@endsection                  

@section('script')
    <script>
    
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            if ($(e.target).attr('id') == 'predefinidas') {
                $('#btnNueva').hide();
            }
            else {
                $('#btnNueva').show();
            }
        });

        $(document).ready(function() {
            // Marcar la pestaña en el menú superior
            activeMenuItem('variable', 'configuracion');

            var targetXls = "{{ route('variable/exportarAExcel')}}";

            var targetPdfCalculo = "{{ route('variable/exportarAPdf', ['tipo' => 'calculo'])}}";
            var targetPdfProyeccion = "{{ route('variable/exportarAPdf', ['tipo' => 'proyeccion'])}}";
            var targetPdfDocumento = "{{ route('variable/exportarAPdf', ['tipo' => 'documento'])}}";
            var targetPdfPredefinidas = "{{ route('variable/exportarAPdf', ['tipo' => 'predefinidas'])}}";

            $('#variables_calculo .btn-exportar').append('<a title="Exportar a PDF" class="datatable-link" href="' + targetPdfCalculo + '"><i class="fa fa-file-pdf-o fa-2x"></i></a><a title="Exportar a Excel" class="datatable-link" style="margin-left:7px" href="' + targetXls + '" ><i class="fa fa-file-excel-o fa-2x"></i></a>');

            $('#variables_proyeccion .btn-exportar').append('<a title="Exportar a PDF" class="datatable-link" href="' + targetPdfProyeccion + '"><i class="fa fa-file-pdf-o fa-2x"></i></a><a title="Exportar a Excel" class="datatable-link" style="margin-left:7px" href="' + targetXls + '" ><i class="fa fa-file-excel-o fa-2x"></i></a>');

            $('#variables_plantilla .btn-exportar').append('<a title="Exportar a PDF" class="datatable-link" href="' + targetPdfDocumento + '"><i class="fa fa-file-pdf-o fa-2x"></i></a><a title="Exportar a Excel" class="datatable-link" style="margin-left:7px" href="' + targetXls + '" ><i class="fa fa-file-excel-o fa-2x"></i></a>');

            $('#variables_predefinidas .btn-exportar').append('<a title="Exportar a PDF" class="datatable-link" href="' + targetPdfPredefinidas + '"><i class="fa fa-file-pdf-o fa-2x"></i></a><a title="Exportar a Excel" class="datatable-link" style="margin-left:7px" href="' + targetXls + '" ><i class="fa fa-file-excel-o fa-2x"></i></a>');

            $('.dataTables_filter :input').removeClass('input-sm');

            $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
                    $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
            });
        }); 

    </script>
@endsection
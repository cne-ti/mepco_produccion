<div class="modal fade" id="modalHistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id='modal-content'>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Histórico Variable</h4>
            </div>
            <div class="modal-body">
                <table class="table table-hover table-bordered" id="tablaHistorico"></table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>                    
            </div>
        </div>
    </div>
</div>

    <script type="text/javascript">
        $('#modalHistory').on('show.bs.modal', function (e) {
            var pava_id = $(e.relatedTarget).attr('data-id');
             $.ajax({
                url: 'variable/historico',
                type: 'POST',
                data: {
                    'pava_id'       : $(e.relatedTarget).attr('data-id'),
                    '_token'        : $('#_token').val() 
                },
                success: function(data) {
                    loadingMessageHide();
                    if(data.status == "success"){                     
                        json = data.historico;
                        tablaHistorico = $('#tablaHistorico').DataTable({
                            // Inicialización del datatable... definición de idioma, columnas, datos
                            'iDisplayLength': 5,
                            'dom': '<"col-md-6 hidden-xs datatable-no-padding-left"l><"col-md-4 col-xs-4 col-md-offset-2 datatable-no-padding-right"f>rt<"clear"><"col-md-9 hidden-xs datatable-no-padding-left"i><"col-md-3 col-xs-12 datatable-no-padding-right"p>',
                            language: {
                                    processing:     "Buscando...",
                                    search:         "Buscar",
                                    lengthMenu:     "Mostrar _MENU_ registros",
                                    info:           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                                    infoEmpty:      "No se encotraron registros",
                                    infoFiltered:   " (De un total de _MAX_ registros)",
                                    infoPostFix:    "",
                                    loadingRecords: "Cargando vista...",
                                    zeroRecords:    "No se encontraron registros",
                                    emptyTable:     "No se encontraron registros",
                                    paginate: {
                                        first:      "Primer",
                                        previous:   "Anterior",
                                        next:       "Siguiente",
                                        last:       "&Uacute;ltimo"
                                }
                            },
                            aaData: json,                 
                            aoColumns: [
                                { 'sTitle': 'Llave',                 'mDataProp' : 'VAHI_LLAVE' },
                                { 'sTitle': 'Nombre',                'mDataProp' : 'VAHI_NOMBRE' },
                                { 'sTitle': 'Valor',                 'mDataProp' : 'VAHI_VALOR' },
                                { 'sTitle': 'Descripción',           'mDataProp' : 'VAHI_DESCRIPCION' },
                                { 'sTitle': 'Fecha de modificación', 'mDataProp' : 'VAHI_FECHA' },
                                { 'sTitle': 'Usuario',               'mDataProp' : 'USUA_EMAIL'},
                            ]});
                        // Agregar el botón 'exportar' a la tabla del modal
                        var targetPdf = "";
                        var targetXls = "";
                        $('.btn-exportar-modal').append('<a title="Exportar a PDF" class="datatable-link" href="' + targetPdf + '"><i class="fa fa-file-pdf-o fa-2x"></i></a><a title="Exportar a Excel" class="datatable-link" style="margin-left:7px" href="' + targetXls + '" ><i class="fa fa-file-excel-o fa-2x"></i></a>');
                    }else{
                        errorProcess(data.status);
                    }
                },
                error: function(e) {
                    console.log(e.message);
                }
            }); 
        });

        $('#modalHistory').on('hide.bs.modal', function (e) {
            // Eliminar la datatable cuando se cierra el modal para poder cargarlo después de nuevo
            tablaHistorico.destroy();
        });

  </script>

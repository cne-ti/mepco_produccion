<div class="form-group">
    <div class="col-md-5 lblObligatorio"></div>
</div>
<div id="seleccionar_tipo">
    <div class="form-group required">
        <label class="col-md-4 control-label">Tipo de variable</label>
        <div class="col-md-3">
            <select id="select_tipo_variable" class="form-control">
                <option value="calculo">Cálculo</option>
                <option value="proyeccion">Proyección</option>
                <option value="documento">Documento</option>
            </select>
        </div>
    </div>
    <div class="form-group required">
        <label class="col-md-4 control-label">Query</label>
        <div class="col-md-6">
            <input type="checkbox" id="pava_query" name="pava_query" class="checkSiNo" />
        </div>
    </div>
</div>
<div class="form-group required">
    <label class="col-md-4 control-label">Llave</label>
    <div class="col-md-6">
        <input type="text" class="form-control" id="pava_llave" name="pava_llave" />
    </div>
</div>
<div class="form-group required">
    <label class="col-md-4 control-label">Nombre</label>
    <div class="col-md-6">
        <input type="text" class="form-control" id="pava_nombre" name="pava_nombre" />
    </div>
</div>
<div class="form-group required">
    <label class="col-md-4 control-label">Descripción</label>
    <div class="col-md-6">
        <textarea class="form-control" id="pava_descripcion" name="pava_descripcion" maxlength="255"></textarea>
        <div class="numChar"></div>
    </div>
</div>
<div id="hidrocarburo" class="form-group required">
    <label class="col-md-4 control-label">Hidrocarburo</label>
    <div class="col-md-6">
        <select id="hidr_id" name="hidr_id" class="form-control">
                <option id="0" value="0">Sin hidrocarburo asociado</option>
            @foreach ($hidrocarburos as $hidrocarburo)
                <option id="{{$hidrocarburo->HIDR_ID}}" value="{{$hidrocarburo->HIDR_ID}}">{{$hidrocarburo->HIDR_NOMBRE_CHILE}}</option>
            @endforeach
        </select>
    </div>
</div>
<div id="no-query">
    <div class="form-group required">
        <label class="col-md-4 control-label">Valor</label>
        <div class="col-md-6">
            <input type="text" class="form-control" id="pava_valor_calculo" name="pava_valor_calculo" />
        </div>
    </div>
</div>
<div id="query">
    <div class="form-group required">
        <label class="col-md-4 control-label">Query</label>
        <div class="col-md-6">
            <textarea class="form-control" id="pava_valor_query" name="pava_valor_query" rows="2"></textarea>
        </div>
    </div>
</div>
<div id="plantilla">
    <div class="form-group required">
        <label class="col-md-4 control-label">Valor</label>
        <div class="col-md-6">
            <textarea class="form-control" id="pava_valor_plantilla" name="pava_valor_plantilla" rows="2"></textarea>
        </div>
    </div>
</div>


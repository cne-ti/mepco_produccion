<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar Variable</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-edit">
                    <!-- Errores en formulario -->
                    @include('partials.error')
                    <input type="hidden" name="pava_id" id="pava_id">
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                    <!-- Añadir campos -->
                    @include('variable.partials.fields')
                    <div class="form-group">
                        <label class="col-md-4 control-label">Fecha de modificación</label>
                        <div class="col-md-3">
                            <input type="input" class="form-control" name="pava_fecha_modificacion" id="pava_fecha_modificacion" disabled>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary loading-message" id="btnGuardar">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#modalEdit').on('show.bs.modal', function (e) {
        // Errores
        $('#form-edit #errors').hide();
        $('#form-edit')[0].reset();
        // Inicializar
        $('.numChar').text('');
        $('#form-edit #pava_descripcion').on('keyup', numChar);

        $('#form-edit #select_tipo_variable').attr('disabled', true);
        $('#form-edit #pava_query').bootstrapSwitch('readonly', false);

        $.ajax({
            url: 'variable/'+$(e.relatedTarget).attr('data-id')+'/edit',
            type: 'GET',
            data: { 
                '_token'        : $('#_token').val() 
            },
            success: function(data) {
                loadingMessageHide();
                if(data.status == "success"){
                    var variable = data.variable;
                    if ((variable.PAVA_TIPO_VARIABLE == {{Util::VARIABLE_CALCULO}})||(variable.PAVA_TIPO_VARIABLE == {{Util::VARIABLE_PROYECCION}})) {
                        $('#form-edit #no-query').show();
                        $('#form-edit #hidrocarburo').show();
                        $('#form-edit #pava_valor_calculo').val(variable.PAVA_VALOR_CALCULO);
                        $('#form-edit #hidr_id option[value="'+variable.HIDR_ID+'"]').prop('selected', true);
                        $('#form-edit #pava_valor_calculo').formatNumber();
                        $('#form-edit #query').hide();
                        $('#form-edit #plantilla').hide();
                        $("#form-edit #pava_query").bootstrapSwitch('state',false);
                        if (variable.PAVA_TIPO_VARIABLE == {{Util::VARIABLE_CALCULO}}) {
                            $("#form-edit #select_tipo_variable").val('calculo');
                        }
                        else {
                            $("#form-edit #select_tipo_variable").val('proyeccion');
                        }
                    }
                    else if ((variable.PAVA_TIPO_VARIABLE == {{Util::VARIABLE_QUERY}})||(variable.PAVA_TIPO_VARIABLE == {{Util::VARIABLE_PROYECCION_QUERY}})) {
                        $("#form-edit #select_tipo_variable").val('calculo');
                        $('#form-edit #no-query').hide();
                        $('#form-edit #hidrocarburo').show();
                        $('#form-edit #pava_valor_query').val(variable.PAVA_VALOR_PLANTILLA);
                        $('#form-edit #hidr_id option[value="'+variable.HIDR_ID+'"]').prop('selected', true);
                        $('#form-edit #query').show();
                        $('#form-edit #plantilla').hide();
                        $("#form-edit #pava_query").bootstrapSwitch('state',true);
                        if (variable.PAVA_TIPO_VARIABLE == {{Util::VARIABLE_QUERY}}) {
                            $("#form-edit #select_tipo_variable").val('calculo');
                        }
                        else {
                            $("#form-edit #select_tipo_variable").val('proyeccion');
                        }
                    }
                    else if (variable.PAVA_TIPO_VARIABLE == {{Util::VARIABLE_PLANTILLA}}) {
                        $("#form-edit #select_tipo_variable").val('documento');
                        $('#form-edit #no-query').hide();
                        $('#form-edit #hidrocarburo').hide();
                        $('#form-edit #query').hide();
                        $('#form-edit #plantilla').show();
                        $('#form-edit #pava_valor_plantilla').val(variable.PAVA_VALOR_PLANTILLA);
                        $("#form-edit #pava_query").bootstrapSwitch('state',false);
                    }
                    else if (variable.PAVA_TIPO_VARIABLE == {{Util::VARIABLE_QUERY_PLANTILLA}}) {
                        $("#form-edit #select_tipo_variable").val('documento');
                        $('#form-edit #no-query').hide();
                        $('#form-edit #hidrocarburo').hide();
                        $('#form-edit #query').show();
                        $('#form-edit #plantilla').hide();
                        $('#form-edit #pava_valor_query').val(variable.PAVA_VALOR_PLANTILLA);
                        $("#form-edit #pava_query").bootstrapSwitch('state',true);
                    }

                    $('#form-edit #pava_id').val(variable.PAVA_ID);
                    $('#form-edit #pava_llave').val(variable.PAVA_LLAVE);
                    $('#form-edit #pava_nombre').val(variable.PAVA_NOMBRE);
                    $('#form-edit #pava_descripcion').val(variable.PAVA_DESCRIPCION);
                    $('#form-edit #pava_fecha_modificacion').val(variable.PAVA_FECHA_MODIFICACION);
                    // Readonly al tipo de variable
                    $('#form-edit #pava_tipo_variable').bootstrapSwitch('readonly', true);
                    $('#form-edit #pava_query').bootstrapSwitch('readonly', true);
                }else{
                    errorProcess(data.status);
                }
            },
            error: function(e) {
                console.log(e.message);
            }
        });           
    });

    $('#modalEdit #btnGuardar').click(function(){
        var formData = $('#form-edit').serialize();
        $.ajax({
            url: 'variable/'+$("#form-edit #pava_id").val(),
            type: 'PUT',
            data: { 
                'formData'  : formData,
                '_token'    : $('#modalEdit #_token').val() 
            },
            success: function(data) {
                loadingMessageHide();
                if(data.status == 'success'){
                    $('#form-edit #errors').hide();
                    $('#modalEdit').modal('hide');
                    location.reload();
                }else{
                    errorProcess('form-edit', data.status);
                }
            },
            error: function(e) {
                console.log(e.message);
            }
        });
    });

  </script>
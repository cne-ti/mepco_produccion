<div class="modal fade" id="modalQuery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Resultado de la Query</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-query">
                    <!-- Errores en formulario -->
                    @include('partials.error')
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Resultado: </label>
                        <div class="col-md-6">
                            <input type="text" id="resultado" name="resultado" class="form-control" />
                        </div>
                    </div>                   
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#modalQuery').on('show.bs.modal', function (e) {
        $('#loading-message').show();
        // Errores
        $('#form-query #errors').hide();
        $('#form-query')[0].reset();

        $.ajax({
            url: 'variable/ejecutarQuery',
            type: 'POST',
            data: { 
                'pava_id'       : $(e.relatedTarget).attr('data-id'),
                '_token'        : $('#_token').val() 
            },
            success: function(data) {
                loadingMessageHide();
                if(data.status == "success"){
                    $('#resultado').val(data.resultado);
                }
                else if (data.status == "parameters") {
                    var parametros = data.parametros;
                    $('#form-fields').html('');
                    $('#form-parametros #pava_id').val($(e.relatedTarget).attr('data-id'));

                    for (i=0;i<parametros.length;i++) {
                        $('#form-fields').append('<div class="form-group"><label class="col-md-4" for="'+parametros[i]+'">'+parametros[i]+'</label><div class="col-md-6"><input type="text" id="'+parametros[i]+'" name="'+parametros[i]+'" class="form-control " /></div></div>');
                    }

                    $('#form-parametros #errors ul').html('');
                    $('#form-parametros #errors').hide();

                    $('#modalQueryParametros').modal('show');
                }
                else{
                    errorProcess('form-query', data.errors);
                }
                $('#loading-message').hide();
            },
            error: function(e) {
                console.log(e.message);
            }
        });           
    });

</script>
<div class="modal fade" id="modalQueryParametros" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Introducir parámetros</h4>
            </div>
             <form class="form-horizontal" id="form-parametros">
                <div class="modal-body">
                        <!-- Errores en formulario -->
                        @include('partials.error')
                        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="pava_id" id="pava_id">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Parámetros: </label>
                        </div>
                        <div id="form-fields"></div>     
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="ejecutarQuery" class="btn btn-default">Ejecutar Query</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#ejecutarQuery').click(function (e){
        var formData = $('#form-parametros').serialize();
        $.ajax({
            url: 'variable/ejecutarQueryParametros',
            type: 'POST',
            data: { 
                'formData'      : formData,
                '_token'        : $('#_token').val() 
            },
            success: function(data) {
                if (data.status=='success') {
                    $('#modalQuery #resultado').val(data.resultado);
                    $('#modalQueryParametros').modal('hide');
                }
                else {
                    errorProcess('form-parametros', data.errors);
                }
            },
            error: function(e) {
                console.log(e);
            }
        });           
    });

</script>
<div class="modal fade" id="modalNew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Nueva variable</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-new">
                    @include('partials.error')
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="pava_tipo_variable" id="pava_tipo_variable">
                    <!-- Añadir campos -->
                    @include('variable.partials.fields')
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btnGuardar" class="btn btn-primary loading-message">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#modalNew').on('show.bs.modal', function () {
        // Errores
        $('#form-new #errors').hide();
        $('#form-new')[0].reset();
        // Ocultar
        $("#form-new #pava_query").bootstrapSwitch('state',false);      
        $('#form-new #calculo').hide();
        $('#form-new #plantilla').hide(); 
        $('#form-new #query').hide();
        $('#form-new #no-query').show();
        $('#form-new #hidrocarburo').show();
        $('#form-new #pava_tipo_variable').val('{{Util::VARIABLE_CALCULO }}');
        $('#form-new #pava_valor_calculo').formatNumber();
        $('.numChar').text('');
        $('#form-new #pava_descripcion').on('keyup', numChar);
    });

    $('#form-new #pava_query').on('switchChange.bootstrapSwitch', campos);
    $('#select_tipo_variable').on('change', campos);

    $('#modalNew #btnGuardar').click(function(){
        var formData = $('#form-new').serialize();

        $.ajax({
            url: 'variable',
            type: 'POST',
            data: { 
                'formData' : formData,
                '_token' : $('#modalNew #_token').val() 
            },
            success: function(data) {
                loadingMessageHide();
                if(data.status == 'success'){
                    $('#form-new #errors').hide();
                    $('#modalNew').modal('hide');
                    location.reload();
                }else{
                    errorProcess('form-new', data.status);
                }
            },
            error: function(e) {
                console.log(e.message);
            }
        });
    });

    function campos() {
        if ($('#select_tipo_variable').val() == 'calculo') {
            if ($('#form-new #pava_query').bootstrapSwitch('state')==true) { // query
                $('#form-new #pava_tipo_variable').val('{{Util::VARIABLE_QUERY }}');
                $('#form-new #hidrocarburo').show();
                $('#form-new #query').show();
                $('#form-new #plantilla').hide();
                $('#form-new #no-query').hide();
            }
            else { // no query
                $('#form-new #pava_tipo_variable').val('{{Util::VARIABLE_CALCULO }}');
                $('#form-new #hidrocarburo').show();
                $('#form-new #query').hide();
                $('#form-new #plantilla').hide();
                $('#form-new #no-query').show();
            }
        }
        else if ($('#select_tipo_variable').val() == 'documento') {
            if ($('#form-new #pava_query').bootstrapSwitch('state')==true) {
                $('#form-new #pava_tipo_variable').val('{{Util::VARIABLE_QUERY_PLANTILLA }}');
                $('#form-new #hidrocarburo').hide();
                $('#form-new #query').show();
                $('#form-new #plantilla').hide();
                $('#form-new #no-query').hide();
            }
            else {
                $('#form-new #pava_tipo_variable').val('{{Util::VARIABLE_PLANTILLA }}');
                $('#form-new #hidrocarburo').hide();
                $('#form-new #query').hide();
                $('#form-new #plantilla').show();
                $('#form-new #no-query').hide();
            }
        }
        else if ($('#select_tipo_variable').val() == 'proyeccion') {
            if ($('#form-new #pava_query').bootstrapSwitch('state')==true) {
                $('#form-new #pava_tipo_variable').val('{{Util::VARIABLE_PROYECCION_QUERY }}');
                $('#form-new #hidrocarburo').show();
                $('#form-new #query').show();
                $('#form-new #plantilla').hide();
                $('#form-new #no-query').hide();
            }
            else {
                $('#form-new #pava_tipo_variable').val('{{Util::VARIABLE_PROYECCION }}');
                $('#form-new #hidrocarburo').show();
                $('#form-new #query').hide();
                $('#form-new #plantilla').hide();
                $('#form-new #no-query').show();
            }
        }
        $('#form-new #pava_valor_calculo').val('');
        $('#form-new #pava_valor_plantilla').val('');
        $('#form-new #pava_valor_query').val('');
    }
  </script>
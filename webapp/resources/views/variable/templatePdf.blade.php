<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<img src='images/Logo_Oficinal_CNE.png'>
<br/>

<h3>Listado de Variables</h3>
<table class="table col-xs-10" cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center; table-layout:fixed; width:100%; word-wrap:break-word">
    <thead style="background-color:#f5f5f5;">
        <tr>
            <th>
                Identificador
            </th>
            <th>
                Llave
            </th>
            <th>
                Nombre
            </th>
            <th>
                Valor
            </th>
            <th>
                Descripción
            </th>
            <th>
                Fecha de modificación
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($variables as $variable)
            <tr>
                <td style="border:1px solid #ddd;">{{ $variable->PAVA_ID }}</td>
                <td style="border:1px solid #ddd;">{{ $variable->PAVA_LLAVE }}</td>
                <td style="border:1px solid #ddd;">{{ $variable->PAVA_NOMBRE }}</td>
                <td style="border:1px solid #ddd;">{{ $variable->PAVA_VALOR_PLANTILLA }}</td>
                <td style="border:1px solid #ddd;">{{ $variable->PAVA_DESCRIPCION }}</td>
                <td style="border:1px solid #ddd;">{{ \App\Util::dateFormat($variable->PAVA_FECHA_MODIFICACION, 'd/m/Y H:i ') . ' Hrs' }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

<br/>
<br/>
<h5>Fecha del reporte: {!! date('d-m-Y') !!}</h5>

@extends('public-layout')
@section('content')

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-10 col-xs-10">
                                <h4>Usuarios
                                <button type="button" class="btn btn-primary" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalNew">
                                Nuevo <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                </button></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table id="list" class="table table-hover table-bordered dataTable">
                        <thead>
                            <tr>
                                <th>
                                    Dirección e-mail
                                </th>
                                <th>
                                    Perfiles asignados
                                </th>
                                <th>
                                    Opciones
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($usuarios as $usuario)
                                <tr>
                                    <td>{{ $usuario->USUA_EMAIL }}</td>
                                    <td>
                                        @foreach($perfilesUsuario as $key1=>$perfil)
                                            @if ($key1 == $usuario->USUA_EMAIL)
                                                @foreach($perfil as $key2=>$val)
                                                    <p>{{$perfil[$key2]->PERF_NOMBRE}}</p>
                                                @endforeach
                                            @endif
                                        @endforeach
                                    </td>
                                    <td style="vertical-align:middle;">
                                        <button type="button" id="editar" name="editar" class="btn btn-default" aria-label="Left Align" title="Gestionar Perfiles" data-toggle="modal" data-target="#modalEdit" data-id={{ $usuario->USUA_EMAIL }}>
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </button>
                                        <button type="button" id="eliminar" class="btn btn-default" aria-label="Left Align" title="Eliminar" data-toggle="modal" data-target="#modalDelete" data-id={{ $usuario->USUA_EMAIL }}>
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- CREAR USUARIO -->
    @include('usuario.partials.modal-new')

    <!-- EDITAR PERFILES -->
    @include('usuario.partials.modal-edit')

    <!-- ELIMINAR USUARIO -->
    @include('usuario.partials.modal-delete')

@endsection                  

@section('script')
    <script>

        $(document).ready(function() {

            // Marcar la pestaña en el menú superior
            activeMenuItem('usuario', 'configuracion');
            
            var targetPdf = "{{ route('usuario/exportarAPdf')}}";
            var targetXls = "{{ route('usuario/exportarAExcel')}}";

            @include('partials.btn-exportar');
        }); 

    </script>
@endsection

<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Confirmar</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-delete">
                    ¿Desea eliminar el usuario seleccionado?
                    <input type="hidden" name="usua_email" id="usua_email">
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="btnEliminar">Eliminar</button>
            </div>
        </div>
    </div>
</div>

<!-- modal delete end  -->

<script type="text/javascript">

    $('#modalDelete').on('show.bs.modal', function (e) {
        $('#form-delete #usua_email').val($(e.relatedTarget).attr('data-id'));   
    });

    $('#btnEliminar').click(function(){
        $.ajax({
            url: 'usuario/eliminar',
            type: 'POST',
            data: { 
                'usua_email' : $('#form-delete #usua_email').val(),
                '_token' : $('#form-delete #_token').val() 
            },
            success: function(data) {
                if(data.status == 'success'){
                    $('#modalDelete').modal('hide');
                    location.reload();
                }else{
                    console.log(data.status);
                }
            },
            error: function(e) {
                console.log(e.message);
            }
        });
    });

</script>
<div class="modal fade" id="modalNew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Nuevo Usuario</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-new">
                    <!-- Errores en formulario -->
                    @include('partials.error')
                    <!-- Añadir campos -->
                    @include('usuario.partials.fields')
                </form>
                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btnGuardar" class="btn btn-primary loading-message">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#modalNew .checkSiNo').on('switchChange.bootstrapSwitch', function() {
        var seleccionados = 0;
        $('#modalNew .checkSiNo').each(function(i) {
            if ($(this).bootstrapSwitch('state') == true) {
                seleccionados++;   
            }
        });
        if (seleccionados>0) {
            $('#modalNew #btnGuardar').attr('disabled', false);
        }
        else {
            $('#modalNew #btnGuardar').attr('disabled', true);
        }
    });

    $('#modalNew').on('show.bs.modal', function () {
        $('#form-new #errors').hide();
        $('#form-new #usua_email').val('');
        // Eliminar los checked de todos los checkbox
        $('#modalNew input:checkbox').bootstrapSwitch('state',false);
        $('#btnGuardar').attr('disabled', true);
    });

    $('#modalNew #btnGuardar').click(function(){
        var formData = $('#form-new').serialize();

        $.ajax({
            url: 'usuario',
            type: 'POST',
            data: { 
                'formData' : formData,
                '_token' : $('#modalNew #_token').val(),
            },
            success: function(data) {
                loadingMessageHide();
                if(data.status == 'success'){
                    $('#form-new #errors').hide();
                    $('#modalNew').modal('hide');
                    location.reload();
                }else{
                    errorProcess('form-new', data.status);
                }
            },
            error: function(e) {
                console.log(e.message);
            }
        });
    });
  </script>
<div class="form-group">
    <div class="col-md-5 lblObligatorio"></div>
</div>
<div class="form-group required">
    <label class="col-md-5 control-label">Dirección Correo Electrónico</label>
    <div class="col-md-5">
        <input type="text" class="form-control" id="usua_email" name="usua_email" />
    </div>
</div>

<table class="table table-bordered table-hover" id="usuarios">
	<thead>
	    <tr>
	        <th>Nombre del perfil</th>
	        <th>¿Asignado a usuario?</th>
	    </tr>
    </thead>
    <tbody>
		@foreach ($perfiles as $perfil)
			<tr>
	       		<td>{{ $perfil->PERF_NOMBRE }}</td>
	       		<td><input type="checkbox" name="check[{{ $perfil->PERF_ID }}]" class="checkSiNo" target-input="{{ $perfil->PERF_NOMBRE }}"></td>
			</tr>
		@endforeach
	</tbody>
</table>
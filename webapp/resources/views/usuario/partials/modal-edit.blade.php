<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar Usuario</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-edit">
                    <!-- Errores en formulario -->
                    @include('partials.error')
                    <!-- Añadir campos -->
                    @include('usuario.partials.fields')
                </form>
                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary loading-message" id="btnGuardar">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#modalEdit .checkSiNo').on('switchChange.bootstrapSwitch', function() {
        var seleccionados = 0;
        $('#modalEdit .checkSiNo').each(function(i) {
            if ($(this).bootstrapSwitch('state') == true) {
                seleccionados++;   
            }
        });
        if (seleccionados>0) {
            $('#modalEdit #btnGuardar').attr('disabled', false);
        }
        else {
            $('#modalEdit #btnGuardar').attr('disabled', true);
        }
    });

    $('#modalEdit').on('show.bs.modal', function (e) {
        $('#form-edit #errors').hide();
        usua_email=$(e.relatedTarget).attr('data-id');
        $('#form-edit #usua_email').val(usua_email);
        $('#form-edit #usua_email').attr('readonly', true);
        // Eliminar los checked de todos los checkbox
        $('#modalEdit input:checkbox').bootstrapSwitch('state',false);
        $.ajax({
            url: 'usuario/editar',
            type: 'POST',
            data: { 
                'usua_email' : usua_email,
                '_token' : $('#_token').val() 
            },
            success: function(data) {
                loadingMessageHide();
                if(data.status == "success"){
                    var perfiles = data.perfiles;
                    for (var i=0;i<perfiles.length;i++) {
                        var perf_id = perfiles[i].PERF_ID;
                        $("[name='check["+perf_id+"]']").bootstrapSwitch('state',true);
                    }
                }else{
                    errorProcess(data.status);
                }
            },
            error: function(e) {
                console.log(e.message);
            }
        });           
    });

    $('#modalEdit #btnGuardar').click(function(){
        var formData = $('#form-edit').serialize();
        $.ajax({
            url: 'usuario/actualizar',
            type: 'POST',
            data: { 
                'formData' : formData,
                '_token' : $('#modalEdit #_token').val() 
            },
            success: function(data) {
                loadingMessageHide();
                if(data.status == 'success'){
                    $('#form-edit #errors').hide();
                    $('#modalEdit').modal('hide');
                    $('#form-edit input:checkbox').attr('checked', false);
                    location.reload();
                }else{
                    errorProcess('form-edit', data.status);
                }
            },
            error: function(e) {
                console.log(e.message);
            }
        });
    });

  </script>
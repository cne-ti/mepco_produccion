<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<img src='images/Logo_Oficinal_CNE.png'>
<br/>

<h3>Listado de Usuarios</h3>
<table class="table table-hover col-xs-10 table-bordered" cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center">
    <thead style="background-color:#f5f5f5;">
        <tr>
            <th>
                E-mail
            </th>
            <th>
                Perfiles asignados
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($usuarios as $usuario)
            <tr>
                <td style="border:1px solid #ddd;">{{ $usuario->USUA_EMAIL }}</td>
                <td style="border:1px solid #ddd;">{{ $usuario->PERF_NOMBRE }}</td>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<br/>
<br/>
<h5>Fecha del reporte: {!! date('d-m-Y') !!}</h5>

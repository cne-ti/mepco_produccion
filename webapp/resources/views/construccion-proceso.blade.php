@extends('public-layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <script type="text/javascript">
                        var btnHabilitar='SeleccionarValidadores';
                    </script>
                    <div class="container-fluid">
                        <div class="row text-center">
                            <div class="col-md-2 col-xs-3">
                                <div style="padding: 10px">
                                    <a id="SeleccionarValidadores" href="{{ route('proceso/validadores', ['id' => 1]) }}" class="loading-message" style="color: #ccc;"><i class="fa fa-hand-paper-o fa-2x"></i><br>Seleccion validadores</a>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-3">
                                <div style="padding: 10px">
                                    <a id="ValorDeMercado" href="{{ route('proceso/valorMercado', ['id' => 1]) }}" class="loading-message" style="color: #ccc;"><i class="fa fa-upload fa-2x"></i><br>Valores de mercado</a>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-3">
                                <div style="padding: 10px">
                                    <a id="Solver" href="{{ route('construccionProceso') }}" class="loading-message" style="color: #ccc"><i class="fa fa-calculator fa-2x"></i><br>Solver</a>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-3">
                                <div style="padding: 10px">
                                    <a id="Escenarios" href="{{ route('construccionProceso') }}" class="loading-message" style="color: #ccc"><i class="fa fa-cog fa-2x"></i><br>Crea Escenarios</a>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-3">
                                <div style="padding: 10px">
                                    <a id="Proyeccion" href="{{ route('construccionProceso') }}" class="loading-message" style="color: #ccc"><i class="fa fa-line-chart fa-2x"></i><br>Proyección</a>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-3">
                                <div style="padding: 10px">
                                    <a id="Documentos" href="{{ route('construccionProceso') }}" class="loading-message" style="color: #ccc"><i class="fa fa-file-text fa-2x"></i><br>Documentos</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                    	$('#' + btnHabilitar).css('color', '');
                    </script>
                </div>
                <div class="panel-body">   
                    <center><h1>Sección en construcción</h1></center>
                    <center><i class='fa fa-exclamation-triangle fa-5x'></i></center>
                </div>
            </div>
        </div>
    </div>

@endsection 

@section('script')
    <script type="text/javascript">
        activeMenuItem('proceso');
    </script>
@endsection

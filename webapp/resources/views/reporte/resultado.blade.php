@extends('public-layout')
@section('content')

<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-2 col-xs-12">
                            <h4>Resultados</h4>
                        </div>
                        <div class="col-md-10 col-xs-12" style="padding-top: 10px;padding-bottom: 10px">
                                <form class="form-inline pull-right" action="{{route('reporte/getResultados')}}" method="POST" id="form-search">
                                    {!! csrf_field() !!}
                                    <div class="form-group" style="padding-right: 10px">
                                        <label for="fecha_vigencia_desde">Fecha vigencia desde</label>
                                        @if (isset($fechas))
                                            <input type="text" name="fecha_vigencia_desde" id="fecha_vigencia_desde" class="form-control fecha-vigencia-all" style="width: 117px" placeholder="dd/mm/yyyy" value={{ $fechas['fecha_vigencia_desde'] }}>
                                        @else
                                            <input type="text" name="fecha_vigencia_desde" id="fecha_vigencia_desde" class="form-control fecha-vigencia-all" style="width: 117px" placeholder="dd/mm/yyyy" >
                                        @endif

                                    </div>
                                    <div class="form-group" style="padding-right: 10px">
                                        <label for="fecha_vigencia_hasta">Fecha vigencia hasta</label>
                                        @if (isset($fechas))
                                            <input type="text" name="fecha_vigencia_hasta" id="fecha_vigencia_hasta" class="form-control fecha-vigencia-all" style="width: 117px" placeholder="dd/mm/yyyy" value={{ $fechas['fecha_vigencia_hasta'] }}>
                                        @else
                                            <input type="text" name="fecha_vigencia_hasta" id="fecha_vigencia_hasta" class="form-control fecha-vigencia-all" style="width: 117px" placeholder="dd/mm/yyyy">
                                        @endif
                                    </div>
                                    <div class="form-group" style="padding-right: 10px">
                                        @if (isset($tipo_crudo) && $tipo_crudo == 'BRENT')
                                            <input type="checkbox" id="tipo_crudo" name="tipo_crudo" class="checkBrentWti" checked="true">
                                        @else
                                            <input type="checkbox" id="tipo_crudo" name="tipo_crudo" class="checkBrentWti">
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" id="btnBuscar" class="btn btn-primary loading-message">Buscar</button>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-md-8 col-md-offset-2" style="padding: 0">
                    <div id="mensaje"></div>
                </div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger col-md-8 col-md-offset-2 ">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div id="resultado">
                    <div class="col-md-1 col-md-offset-11">
                        <div id="exportar"></div>
                        <br/>
                    </div>
                    <br/>
                    @foreach ($tabla as $fecha_vigencia=>$resultados)
                        <table class = 'table table-hover table-condensed table-bordered' style="table-layout:auto;">
                            <thead>
                                <tr>
                                    <th colspan = '12'>
                                        Fecha de vigencia: {{ $fecha_vigencia }}
                                        RVP: {{ $rvp }}
                                    </th>
                                 </tr>
                                <tr>
                                    <th>Combustible</th>
                                    <th>n</th>
                                    <th>m</th>
                                    <th>s</th>
                                    <th>t</th>
                                    <th>Peso Fut.</th>
                                    <th>Paridad Anterior $/m3</th>
                                    <th>PRI Anterior $/m3</th>
                                    <th>PRinf $/m3</th>
                                    <th>PRI $/m3</th>
                                    <th>PRS $/m3</th>
                                    <th>Paridad $/m3</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($resultados as $fila)
                                    <tr>
                                        <td>{{ $fila['COMBUSTIBLE'] }}</td>
                                        <td>{{ $fila['N'] }}</td>
                                        <td>{{ $fila['M'] }}</td>
                                        <td>{{ $fila['S'] }}</td>
                                        <td>{{ $fila['T'] }}</td>
                                        <td>{{ $fila['PESO_FUT'] }}</td>
                                        <td>{{ $fila['PARIDAD_ANTERIOR'] }}</td>
                                        <td>{{ $fila['PRI_ANTERIOR'] }}</td>
                                        <td>{{ $fila['PRINF'] }}</td>
                                        <td>{{ $fila['PRI'] }}</td>
                                        <td>{{ $fila['PRS'] }}</td>
                                        <td>{{ $fila['PARIDAD'] }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<div id="hidden">
    <form id="exportarPdf" action="{{route('reporte/exportarAPdf')}}" method="POST">
        {!! csrf_field() !!}
        <input type="hidden" id="tablaPdf" name="tabla">
        <input type="hidden" id="template" name="template">
    </form>
    <form id="exportarExcel" action="{{route('reporte/exportarAExcel')}}" method="POST" >
        {!! csrf_field() !!}
        <input type="hidden" id="tablaExcel" name="tabla">
        <input type="hidden" id="nombreReporte" name="nombreReporte">
        <input type="hidden" id="vista" name="vista">
    </form>
</div>

@endsection                  

@section('script')

    <script type="text/javascript">
        $.fn.bootstrapSwitch.defaults.size = 'small';
        $.fn.bootstrapSwitch.defaults.onText = 'BRENT';
        $.fn.bootstrapSwitch.defaults.offText = 'WTI';

        $("[name='tipo_crudo']").bootstrapSwitch({
          size   :'mini'
        });

        $('.bootstrap-switch-label').click(function() {
            $('#tipo_crudo').toggle();
        });

        if ('{{ $mensaje }}'!='') {
            $('#resultado').hide();
            $('#mensaje').html('{!! $mensaje !!}');
        }

        $('#fecha_vigencia_desde').change(function (){
            minDate = $(this).datepicker('getDate');
            minDate = new Date(minDate.getTime());
            minDate.setDate(minDate.getDate());
            $('#fecha_vigencia_hasta').datepicker('option', 'minDate', minDate);
        });
        
        $(document).ready(function() {
            activeMenuItem('resultado', 'reportes');

            tabla = JSON.stringify({!! json_encode($tabla) !!});

            $('#fecha_vigencia_desde').datepicker('option', 'maxDate', '{{ $maxDate }}');
            $('#fecha_vigencia_hasta').datepicker('option', 'maxDate', '{{ $maxDate }}');

            // Asignar a los botones 'Exportar' unas funciones de AJAX para poder obtener el contenido filtrado
            $('#exportar').append('<a title="Exportar a PDF" class="datatable-link" id="exportarLogPdf" href="#"><i class="fa fa-file-pdf-o fa-2x"></i></a><a title="Exportar a Excel" class="datatable-link" style="margin-left:7px" id="exportarLogExcel" href="#"><i class="fa fa-file-excel-o fa-2x"></i></a>');

            // Funciones para los botones 'Exportar'
            $('#exportarLogPdf').click(function(){
                $('#tablaPdf').val(tabla);
                $('#template').val('reporte.resultadosTemplatePdf');
                $('#exportarPdf').submit();
            });
            $('#exportarLogExcel').click(function(){
                $('#tablaExcel').val(tabla);
                $('#nombreReporte').val('Reporte de Resultados');
                $('#vista').val('reporte.resultadosTemplatePdf');
                $('#exportarExcel').submit();
            });

        });
    </script>
@endsection
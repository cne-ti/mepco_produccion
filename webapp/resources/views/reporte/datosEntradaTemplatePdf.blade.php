<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<img src='images/Logo_Oficinal_CNE.png'>
<br/>

<h3>Reporte de Datos de Entrada</h3>

    <h4>Valores de Mercado</h4>
    @foreach ($tabla as $fecha_vigencia=>$resultados)
        <h4>Fecha de vigencia: {{ $fecha_vigencia }}</h4>
        <table class="table table-hover col-xs-10 table-bordered" cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center; table-layout:fixed; width:100%; word-wrap:break-word">
            <thead style="background-color:#f5f5f5;">
              <tr>
                <th>Fecha</th>
                <th>Gas Unl 87</th>
                <th>Gas Unl 93</th>
                <th>Ls Diésel</th>
                <th>USG Butano</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($resultados as $fila)
                <tr>
                    <td style="border:1px solid #ddd;">{{ Util::dateFormat($fila->DAHI_FECHA, 'd/m/Y') }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_GAS_UNL_87) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_GAS_UNL_93) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_LD_DIESEL) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_USG_BUTANO) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <br />
        <table class="table table-hover col-xs-10 table-bordered" cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center; table-layout:fixed; width:100%; word-wrap:break-word">
            <thead style="background-color:#f5f5f5;">
              <tr>
                <th>Fecha</th>
                <th>UKC USG</th>
                <th>PPT USG</th>
                <th colspan="2">Mont Belvieu</th>
                <th colspan="2">CIF ARA</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($resultados as $fila)
                <tr>
                    <td style="border:1px solid #ddd;">{{ Util::dateFormat($fila->DAHI_FECHA, 'd/m/Y') }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_UKC_USG) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_PPT_USG) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_MONT_BELVIEU_1) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_MONT_BELVIEU_2) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_CIF_ARA_1) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_CIF_ARA_2) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <br />
        <table class="table table-hover col-xs-10 table-bordered" cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center; table-layout:fixed; width:100%; word-wrap:break-word">
            <thead style="background-color:#f5f5f5;">
              <tr>
                <th>Fecha</th>
                <th>TC Dólar</th>
                <th>TC Publicado</th>
                <th>Líbor</th>
                <th>UTM</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($resultados as $fila)
                <tr>
                    <td style="border:1px solid #ddd;">{{ Util::dateFormat($fila->DAHI_FECHA, 'd/m/Y') }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_TC_DOLAR) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_TC_PUBLICADO) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_LIBOR) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_UTM) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <br />
        <table class="table table-hover col-xs-10 table-bordered" cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center; table-layout:fixed; width:100%; word-wrap:break-word">
            <thead style="background-color:#f5f5f5;">
              <tr>
                <th>Fecha</th>
                <th>Tarifa diaria 82000</th>
                <th>Tarifa diaria 59000</th>
                <th>IFO 380 Houston</th>
                <th>MDO Houston</th>
                <th>IFO 380 Cristobal</th>
                <th>MDO Cristobal</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($resultados as $fila)
                <tr>
                    <td style="border:1px solid #ddd;">{{ Util::dateFormat($fila->DAHI_FECHA, 'd/m/Y') }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_TARIFA_DIARIA_82000) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_TARIFA_DIARIA_59000) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_IFO_380_HOUSTON) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_MDO_HOUSTON) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_IFO_380_CRISTOBAL) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_MDO_CRISTOBAL) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <br />
        <table class="table table-hover col-xs-10 table-bordered" cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center; table-layout:fixed; width:100%; word-wrap:break-word">
            <thead style="background-color:#f5f5f5;">
              <tr>
                <th>Fecha</th>
                <th>Brent</th>
                <th>WTI</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($resultados as $fila)
                <tr>
                    <td style="border:1px solid #ddd;">{{ Util::dateFormat($fila->DAHI_FECHA, 'd/m/Y') }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_BRENT) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_WTI) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <br />
        <table class="table table-hover col-xs-10 table-bordered" cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center; table-layout:fixed; width:100%; word-wrap:break-word">
            <thead style="background-color:#f5f5f5;">
              <tr>
                <th>Fecha</th>
                <th colspan='6'>Brent</th>
              </tr>
              <tr>
                <th></th>
                <th>m1</th>
                <th>m2</th>
                <th>m3</th>
                <th>m4</th>
                <th>m5</th>
                <th>m6</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($resultados as $fila)
                <tr>
                    <td style="border:1px solid #ddd;">{{ Util::dateFormat($fila->DAHI_FECHA, 'd/m/Y') }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_BRENT_M1) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_BRENT_M2) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_BRENT_M3) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_BRENT_M4) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_BRENT_M5) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_BRENT_M6) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <br />
        <table class="table table-hover col-xs-10 table-bordered" cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center; table-layout:fixed; width:100%; word-wrap:break-word">
            <thead style="background-color:#f5f5f5;">
              <tr>
                <th>Fecha</th>
                <th colspan='6'>WTI</th>
              </tr>
              <tr>
                <th></th>
                <th>m1</th>
                <th>m2</th>
                <th>m3</th>
                <th>m4</th>
                <th>m5</th>
                <th>m6</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($resultados as $fila)
                <tr>
                    <td style="border:1px solid #ddd;">{{ Util::dateFormat($fila->DAHI_FECHA, 'd/m/Y') }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_WTI_M1) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_WTI_M2) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_WTI_M3) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_WTI_M4) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_WTI_M5) }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($fila->DAHI_WTI_M6) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <br/>
    @endforeach
    <br/>
    <h4>Parámetros</h4>
    @foreach ($tabla as $fecha_vigencia=>$resultados)
        <h5>Fecha de vigencia: {{ $fecha_vigencia }}</h5>
        <table class="table table-hover col-xs-10 table-bordered" cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center; table-layout:fixed; width:100%; word-wrap:break-word">
            <thead style="background-color:#f5f5f5;">
                <tr>
                    <th>Combustible</th>
                    <th>n</th>
                    <th>m</th>
                    <th>s</th>
                    <th>t</th>
                    <th>f</th>
                </tr>
            </thead>
            <tbody>
            @if (count($resultados)>0)
                <tr>
                    <td style="border:1px solid #ddd;">Gasolina 93</td>
                    <td style="border:1px solid #ddd;">{{ $resultados[0]->DAHI_N_GAS_87 }}</td>
                    <td style="border:1px solid #ddd;">{{ $resultados[0]->DAHI_M_GAS_87 }}</td>
                    <td style="border:1px solid #ddd;">{{ $resultados[0]->DAHI_S_GAS_87 }}</td>
                    <td style="border:1px solid #ddd;">{{ $resultados[0]->DAHI_T_GAS_87 }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($resultados[0]->DAHI_F_GAS_87) }}</td>
                </tr>
                <tr>
                    <td style="border:1px solid #ddd;">Gasolina 97</td>
                    <td style="border:1px solid #ddd;">{{ $resultados[0]->DAHI_N_GAS_93 }}</td>
                    <td style="border:1px solid #ddd;">{{ $resultados[0]->DAHI_M_GAS_93 }}</td>
                    <td style="border:1px solid #ddd;">{{ $resultados[0]->DAHI_S_GAS_93 }}</td>
                    <td style="border:1px solid #ddd;">{{ $resultados[0]->DAHI_T_GAS_93 }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($resultados[0]->DAHI_F_GAS_93) }}</td>
                </tr>
                <tr>
                    <td style="border:1px solid #ddd;">Diesel</td>
                    <td style="border:1px solid #ddd;">{{ $resultados[0]->DAHI_N_DIESEL }}</td>
                    <td style="border:1px solid #ddd;">{{ $resultados[0]->DAHI_M_DIESEL }}</td>
                    <td style="border:1px solid #ddd;">{{ $resultados[0]->DAHI_S_DIESEL }}</td>
                    <td style="border:1px solid #ddd;">{{ $resultados[0]->DAHI_T_DIESEL }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($resultados[0]->DAHI_F_DIESEL) }}</td>
                </tr>
                <tr>
                    <td style="border:1px solid #ddd;">GLP</td>
                    <td style="border:1px solid #ddd;">{{ $resultados[0]->DAHI_N_GLP }}</td>
                    <td style="border:1px solid #ddd;">{{ $resultados[0]->DAHI_M_GLP }}</td>
                    <td style="border:1px solid #ddd;">{{ $resultados[0]->DAHI_S_GLP }}</td>
                    <td style="border:1px solid #ddd;">{{ $resultados[0]->DAHI_T_GLP }}</td>
                    <td style="border:1px solid #ddd;">{{ Util::formatNumber($resultados[0]->DAHI_F_GLP) }}</td>
                </tr>
            @endif
            </tbody>
        </table>
        <br/>
        @endforeach
<br/>
<br/>
<h5>Fecha del reporte: {!! date('d-m-Y') !!}</h5>
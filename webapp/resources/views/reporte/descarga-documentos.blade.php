@extends('public-layout')
@section('content')

        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <h4>Descarga de reportes</h4>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="col-md-8 col-md-offset-2 ">
                            <div id="mensaje"></div>
                        </div>
                        <div class="col-md-8 col-md-offset-2 " style="padding-top: 10px;padding-bottom: 10px">
                            <form class="form-horizontal" role="form" action="{{route('reporte/getDescargaDocumentos')}}" method="POST" id="form-descarga">
                                {!! csrf_field() !!}

                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="lblObligatorio">
                                    <div class="form-group required">
                                        <label class="col-md-3 control-label">Fecha vigencia desde: </label>
                                        <div class="col-md-2">
                                            <input type="text" name="fecha_vigencia_desde" id="fecha_vigencia_desde" class="form-control fecha-vigencia-all" style="width: 117px" placeholder="dd/mm/yyyy">
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-md-3 control-label">Fecha vigencia hasta: </label>
                                        <div class="col-md-2">
                                            <input type="text" name="fecha_vigencia_hasta" id="fecha_vigencia_hasta" class="form-control fecha-vigencia-all" style="width: 117px" placeholder="dd/mm/yyyy">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div id="btnDescargar" class="col-md-7 col-md-offset-5" style="margin-bottom:20px">
                            <a href="#" class="btn btn-primary"><i class="fa fa-download"></i> Descargar Documentación</a>
                        </div>
                  </div>
            </div>
        </div>
    </div>

@endsection                  

@section('script')

    <script type="text/javascript">

        activeMenuItem('descargaDocumentos', 'reportes');

        $.fn.bootstrapSwitch.defaults.size = 'small';
        $.fn.bootstrapSwitch.defaults.onText = 'BRENT';
        $.fn.bootstrapSwitch.defaults.offText = 'WTI';

        $("[name='tipo_crudo']").bootstrapSwitch({
          size   :'mini'
        });

        $('.bootstrap-switch-label').click(function() {
            $('#tipo_crudo').toggle();
        });

        $('#mensaje').html('{!! $mensaje !!}');

        $('#btnDescargar').click(function(){
            $('#form-descarga').submit();
        });

        // Hacer que la fecha mínima del fecha_vigencia_hasta sea como mínimo hasta
        // la del datepicker fecha_vigencia_desde
        $('#fecha_vigencia_desde').change(function (){
            minDate = $(this).datepicker('getDate');
            minDate = new Date(minDate.getTime());
            minDate.setDate(minDate.getDate());
            $('#fecha_vigencia_hasta').datepicker('option', 'minDate', minDate);
        });

        $(document).ready(function() {
            $('#fecha_vigencia_desde').datepicker('option', 'maxDate', '{{ $maxDate }}');

            $('#fecha_vigencia_hasta').datepicker('option', 'maxDate', '{{ $maxDate }}');
        });
        
    </script>
@endsection
@extends('public-layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-3 col-xs-3">
                                <h4>Datos Históricos</h4>
                            </div>
                            <div class="col-md-8 col-md-offset-1 " style="padding-top: 10px;padding-bottom: 10px">
                                <form class="form-inline pull-right" action="{{route('reporte/getDatosHistoricos')}}" method="POST" id="form-search">
                                    {!! csrf_field() !!}
                                    <div class="form-group" style="padding-right: 15px">
                                        <label for="fecha_vigencia_desde">Fecha vigencia desde</label>
                                        @if (isset($fechas))
                                            <input type="text" name="fecha_vigencia_desde" id="fecha_vigencia_desde" class="form-control fecha-vigencia-all" style="width: 117px" placeholder="dd/mm/yyyy" value={{ $fechas['fecha_vigencia_desde'] }}>
                                        @else
                                            <input type="text" name="fecha_vigencia_desde" id="fecha_vigencia_desde" class="form-control fecha-vigencia-all" style="width: 117px" placeholder="dd/mm/yyyy">
                                        @endif
                                    </div>
                                    <div class="form-group" style="padding-right: 10px">
                                        <label for="fecha_vigencia_hasta">Fecha vigencia hasta</label>
                                        @if (isset($fechas))
                                            <input type="text" name="fecha_vigencia_hasta" id="fecha_vigencia_hasta" class="form-control fecha-vigencia-all" style="width: 117px" placeholder="dd/mm/yyyy" value={{ $fechas['fecha_vigencia_hasta'] }}>
                                        @else
                                            <input type="text" name="fecha_vigencia_hasta" id="fecha_vigencia_hasta" class="form-control fecha-vigencia-all" style="width: 117px" placeholder="dd/mm/yyyy">
                                        @endif
                                    </div>
                                    <div class="form-group" style="padding-right: 10px">
                                        @if (isset($tipo_crudo) && $tipo_crudo == 'BRENT')
                                            <input type="checkbox" id="tipo_crudo" name="tipo_crudo" class="checkBrentWti" checked="true">
                                        @else
                                            <input type="checkbox" id="tipo_crudo" name="tipo_crudo" class="checkBrentWti">
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" id="btnBuscar" class="btn btn-primary loading-message">Buscar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-md-8 col-md-offset-2 " style="padding: 0">
                        <div id="mensaje"></div>
                    </div>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger col-md-8 col-md-offset-2 ">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div id="contenidoHistorico">
                        <div class="col-md-1 col-md-offset-11">
                            <div id="exportar"></div>
                            <br/>
                        </div>
                        <br/>
                        @foreach ($tabla as $combustible=>$fechas_vigencia)
                            @foreach ($fechas_vigencia as $fecha_vigencia=>$val)
                            <div style="min-width:100%; overflow-x:auto">
                                <table class="table table-hover table-bordered" cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center; table-layout:auto; display:table">
                                    <thead>
                                        @if ($combustible != Util::COMBUSTIBLE_GLP)
                                            <th colspan = "14">Fecha de Vigencia: {{ $fecha_vigencia }}, Combustible: {{ $val[0]->HIDR_NOMBRE_CHILE }}</th>
                                        @else
                                            <th colspan = "19">Fecha de Vigencia: {{ $fecha_vigencia }}, Combustible: {{ $val[0]->HIDR_NOMBRE_CHILE }}</th>
                                        @endif
                                        <tr>
                                            @if ($combustible != Util::COMBUSTIBLE_GLP)
                                                <th rowspan = "2">Fecha</th>
                                                <th colspan = "13">USG</th>
                                            @else
                                                <th rowspan = "2">Fecha</th>
                                                <th colspan = "18">USG</th>
                                            @endif
                                        </tr>
                                        <tr>
                                            <th>FOB</th>
                                            <th>FM</th>
                                            <th>SM</th>
                                            <th>CIF</th>
                                            <th>GCC</th>
                                            <th>MERM</th>
                                            <th>DA</th>
                                            <th>PPIBL</th>
                                            <th>IVA</th>
                                            <th>CF</th>
                                            <th>CD</th>
                                            <th>CA</th>
                                            <th>Paridad c_a</th>
                                            @if ($combustible== Util::COMBUSTIBLE_GLP)
                                                <th>Flete</th>
                                                <th>Arbitraje</th>
                                                <th>Netback US$/Ton</th>
                                                <th>Netback US$/Gal</th>
                                                <th>FOB arbitr.</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($val as $row)
                                            <tr> 
                                                <td> {{ $row->ESPA_FECHA }} </td>
                                                <td> {{ $row->ESPA_FOB }} </td>
                                                <td> {{ $row->ESPA_FM }} </td>
                                                <td> {{ $row->ESPA_SM }} </td>
                                                <td> {{ $row->ESPA_CIF }} </td>
                                                <td> {{ $row->ESPA_GCC }} </td>
                                                <td> {{ $row->ESPA_MERM }} </td>
                                                <td> {{ $row->ESPA_DA }} </td>
                                                <td> {{ $row->ESPA_PPIBL }} </td>
                                                <td> {{ $row->ESPA_IVA }} </td>
                                                <td> {{ $row->ESPA_CF }} </td>
                                                <td> {{ $row->ESPA_CD }} </td>
                                                <td> {{ $row->ESPA_CA }} </td>
                                                <td> {{ $row->ESPA_PARIDAD_CA }} </td>
                                                @if ($combustible == Util::COMBUSTIBLE_GLP)
                                                    <td> {{ $row->ESPA_FLETE }} </td>
                                                    <td> {{ $row->ESPA_ARBITRAJE }} </td>
                                                    <td> {{ $row->ESPA_NETBACK_US_TON }} </td>
                                                    <td> {{ $row->ESPA_NETBACK_US_GAL }} </td>
                                                    <td> {{ $row->ESPA_FOB_GLP_ARBITRADO }} </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td>{{ $crudos[$fecha_vigencia][$combustible]['CRUDO'][0] }}</td>
                                            <td>{{ $crudos[$fecha_vigencia][$combustible]['DATOS_CRUDO']['HISTORICO'] }}</td>
                                            <td>{{ $crudos[$fecha_vigencia][$combustible]['CRUDO'][1] }}</td>
                                            <td>{{ $crudos[$fecha_vigencia][$combustible]['DATOS_CRUDO']['FUTURO'] }}</td>
                                            <td>{{ $crudos[$fecha_vigencia][$combustible]['CRUDO'][2] }}</td>
                                            <td>{{ $crudos[$fecha_vigencia][$combustible]['DATOS_CRUDO']['PONDERADO'] }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

<div id="hidden">
    <form id="exportarPdf" action="{{route('reporte/exportarAPdf')}}" method="POST">
        {!! csrf_field() !!}
        <input type="hidden" id="tablaPdf" name="tabla">
        <input type="hidden" id="crudosPdf" name="crudos">
        <input type="hidden" id="template" name="template">
    </form>
    <form id="exportarExcel" action="{{route('reporte/exportarAExcelDatosHistoricos')}}" method="POST" >
        {!! csrf_field() !!}
        <input type="hidden" id="tablaExcel" name="tabla">
        <input type="hidden" id="crudosExcel" name="crudos">
        <input type="hidden" id="nombreReporte" name="nombreReporte">
        <input type="hidden" id="vista" name="vista">
    </form>
</div>

@endsection                  

@section('script')

    <script type="text/javascript">

        $.fn.bootstrapSwitch.defaults.size = 'small';
        $.fn.bootstrapSwitch.defaults.onText = 'BRENT';
        $.fn.bootstrapSwitch.defaults.offText = 'WTI';
        $("[name='tipo_crudo']").bootstrapSwitch({
          size   :'mini'
        });
        
        $('.bootstrap-switch-label').click(function() {
            $('#tipo_crudo').toggle();
        });

        if ('{{ $mensaje }}'!='') {
            $('#contenidoHistorico').hide();
            $('#mensaje').html('{!! $mensaje !!}');
        }

        $('#fecha_vigencia_desde').change(function (){
            minDate = $(this).datepicker('getDate');
            minDate = new Date(minDate.getTime());
            minDate.setDate(minDate.getDate());
            $('#fecha_vigencia_hasta').datepicker('option', 'minDate', minDate);
        });
        
        $(document).ready(function() {
            activeMenuItem('datosHistoricos', 'reportes');

            tabla   = JSON.stringify({!! json_encode($tabla) !!});
            crudos  = JSON.stringify({!! json_encode($crudos) !!});

            $('#fecha_vigencia_desde').datepicker('option', 'maxDate', '{{ $maxDate }}');
            $('#fecha_vigencia_hasta').datepicker('option', 'maxDate', '{{ $maxDate }}');
            
            // Asignar a los botones 'Exportar' unas funciones de AJAX para poder obtener el contenido filtrado
            $('#exportar').append('<a title="Exportar a PDF" class="datatable-link" id="exportarLogPdf" href="#"><i class="fa fa-file-pdf-o fa-2x"></i></a><a title="Exportar a Excel" class="datatable-link" style="margin-left:7px" id="exportarLogExcel" href="#"><i class="fa fa-file-excel-o fa-2x"></i></a>');

            // Funciones para los botones 'Exportar'
            $('#exportarLogPdf').click(function(){
                $('#tablaPdf').val(tabla);
                $('#crudosPdf').val(crudos);
                $('#template').val('reporte.datosHistoricosTemplatePdf');
                $('#exportarPdf').submit();
            });
            $('#exportarLogExcel').click(function(){
                $('#tablaExcel').val(tabla);
                $('#crudosExcel').val(crudos);
                $('#nombreReporte').val('Reporte de Datos Históricos');
                $('#vista').val('reporte.datosHistoricosTemplatePdf');
                $('#exportarExcel').submit();
            });
        });

    </script>
@endsection
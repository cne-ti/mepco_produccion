@extends('public-layout')
@section('content')

        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <h4>Log de acciones</h4>
                                </div>
                                <div class="col-md-8 col-md-offset-1 " style="padding-top: 10px;padding-bottom: 10px">
                                    <form class="form-inline pull-right" action="{{route('reporte/getLog')}}" method="POST" id="form-search">
                                        {!! csrf_field() !!}
                                        <div class="form-group" style="padding-right: 15px">
                                            <label for="fecha_vigencia_desde">Fecha vigencia desde</label>
                                            @if (isset($fechas))
                                                <input type="text" name="fecha_vigencia_desde" id="fecha_vigencia_desde" class="form-control fecha-vigencia-all" style="width: 117px" placeholder="dd/mm/yyyy" value={{ $fechas['fecha_vigencia_desde'] }}>
                                            @else
                                                <input type="text" name="fecha_vigencia_desde" id="fecha_vigencia_desde" class="form-control fecha-vigencia-all" style="width: 117px" placeholder="dd/mm/yyyy">
                                            @endif
                                        </div>
                                        <div class="form-group" style="padding-right: 10px">
                                            <label for="fecha_vigencia_hasta">Fecha vigencia hasta</label>
                                            @if (isset($fechas))
                                                <input type="text" name="fecha_vigencia_hasta" id="fecha_vigencia_hasta" class="form-control fecha-vigencia-all" style="width: 117px" placeholder="dd/mm/yyyy" value={{ $fechas['fecha_vigencia_hasta'] }}>
                                            @else
                                                <input type="text" name="fecha_vigencia_hasta" id="fecha_vigencia_hasta" class="form-control fecha-vigencia-all" style="width: 117px" placeholder="dd/mm/yyyy">
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" id="btnBuscar" class="btn btn-primary">Buscar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body" id="tablaLog">
                        <div class="col-md-8 col-md-offset-2 " style="padding: 0">
                            <div id="mensaje"></div>
                        </div>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger col-md-8 col-md-offset-2 ">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div id="contenidoLog">
                            <table class = 'table table-hover table-bordered dataTable' style="table-layout:auto; overflow-x:scroll; max-width:100%; display:block">
                                <thead>
                                  <tr>
                                    <th rowspan='2'>Fecha de Vigencia</th>
                                    <th colspan='8'>Acciones</th>
                                  </tr>
                                  <tr>
                                    <th>Carga</th>
                                    <th>Fecha Carga</th>
                                    <th>Validación Carga</th>
                                    <th>Fecha validación Carga</th>
                                    <th>Cálculo</th>
                                    <th>Fecha Cálculo</th>
                                    <th>Validación Cálculo</th>
                                    <th>Fecha validación Cálculo</th>
                                  </tr>
                                </thead>
                                @foreach ($tabla as $fecha_vigencia=>$fila)
                                    <tr>   
                                        <td>{{ $fecha_vigencia }}</td>
                                        <td>{{ $fila['VAUS_EMAIL_CREACION'] }}</td>
                                        <td>{{ $fila['VAUS_FECHA_CREACION'] }}</td>
                                        <td>{{ $fila['VAUS_EMAIL_VALIDADO'] }}</td>
                                        <td>{{ $fila['VAUS_FECHA_VALIDADO'] }}</td>
                                        <td>{{ $fila['USUA_EMAIL_CREACION'] }}</td>
                                        <td>{{ $fila['ESCE_FECHA_CREACION'] }}</td>
                                        <td>{{ $fila['USUA_EMAIL_VALIDADO'] }}</td>
                                        <td>{{ $fila['ESCE_FECHA_VALIDADO'] }}</td>
                                    </tr>
                                @endforeach
                              </table>
                            </div>
                  </div>
            </div>
        </div>
    </div>

    <div id="hidden">
        <form id="exportarPdf" action="{{route('reporte/exportarAPdf')}}" method="POST">
            {!! csrf_field() !!}
            <input type="hidden" id="tablaPdf" name="tabla">
            <input type="hidden" id="template" name="template">
        </form>
        <form id="exportarExcel" action="{{route('reporte/exportarAExcel')}}" method="POST" >
            {!! csrf_field() !!}
            <input type="hidden" id="tablaExcel" name="tabla">
            <input type="hidden" id="nombreReporte" name="nombreReporte">
            <input type="hidden" id="vista" name="vista">
        </form>
    </div>

@endsection                  

@section('script')

    <script type="text/javascript">

        $(document).ready(function() {
            activeMenuItem('log', 'reportes');
            tabla = JSON.stringify({!! json_encode($tabla) !!});

            $('#fecha_vigencia_desde').datepicker('option', 'maxDate', '{{ $maxDate }}');
            $('#fecha_vigencia_hasta').datepicker('option', 'maxDate', '{{ $maxDate }}');

            // Asignar a los botones 'Exportar' unas funciones de AJAX para poder obtener el contenido filtrado
            $('.btn-exportar').append('<a title="Exportar a PDF" class="datatable-link" id="exportarLogPdf" href="#"><i class="fa fa-file-pdf-o fa-2x"></i></a><a title="Exportar a Excel" class="datatable-link" style="margin-left:7px" id="exportarLogExcel" href="#"><i class="fa fa-file-excel-o fa-2x"></i></a>');

            // Funciones para los botones 'Exportar'
            $('#exportarLogPdf').click(function(){
                $('#tablaPdf').val(tabla);
                $('#template').val('reporte.logTemplatePdf');
                $('#exportarPdf').submit();
            });
            $('#exportarLogExcel').click(function(){
                $('#tablaExcel').val(tabla);
                $('#nombreReporte').val('Log de Acciones');
                $('#vista').val('reporte.logTemplatePdf');
                $('#exportarExcel').submit();
            });
        });

        // Ocultar el formulario oculto
        $('#hidden').hide();

        if ('{{ $mensaje }}'!='') {
            $('#contenidoLog').hide();
            $('#mensaje').html('{!! $mensaje !!}');
        }

        $('#fecha_vigencia_desde').change(function (){
            minDate = $(this).datepicker('getDate');
            minDate = new Date(minDate.getTime());
            minDate.setDate(minDate.getDate());
            $('#fecha_vigencia_hasta').datepicker('option', 'minDate', minDate);
        });
        
    </script>
@endsection
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<img src='images/Logo_Oficinal_CNE.png'>
<br/>

<h3>Reporte de Datos de Entrada</h3>

@foreach ($tabla as $fecha_vigencia=>$resultados)
    <table class="table table-hover col-xs-10 table-bordered" cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center; table-layout:fixed; width:100%; word-wrap:break-word">
        <thead style="background-color:#f5f5f5;">
            <tr>
                <th colspan = '12'>Fecha de vigencia: {{ $fecha_vigencia }}</th>
             </tr>
            <tr>
                <th>Combustible</th>
                <th>n</th>
                <th>m</th>
                <th>s</th>
                <th>t</th>
                <th>Peso Fut.</th>
                <th>Paridad Anterior $/m3</th>
                <th>PRI Anterior $/m3</th>
                <th>PRinf $/m3</th>
                <th>PRI $/m3</th>
                <th>PRS $/m3</th>
                <th>Paridad $/m3</th>
            </tr>
        </thead>
        <tbody>
            @foreach($resultados as $fila)
                <tr>
                    <td style="border:1px solid #ddd;">{{ $fila->COMBUSTIBLE }}</td>
                    <td style="border:1px solid #ddd;">{{ $fila->N }}</td>
                    <td style="border:1px solid #ddd;">{{ $fila->M }}</td>
                    <td style="border:1px solid #ddd;">{{ $fila->S }}</td>
                    <td style="border:1px solid #ddd;">{{ $fila->T }}</td>
                    <td style="border:1px solid #ddd;">{{ $fila->PESO_FUT }}</td>
                    <td style="border:1px solid #ddd;">{{ $fila->PARIDAD_ANTERIOR }}</td>
                    <td style="border:1px solid #ddd;">{{ $fila->PRI_ANTERIOR }}</td>
                    <td style="border:1px solid #ddd;">{{ $fila->PRINF }}</td>
                    <td style="border:1px solid #ddd;">{{ $fila->PRI }}</td>
                    <td style="border:1px solid #ddd;">{{ $fila->PRS }}</td>
                    <td style="border:1px solid #ddd;">{{ $fila->PARIDAD }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endforeach

<br/>
<br/>
<h5>Fecha del reporte: {!! date('d-m-Y') !!}</h5>

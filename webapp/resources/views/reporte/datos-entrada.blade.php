@extends('public-layout')
@section('content')
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-3 col-xs-3">
                            <h4>Datos de Entrada</h4>
                        </div>
                        <div class="col-md-8 col-md-offset-1 " style="padding-top: 10px;padding-bottom: 10px">
                            <form class="form-inline pull-right" action="{{route('reporte/getDatosEntrada')}}" method="POST" id="form-search">
                                {!! csrf_field() !!}
                                <div class="form-group" style="padding-right: 15px">
                                    <label for="fecha_vigencia_desde">Fecha vigencia desde</label>
                                    @if (isset($fechas))
                                        <input type="text" name="fecha_vigencia_desde" id="fecha_vigencia_desde" class="form-control fecha-vigencia-all" style="width: 117px" placeholder="dd/mm/yyyy" value={{ $fechas['fecha_vigencia_desde'] }}>
                                    @else
                                        <input type="text" name="fecha_vigencia_desde" id="fecha_vigencia_desde" class="form-control fecha-vigencia-all" style="width: 117px" placeholder="dd/mm/yyyy">
                                    @endif
                                </div>
                                <div class="form-group" style="padding-right: 10px">
                                    <label for="fecha_vigencia_hasta">Fecha vigencia hasta</label>
                                    @if (isset($fechas))
                                        <input type="text" name="fecha_vigencia_hasta" id="fecha_vigencia_hasta" class="form-control fecha-vigencia-all" style="width: 117px" placeholder="dd/mm/yyyy" value={{ $fechas['fecha_vigencia_hasta'] }}>
                                    @else
                                        <input type="text" name="fecha_vigencia_hasta" id="fecha_vigencia_hasta" class="form-control fecha-vigencia-all" style="width: 117px" placeholder="dd/mm/yyyy">
                                    @endif
                                </div>
                                <div class="form-group" style="margin-right:15px;">
                                    <button type="submit" id="btnBuscar" class="btn btn-primary">Buscar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-md-8 col-md-offset-2 " style="padding: 0">
                    <div id="mensaje"></div>
                </div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger col-md-8 col-md-offset-2 ">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div id="datos-entrada">
                    <ul class="nav nav-tabs nav-justified">
                        <div class="col-md-1 col-md-offset-11">
                            <div id="exportar"></div>
                            <br/>
                        </div>
                        <li class="active">
                            <a href="#valoresMercado" data-toggle="tab"><i class="fa fa-file-text fa-2x"></i><br>VALORES DE MERCADO</a>
                        </li>
                        <li>
                            <a href="#parametros" data-toggle="tab"><i class="fa fa-calculator fa-2x"></i><br>PARÁMETROS</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="valoresMercado" class="tab-pane active">
                            @foreach ($tabla as $fecha_vigencia=>$resultados) 
                                @if (count($resultados) > 0) 
                                    <h4>Fecha de vigencia: {{ $fecha_vigencia }}</h4>
                                    <table id="tbValoresMercado" class="table table-hover table-bordered" style="table-layout:auto; overflow-x:scroll; max-width:100%; display:block">
                                        <thead>
                                          <tr>
                                            <th rowspan='2'>Fecha</th>
                                            <th rowspan="2">Gas Unl 87</th>
                                            <th rowspan="2">Gas Unl 93</th>
                                            <th rowspan="2">Ls Diésel</th>
                                            <th rowspan="2">USG Butano</th>
                                            <th rowspan="2">UKC USG</th>
                                            <th rowspan="2">PPT USG</th>
                                            <th colspan='2' rowspan="2">Mont Belvieu</th>
                                            <th colspan='2' rowspan="2">CIF ARA</th>
                                            <th rowspan='2'>TC Dólar</th>
                                            <th rowspan='2'>TC Publicado</th>
                                            <th rowspan='2'>Líbor</th>
                                            <th rowspan="2">UTM</th>
                                            <th rowspan='2'>Tarifa diaria 82000</th>
                                            <th rowspan='2'>Tarifa diaria 59000</th>
                                            <th rowspan='2'>IFO 380 Houston</th>
                                            <th rowspan='2'>MDO Houston</th>
                                            <th rowspan='2'>IFO 380 Cristobal</th>
                                            <th rowspan='2'>MDO Cristobal</th>
                                            <th rowspan='2'>Brent</th>
                                            <th colspan='6'>Brent</th>
                                            <th rowspan='2'>WTI</th>
                                            <th colspan='6'>WTI</th>
                                          </tr>
                                          <tr>
                                            <th>m1</th>
                                            <th>m2</th>
                                            <th>m3</th>
                                            <th>m4</th>
                                            <th>m5</th>
                                            <th>m6</th>
                                            <th>m1</th>
                                            <th>m2</th>
                                            <th>m3</th>
                                            <th>m4</th>
                                            <th>m5</th>
                                            <th>m6</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($resultados as $fila)
                                            <tr>
                                                <td>{{ Util::dateFormat($fila->DAHI_FECHA, 'd/m/Y') }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_GAS_UNL_87) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_GAS_UNL_93) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_LD_DIESEL) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_USG_BUTANO) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_UKC_USG) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_PPT_USG) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_MONT_BELVIEU_1) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_MONT_BELVIEU_2) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_CIF_ARA_1) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_CIF_ARA_2) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_TC_DOLAR) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_TC_PUBLICADO) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_LIBOR) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_UTM) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_TARIFA_DIARIA_82000) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_TARIFA_DIARIA_59000) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_IFO_380_HOUSTON) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_MDO_HOUSTON) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_IFO_380_CRISTOBAL) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_MDO_CRISTOBAL) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_BRENT) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_BRENT_M1) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_BRENT_M2) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_BRENT_M3) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_BRENT_M4) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_BRENT_M5) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_BRENT_M6) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_WTI) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_WTI_M1) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_WTI_M2) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_WTI_M3) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_WTI_M4) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_WTI_M5) }}</td>
                                                <td>{{ Util::formatNumber($fila->DAHI_WTI_M6) }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @endif
                            @endforeach
                        </div>
                        <div id="parametros" class="tab-pane">
                        @foreach ($tabla as $fecha_vigencia=>$resultados)
                            @if (count($resultados) > 0) 
                                <h4>Fecha de vigencia: {{ $fecha_vigencia }}</h4>
                                <table class="table table-hover table-bordered" id="tbParametros">
                                    <thead>
                                        <tr>
                                            <th>Combustible</th>
                                            <th>n</th>
                                            <th>m</th>
                                            <th>s</th>
                                            <th>t</th>
                                            <th>f</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if (count($resultados)>0)
                                        <tr>
                                            <td>Gasolina 93</td>
                                            <td>{{ $resultados[0]->DAHI_N_GAS_87 }}</td>
                                            <td>{{ $resultados[0]->DAHI_M_GAS_87 }}</td>
                                            <td>{{ $resultados[0]->DAHI_S_GAS_87 }}</td>
                                            <td>{{ $resultados[0]->DAHI_T_GAS_87 }}</td>
                                            <td>{{ Util::formatNumber($resultados[0]->DAHI_F_GAS_87) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Gasolina 97</td>
                                            <td>{{ $resultados[0]->DAHI_N_GAS_93 }}</td>
                                            <td>{{ $resultados[0]->DAHI_M_GAS_93 }}</td>
                                            <td>{{ $resultados[0]->DAHI_S_GAS_93 }}</td>
                                            <td>{{ $resultados[0]->DAHI_T_GAS_93 }}</td>
                                            <td>{{ Util::formatNumber($resultados[0]->DAHI_F_GAS_93) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Diesel</td>
                                            <td>{{ $resultados[0]->DAHI_N_DIESEL }}</td>
                                            <td>{{ $resultados[0]->DAHI_M_DIESEL }}</td>
                                            <td>{{ $resultados[0]->DAHI_S_DIESEL }}</td>
                                            <td>{{ $resultados[0]->DAHI_T_DIESEL }}</td>
                                            <td>{{ Util::formatNumber($resultados[0]->DAHI_F_DIESEL) }}</td>
                                        </tr>
                                        <tr>
                                            <td>GLP</td>
                                            <td>{{ $resultados[0]->DAHI_N_GLP }}</td>
                                            <td>{{ $resultados[0]->DAHI_M_GLP }}</td>
                                            <td>{{ $resultados[0]->DAHI_S_GLP }}</td>
                                            <td>{{ $resultados[0]->DAHI_T_GLP }}</td>
                                            <td>{{ Util::formatNumber($resultados[0]->DAHI_F_GLP) }}</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="hidden">
    <form id="exportarPdf" action="{{route('reporte/exportarAPdf')}}" method="POST">
        {!! csrf_field() !!}
        <input type="hidden" id="tablaPdf" name="tabla">
        <input type="hidden" id="template" name="template">
    </form>
    <form id="exportarExcel" action="{{route('reporte/exportarAExcel')}}" method="POST" >
        {!! csrf_field() !!}
        <input type="hidden" id="tablaExcel" name="tabla">
        <input type="hidden" id="nombreReporte" name="nombreReporte">
        <input type="hidden" id="vista" name="vista">
    </form>
</div>

@endsection                  
@section('script')

<script type="text/javascript">
    activeMenuItem('datosEntrada', 'reportes');
    
    $.fn.bootstrapSwitch.defaults.size = 'small';
    $.fn.bootstrapSwitch.defaults.onText = 'BRENT';
    $.fn.bootstrapSwitch.defaults.offText = 'WTI';
    
    $("[name='tipo_crudo']").bootstrapSwitch({
      size   :'mini'
    });
    
    $('.bootstrap-switch-label').click(function() {
        $('#tipo_crudo').toggle();
    });

    if ('{{ $mensaje }}'!='') {
        $('#datos-entrada').hide();
        $('#mensaje').html('{!! $mensaje !!}');
    }

    // Ocultar el formulario oculto
    $('#hidden').hide();

    $('#fecha_vigencia_desde').change(function (){
        minDate = $(this).datepicker('getDate');
        minDate = new Date(minDate.getTime());
        minDate.setDate(minDate.getDate());
        $('#fecha_vigencia_hasta').datepicker('option', 'minDate', minDate);
    });

    $(document).ready(function() {
        $('.mercado').DataTable({
            'sScrollX' : '100%',
            'iDisplayLength': 10,
            'dom': '<"col-md-6 hidden-xs datatable-no-padding-left"l><"col-md-2 col-xs-2 btn-exportar datatable-float-right datatable-no-padding-right"><"col-md-4 col-xs-4 datatable-no-padding-right"f>rt<"clear"><"col-md-7 hidden-xs datatable-no-padding-left"i><"col-md-5 col-xs-12 datatable-no-padding-right"p>',
            language: {
                    processing:     "Buscando...",
                    search:         "Buscar",
                    lengthMenu:     "Mostrar _MENU_ registros",
                    info:           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    infoEmpty:      "No se encontraron registros",
                    infoFiltered:   " (De un total de _MAX_ registros)",
                    infoPostFix:    "",
                    loadingRecords: "Cargando vista...",
                    zeroRecords:    "No se encontraron registros",
                    emptyTable:     "No se encontraron registros",
                    paginate: {
                        first:      "Primer",
                        previous:   "Anterior",
                        next:       "Siguiente",
                        last:       "&Uacute;ltimo"
                }
            }
        });

        tabla = JSON.stringify({!! json_encode($tabla) !!});
        // Asignar a los botones 'Exportar' unas funciones de AJAX para poder obtener el contenido filtrado
        $('#exportar').append('<a title="Exportar a PDF" class="datatable-link" id="exportarLogPdf" href="#"><i class="fa fa-file-pdf-o fa-2x"></i></a><a title="Exportar a Excel" class="datatable-link" style="margin-left:7px" id="exportarLogExcel" href="#"><i class="fa fa-file-excel-o fa-2x"></i></a>');

        $('#fecha_vigencia_desde').datepicker('option', 'maxDate', '{{ $maxDate }}');
        $('#fecha_vigencia_hasta').datepicker('option', 'maxDate', '{{ $maxDate }}');

        // Funciones para los botones 'Exportar'
        $('#exportarLogPdf').click(function(){
            $('#tablaPdf').val(tabla);
            $('#template').val('reporte.datosEntradaTemplatePdf');
            $('#exportarPdf').submit();
        });
        $('#exportarLogExcel').click(function(){
            $('#tablaExcel').val(tabla);
            $('#nombreReporte').val('Reporte de Datos de Entrada');
            $('#vista').val('reporte.datosEntradaTemplatePdf');
            $('#exportarExcel').submit();
        });

    });
</script>
@endsection
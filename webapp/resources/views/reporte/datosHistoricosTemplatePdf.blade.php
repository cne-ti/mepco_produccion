<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<img src='images/Logo_Oficinal_CNE.png'>
<br/>

<h3>Reporte de Datos Históricos</h3>

    @foreach ($tabla as $combustible => $fechas_vigencia)
        @foreach ($fechas_vigencia as $fecha_vigencia => $val)
            <table class="table">
                <thead style="background-color:#f5f5f5;">
                    <tr>
                        @if ($combustible != Util::COMBUSTIBLE_GLP)
                            <th colspan="14">Fecha de Vigencia: {{ $fecha_vigencia }}, Combustible: {{ $val[0]->HIDR_NOMBRE_CHILE }}</th>
                        @else
                            <th colspan="19">Fecha de Vigencia: {{ $fecha_vigencia }}, Combustible: {{ $val[0]->HIDR_NOMBRE_CHILE }}</th>
                        @endif
                    </tr>
                    <tr>
                        @if ($combustible != Util::COMBUSTIBLE_GLP)
                            <th rowspan="2">Fecha</th>
                            <th colspan="14">USG</th>
                        @else
                            <th rowspan="2">Fecha</th>
                            <th colspan="18">USG</th>
                        @endif
                    </tr>
                    <tr>
                        <th>FOB</th>
                        <th>FM</th>
                        <th>SM</th>
                        <th>CIF</th>
                        <th>GCC</th>
                        <th>MERM</th>
                        <th>DA</th>
                        <th>PPIBL</th>
                        <th>IVA</th>
                        <th>CF</th>
                        <th>CD</th>
                        <th>CA</th>
                        <th>Paridad c_a</th>
                        @if ($combustible== Util::COMBUSTIBLE_GLP)
                            <th>Flete</th>
                            <th>Arbitraje</th>
                            <th>Netback US$/Ton</th>
                            <th>Netback US$/Gal</th>
                            <th>FOB arbitrado</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach($val as $row)
                        <tr> 
                            <td style="border:1px solid #ddd;"> {{ $row->ESPA_FECHA }} </td>
                            <td style="border:1px solid #ddd;"> {{ $row->ESPA_FOB }} </td>
                            <td style="border:1px solid #ddd;"> {{ $row->ESPA_FM }} </td>
                            <td style="border:1px solid #ddd;"> {{ $row->ESPA_SM }} </td>
                            <td style="border:1px solid #ddd;"> {{ $row->ESPA_CIF }} </td>
                            <td style="border:1px solid #ddd;"> {{ $row->ESPA_GCC }} </td>
                            <td style="border:1px solid #ddd;"> {{ $row->ESPA_MERM }} </td>
                            <td style="border:1px solid #ddd;"> {{ $row->ESPA_DA }} </td>
                            <td style="border:1px solid #ddd;"> {{ $row->ESPA_PPIBL }} </td>
                            <td style="border:1px solid #ddd;"> {{ $row->ESPA_IVA }} </td>
                            <td style="border:1px solid #ddd;"> {{ $row->ESPA_CF }} </td>
                            <td style="border:1px solid #ddd;"> {{ $row->ESPA_CD }} </td>
                            <td style="border:1px solid #ddd;"> {{ $row->ESPA_CA }} </td>
                            <td style="border:1px solid #ddd;"> {{ $row->ESPA_PARIDAD_CA }} </td>
                            @if ($combustible == Util::COMBUSTIBLE_GLP)
                                <td style="border:1px solid #ddd;"> {{ $row->ESPA_FLETE }} </td>
                                <td style="border:1px solid #ddd;"> {{ $row->ESPA_ARBITRAJE }} </td>
                                <td style="border:1px solid #ddd;"> {{ $row->ESPA_NETBACK_US_TON }} </td>
                                <td style="border:1px solid #ddd;"> {{ $row->ESPA_NETBACK_US_GAL }} </td>
                                <td style="border:1px solid #ddd;"> {{ $row->ESPA_FOB_GLP_ARBITRADO }} </td>
                            @endif
                        </tr>
                    @endforeach
                    <tr>
                        <td>{{ $crudos->{$fecha_vigencia}->{$combustible}->CRUDO[0] }}</td>
                        <td>{{ $crudos->{$fecha_vigencia}->{$combustible}->DATOS_CRUDO->HISTORICO }}</td>
                        <td>{{ $crudos->{$fecha_vigencia}->{$combustible}->CRUDO[1] }}</td>
                        <td>{{ $crudos->{$fecha_vigencia}->{$combustible}->DATOS_CRUDO->FUTURO }}</td>
                        <td>{{ $crudos->{$fecha_vigencia}->{$combustible}->CRUDO[2] }}</td>
                        <td>{{ $crudos->{$fecha_vigencia}->{$combustible}->DATOS_CRUDO->PONDERADO }}</td>
                    </tr>
                </tbody>
            </table>
            <br/>
        @endforeach
        <br/>
        <hr>
    @endforeach

<br/>
<br/>
<h5>Fecha del reporte: {!! date('d-m-Y') !!}</h5>

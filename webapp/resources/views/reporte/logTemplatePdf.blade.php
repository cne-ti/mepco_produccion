<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<img src='images/Logo_Oficinal_CNE.png'>
<br/>

<h3>Log de Acciones</h3>
<table class="table table-hover col-xs-10 table-bordered" id="log" cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center; table-layout:fixed; width:100%; word-wrap:break-word">
    <thead style="background-color:#f5f5f5;">
        <tr>
          <th>Fecha de Vigencia</th>
          <th>Carga</th>
          <th>Fecha Carga</th>
          <th>Validación Carga</th>
          <th>Fecha validación Carga</th>
          <th>Cálculo</th>
          <th>Fecha Cálculo</th>
          <th>Validación Cálculo</th>
          <th>Fecha validación Cálculo</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($tabla as $fecha_vigencia=>$fila)
            <tr>
                <td style="border:1px solid #ddd;">{{ $fecha_vigencia }}</td>
                <td style="border:1px solid #ddd;">{{ $fila->VAUS_EMAIL_CREACION }}</td>
                <td style="border:1px solid #ddd;">{{ $fila->VAUS_FECHA_CREACION }}</td>
                <td style="border:1px solid #ddd;">{{ $fila->VAUS_EMAIL_VALIDADO }}</td>
                <td style="border:1px solid #ddd;">{{ $fila->VAUS_FECHA_VALIDADO }}</td>
                <td style="border:1px solid #ddd;">{{ $fila->USUA_EMAIL_CREACION }}</td>
                <td style="border:1px solid #ddd;">{{ $fila->ESCE_FECHA_CREACION }}</td>
                <td style="border:1px solid #ddd;">{{ $fila->USUA_EMAIL_VALIDADO }}</td>
                <td style="border:1px solid #ddd;">{{ $fila->ESCE_FECHA_VALIDADO }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

<br/>
<br/>
<h5>Fecha del reporte: {!! date('d-m-Y') !!}</h5>

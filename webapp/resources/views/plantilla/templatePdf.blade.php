<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<img src='images/Logo_Oficinal_CNE.png'>
<br/>

<h3>Listado de Plantillas</h3>
<table class="table table-hover col-xs-10 table-bordered" cellpadding="10" style="border-collapse:collapse; border:1px solid #ddd; text-align:center">
    <thead style="background-color:#f5f5f5;">
        <tr>
            <th>
                Identificador
            </th>
            <th>
                Nombre de la plantilla
            </th>
            <th>
                Descripción
            </th>
            <th>
                Nombre del documento
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($plantillas as $plantilla)
            <tr>
                <td style="border:1px solid #ddd;">{{ $plantilla->PLDO_ID }}</td>
                <td style="border:1px solid #ddd;">{{ $plantilla->PLDO_NOMBRE }}</td>
                <td style="border:1px solid #ddd;">{{ $plantilla->PLDO_DESCRIPCION }}</td>
                <td style="border:1px solid #ddd;">{{ $plantilla->PLDO_NOMBRE_ARCHIVO }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

<br/>
<br/>
<h5>Fecha del reporte: {!! date('d-m-Y') !!}</h5>

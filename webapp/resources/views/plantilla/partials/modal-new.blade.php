<div class="modal fade" id="modalNew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Nueva Plantilla</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-new" method="POST" enctype="multipart/form-data">
                    <!-- Añadir campos -->
                    @include('plantilla.partials.fields')
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btnGuardar" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#modalNew').on('show.bs.modal', function () {
        $('#form-new #errors').remove();
        $('#form-new #pldo_nombre').val('');
        $('#form-new #pldo_descripcion').val('');
        $('#form-new')[0].reset();
        $('.numChar').text('');
        $('#form-new #pldo_descripcion').on('keyup', numChar);
    });

    $('#modalNew #btnGuardar').click(function(){
        var formData = new FormData(document.getElementById("form-new")); 
        $.ajax({
            url: 'plantillaDocumento',
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data) {
                loadingMessageHide();
                if(data.status == 'success'){
                    $('#form-new #errors').remove();
                    $('#modalNew').modal('hide');
                    location.reload();
                }else{
                    errors = data.errors;
                    $('#form-new #errors').remove();
                    var append = '';
                    for (key in errors) {
                        if (errors.hasOwnProperty(key)) {
                            append = append+'<li>'+errors[key]+'</li>';
                        }
                    }
                    $('#form-new').prepend('<div id="errors" class="alert alert-danger"><ul>'+append+'</ul></div>');
                }
            },
            error: function(e) {
                console.log(e.message);
            }
        });
    });

  </script>
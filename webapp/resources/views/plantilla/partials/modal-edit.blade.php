<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar Plantilla</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-edit">
                    <!-- Identificador de la plantilla -->
                    <input type="hidden" name="pldo_id" id="pldo_id">
                    <!-- Errores en formulario -->
                    @include('partials.error')
                    <!-- Añadir campos -->
                    @include('plantilla.partials.fields')
                    <div class="form-group">
                        <label class="col-md-10 control-label" id="nombre_documento"></label>
                    </div>
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary loading-message" id="btnGuardar">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#modalEdit').on('show.bs.modal', function (e) {
        $.ajax({
            url: 'plantillaDocumento/' + $(e.relatedTarget).attr('data-id')+'/edit',
            type: 'GET',
            data: { 
                '_token' : $('#_token').val() 
            },
            success: function(data) {
                loadingMessageHide();
                if(data.status == "success"){
                    var plantilla = data.plantilla;
                    $('#form-edit #errors').hide();
                    $('#form-edit #pldo_id').val($(e.relatedTarget).attr('data-id'));
                    $('#form-edit #nombre_documento').text('Nombre del documento adjunto: '+plantilla.PLDO_NOMBRE_ARCHIVO);
                    $('#form-edit #pldo_nombre').val(plantilla.PLDO_NOMBRE);
                    $('#form-edit #pldo_descripcion').val(plantilla.PLDO_DESCRIPCION);
                }else{
                    errorProcess(data.status);
                }
            },
            error: function(e) {
                console.log(e.message);
            }
        });
    });

    $('#modalEdit #btnGuardar').click(function(){
        var formData = new FormData(document.getElementById("form-edit")); 
        $.ajax({
            url: 'plantillaDocumento/update',
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data) {
                loadingMessageHide();
                if(data.status == 'success'){
                    $('#form-edit #errors').hide();
                    $('#modalEdit').modal('hide');
                    location.reload();
                }else{
                    errors = data.errors;
                    $('#form-edit #errors').remove();
                    var append = '';
                    for (key in errors) {
                        if (errors.hasOwnProperty(key)) {
                            append = append+'<li>'+errors[key]+'</li>';
                        }
                    }
                    $('#form-edit').prepend('<div id="errors" class="alert alert-danger"><ul>'+append+'</ul></div>');
                }
            },
            error: function(e) {
                console.log(e.message);
            }
        });
    });
  </script>
<div class="form-group">
    <div class="col-md-5 lblObligatorio"></div>
</div>
<div class="form-group required">
    <label class="col-md-4 control-label">Nombre</label>
    <div class="col-md-6">
        <input type="text" class="form-control" id="pldo_nombre" name="pldo_nombre" />
    </div>
</div>
<div class="form-group required">
    <label class="col-md-4 control-label">Descripción</label>
    <div class="col-md-6">
        <textarea class="form-control" id="pldo_descripcion" name="pldo_descripcion" maxlength="255"></textarea>
        <div class="numChar"></div>
    </div>
</div>
<div class="form-group required">
    <label class="col-md-4 control-label">Documento</label>
    <div class="col-md-6">
        <input type="file" class="form-control" id="pldo_nombre_archivo" name="pldo_nombre_archivo" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword" style="height:auto;"/>
    </div>
</div>
<div class="modal fade" id="modalSelectTemplate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Seleccionar Plantilla</h4>
            </div>
            <form class="form-horizontal" id="form-select" action="{{ route('plantillaDocumento/generarDocumento')}}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Listado de plantillas disponibles:</label>
                        <div class="col-md-6">
                            <select id="listadoPlantillas" name="listadoPlantillas" class="form-control" size="5">
                                <!--Listado de plantillas-->
                            </select>
                            <input type="hidden" id="pldo" name="pldo" />
                            <input type="hidden" id="proc_id" name="proc_id" value="{{ $proceso->PROC_ID }}" />
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" id="btnGenerar" class="btn btn-primary">Generar documento</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#modalSelectTemplate').on('show.bs.modal', function () {
        $('#listadoPlantillas').html('');
        // Obtener el listado de plantillas
        $.ajax({
            url: "{{ route('plantillaDocumento/indexAjax')}}",
            type: 'POST',
            data: {
                '_token' : '{{ csrf_token() }}'
            },
            success: function(data) {
                loadingMessageHide();
                if(data.status == 'success'){
                    // Cargar el listado de plantillas
                    listadoPlantillas = data.plantillas;
                    for (var i=0;i<listadoPlantillas.length;i++) {
                        plantilla=listadoPlantillas[i];
                        $('#listadoPlantillas').append('<option value=\''+plantilla['PLDO_ID']+'\'>'+plantilla['PLDO_NOMBRE']+'</option>');
                    }
                }else{
                    errorProcess('form-select', data.status);
                }
            },
            error: function(e) {
                console.log(e.message);
            }
        });
    });

    $('#modalSelectTemplate').on('hidden.bs.modal', function () {
        location.reload();
    });

    $('#modalSelectTemplate #form-select').submit(function (){
        $('#modalSelectTemplate #pldo').val($("#listadoPlantillas").val());
    });

  </script>
@extends('public-layout')
@section('content')

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-10 col-xs-10">
                                <h4>Plantillas
                                <button type="button" class="btn btn-primary" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalNew">
                                Nueva <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                </button></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table id="list" class="table table-hover table-bordered dataTable">
                        <thead>
                            <tr>
                                <th>
                                    Nombre
                                </th>
                                <th>
                                    Descripción
                                </th>
                                <th>
                                    Ver plantilla
                                </th>
                                <th>
                                    Opciones
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($plantillas as $plantilla)
                                <tr>
                                    <td>{{ $plantilla->PLDO_NOMBRE }}</td>
                                    <td>{{ $plantilla->PLDO_DESCRIPCION }}</td>
                                    <td><a href ="{{ asset('download/'.$plantilla->PLDO_PATH) }}">Descargar</a></td>
                                    <td>
                                        <button type="button" id="editar" name="editar" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#modalEdit" data-id={{ $plantilla->PLDO_ID }}>
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </button>
                                        <button type="button" id="eliminar" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-toggle="modal" data-target="#modalDelete" data-id={{ $plantilla->PLDO_ID }}>
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- NUEVO -->
    @include('plantilla.partials.modal-new')

    <!-- EDITAR -->
    @include('plantilla.partials.modal-edit')

    <!-- ELIMINAR -->
    @include('plantilla.partials.modal-delete')

@endsection                  

@section('script')
    <script>
        $(document).ready(function() {
            // Marcar la pestaña en el menú superior
            activeMenuItem('administrarDocumentos', 'administracion');
            var targetPdf = "{{ route('plantillaDocumento/exportarAPdf')}}";
            var targetXls = "{{ route('plantillaDocumento/exportarAExcel')}}";
            @include('partials.btn-exportar');
        }); 
    </script>
@endsection

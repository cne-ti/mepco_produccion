@extends('public-layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-10 col-xs-10">
                                <h4>Carga Histórica</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="form-horizontal" id="cargaHistorica" role="form" method="POST" action="cargaHistorica" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <div class="text-center" style="padding-top:25px;">
                                <div class="col-md-2 col-md-offset-2">
                                    <p><i class="fa fa-file-excel-o fa-2x"></i><br>Archivo .xls .xlsx</p>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <input type="file" class="form-control" name="ficheroCarga" id="ficheroCarga" style="height: auto;">
                            </div>
                            <div class="col-md-1">
                                <button type="submit" id="btnGuardar" class="btn btn-primary loading-message" style="align:left;">
                                    Cargar
                                </button>
                            </div>
                        </div>
                        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                    </form>
                    <div class="col-md-6 col-md-offset-3" id="descargaArchivo">
                        <div class="panel panel-default ">
                            <div class="panel-heading">
                                Fichero de carga masiva
                            </div>
                            <div class="panel-body text-center">
                                <div class="row">
                                    Descarga la plantilla de datos históricos actualizada:
                                </div>
                                <div class="row">
                                    <form id="descargar" action="{{route('cargaHistorica/descargarExcel')}}" method="POST">
                                        {!! csrf_field() !!}
                                        <div class="col-md-6 col-md-offset-3">
                                            <a href="#" id="btnDescargar" class="btn btn-default">
                                                <i class="fa fa-download"></i>Descargar
                                            </a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection  

@section('script')
    <script type="text/javascript">

        activeMenuItem('administracion', 'cargaHistorica');

        $('#btnDescargar').click(function() {
            $('#descargar').submit();
        });

    </script>
@endsection

@extends('public-layout')
@section('content')

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-10 col-xs-10">
                                <h4>Datos Históricos
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table id="list" class="table table-hover table-bordered dataTable">
                        <thead>
                            <tr>
                                <th>
                                    Fecha
                                </th>
                                <th>
                                    Ukc Usg
                                </th>
                                <th>
                                    Ppt Usg
                                </th>
                                <th>
                                    Mont Belvieu
                                </th>
                                <th>
                                    Cif Ara
                                </th>
                                <th>
                                    Dólar
                                </th>
                                <th>
                                    Tc publicado 
                                </th>
                                <th>
                                    UTM
                                </th>
                                <th>
                                    Ver detalle
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($historico as $row)
                                <tr>
                                    <td><span style="display:none;">{{ $row->DAHI_FECHA_SORT }}</span>{{ \App\Util::dateFormat($row->DAHI_FECHA, 'd/m/Y')}}</td>
                                    <td>{{ Util::formatNumber($row->DAHI_UKC_USG) }}</td>
                                    <td>{{ Util::formatNumber($row->DAHI_PPT_USG) }}</td>
                                    <td>{{ Util::formatNumber($row->DAHI_MONT_BELVIEU) }}</td>
                                    <td>{{ Util::formatNumber($row->DAHI_CIF_ARA) }}</td>
                                    <td>{{ Util::formatNumber($row->DAHI_TC_DOLAR) }}</td>
                                    <td>{{ Util::formatNumber($row->DAHI_TC_PUBLICADO) }}</td>
                                    <td>{{ Util::formatNumber($row->DAHI_UTM) }}</td>
                                    <td>
                                        <button type="button" id="ver" name="ver" class="btn btn-default" aria-label="Left Align" data-toggle="modal" data-target="#modalView" title="Detalle" data-id={{ $row->DAHI_ID }}>
                                            <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- NUEVO -->
    @include('cargaHistorica.partials.modal-view')

@endsection                  

@section('script')
    <script>
        $(document).ready(function() {
            // Marcar la pestaña en el menú superior
            activeMenuItem('datosHistoricos', 'administracion');
        }); 
    </script>
@endsection

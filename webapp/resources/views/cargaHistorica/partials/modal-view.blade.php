<div class="modal fade" id="modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Detalle</h4>
            </div>
            <div class="modal-body">
                @include('cargaHistorica.partials.fields')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#modalView').on('show.bs.modal', function (e) {
        $.ajax({
            url: 'cargaHistorica/' + $(e.relatedTarget).attr('data-id'),
            type: 'GET',
            data: { 
                '_token' : $('#_token').val() 
            },
            success: function(data) {
                loadingMessageHide();
                if(data.status == "success"){
                    row = data.row;
                    $('#dahi_gas_unl_87').val(row.DAHI_GAS_UNL_87);
                    $('#dahi_gas_unl_93').val(row.DAHI_GAS_UNL_93);
                    $('#dahi_ld_diesel').val(row.DAHI_LD_DIESEL);
                    $('#dahi_usg_butano').val(row.DAHI_USG_BUTANO);

                    $('#dahi_libor').val(row.DAHI_LIBOR);
                    $('#dahi_tarifa_diaria_82000').val(row.DAHI_TARIFA_DIARIA_82000);
                    $('#dahi_tarifa_diaria_59000').val(row.DAHI_TARIFA_DIARIA_59000);
                    $('#dahi_ifo_380_houston').val(row.DAHI_IFO_380_HOUSTON);
                    $('#dahi_mdo_houston').val(row.DAHI_MDO_HOUSTON);
                    $('#dahi_ifo_380_cristobal').val(row.DAHI_IFO_380_CRISTOBAL);
                    $('#dahi_mdo_cristobal').val(row.DAHI_MDO_CRISTOBAL);

                    $('#dahi_brent').val(row.DAHI_BRENT);
                    $('#dahi_brent_m1').val(row.DAHI_BRENT_M1);
                    $('#dahi_brent_m2').val(row.DAHI_BRENT_M2);
                    $('#dahi_brent_m3').val(row.DAHI_BRENT_M3);
                    $('#dahi_brent_m4').val(row.DAHI_BRENT_M4);
                    $('#dahi_brent_m5').val(row.DAHI_BRENT_M5);
                    $('#dahi_brent_m6').val(row.DAHI_BRENT_M6);

                    $('#dahi_wti').val(row.DAHI_WTI);
                    $('#dahi_wti_m1').val(row.DAHI_WTI_M1);
                    $('#dahi_wti_m2').val(row.DAHI_WTI_M2);
                    $('#dahi_wti_m3').val(row.DAHI_WTI_M3);
                    $('#dahi_wti_m4').val(row.DAHI_WTI_M4);
                    $('#dahi_wti_m5').val(row.DAHI_WTI_M5);
                    $('#dahi_wti_m6').val(row.DAHI_WTI_M6);

                    $('#dahi_n_gas_87').val(row.DAHI_N_GAS_87);
                    $('#dahi_m_gas_87').val(row.DAHI_M_GAS_87);
                    $('#dahi_s_gas_87').val(row.DAHI_S_GAS_87);
                    $('#dahi_t_gas_87').val(row.DAHI_T_GAS_87);
                    $('#dahi_f_gas_87').val(row.DAHI_F_GAS_87);

                    $('#dahi_n_gas_93').val(row.DAHI_N_GAS_93);
                    $('#dahi_m_gas_93').val(row.DAHI_M_GAS_93);
                    $('#dahi_s_gas_93').val(row.DAHI_S_GAS_93);
                    $('#dahi_t_gas_93').val(row.DAHI_T_GAS_93);
                    $('#dahi_f_gas_93').val(row.DAHI_F_GAS_93);

                    $('#dahi_n_diesel').val(row.DAHI_N_DIESEL);
                    $('#dahi_m_diesel').val(row.DAHI_M_DIESEL);
                    $('#dahi_s_diesel').val(row.DAHI_S_DIESEL);
                    $('#dahi_t_diesel').val(row.DAHI_T_DIESEL);
                    $('#dahi_f_diesel').val(row.DAHI_F_DIESEL);

                    $('#dahi_n_glp').val(row.DAHI_N_GLP);
                    $('#dahi_m_glp').val(row.DAHI_M_GLP);
                    $('#dahi_s_glp').val(row.DAHI_S_GLP);
                    $('#dahi_t_glp').val(row.DAHI_T_GLP);
                    $('#dahi_f_glp').val(row.DAHI_F_GLP);
                }else{
                    errorProcess(data.status);
                }
            },
            error: function(e) {
                console.log(e.message);
            }
        });
    });

  </script>
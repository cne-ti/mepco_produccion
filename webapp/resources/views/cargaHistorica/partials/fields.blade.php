<div class="form-group">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Datos FOB</h3>
		</div>
		<div class="panel-body">
		        <table class="table table-hover table-bordered" id="fob">
		            <thead>
		                <tr>
		                    <th>
		                        Gasolina 87
		                    </th>
		                    <th>
		                        Gasolina 93
		                    </th>
		                    <th>
		                        Diésel
		                    </th>
		                    <th>
		                    	GLP
		                    </th>
		                </tr>
		            </thead>
		            <tbody>
		                <tr>
		                    <td><input type="text" id="dahi_gas_unl_87" class="form-control text-center" disabled/></td>
		                    <td><input type="text" id="dahi_gas_unl_93" class="form-control text-center" disabled/></td>
		                    <td><input type="text" id="dahi_ld_diesel" class="form-control text-center" disabled/></td>
		                    <td><input type="text" id="dahi_usg_butano" class="form-control text-center" disabled/></td>
		                </tr>
		            </tbody>
		        </table>
	        </div>
	    </div>
</div>

<div class="form-group">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Otros datos</h3>
		</div>
		<div class="panel-body">
		        <table class="table table-hover table-bordered" id="brent">
		            <thead>
		                <tr>
		                    <th>
		                        Libor
		                    </th>
		                    <th>
		                        Tarifa diaria 82000
		                    </th>
		                    <th>
		                        Tarifa diaria 52000
		                    </th>
		                    <th>
		                    	Ifo 380 Houston
		                    </th>
		                    <th>
		                    	MDO Houston
		                    </th>
		                    <th>
		                    	Ifo 380 Cristobal
		                    </th>
		                    <th>
		                    	MDO Cristobal
		                    </th>
		                </tr>
		            </thead>
		            <tbody>
		                <tr>
		                    <td><input type="text" id="dahi_libor" class="form-control text-center" disabled/></td>
		                    <td><input type="text" id="dahi_tarifa_diaria_82000" class="form-control text-center" disabled/></td>
		                    <td><input type="text" id="dahi_tarifa_diaria_59000" class="form-control text-center" disabled/></td>
		                    <td><input type="text" id="dahi_ifo_380_houston" class="form-control text-center" disabled/></td>
		                    <td><input type="text" id="dahi_mdo_houston" class="form-control text-center" disabled/></td>
		                    <td><input type="text" id="dahi_ifo_380_cristobal" class="form-control text-center" disabled/></td>
		                    <td><input type="text" id="dahi_mdo_cristobal" class="form-control text-center" disabled/></td>
		                </tr>
		            </tbody>
		        </table>
	        </div>
	    </div>
</div>

<div class="form-group">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Brent</h3>
		</div>
		<div class="panel-body">
		        <table class="table table-hover table-bordered" id="brent">
		            <thead>
		                <tr>
		                    <th>
		                        Brent
		                    </th>
		                    <th>
		                        m1
		                    </th>
		                    <th>
		                        m2
		                    </th>
		                    <th>
		                    	m3
		                    </th>
		                    <th>
		                    	m4
		                    </th>
		                    <th>
		                    	m5
		                    </th>
		                    <th>
		                    	m6
		                    </th>
		                </tr>
		            </thead>
		            <tbody>
		                <tr>
		                    <td><input type="text" id="dahi_brent" class="form-control text-center" disabled/></td>
		                    <td><input type="text" id="dahi_brent_m1" class="form-control text-center" disabled/></td>
		                    <td><input type="text" id="dahi_brent_m2" class="form-control text-center" disabled/></td>
		                    <td><input type="text" id="dahi_brent_m3" class="form-control text-center" disabled/></td>
		                    <td><input type="text" id="dahi_brent_m4" class="form-control text-center" disabled/></td>
		                    <td><input type="text" id="dahi_brent_m5" class="form-control text-center" disabled/></td>
		                    <td><input type="text" id="dahi_brent_m6" class="form-control text-center" disabled/></td>
		                </tr>
		            </tbody>
		        </table>
	        </div>
	    </div>
</div>

<div class="form-group">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">WTI</h3>
		</div>
		<div class="panel-body">
	        <table class="table table-hover table-bordered" id="wti">
	            <thead>
	                <tr>
	                    <th>
	                        WTI
	                    </th>
	                    <th>
	                        m1
	                    </th>
	                    <th>
	                        m2
	                    </th>
	                    <th>
	                    	m3
	                    </th>
	                    <th>
	                    	m4
	                    </th>
	                    <th>
	                    	m5
	                    </th>
	                    <th>
	                    	m6
	                    </th>
	                </tr>
	            </thead>
	            <tbody>
	                <tr>
	                    <td><input type="text" id="dahi_wti" class="form-control text-center" disabled /></td>
	                    <td><input type="text" id="dahi_wti_m1" class="form-control text-center" disabled /></td>
	                    <td><input type="text" id="dahi_wti_m2" class="form-control text-center" disabled /></td>
	                    <td><input type="text" id="dahi_wti_m3" class="form-control text-center" disabled /></td>
	                    <td><input type="text" id="dahi_wti_m4" class="form-control text-center" disabled /></td>
	                    <td><input type="text" id="dahi_wti_m5" class="form-control text-center" disabled /></td>
	                    <td><input type="text" id="dahi_wti_m6" class="form-control text-center" disabled /></td>
	                </tr>
	            </tbody>
	        </table>
	    </div>
	</div>
</div>

<div class="form-group">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Parámetros Gasolina 87</h3>
		</div>
		<div class="panel-body">
	        <table class="table table-hover col-xs-10 table-bordered" id="param_gas_87">
	            <thead>
	                <tr>
	                    <th>
	                        n
	                    </th>
	                    <th>
	                        m
	                    </th>
	                    <th>
	                        s
	                    </th>
	                    <th>
	                    	t
	                    </th>
	                    <th>
	                    	f
	                    </th>
	                </tr>
	            </thead>
	            <tbody>
	                <tr>
	                    <td><input type="text" id="dahi_n_gas_87" class="form-control text-center" disabled /></td>
	                    <td><input type="text" id="dahi_m_gas_87" class="form-control text-center" disabled /></td>
	                    <td><input type="text" id="dahi_s_gas_87" class="form-control text-center" disabled /></td>
	                    <td><input type="text" id="dahi_t_gas_87" class="form-control text-center" disabled /></td>
	                    <td><input type="text" id="dahi_f_gas_87" class="form-control text-center" disabled /></td>
	                </tr>
	            </tbody>
	        </table>
	    </div>
	</div>
</div>

<div class="form-group">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Parámetros Gasolina 93</h3>
		</div>
		<div class="panel-body">
	        <table class="table table-hover col-xs-10 table-bordered" id="param_gas_93">
	            <thead>
	                <tr>
	                    <th>
	                        n
	                    </th>
	                    <th>
	                        m
	                    </th>
	                    <th>
	                        s
	                    </th>
	                    <th>
	                    	t
	                    </th>
	                    <th>
	                    	f
	                    </th>
	                </tr>
	            </thead>
	            <tbody>
	                <tr>
	                    <td><input type="text" id="dahi_n_gas_93" class="form-control text-center" disabled /></td>
	                    <td><input type="text" id="dahi_m_gas_93" class="form-control text-center" disabled /></td>
	                    <td><input type="text" id="dahi_s_gas_93" class="form-control text-center" disabled /></td>
	                    <td><input type="text" id="dahi_t_gas_93" class="form-control text-center" disabled /></td>
	                    <td><input type="text" id="dahi_f_gas_93" class="form-control text-center" disabled /></td>
	                </tr>
	            </tbody>
	        </table>
	    </div>
	</div>
</div>

<div class="form-group">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Parámetros Diesel</h3>
		</div>
		<div class="panel-body">
	        <table class="table table-hover col-xs-10 table-bordered" id="param_diesel">
	            <thead>
	                <tr>
	                    <th>
	                        n
	                    </th>
	                    <th>
	                        m
	                    </th>
	                    <th>
	                        s
	                    </th>
	                    <th>
	                    	t
	                    </th>
	                    <th>
	                    	f
	                    </th>
	                </tr>
	            </thead>
	            <tbody>
	                <tr>
	                    <td><input type="text" id="dahi_n_diesel" class="form-control text-center" disabled /></td>
	                    <td><input type="text" id="dahi_m_diesel" class="form-control text-center" disabled /></td>
	                    <td><input type="text" id="dahi_s_diesel" class="form-control text-center" disabled /></td>
	                    <td><input type="text" id="dahi_t_diesel" class="form-control text-center" disabled /></td>
	                    <td><input type="text" id="dahi_f_diesel" class="form-control text-center" disabled /></td>
	                </tr>
	            </tbody>
	        </table>
	    </div>
	</div>
</div>

<div class="form-group">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Parámetros GLP</h3>
		</div>
		<div class="panel-body">
	        <table class="table table-hover col-xs-10 table-bordered" id="param_glp">
	            <thead>
	                <tr>
	                    <th>
	                        n
	                    </th>
	                    <th>
	                        m
	                    </th>
	                    <th>
	                        s
	                    </th>
	                    <th>
	                    	t
	                    </th>
	                    <th>
	                    	f
	                    </th>
	                </tr>
	            </thead>
	            <tbody>
	                <tr>
	                    <td><input type="text" id="dahi_n_glp" class="form-control text-center" disabled /></td>
	                    <td><input type="text" id="dahi_m_glp" class="form-control text-center" disabled /></td>
	                    <td><input type="text" id="dahi_s_glp" class="form-control text-center" disabled /></td>
	                    <td><input type="text" id="dahi_t_glp" class="form-control text-center" disabled /></td>
	                    <td><input type="text" id="dahi_f_glp" class="form-control text-center" disabled /></td>
	                </tr>
	            </tbody>
	        </table>
	    </div>
	</div>
</div>

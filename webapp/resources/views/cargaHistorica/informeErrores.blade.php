@extends('public-layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                @if (count($errors) > 0)
                    <div class="panel-heading">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-10 col-xs-10">
                                    <h4>Informe de Errores</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="alert alert-danger">
                        {{ $mensajeErrores }}
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
                @if (count($warnings) >0)
                    <div class="panel-heading">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-10 col-xs-10">
                                    <h4>Informe de Advertencias</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="alert alert-warning">
                        {{ $mensajeAdvertencias }}
                            <ul>
                                @foreach ($warnings->all() as $warning)
                                    <li>{{ $warning }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
                @if ((count($warnings)==0) && (count($errors)==0))
                    <div class="panel-heading">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-10 col-xs-10">
                                    <h4>Carga histórica realizada</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="alert alert-success">
                            La carga histórica se ha realizado correctamente.
                        </div>
                    </div>
                @endif

            </div>
        </div>
    </div>

@endsection  

@section('script')
    <script type="text/javascript">
        activeMenuItem('administracion', 'cargaHistorica');
    </script>
@endsection

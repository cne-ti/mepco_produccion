(function($){
	$.fn.formatNumber = function(options){

		options = $.extend($.fn.formatNumber.defaults, options);

		return this.each( function() {
			$(this).val(format($(this).val()));

			$(this).on('keydown', function(e){
				if(!$.fn.formatNumber.isKeyNumber(e))
				{
					e.preventDefault();
				}
			});
			$(this).on('keyup', function(e){
				$(this).val(format(e.target.value));
			});

			if(!options.active)
			{
				$(this).off('keydown');
				$(this).off('keyup');
			}

			function format(strInput)
			{
				var strOutput    = "";
				var arrNum       = new Array();
				arrNum[0]        = 0;
				arrNum[1]        = null;

				if(options.chrMiles === '.')
				{
					strInput = strInput.replace(/\./g, '');
				}else if(options.chrMiles === ',')
				{
					strInput = strInput.replace(/\,/g, '');
				}
				if(strInput.indexOf(options.chrDecimales) > -1)
				{
					arrNum = strInput.split(options.chrDecimales);
				}else
				{
					arrNum[0] = strInput;
				}
				
				for(var i = arrNum[0].length; i--; )
				{
					strOutput += arrNum[0][(arrNum[0].length - 1) -i];
					if(i != 0 && i % 3 === 0)
					{
						strOutput += options.chrMiles;
					}
				}

				if(arrNum[1] !== null)
				{
					strOutput = strOutput + options.chrDecimales + arrNum[1];
				}
				return strOutput;
			}
		});
	};

	$.fn.formatNumber.isKeyNumber = function(e)
	{
	    var charCode = (e.which) ? e.which : event.keyCode
	    if (charCode != 43 && charCode != 188 && charCode != 190 && charCode > 31 && (charCode < 48 || charCode > 57))
	        return false;
	    return true;
	};

	$.fn.formatNumber.defaults = {
	    chrMiles     : '.',
	    chrDecimales : ',',
	    active       : true
	};
}(jQuery));
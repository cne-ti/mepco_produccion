$(document).ready(function() {

    /**
    * CLASE PARA FORMATEO DE ELEMENTOS NUMÉRICOS DECIMALES (coma decimal, punto para millares)
    * FORMATEA NUMERO #.###,###
    */
    $('.formatNumber').formatNumber();

    /**
    * CREA CONTROL FECHA ESTANDAR
    */
    $('.fecha').datepicker({
        'dateFormat':'dd/mm/yy'
    });

    /**
    * CREA CONTROL FECHA CON SOLO LOS JUEVES HABILITADOS
    */
    $('.fecha-vigencia').datepicker({
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        minDate: 0,
        beforeShowDay: function(date) {
            return [date.getDay() != 1 && date.getDay() != 2 && date.getDay() != 3 && date.getDay() != 5 && date.getDay() != 6 && date.getDay() != 0, ''];
        }
    });

    /**
    * CREA CONTROL FECHA CON SOLO LOS JUEVES HABILITADOS, SIN FECHA MÍNIMA
    */
    $('.fecha-vigencia-all').datepicker({
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        beforeShowDay: function(date) {
            return [date.getDay() != 1 && date.getDay() != 2 && date.getDay() != 3 && date.getDay() != 5 && date.getDay() != 6 && date.getDay() != 0, ''];
        }
    });

    /**
    * CREA CONTROL CHECKBOX PARA BRENT/WTI
    */
    $(".checkBrentWti").bootstrapSwitch({
        onText :'BRENT',
        offText:'WTI',
        size   :'mini'
    });

    /**
    * CREA CONTROL CHECKBOX PARA SI/NO
    */
    $(".checkSiNo").bootstrapSwitch({
        onText :'SI',
        offText:'NO',
        size   :'mini'
    });

    /**
    * CREA CONTROL CHECKBOX PARA TIPO DE VARIABLE
    */
    $(".checkTipoVariable").bootstrapSwitch({
        onText :'CÁLCULO',
        offText:'DOCUMENTO',
        size   :'mini'
    });

    /**
    * MUESTRA DIV LOADING
    */
    $('.loading-message').click(function(){
        $('#loading-message').show();
    });

    /**
    * INICIA DATATABLE TRADUCIDO AL ESPAÑOL
    */
    $('.dataTable').DataTable({
        'iDisplayLength': 10,
        'dom': '<"col-md-7 hidden-xs datatable-no-padding-left"l><"col-md-2 col-xs-2 btn-exportar datatable-float-right datatable-no-padding-right"><"col-md-3 col-xs-3 datatable-no-padding-right"f>rt<"clear"><"col-md-7 hidden-xs datatable-no-padding-left"i><"col-md-5 col-xs-12 datatable-no-padding-right"p>',
        language: {
                processing:     "Buscando...",
                search:         "Buscar",
                lengthMenu:     "Mostrar _MENU_ registros",
                info:           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                infoEmpty:      "No se encontraron registros",
                infoFiltered:   " (De un total de _MAX_ registros)",
                infoPostFix:    "",
                loadingRecords: "Cargando vista...",
                zeroRecords:    "No se encontraron registros",
                emptyTable:     "No se encontraron registros",
                paginate: {
                    first:      "Primer",
                    previous:   "Anterior",
                    next:       "Siguiente",
                    last:       "&Uacute;ltimo",
            }
        }
    });

    /**
    * INICIA DATATABLE TRADUCIDO AL ESPAÑOL EN TAMAÑO PARA MODAL-LG
    */
    $('.dataTable-xs').DataTable({
        'iDisplayLength': 5,
        'dom': '<"col-md-6 hidden-xs datatable-no-padding-left"l><"col-md-2 col-xs-2 btn-exportar datatable-float-right datatable-no-padding-right"><"col-md-4 col-xs-4 datatable-no-padding-right"f>rt<"clear"><"col-md-7 hidden-xs datatable-no-padding-left"i><"col-md-5 col-xs-12 datatable-no-padding-right"p>',
        language: {
                processing:     "Buscando...",
                search:         "Buscar",
                lengthMenu:     "Mostrar _MENU_ registros",
                info:           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                infoEmpty:      "No se encontraron registros",
                infoFiltered:   " (De un total de _MAX_ registros)",
                infoPostFix:    "",
                loadingRecords: "Cargando vista...",
                zeroRecords:    "No se encontraron registros",
                emptyTable:     "No se encontraron registros",
                paginate: {
                    first:      "Primer",
                    previous:   "Anterior",
                    next:       "Siguiente",
                    last:       "&Uacute;ltimo"
            }
        }
    });

    // Solver
    $('.solverDataTable').DataTable({
        'iDisplayLength': 10,
        'dom': 'rt<"clear"><"col-md-7 hidden-xs datatable-no-padding-left"i><"col-md-5 col-xs-12 datatable-no-padding-right"p>',
        language: {
                processing:     "Buscando...",
                search:         "Buscar",
                lengthMenu:     "Mostrar _MENU_ registros",
                info:           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                infoEmpty:      "No se encontraron registros",
                infoFiltered:   " (De un total de _MAX_ registros)",
                infoPostFix:    "",
                loadingRecords: "Cargando vista...",
                zeroRecords:    "No se encontraron registros",
                emptyTable:     "No se encontraron registros",
                paginate: {
                    first:      "Primer",
                    previous:   "Anterior",
                    next:       "Siguiente",
                    last:       "&Uacute;ltimo",
            }
        },
        iDisplayLength : 5,
    });

    /**
    * SETEA VALORES DE DATEPICKER EN ESPAÑOL
    */
    $.datepicker.regional['es'] = {
         closeText: 'Cerrar',
         prevText: '<Ant',
         nextText: 'Sig>',
         currentText: 'Hoy',
         monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
         monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
         dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
         dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
         dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
         weekHeader: 'Sm',
         dateFormat: 'dd/mm/yy',
         firstDay: 1,
         isRTL: false,
         showMonthAfterYear: false,
         yearSuffix: ''
         };
     $.datepicker.setDefaults($.datepicker.regional['es']);
     
});

/**
 * @param {string} subItem
 * @param {string} item
 * ACTIVA ITEM EN MENU    
 */
function activeMenuItem(subItem, item)
{
    $('#navbar ul li').each(function(index){
        $(this).removeClass('active');
    });

    $('#' + subItem).addClass('active');

    if(typeof item !== 'undefined' && item !== null )
    {
        $('#' + item).addClass('active');
    }
}

/**
 * @param {string} parent
 * @param {array}  data
 * PROCESA Y MUESTRA LOS ERRORES EN DIV ERROR
 */
function errorProcess(parent, data)
{
    $('#' + parent + ' #errors ul').empty();
    $('#' + parent + ' #errors').show();
    var x;
    for(x in data){
      $('#' + parent + ' #errors ul').append('<li>' + data[x] + '</li>');
    }
}

/**
* OCULTA DIV LOADING
*/
function loadingMessageHide()
{
    $('#loading-message').fadeOut(100);
}

/**
* Cuenta caracteres para llevar el máximo número de caracteres y mostrarlos en un textarea
*/
function numChar()
{    
    var max = 255;
    var len = $(this).val().length;
    var charLeft = max-len;
    $('.numChar').text(charLeft);
}

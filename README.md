## Mecanismo Estabilización Precios de Combustibles - MEPCO

El MEPCO fue creado por la Ley Nº 20.765 publicada el 9 de julio de 2014, con la finalidad de establecer un mecanismo de estabilización de los precios de venta internos de los combustibles incluidos en la Ley N° 18.502. Este mecanismo opera a través de incrementos y rebajas a los impuestos específicos a los combustibles establecidos en la ley Nº 18.502, los que se modificarán sumando al componente base establecido en esa ley  un componente variable (que puede ser positivo o negativo) determinado para cada uno de los combustibles gasolina automotriz, petróleo diésel, gas natural comprimido y gas licuado de petróleo.
MEPCO establece la determinación de la componente variable en dos etapas, una de las cuales implica la comparación entre precio de paridad y una banda de precios de referencia, todos definidos en moneda nacional; la segunda etapa se encuentra a cargo del Ministerio de Hacienda.
La metodología de cálculo utilizada en ambos casos se enuncia en la Ley y el reglamento (DS N° 1.119 – publicado en el diario oficial el 1/08/14).

La Comisión Nacional de Energía, durante el año 2016 ha puesto en marcha un sistema de apoyo para el Cálculo del MEPCO. Este sistema según lo establecido en la Resolución Exenta N° 60 del 31 de enero de 2017, resuelve lo siguiente:

1. Informa utilización, por parte de la Comisión Nacional de Energía, del software “Sistema de Cálculo de los Precios de Paridad y de Referencia de Combustibles Líquidos derivados del Petróleo”, para los cálculos semanales asociados a los sistemas de estabilización de precios de combustibles líquidos derivados del petróleo, en especial, para los precios de paridad y referencia señalados en la Ley N° 20.765 de 2014, que crea Mecanismo de Estabilización de Precios de los Combustibles, y en la Ley N° 19.030 de 1991, que crea el Fondo de Estabilización de Precios del Petróleo.

2. Utilícese el software individualizado en el resuelvo precedente en los procedimientos de cálculo de los informes de Precios de Paridad y de Referencia asociados al MEPCO, que sean realizados desde la semana siguiente a la publicación de la presente resolución.

3. El uso del software para el cálculo de los informes de Precios de Paridad y de Referencia asociados al FEEP se efectuará dentro del primer semestre del año 2017. 

4. Autorícese el uso y disposición por parte de terceros distintos a la Comisión Nacional de Energía del software “Sistema de Cálculo de los Precios de Paridad y de Referencia de Combustibles Líquidos Derivados del Petróleo”, desarrollado por ésta última, y su incorporación al patrimonio cultural común a partir de 30 días hábiles contados desde la publicación de la presente resolución, estando desde ese plazo disponible en el sitio de dominio electrofónico de la Comisión Nacional de Energía. El uso y disposición por parte de terceros deberá efectuarse bajo las condiciones y términos establecidos en la licencia “Berkeley Software Distribution 3-Clause” [BSD-3](https://opensource.org/licenses/BSD-3-Clause), cuyos términos estarán disponibles en la página web en que se aloje el software, y que se entenderán aceptados por el sólo hecho de su descarga.

## Licenciamiento
El Sistema de Cálculo de los Precios de Paridad y de Referencia de Combustibles Líquidos derivados del Petróleo esta licenciado bajo [BSD-3](https://opensource.org/licenses/BSD-3-Clause)
